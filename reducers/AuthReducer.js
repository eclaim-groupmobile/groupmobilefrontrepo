// manager/src/reducers/AuthReducers.js
import {
  LANGUAGE_CHANGED,
  NOTIFICATION_CHANGED,
  NOTIFICATIONLIST_CHANGED,
  LASTSCREEN_CHANGED,
  LOGIN,
  LOGOUT,
  TITLE_CHANGED,
  MEMBERDATA_CHANGED,
  ECARDDATA_CHANGED,
  ECARDDATA_CLEAR,
  HOSPITALDATA_CHANGED,
  CLINICDATA_CHANGED,
  PROVINCEDATA_CHANGED,
  CITYDATA_CHANGED,
  LINKLIST_CHANGED,
  HOWTOCLAIMLIST_CHANGED,
  HEALTHTIPSLIST_CHANGED,
  NEWSLIST_CHANGED,
  HASECARD_CHANGED,
  PIN_FAILED,
} from '../actions/types';
import * as utils from "../functions";

const INITIAL_STATE = {
  language: null,
  notification: 0,
  notificationList:null,
  lastScreen: "",
  
  //login data
  loginStatus: false,
  lastLogin: null,

  title: "", //for register or forget password

  memberData: null,
  eCardData: null,
  eCardLastDate: null,

  hospitalData: null,
  clinicData: null,
  provinceData: null,
  cityData: null,

  linkList: null,
  howToClaimList: null,
  healthTipsList: null,
  newsList: null,
  hasECard: null,

  numberOfPinFail: 0,
 };

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LANGUAGE_CHANGED:
      return { ...state, language: action.payload };
    case NOTIFICATION_CHANGED:
      return { ...state, notification: action.payload };
    case NOTIFICATIONLIST_CHANGED:
      return { ...state, notificationList: action.payload };
    case LASTSCREEN_CHANGED:
      return { ...state, lastScreen: action.payload };
    case LOGIN:
      return { ...state, loginStatus: true,lastLogin: new Date()};
    case LOGOUT:
      return { ...state, loginStatus: false };
    // case LOGIN_USER_SUCCESS:
    //   return { ...state,
    //     chdrNum: action.payload_chdrnum,
    //     memberNo1: action.payload_memberno1,
    //     memberNo2: action.payload_memberno2,
    //     error: '',
    //     loading: false,
    //     email: '',
    //     password: ''
    //   };
    case TITLE_CHANGED:
      return { ...state, title: action.payload };
    case MEMBERDATA_CHANGED:
      return { ...state, memberData: action.payload };
    case ECARDDATA_CHANGED:
      return { ...state, eCardData: action.payload, eCardLastDate: utils.getToday("en") };
    case ECARDDATA_CLEAR:
      return { ...state, eCardData: null, eCardLastDate: null };
    case HOSPITALDATA_CHANGED:
      return { ...state, hospitalData: action.payload };
    case CLINICDATA_CHANGED:
      return { ...state, clinicData: action.payload };
    case PROVINCEDATA_CHANGED:
      return { ...state, provinceData: action.payload };
    case CITYDATA_CHANGED:
      return { ...state, cityData: action.payload };
    case LINKLIST_CHANGED:
      return { ...state, linkList: action.payload };
    case HOWTOCLAIMLIST_CHANGED:
      return { ...state, howToClaimList: action.payload };
    case HEALTHTIPSLIST_CHANGED:
      return { ...state, healthTipsList: action.payload };
    case NEWSLIST_CHANGED:
      return { ...state, newsList: action.payload };
    case HASECARD_CHANGED:
      return { ...state, hasECard: action.payload };
    case PIN_FAILED:
        return { ...state, numberOfPinFail: action.payload };
    default:
      return state;
  }
};
