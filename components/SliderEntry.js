import React, { Component } from 'react';
import { Linking, View, Text, Image, TouchableOpacity } from 'react-native';
import Constants from 'expo-constants';
import PropTypes from 'prop-types';
import { ParallaxImage } from 'react-native-snap-carousel';
import styles from '../styles/SliderEntry.style';
import DefaultText from "../components/DefaultText"

export default class SliderEntry extends Component {

    static propTypes = {
        data: PropTypes.object.isRequired,
        even: PropTypes.bool,
        parallax: PropTypes.bool,
        parallaxProps: PropTypes.object
    };

    get image () {
        const { data: { coverPictureLink }, parallax, parallaxProps, even } = this.props;

        return parallax ? (
            <ParallaxImage
              source={{ uri: Constants.manifest.extra.urlFile2 + coverPictureLink}}
              containerStyle={[styles.imageContainer, even ? styles.imageContainerEven : {}]}
              style={styles.image}
              parallaxFactor={0.35}
              showSpinner={true}
            //   spinnerColor={even ? 'rgba(255, 255, 255, 0.4)' : 'rgba(0, 0, 0, 0.25)'}
                spinnerColor={'rgba(255, 255, 255, 0.4)'}
              {...parallaxProps}
            />
        ) : (
            <Image
              source={{ uri: Constants.manifest.extra.urlFile2 + coverPictureLink}}
              style={styles.image}
            />
        );
    }

    render () {
        const { data: { title, detail }, even } = this.props;

        const uppercaseTitle = title ? (
            <DefaultText
              bold
              style={[styles.title]}
              numberOfLines={2}
            >
                { title.toUpperCase() }
            </DefaultText>
        ) : false;

        return (
            <TouchableOpacity
              activeOpacity={1}
              style={styles.slideInnerContainer}
              >
                <View style={styles.shadow} />
                <View style={[styles.imageContainer]}>
                    { this.image }
                    <View style={[styles.radiusMask]} />
                </View>
                <View style={[styles.textContainer]}>
                    { uppercaseTitle }
                    <DefaultText
                      italic
                      style={[styles.subtitle]}
                      numberOfLines={2}
                    >
                        { detail }
                    </DefaultText>
                </View>
            </TouchableOpacity>
        );
    }
}