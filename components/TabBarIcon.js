import React from 'react';
import { Icon } from 'expo';
import { Image, Text, View } from 'react-native';
import { Badge } from 'react-native-elements';
import Colors from '../constants/Colors';
import { connect } from "react-redux";
import DefaultText from "./DefaultText"
import * as labels from "../constants/label";

class TabBarIcon extends React.Component {
  genBadge = () => {
    if(this.props.badge){
      if(Number(this.props.notification)>0){
        return(
          <View style={{position:'absolute',right:0,top:0}}>
            <View style={{width:22,height:22,borderRadius:10,backgroundColor:'red',alignItems:'center',justifyContent:'center'}}>
              <Text style={{fontSize:10,color:'white'}}>{this.props.notification}</Text>
            </View>
          </View>
        )
      }
    }
    return null;
  }

  render() {
    if((this.props.hasECard=='TRUE' && this.props.label=="ECard") || this.props.label!="ECard")
    return (
      <View style={{justifyContent:'center',alignItems: 'center',flex:1}}>
        <Image
          source={this.props.focused ? this.props.iconActive : this.props.iconInactive}
          style={{width: 48, height: 39}} //32 //26
        />
        <DefaultText style={{fontSize:10,marginTop:-8,color:this.props.focused?Colors.bondiBlue:Colors.TMFreshGrey75,textAlign:'center'}}>{labels.getMenu(this.props.language,this.props.label)}</DefaultText>
        {this.genBadge()}
      </View>
    )
    else return null
  }
}

const mapStateToProps = ({ auth }) => {
  const { notification, language, hasECard } = auth;
  return { notification, language, hasECard };
};

export default connect(
  mapStateToProps,
  {}
)(TabBarIcon);