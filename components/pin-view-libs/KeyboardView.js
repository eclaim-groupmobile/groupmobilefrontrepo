import React from 'react';
import { Animated, FlatList, Text, TouchableOpacity } from "react-native";
import { Icon } from "react-native-elements"
import Colors from "../../constants/Colors";
import Styles, { buttonWidth } from './styles'

const KeyboardView = ({ keyboardOnPress, pinLength, onComplete, bgColor, returnType, textColor, animatedDeleteButton, deleteText, animatedDeleteButtonOnPress, styles, fingerTouch, onFingerTouch }) => {
  const data = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "", "0",deleteText];
  const dataTouch = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "FINGERICON", "0",deleteText];
  const renderItem = ({ item, index }) => {
    let style;
    let onPressActive;
    if (item === deleteText) {
      onPressActive = animatedDeleteButtonOnPress;
    } else {
      onPressActive = false;
    }

    if(item === ""){
      return (
        <TouchableOpacity >
          <Animated.View style={[styles[2], {
          }]}>
          </Animated.View>
        </TouchableOpacity>
      )
    }

    if(item === "FINGERICON"){
      return (
        <TouchableOpacity activeOpacity={0.5}
                          onPress={() => onFingerTouch()}
                          disabled={onPressActive}
                          style={{marginTop:10}}>
          <Animated.View style={[styles[3], {
            backgroundColor: bgColor,
            borderColor: textColor
          }]}>
            <Icon
              name='fingerprint'
              type='material-community'
              color='black'
              size={buttonWidth}
            />
          </Animated.View>
        </TouchableOpacity>
      )
    }

    if(item === "DELICON"){
      return (
        <TouchableOpacity activeOpacity={0.5}
                          onPress={() => keyboardOnPress(item, returnType, pinLength, onComplete)}
                          disabled={onPressActive}
                          style={{marginTop:10}}>
          <Animated.View style={[styles[0], {
            backgroundColor: bgColor,
            borderColor: textColor
          }]}>
            <Icon
              name='delete'
              type='feather'
              color='black'
              size={buttonWidth * 4 / 9}
            />
          </Animated.View>
        </TouchableOpacity>
      )
    }

    return (
        <TouchableOpacity activeOpacity={0.5}
                          onPress={() => keyboardOnPress(item, returnType, pinLength, onComplete)}
                          disabled={onPressActive}
                          style={{marginTop:10}}>
          <Animated.View style={[styles[0], {
            backgroundColor: bgColor,
            borderColor: textColor
          }]}>
            <Text style={[styles[1], {
              color: textColor,
              opacity: 1,
            }]}>{item}</Text>
          </Animated.View>
        </TouchableOpacity>
    )
  };
  
  return (
      <FlatList
          scrollEnabled={false}
          horizontal={false}
          vertical={true}
          numColumns={3}
          renderItem={renderItem}
          data={fingerTouch ? dataTouch : data}
          keyExtractor={(val, index) => "pinViewItem-" + index}
      />
  )
};
export default KeyboardView