import { StyleSheet } from 'react-native'
import Layout from "../../constants/Layout";

const width = Layout.window.width; //Dimensions.get('window').width
const widthScale = width / 412;
const inputWidth = parseInt(25 * widthScale);
export const buttonWidth = parseInt(75 * widthScale);

export default StyleSheet.create({
  //passwordInputView
  passwordInputView          : {
    flexDirection: 'row',
    alignSelf    : 'center',
  },
  passwordInputViewItem      : {
    alignItems     : 'center',
    justifyContent : 'center',
    height         : inputWidth,
    margin         : 5,
    width          : inputWidth,
    borderRadius   : inputWidth / 2,
    borderWidth    : 1,
    borderColor    : '#000',
  },
  passwordInputViewItemActive: {
    alignItems     : 'center',
    justifyContent : 'center',
    height         : inputWidth,
    width          : inputWidth,
    margin         : 5,
    borderRadius   : inputWidth / 2,
    borderWidth    : 1,
    borderColor    : '#000',
  },
  // KeyboardView
  keyboardView               : {
    alignItems: 'center',
    marginTop : 15,
  },
  keyboardViewItem           : {
    alignItems      : 'center',
    justifyContent  : 'center',
    height          : buttonWidth,
    width           : buttonWidth,
    marginHorizontal: 20,
    marginVertical  : 5,
    borderRadius    : buttonWidth / 2,
    borderColor     : 'black',
    borderWidth     : 1,
  },
  keyboardViewFingerItem     : {
    alignItems      : 'center',
    justifyContent  : 'center',
    height          : buttonWidth,
    width           : buttonWidth,
    marginHorizontal: 20,
    marginVertical  : 5,
    borderRadius    : buttonWidth / 2,
  },
  keyboardViewItemHide           : {
    alignItems      : 'center',
    justifyContent  : 'center',
    height          : buttonWidth,
    width           : buttonWidth,
    marginHorizontal: 20,
    marginVertical  : 5,
  },
  keyboardViewItemText       : {
    fontSize  : 22,
    fontWeight: '400',
  },
})