import React, { Component } from "react";
import {
  Alert,
  View,StyleSheet ,SafeAreaView
} from "react-native";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import TabBarIcon from "../components/TabBarIcon";
import _ from 'lodash';

class CustomTabBar extends React.Component {

  render() {

    const {navigation, language} = this.props;
    // a navigator component receives a routes object, which holds all the routes of your tab bar
    const routes = navigation.state.routes;

    if (language=='th') {
      return <View/>;
    };

    return (
      <SafeAreaView>
        <View style={styles.container}>
          {routes.map((route, index) => {
            return (
              <View style={styles.tabBarItem} key={route.routeName}>
                <TabBarIcon
                  iconActive={require('../assets/icon/Setting-icon-selected.png')}
                  iconInactive={require('../assets/icon/Setting-icon.png')}
                  label="Settings"
                />
              </View>
            );
          })}
        </View>
      </SafeAreaView>
    );
  }
}


const styles = StyleSheet.create({

  container: {
    flexDirection: 'row',
    alignContent: 'center',
    height: 80,
    width: '100%',
  },
  tabBarItem: {
    flex: 1,
    alignItems: 'center'
  }
});

const mapStateToProps = ({ auth }) => {
  const { notification, language } = auth;
  return { notification, language };
};

export default connect(mapStateToProps)(CustomTabBar);