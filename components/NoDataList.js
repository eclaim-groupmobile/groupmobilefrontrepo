import React, { Component } from "react";
import {
  Text,
  View
} from "react-native";
import { Icon, CheckBox, FormLabel } from "react-native-elements";
import { connect } from "react-redux";
import * as labels from "../constants/label";
import DefaultText from "../components/DefaultText"

class NoDataList extends Component {

  render() {
    let lang = this.props.language;
    let text = this.props.noDataNear ? labels.getError(lang,"noDataNear") : labels.getError(lang,"noData")
    return (
        <View style={{justifyContent:"center",}}>
            {this.props.noIcon ? <View></View> :
            <Icon
                name='emoticon-sad'
                type='material-community'
                color='#727272'
                size={40}
            />
            }
            <DefaultText style={{textAlign:'center',fontSize:18,color:'#727272'}}>{text}</DefaultText>
        </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
    const { language } = auth;
    return { language };
  };
  
export default connect(
  mapStateToProps,
  {
  }
)(NoDataList);
