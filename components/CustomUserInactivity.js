import React, { Component } from "react";
import {
  Alert,
  View,
  YellowBox 
} from "react-native";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import UserInactivity from "react-native-user-inactivity";
import { logOut } from "../actions";
import * as labels from "../constants/label";
import * as utils from "../functions"
import _ from 'lodash';

class CustomUserInactivity extends Component {
  constructor() {
    super();
    YellowBox.ignoreWarnings(['Setting a timer']);
    const _console = _.clone(console);
    console.warn = message => {
      if (message.indexOf('Setting a timer') <= -1) {
        _console.warn(message);
      }
    };
  }

  static propTypes = {
    onAction: PropTypes.func.isRequired,
		children: PropTypes.any.isRequired,
	};
  
  _action = async ( active ) => {
    //console.log("Fsfs" + active + this.props.loginStatus)
    let flag = false
    //console.log(this.props.lastLogin + "             " + new Date() + "             " +  this.props.lastLogin + (10 * 1000))
    if(this.props.loginStatus && active == false ){
      flag = true
    }
    if(this.props.loginStatus && new Date() - (14400 * 1000) >= this.props.lastLogin){ //14400 default
      flag = true
    }

    if(flag==true){
      let policyNumberStore = await this.getData("policyNumberStore")
      let memberNoStore = await this.getData("memberNoStore")
      let tokenStore = await this.getData("tokenStore")
      Alert.alert(labels.getLabel(this.props.language, "Settings_logOut"))
      let lang = this.props.language
      let data = null
      let result = ""
      console.log (policyNumberStore,memberNoStore,tokenStore)
      let xmlBody = utils.LogoutBody(policyNumberStore,memberNoStore,tokenStore);
      console.log("Request body: " + xmlBody);
      await fetch(utils.urlUserService, utils.genRequest(utils.LogoutSOAPAction,xmlBody) )
        .then(response => response.text())
        .then(response => {
          result = utils.getResult(response)
          // console.log(result)
          let errorCode = result["LogoutResponse"][0]["LogoutResult"][0]["a:errorCode"][0].toString()
          let errorDesc = ""
          if(lang=="th"){
            errorDesc = result["LogoutResponse"][0]["LogoutResult"][0]["a:errorDescTH"][0].toString()
          }else{
            errorDesc = result["LogoutResponse"][0]["LogoutResult"][0]["a:errorDescEN"][0].toString()
          }
          if(Number(errorCode)==0){
            console.log("logout")
          }else{
            console.log("fetch", errorDesc);
            // Alert.alert(labels.getLabel(lang,"error"),errorDesc)
          }
        })
        .catch(err => {
          console.log("fetch", err);
          //Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
        });
      this.props.logOut();
      this.props.onAction();
    }
  }

  getData = async key => {
    let value = await utils.retrieveData(key)
    return value
  }

  componentDidUpdate(){
    this.child.handleInactivity()
  }

  render() {
    const { onAction, children, ...rest } = this.props;
    // 4hr = 14400
    // 40m = 2400
    // 20m = 1200
    // 15m = 900 <<
    return(
      <UserInactivity
            ref = {(va) => this.child = va}
            timeForInactivity={900 * 1000}
            onAction={this._action}
          >
          {children}
      </UserInactivity>
    )
  }
}

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus, lastLogin } = auth;
  return { language, lastScreen, loginStatus, lastLogin };
};

export default connect(
  mapStateToProps,
  {
    logOut
  }
)(CustomUserInactivity);