import React from 'react';
import { Text } from 'react-native';
import { connect } from "react-redux";

class DefaultText extends React.Component {
  render() {
    lang = this.props.language
    if(this.props.forceLanguage!=null && this.props.forceLanguage!=""){
      if(this.props.forceLanguage=="th"){
        lang = 'th'
      }else{
        lang = 'en'
      }
    }
    
    let lastChar = ''
    if(typeof(this.props.children) == 'string'){
      lastChar = this.props.children.slice(-1)
    }
    if(Array.isArray(this.props.children)){
      if(typeof(this.props.children[this.props.children.length-1]) == 'string'){
        lastChar = this.props.children[this.props.children.length-1].slice(-1)
      }
    }
    let addChar = ''
    if(lastChar=='้' || lastChar=='์') addChar = ' '
    if(this.props.bold){
      return <Text allowFontScaling={false} {...this.props} style={[this.props.style, { fontFamily: 'Prompt_bold' }]} >{this.props.children}{addChar}</Text>;
    }else if(this.props.italic){
      return <Text allowFontScaling={false} {...this.props} style={[this.props.style, { fontFamily: 'Prompt_italic' }]} >{this.props.children}{addChar}</Text>;
    }else{
      return <Text allowFontScaling={false} {...this.props} style={[this.props.style, { fontFamily: 'Prompt' }]} >{this.props.children}{addChar}</Text>;
    }
  }
}

const mapStateToProps = ({ auth }) => {
  const { language } = auth;
  return { language };
};

export default connect(
mapStateToProps,
{
}
)(DefaultText);

