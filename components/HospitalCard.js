import React, { Component } from "react";
import {
  Linking,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { CheckBox } from 'react-native-elements';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import Colors from "../constants/Colors";
import * as labels from "../constants/label";
import HospitalStyles from "../styles/Hospital.style"
import * as utils from "../functions"
import DefaultText from "../components/DefaultText"

class HospitalCard extends Component {

    static propTypes = {
        item: PropTypes.object.isRequired,
        index: PropTypes.number.isRequired,
        lat: PropTypes.number,
        long: PropTypes.number,
    };

    constructor(props, context) {
        super(props, context);
    }

    genDistant(lat1,lon1,lat2,lon2,lang){
        if(lat1!=null && lon1!=null && lat2!=null && lon2!=null){
            return(
                <DefaultText style={{color:Colors.bondiBlue,textAlign:'right'}}>{utils.distance(Number(lat1),Number(lon1),Number(lat2),Number(lon2),"K")} {labels.getLabel(lang,"km")}</DefaultText>
            )
        }
    }

    render() {
        let item = this.props.item
        let index = this.props.index
        let IPDCheck = false
        let OPDCheck = false
        let DentalCheck = false
        let lang = this.props.language
        if(item.IPDGroup=='True') IPDCheck = true
        if(item.OPDGroup=='True') OPDCheck = true
        if(item.Dental=='True') DentalCheck = true
        return (
          <View style={HospitalStyles.card}>
            <View style={HospitalStyles.cardHeader}>
              <DefaultText style={{color:Colors.bondiBlue}}>{lang=='th' ? item.nameTH : item.nameEN}</DefaultText>
              {this.genDistant(item.latitude,item.longtitude,this.props.lat,this.props.long,lang)}
            </View>
    
            <View style={HospitalStyles.cardData}>
              <View style={{padding:10}}>
                <DefaultText>{lang=='th' ? item.addressTH : item.addressEN}</DefaultText>
              </View>
    
              <View style={{flexDirection:'row',justifyContent:'center',padding:10}}>
                {/* IPD */}
                <View style={{flex:1,flexDirection:'row'}}>
                  <DefaultText>IPD</DefaultText>
                  <CheckBox
                    checkedColor='green'
                    checked={IPDCheck}
                    containerStyle={HospitalStyles.checkBox}
                  />
                </View>
    
                {/* OPD */}
                <View style={{flex:1,flexDirection:'row'}}>
                  <DefaultText>OPD</DefaultText>
                  <CheckBox
                    checkedColor='green'
                    checked={OPDCheck}
                    containerStyle={HospitalStyles.checkBox}
                  />
                </View>
    
                {/* Dental */}
                <View style={{flex:1,flexDirection:'row'}}>
                  <DefaultText>Dental</DefaultText>
                  <CheckBox
                    checkedColor='green'
                    checked={DentalCheck}
                    containerStyle={HospitalStyles.checkBox}
                  />
                </View>
    
                {/* Map & Call */}
                
                <TouchableOpacity style={{flex:1}} onPress={() => Linking.openURL("https://www.google.com/maps/search/?api=1&query=" + item.latitude + "," + item.longtitude)}>
                  <DefaultText style={{color:Colors.bondiBlue,textAlign:'center'}}>{labels.getLabel(this.props.language,"Hospital_map")}</DefaultText>
                </TouchableOpacity>
                <TouchableOpacity style={{flex:1}} onPress={() => Linking.openURL("tel:" + item.telephone.substring(0,9))}>
                  <DefaultText style={{color:Colors.bondiBlue,textAlign:'center'}}>{labels.getLabel(this.props.language,"Hospital_call")}</DefaultText>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        )
    }
}
const mapStateToProps = ({ auth }) => {
    const { language, } = auth;
    return { language, };
  };
  
export default connect(
  mapStateToProps,
  {
  }
)(HospitalCard);