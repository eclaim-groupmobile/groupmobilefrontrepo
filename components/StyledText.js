import React from 'react';
import { Text } from 'react-native';

export class StylesText extends React.Component {
  render() {
    let lastChar = ''
    if(typeof(this.props.children) == 'string'){
      lastChar = this.props.children.slice(-1)
    }
    if(Array.isArray(this.props.children)){
      if(typeof(this.props.children[this.props.children.length-1]) == 'string'){
        lastChar = this.props.children[this.props.children.length-1].slice(-1)
      }
    }
    let addChar = ''
    if(lastChar=='้' || lastChar=='์') addChar = ' '
    if(this.props.bold){
      return <Text allowFontScaling={false} {...this.props} style={[this.props.style, { fontFamily: 'thsarabun_bold'}]} >{this.props.children}{addChar}</Text>;
    }else{
      return <Text allowFontScaling={false} {...this.props} style={[this.props.style, { fontFamily: 'thsarabun' }]} >{this.props.children}{addChar}</Text>;
    }
    
  }
}
