import {
  LANGUAGE_CHANGED,
  NOTIFICATION_CHANGED,
  NOTIFICATIONLIST_CHANGED,
  LASTSCREEN_CHANGED,
  LOGIN,
  LOGOUT,
  TITLE_CHANGED,
  MEMBERDATA_CHANGED,
  ECARDDATA_CHANGED,
  ECARDDATA_CLEAR,
  HOSPITALDATA_CHANGED,
  CLINICDATA_CHANGED,
  PROVINCEDATA_CHANGED,
  CITYDATA_CHANGED,
  LINKLIST_CHANGED,
  HOWTOCLAIMLIST_CHANGED,
  HEALTHTIPSLIST_CHANGED,
  NEWSLIST_CHANGED,
  HASECARD_CHANGED,
  PIN_FAILED,
} from './types';

export const languageChanged = (text) => {
  return {
    type: LANGUAGE_CHANGED,
    payload: text
  };
};

export const notificationChanged = (text) => {
  return {
    type: NOTIFICATION_CHANGED,
    payload: text
  };
};

export const notificationListChanged = (data) => {
  return {
    type: NOTIFICATIONLIST_CHANGED,
    payload: data
  };
};

export const lastScreenChanged = (text) => {
  return {
    type: LASTSCREEN_CHANGED,
    payload: text
  };
};

export const logIn = () => {
  return {
    type: LOGIN,
  };
};

export const logOut = () => {
  return {
    type: LOGOUT,
  };
};

export const titleChanged = (text) => {
  return {
    type: TITLE_CHANGED,
    payload: text
  };
};

export const memberDataChanged = (data) => {
  return {
    type: MEMBERDATA_CHANGED,
    payload: data
  };
};

export const eCardDataChanged = (data) => {
  return {
    type: ECARDDATA_CHANGED,
    payload: data
  };
};

export const eCardDataClear = () => {
  return {
    type: ECARDDATA_CLEAR
  };
};

export const hospitalDataChanged = (data) => {
  return {
    type: HOSPITALDATA_CHANGED,
    payload: data
  };
};

export const clinicDataChanged = (data) => {
  return {
    type: CLINICDATA_CHANGED,
    payload: data
  };
};

export const provinceDataChanged = (data) => {
  return {
    type: PROVINCEDATA_CHANGED,
    payload: data
  };
};

export const cityDataChanged = (data) => {
  return {
    type: CITYDATA_CHANGED,
    payload: data
  };
};

export const linkListChanged = (data) => {
  return {
    type: LINKLIST_CHANGED,
    payload: data
  };
};

export const howToClaimListChanged = (data) => {
  return {
    type: HOWTOCLAIMLIST_CHANGED,
    payload: data
  };
};

export const healthTipsListChanged = (data) => {
  return {
    type: HEALTHTIPSLIST_CHANGED,
    payload: data
  };
};

export const newsListChanged = (data) => {
  return {
    type: NEWSLIST_CHANGED,
    payload: data
  };
};

export const hasECardChanged = (data) => {
  return {
    type: HASECARD_CHANGED,
    payload: data
  };
};


export const pinFailed = (data) => {
  return {
    type: PIN_FAILED,
    payload: data
  };
}