// manager/src/actions/types.js
export const LANGUAGE_CHANGED = 'language_changed';
export const NOTIFICATION_CHANGED = 'notification_changed';
export const NOTIFICATIONLIST_CHANGED = 'notificationlist_changed'
export const LASTSCREEN_CHANGED = 'lastScreen_Changed';
export const LOGIN = 'login';
export const LOGOUT = 'logout';
export const TITLE_CHANGED = 'title_changed'
export const MEMBERDATA_CHANGED = 'memberdata_changed'
export const ECARDDATA_CHANGED = 'ecarddata_changed'
export const ECARDDATA_CLEAR = 'ecarddata_clear'

//hospital master data
export const HOSPITALDATA_CHANGED = 'hospitaldata_changed'
export const CLINICDATA_CHANGED = 'clinicdata_changed'
export const PROVINCEDATA_CHANGED = 'provincedata_changed'
export const CITYDATA_CHANGED = 'citydata_changed'

//home master data
export const LINKLIST_CHANGED = 'linkList_changed'
export const HOWTOCLAIMLIST_CHANGED = 'howToClaimList_changed'
export const HEALTHTIPSLIST_CHANGED = 'healthTipsList_changed,'
export const NEWSLIST_CHANGED = 'newsList_changed'
export const HASECARD_CHANGED = 'hasECard_changed'
export const PIN_FAILED = 'pin_failed'