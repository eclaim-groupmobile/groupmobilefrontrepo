import React, { Component } from "react";
import {
  Image,
  Dimensions,
  Keyboard,
  KeyboardAvoidingView,
  PixelRatio,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Header } from "react-navigation";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import LoginStyles from "../styles/LoginStyle.style";
import * as labels from "../constants/label";
import { TextField } from "react-native-materialui-textfield";
import * as utils from "../functions";
import DefaultText from "../components/DefaultText"

const now = new Date()

class ReRegisterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      policyNumber: "",
      memberNo: "",
      // password: "",
      passport: "",
      dob: "",
      tel: "",
      email: "",
      lineId: "",
      KeyboardAvoidingView:false,
      errors: {},
    }

    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitTel = this.onSubmitTel.bind(this);
    this.onSubmitEmail = this.onSubmitEmail.bind(this);
    this.onSubmitLineId = this.onSubmitLineId.bind(this);

    this.telRef = this.updateRef.bind(this, 'tel');
    this.emailRef = this.updateRef.bind(this, 'email');
    this.lineIdRef = this.updateRef.bind(this, 'lineId');
  }
  
  policyNumber = '';
  memberNo = '';
  // password = '';
  passport = '';
  dob = '';

  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {utils.confirmBack(navigation.state.params.lang,navigation,"Home")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });
  
  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start ReRegisterScreen");
    policyNumber = this.props.navigation.getParam('policyNumber', '');
    memberNo = utils.formatMemberNo(this.props.navigation.getParam('memberNo', ''));
    // password = this.props.navigation.getParam('password', '');
    passport = this.props.navigation.getParam('passport', '');
    dob = this.props.navigation.getParam('dob', '');
    
    tel = this.props.navigation.getParam('tel', '');
    email = this.props.navigation.getParam('email', '');
    lineId = this.props.navigation.getParam('lineId', '');
    // console.log(policyNumber,memberNo,passport,dob)
    this.setState({
      policyNumber,
      memberNo,
      // password,
      passport,
      dob,
      tel,
      email,
      lineId,
      KeyboardAvoidingView:false,
      errors: {},
    })
    this.handleCheckCompleteField(tel,'tel')
    this.setTitle(this.props.language)
  };

  navigateBack = () => {
    // if(this.picker.isPickerShow()){
    //   this._onCalendarHandle();
    // }else{
      // this.props.navigation.navigate("RegisterStep1");
      utils.confirmBack(this.props.language,this.props.navigation,"Home")
    // }
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "login"),
      lang: lang,
    });
  }

  handleNextPress = () => {
    let errorFlag = false
    let errors = {};

    let telError = labels.getError(this.props.language,utils.validateTel(this.state.tel));
    if(telError!=""){
      errorFlag = true
      errors['tel'] = telError
    }

    let emailError = labels.getError(this.props.language,utils.validateEmail(this.state.email));
    if(emailError!=""){
      errorFlag = true
      errors['email'] = emailError
    }

    let lineIdError = labels.getError(this.props.language,utils.validateLineId(this.state.lineId));
    if(lineIdError!=""){
      errorFlag = true
      errors['lineId'] = lineIdError
    }
    
    this.setState({ errors });
    if(errorFlag){
      
    }else{
      this.props.navigation.navigate("ReRegisterTerm",{
        policyNumber: this.state.policyNumber,
        memberNo: this.state.memberNo,
        // password: this.state.password,
        passport: this.state.passport,
        dob: this.state.dob,
        tel: this.state.tel,
        email: this.state.email,
        lineId: this.state.lineId,
      })
    }
  }

  handleCheckCompleteField = (text,name) => {
    let count = 0

    if(name!='tel'){
      let tel = this.state.tel
      if(tel==null) tel = ""
      if(tel.length>0) count = count + 1
    }

    // if(name!='email'){
    //   let email = this.state.email
    //   if(email==null) email = ""
    //   if(email.length>0) count = count + 1
    // }

    // if(name!='lineId'){
    //   let lineId = this.state.lineId
    //   if(lineId==null) lineId = ""
    //   if(lineId.length>0) count = count + 1
    // }

    if(text==null) text = ""
    if(text.length>0) count = count + 1
    
    if(count>=1) this.setState({complete: true})
    else this.setState({complete:false})
  }

  onChangeText(text) {
    let errors = this.state.errors;
    ['tel', 'email', 'lineId']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          errors[name] = ''
          this.setState({ [name]: text, errors });
          this.handleCheckCompleteField(text,name)
        }
      });
  }

  onSubmitTel() {
    this.email.focus();
  }

  onSubmitEmail() {
    this.lineId.focus();
  }

  onSubmitLineId() {
    this.lineId.blur();
  }
  
  updateRef(name, ref) {
    this[name] = ref;
  }

  render() {
    // const _osBehavior = Platform.OS === "ios" ? "position" : null;
    let { errors = {}, ...data } = this.state;
    let lang = this.props.language;
    const deviceWidth = Dimensions.get("window").width;
    const deviceHeight = Dimensions.get("window").height;

    return (
      
      <View style={LoginStyles.main}>
      <ScrollView keyboardShouldPersistTaps='always'>
        <KeyboardAvoidingView style={[LoginStyles.container,{marginBottom:26,marginTop:20}]} behavior="position" enabled={this.state.KeyboardAvoidingView} keyboardVerticalOffset={Header.HEIGHT + 20}>
          <TextField
            ref={this.telRef}
            value={data.tel}
            maxLength={10}
            inputContainerPadding={2}
            lineWidth={2}
            autoCorrect={false}
            enablesReturnKeyAutomatically={true}
            onChangeText={this.onChangeText}
            onSubmitEditing={this.onSubmitTel}
            returnKeyType='done'
            label={labels.getLabel(lang,"tel")}
            error={errors.tel}
            tintColor={Colors.bondiBlue}
            baseColor={Colors.bondiBlue}
            placeholder={' '}
            keyboardType="phone-pad"
            onFocus={()=>this.setState({KeyboardAvoidingView:false})}
            helpersNumberOfLines={1.2}
            style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
            labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            labelFontSize={utils.getFontScale(14)}
            containerStyle={{marginTop:10}}
            labelPadding={20}
          />

          <TextField
            ref={this.emailRef}
            value={data.email}
            maxLength={128}
            inputContainerPadding={2}
            lineWidth={2}
            autoCorrect={false}
            enablesReturnKeyAutomatically={true}
            onChangeText={this.onChangeText}
            onSubmitEditing={this.onSubmitEmail}
            returnKeyType='done'
            label={labels.getLabel(lang,"email")}
            error={errors.email}
            tintColor={Colors.bondiBlue}
            baseColor={Colors.bondiBlue}
            placeholder={' '}
            keyboardType="email-address"
            onFocus={()=>this.setState({KeyboardAvoidingView:false})}
            helpersNumberOfLines={1.2}
            style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
            labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            labelFontSize={utils.getFontScale(14)}
            containerStyle={{marginTop:10}}
            labelPadding={20}
          />

          <TextField
            ref={this.lineIdRef}
            value={data.lineId}
            maxLength={20}
            inputContainerPadding={2}
            lineWidth={2}
            labelFontSize={14}
            autoCorrect={false}
            enablesReturnKeyAutomatically={true}
            onChangeText={this.onChangeText}
            onSubmitEditing={this.onSubmitLineId}
            returnKeyType='done'
            label={labels.getLabel(lang,"lineId")}
            error={errors.lineId}
            tintColor={Colors.bondiBlue}
            baseColor={Colors.bondiBlue}
            placeholder={' '}
            onFocus={()=>this.setState({KeyboardAvoidingView:true})}
            helpersNumberOfLines={1.2}
            style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
            labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            labelFontSize={utils.getFontScale(14)}
            containerStyle={{marginTop:10}}
            labelPadding={20}
          />

          <TouchableOpacity onPress={this.handleNextPress}
            style={[
              this.state.complete ? LoginStyles.button : LoginStyles.buttonInactive,
                { marginTop: 50 }
            ]}
            disabled={!this.state.complete}
          >
            <DefaultText bold style={{color: this.state.complete ? Colors.buttonTextActive : Colors.buttonTextInactive}}>{labels.getLabel(lang,"next")}</DefaultText>
          </TouchableOpacity>
        </KeyboardAvoidingView>
      </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus } = auth;
  return { language, lastScreen, loginStatus };
};

export default connect(
  mapStateToProps,
  {

  }
)(ReRegisterScreen);
