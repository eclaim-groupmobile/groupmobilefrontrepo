import React, { Component } from "react";
import {
  Alert,
  ActivityIndicator,
  AppState,
  Dimensions,
  FlatList,
  Image,
  Linking,
  PixelRatio,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import Constants from 'expo-constants';
import * as SQLite from 'expo-sqlite';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import { Icon, CheckBox, FormLabel } from "react-native-elements";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import CustomUserInactivity from "../components/CustomUserInactivity";
import * as labels from "../constants/label";
import Picker from "../components/picker";
import MainStyles from "../styles/Main.styles";
import HospitalStyles from "../styles/Hospital.style";
import * as utils from "../functions";
import HospitalCard from "../components/HospitalCard";
import NodataList from "../components/NoDataList"
import DefaultText from "../components/DefaultText"

class HospitalByHospitalScreen extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      apiData:[],
      search:"",
      refreshing: true,
      location: null,
      permission:"false",
      locationEnable:"false",
    };
  }

  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily: typeof(navigation.state.params)==='undefined' || navigation.state.params.lang == 'th' ? 'Prompt_bold' : 'Helvetica_bold',fontWeight:'200',fontSize:16/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {navigation.navigate("Hospital")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });

  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start HospitalByNearScreen");
    let lang = this.props.language
    this.setState({
      apiData:[],
      search:this.props.hospitalData[lang][0],
      permission:"false",
      locationEnable:"false",
    })
    this.setTitle(this.props.language)
    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({ permission: true });
      Alert.alert(
        labels.getLabel(lang,'error'),
        'This page will not work on android emulator',
        [
          {text: 'OK', onPress: () => this.props.navigation.navigate("Home")},
        ],
        { cancelable: false }
        )
    } else {
      this.getLocationAsync();
    }
  };

  getLocationAsync = async () => {
    let lang = this.props.language
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({ permission: false });
      Alert.alert(labels.getError(lang,"cannotFindYourCurrentLocation"))
      this.props.navigation.navigate("Hospital")
    }else{
      this.setState({ permission: true });
      let locationStatus = await Location.getProviderStatusAsync()
      if(locationStatus.locationServicesEnabled){
        this.setState({ locationEnable: true });
        let location = await Location.getCurrentPositionAsync({accuracy : 6});
        this.setState({lat: location.coords.latitude,long: location.coords.longitude})
        this.onFetch(location.coords.latitude,location.coords.longitude)
      }else{
        this.setState({ locationEnable: false });
        Alert.alert(
          labels.getLabel(lang,'error'),
          labels.getLabel(lang,'locationServicesDisable'),
          [
            {text: labels.getLabel(lang,'cancel'), onPress: () => this.props.navigation.navigate("Hospital"), style: 'cancel'},
            {text: labels.getLabel(lang,'ok'), onPress: () => this.getLocationAsync()},
          ],
          { cancelable: false }
        )
      }
    }
  };

  navigateBack = () => {
    this.props.navigation.navigate("Hospital");
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "hospitalByNear"),
      lang: lang,
    });
  }

  onFetch = async (lat,long) => {
    this.setState({refreshing: true});
    let lang = this.props.language
    let name = lang == 'th' ? 'nameTH' : 'nameEN'
    let distant = 3 //km
    let distantLatLong = utils.destinationPoint(lat,long,45,distant)
    let distantLat = distantLatLong.split(",")[0]
    let distantLong = distantLatLong.split(",")[1]
    let lat1 = Number(lat) - Number(distantLat)
    let lat2 = Number(lat) + Number(distantLat)
    let long1 = Number(long) - Number(distantLong)
    let long2 = Number(long) + Number(distantLong)
    const db = SQLite.openDatabase('db.MasterData');
    
    let sql = "select * from hospital where latitude >= ? and latitude <= ? and longtitude >= ? and longtitude <= ?;"
    await db.transaction(tx => {
        tx.executeSql(
          sql,
          [lat1,lat2,long1,long2],
          (_, { rows: { _array } }) => {
            // console.log(_array)
            this.setState({apiData: _array});
          }
        );
    })
    
    let sql2 = "select * from clinic where latitude >= ? and latitude <= ? and longtitude >= ? and longtitude <= ?;"
    await db.transaction(tx => {
        tx.executeSql(
          sql2,
          [lat1,lat2,long1,long2],
          (_, { rows: { _array } }) => {
            // console.log(_array)
            let tmpArr = this.state.apiData
            tmpArr = tmpArr.concat(_array).sort((a, b) => utils.compareObjectName(a,b,name))
            this.setState({apiData: tmpArr.sort()},() => this.setState({refreshing: false}));
          }
        );
    })
  }

  genBox = (item,index) => {
    return (
      <HospitalCard 
        item={item}
        index={index}
        lat={this.state.lat}
        long={this.state.long}
      />
    )
  }

  render() {
    let lang = this.props.language;
    let refreshing = this.state.refreshing;
    const deviceWidth = Dimensions.get("window").width;
    const deviceHeight = Dimensions.get("window").height;
    return (
      <View style={MainStyles.main}>
        <CustomUserInactivity onAction={() => this.props.navigation.navigate("Home")}>
          <View
            style={[
              MainStyles.container,
              { paddingLeft: 10, paddingRight: 10, paddingTop: 10 }
            ]}
          >
            { /*refreshing ? 
            <ActivityIndicator size="large" color="grey" />
              : */}
            <FlatList
              data={this.state.apiData}
              renderItem={({item,index}) => this.genBox(item,index)}
              keyExtractor={(item) => (item.id.toString()+item.BType)}
              refreshing={this.state.refreshing}
              onRefresh={this.onFetch}
              ListEmptyComponent={!this.state.refreshing && <NodataList noDataNear/>}
            />
            
          </View>
        </CustomUserInactivity>
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
    const { language, lastScreen, hospitalData} = auth;
    return { language, lastScreen, hospitalData};
  };
  
export default connect(
  mapStateToProps,
  {
  }
)(HospitalByHospitalScreen);
