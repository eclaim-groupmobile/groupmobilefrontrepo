import React, { Component } from "react";
import {
  Alert,
  Image,
  Keyboard,
  PixelRatio,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import LoginStyles from "../styles/LoginStyle.style";
import * as labels from "../constants/label";
import CodePin from "../components/CodePin";
import * as utils from "../functions";
import DefaultText from "../components/DefaultText"

class ForgetPinOTPScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      policyNumber: "",
      memberNo: "",
      passport: "",
      dob: "",
      tel: "",
      email: "",
      lineId: "",

      otpError:"",
    };
  }
  
  policyNumber = '';
  memberNo = '';
  passport = '';
  dob = '';
  tel = '';
  email = '';
  lineId = '';

  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {utils.confirmBack(navigation.state.params.lang,navigation,"Home")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });
  
  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start ForgetPinOTPScreen");
    let pin = this.props.navigation.getParam('pin', '');
    let localAuthentication = this.props.navigation.getParam('localAuthentication', '0');
    let tel = this.props.navigation.getParam('tel', '');
    let policyNumber = this.props.navigation.getParam('policyNumber', '');
    let memberNo = this.props.navigation.getParam('memberNo', '');
    let passport = this.props.navigation.getParam('passport', '');
    let token = this.props.navigation.getParam('token', '');
    this.setState({
      pin, 
      localAuthentication,
      tel,
      policyNumber,
      memberNo,
      token,
      complete:false,
      telError:""
    })
    this.setTitle(this.props.language)
    this.getOTP(policyNumber,memberNo,passport,tel,token)
  };

  navigateBack = () => {
    utils.confirmBack(this.props.language,this.props.navigation,"Home")
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "otp"),
      lang: lang,
    });
  }

  getData = async key => {
    let value = await utils.retrieveData(key)
    this.setState({[key]:value})
  }

  onsuccess() {
    Alert.alert(labels.getLabel(this.props.language,"changePinSuccess"))
    this.props.navigation.navigate("Home")
  }

  checkPin = async (code,callback) => {
    let lang = this.props.language
    let result = ""
    let xmlBody = utils.ChkOTPBody("RESETPWD",this.state.policyNumber,this.state.memberNo,this.state.passport,this.state.tel,this.state.ref,code,this.state.token);
    console.log(`Request body: ${xmlBody}`);
    await fetch(utils.urlUserService, utils.genRequest(utils.ChkOTPSOAPAction,xmlBody) )
      .then(response => response.text())
      .then(response => {
        result = utils.getResult(response)
        let errorCode = result["ChkOTPResponse"][0]["ChkOTPResult"][0]["a:errorCode"][0].toString()
        if(Number(errorCode)==0){
          let status = result["ChkOTPResponse"][0]["ChkOTPResult"][0]["a:status"][0].toString()
          if(status=="VALID"){
            utils.setPin(this.state.pin)
            utils.setLocalAuthenticationStore(this.state.localAuthentication)
            callback(true);
          }else{
            if(status=="INVALID"){
              Alert.alert(labels.getError(lang,"otpInvalid"))
            }else if(status="EXPIRE"){
              Alert.alert(labels.getError(lang,"otpExpire"))
            }
            callback(false);
          }
        }else{
          callback(false);
          let labelError = "otp"
          this.setState({otpError:labelError})
        }
      })
      .catch(err => {
        console.log("fetch", err);
        Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
        callback(false);
        this.setState({otpError:""})
      });
  }

  resend = () => {
    console.log("resend OTP")
    this.getOTP(this.state.policyNumber,this.state.memberNo,this.state.passport,this.state.tel,this.state.token)
  }

  getOTP = async (policyNumber,memberNo,passport,tel,token) => {
    let lang = this.props.language
    let result = ""
    let xmlBody = utils.GetOTPBody("RESETPWD",policyNumber,memberNo,passport,tel,token);
    console.log(`Request body: ${xmlBody}`);
    await fetch(utils.urlUserService, utils.genRequest(utils.GetOTPSOAPAction,xmlBody) )
      .then(response => response.text())
      .then(response => {
        result = utils.getResult(response)
        let errorCode = result["GetOTPResponse"][0]["GetOTPResult"][0]["a:errorCode"][0].toString()
        let errorDesc = ""
        if(lang=="th"){
          errorDesc = result["GetOTPResponse"][0]["GetOTPResult"][0]["a:errorDescTH"][0].toString()
        }else{
          errorDesc = result["GetOTPResponse"][0]["GetOTPResult"][0]["a:errorDescEN"][0].toString()
        }
        if(Number(errorCode)=="0"){
          let ref = result["GetOTPResponse"][0]["GetOTPResult"][0]["a:refID"][0].toString()
          this.setState({ref})
        }else{
          Alert.alert(labels.getLabel(lang,"error"),errorDesc)
        }
      })
      .catch(err => {
        console.log("fetch", err);
        Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
      });
  }

  render() {
    let lang = this.props.language;

    return (
      <View style={LoginStyles.main}>
        <View style={[{flex:1,alignItems:'center'}]}>
          <View style={{position:'absolute',top:20,left:0,right:0,alignItems:'center'}}>
            <DefaultText style={{color: Colors.eagle,fontSize:18}}>{labels.getLabel(lang,"otpHead1")} x{this.state.tel.slice(-4)}</DefaultText>
            <DefaultText style={{color: Colors.eagle,fontSize:18,}}>{labels.getLabel(lang,"otpHead2")} {this.state.ref}</DefaultText>
            <DefaultText style={{color: 'red',fontSize:18,}}>{labels.getError(this.props.language,this.state.otpError)}</DefaultText>
          </View>
          <View style={{flex:1,justifyContent: 'center',}}>
            <CodePin 
              number={6}
              success={() => this.onsuccess()}
              checkPinCode={(code, callback) => this.checkPin(code,callback)}
              error={''}
              keyboardType="phone-pad"
              language={lang}
            />

            <View style={[{marginTop:40,alignItems:'flex-end'}]}>
              <View style={{flexDirection: 'row',alignItems:'flex-end'}}>
                <DefaultText style={{color:Colors.bondiBlue,fontSize:18}} onPress={this.resend}>{labels.getLabel(this.props.language,"smsResend")}</DefaultText>
              </View>
            </View>

          </View>
          
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus } = auth;
  return { language, lastScreen, loginStatus };
};

export default connect(
  mapStateToProps,
  {

  }
)(ForgetPinOTPScreen);
