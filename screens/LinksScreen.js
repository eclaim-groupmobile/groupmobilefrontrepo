import React from "react";
import {
  ActivityIndicator,
  Dimensions,
  Alert,
  FlatList,
  Image,
  Linking,
  PixelRatio,
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  WebView,
} from "react-native";
import Constants from 'expo-constants';
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { languageChanged, lastScreenChanged, logOut } from "../actions";
import * as labels from "../constants/label";
import CustomUserInactivity from "../components/CustomUserInactivity";
import LoginStyles from "../styles/LoginStyle.style";
import Colors from "../constants/Colors";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import * as utils from "../functions";
import { Card, Title, Paragraph } from 'react-native-paper';
import Modal from "react-native-modal";
import DefaultText from "../components/DefaultText"

class LinksScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {navigation.navigate("Home")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });

  constructor(props) {
    super(props);
    this.state = { 
      onLoad:true,
    };
  }

  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start LinksScreen");
    this.setTitle(this.props.language)
    this.setState({
      isModalVisible:false,
      onLoad:true
    })
  };

  navigateBack = () => {
    this.props.navigation.navigate("Home");
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "links"),
      lang: lang,
    });
  }

  genBox = (item,index) => {
    let lang = this.props.language
    let name = ''
    if(lang=='th'){
      name = item.titleTH
    }else{
      name = item.titleEN
    }
    if(name!=''){
      return (
        <TouchableOpacity onPress={() => Linking.openURL(item.link)} style={[styles.link]}>
          <Image
            source={require("../assets/icon/Globe.png")}
            style={{width: 25, height: 25}}
          />
          <DefaultText style={{marginLeft:10,color:Colors.bondiBlue,flex:1,flexWrap: 'wrap'}}>{name}</DefaultText>
        </TouchableOpacity>
      )
    }
  }

  render() {
    let lang = this.props.language;
    return (
      <View style={LoginStyles.main}>
        <CustomUserInactivity onAction={() => this.props.navigation.navigate("Home")}>
          <View style={styles.option}>
            <FlatList
              data={this.props.linkList}
              renderItem={({item,index}) => this.genBox(item,index)}
              keyExtractor={(item) => item.titleEN}
              extraData={this.props.language}
            />
          </View>
        </CustomUserInactivity>
      </View>
    );
  }

  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
  },
  row:{
    flexDirection: "row", 
    justifyContent: "space-between",
    padding:10
  },
  option:{
    justifyContent:'center',
  },
  link:{
    flexDirection:'row',
    textAlign:'center',
    marginLeft:20,
    marginRight:20,
    marginTop:20,
  },
  fullModal: {
    margin: 0,
    flex:1
  },
  subModal: {
    height: Dimensions.get('window').height,
    width:Dimensions.get('window').width,
    flex:1,
    backgroundColor: "white",
    alignItems: "center",
  },
  webview: {
    width:Dimensions.get('window').width,
  }
});

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus, linkList } = auth;
  return { language, lastScreen, loginStatus, linkList };
};

export default connect(
  mapStateToProps,
  {
    languageChanged,
    lastScreenChanged,
    logOut
  }
)(LinksScreen);
