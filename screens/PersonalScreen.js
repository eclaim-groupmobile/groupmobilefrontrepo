import React, { Component } from "react";
import {
  Alert,
  ActivityIndicator,
  BackHandler,
  Dimensions,
  FlatList,
  Image,
  ImageBackground,
  Linking,
  PixelRatio,
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  WebView
} from "react-native";
import Constants from 'expo-constants';
import { Icon } from 'react-native-elements';
import Modal from "react-native-modal";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import { languageChanged, lastScreenChanged, logOut } from "../actions";
import CustomUserInactivity from "../components/CustomUserInactivity";
import SliderPersonal, { sliderWidth, itemWidth } from "../styles/SliderPersonal.style";
import Carousel from "react-native-snap-carousel";
import * as labels from "../constants/label";
import MainStyles from "../styles/Main.styles"
import * as utils from "../functions"
import NodataList from "../components/NoDataList"
import DefaultText from "../components/DefaultText"

class PersonalScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pinStore: "",
      apiData:[],
      apiLength:0,
      slider1ActiveSlide:0,
      onLoad:true,
      EnableView:false,
    };
  }

  indexStart = null

  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
  });

  componentDidMount() {
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = async () => {
    utils.log("Start PersonalScreen");
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton.bind(this));
    if(this.props.lastScreen=="Member"){
      this.indexStart = this.props.navigation.getParam('indexStart', 0);
      this.props.navigation.setParams({indexStart:0})
    }else{
      this.indexStart = 0
    }
    this.onScreenChanged()
    this.getLoginData()
    this.setTitle(this.props.language)
  };
  handleBackButton = () => {
    BackHandler.exitApp();
    return true;
  };

  onScreenChanged() {
    this.props.lastScreenChanged("Personal");
  }

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "personal"),
      lang: lang,
    });
  }

  getData = async key => {
    let value = await utils.retrieveData(key)
    this.setState({[key]:value})
  }

  getLoginData = async key => {
    if(!this.props.loginStatus){
      this.setState({EnableView:false})
      let pinStoreVal = await utils.retrieveData("pinStore")
      if(pinStoreVal.length>0) this.props.navigation.navigate('LoginPin')
      else this.props.navigation.navigate('Login')
    }else{
      this.setState({EnableView:true})
      if(this.props.memberData!=null){
        this.setState({
          apiData: this.props.memberData['memberInfo'],
          memberNoMain: this.props.memberData['memberInfo'][0]['MemberNo'].substring(0,5),
          apiLength: this.props.memberData['memberInfo'].length,
          slider1ActiveSlide:0,
          indexStart: this.indexStart
        }, () => this._slider1Ref.snapToItem(this.indexStart))
      }
      this.setState({
        isModalVisible:false,
        onLoad:true
      })
    }
  }

  genProfileData = (header,...data) => {
    let data1 = data[0]
    let data2 = ""
    if(data.length==2) data2 = ` - ${data[1]}`
    let valid = true
    for(let i=0;i<data.length;i++){
      if(data[i]==null || data[i]=="") valid = false
    }
    if(valid){
      return (
        <View style={{flexDirection:'row'}}>
          <DefaultText style={SliderPersonal.personalLabel}>{header} :</DefaultText>
          <DefaultText style={SliderPersonal.personalData}>{data1}{data2}</DefaultText>
        </View>
      )
    }
  }

  //? Life Benefit
  genHeader1 = (lang,item,...data) => {
    let i = 0
    return(
      <View>
        <DefaultText bold style={[SliderPersonal.containerLabel]}>{labels.getLabel(lang, "Personal_header1")}</DefaultText>
        <View style={[SliderPersonal.containerInsideBox,{overflow:'hidden'}]}>

          {/* GTL */}
          {
            (item.benefitInfo.GTL != null && item.benefitInfo.GTL != "" && Number(item.benefitInfo.GTL.replace(/,/g, '')) > 1000 ) &&
            this.genData1(lang,data[0],item.benefitInfo.GTL,i++)
          }

          {/* GAD */}
          {
            (item.benefitInfo.GAD != null && item.benefitInfo.GAD != "") &&
            this.genData1(lang,data[1],item.benefitInfo.GAD,i++)
          }

          {/* GDA */}
          {
            (item.benefitInfo.GDA != null && item.benefitInfo.GDA != "") &&
            this.genData1(lang,data[2],item.benefitInfo.GDA,i++)
          }

          {/* GRC */}
          {
            (item.benefitInfo.GRC != null && item.benefitInfo.GRC != "") &&
            this.genData1(lang,data[3],item.benefitInfo.GRC,i++)
          }

          {/* GPTD */}
          {
            (item.benefitInfo.GPTD != null && item.benefitInfo.GPTD != "") &&
            this.genData1(lang,data[4],item.benefitInfo.GPTD,i++)
          }

          {/* GCI */}
          {
            (item.benefitInfo.GCI != null && item.benefitInfo.GCI != "") &&
            this.genData1(lang,data[5],item.benefitInfo.GCI,i++)
          }
        </View>
      </View>
    )
  }

  genData1 = (lang,header,data,num) => {
    {/*<View style={{backgroundColor: num%2==0 ? 'rgba(256, 256, 256, 0)' : "#deeaf6",paddingHorizontal:5,paddingVertical:3}}>*/}
    return (
      <View style={{backgroundColor:'white',paddingHorizontal:5,paddingVertical:3}}>
        <DefaultText bold style={SliderPersonal.personalDataBold}>{header}</DefaultText>
        <DefaultText style={SliderPersonal.personalData}>     {labels.getLabel(lang, "Personal_sumAssured")}: {utils.formatMoney(data)} {labels.getLabel(lang, "baht")}</DefaultText>
      </View>
    )
  }

  //? Health Benefit
  genHeader2 = (lang,...data) => {
    if((data[0] != null && data[0].length > 0)||(data[1] != null && data[1].length > 0)||(data[2] != null && data[2].length > 0)||(data[3] != null && data[3].length > 0)||(data[4] != null && data[4].length > 0)||(data[5] != null && data[5].length > 0)){
      let i = 0
      return (
        <View>
          <DefaultText bold style={[SliderPersonal.containerLabel,{marginTop:20}]}>{labels.getLabel(lang, "Personal_header2")}</DefaultText>
          <View style={[SliderPersonal.containerInsideBox,{overflow:'hidden'}]}>
            {/* IPD */}
            {
              (data[0] != null && data[0].length > 0) &&
              this.genData2(lang,"Personal_healthIPD",data[0],i++,true)
            }

            {/* OPD */}
            {
              (data[1] != null && data[1].length > 0) &&
              this.genData2(lang,"Personal_healthOPD",data[1],i++,false)
            }

            {/* Dental */}
            {
              (data[2] != null && data[2].length > 0) &&
              this.genData2(lang,"Personal_healthDental",data[2],i++,false)
            }

            {/* Accident */}
            {
              (data[3] != null && data[3].length > 0) &&
              this.genData2(lang,"Personal_healthAccident",data[3],i++,false)
            }

            {/* Critical */}
            {
              (data[4] != null && data[4].length > 0) &&
              this.genData2(lang,"Personal_healthCritical",data[4],i++,false)
            }

            {/* Maternity */}
            {
              (data[5] != null && data[5].length > 0) &&
              this.genData2(lang,"Personal_healthMaternity",data[5],i++,false)
            }
          </View>
        </View>
      )
    }
  }

  genData2 = (lang,header,data,num,highlight) => {
    {/* <View style={{backgroundColor: num%2==0 ? 'rgba(256, 256, 256, 0)' : "#deeaf6",paddingHorizontal:5,paddingVertical:3}}> */}
    return (
      <View style={{backgroundColor:'white',paddingVertical:3}}>
        <DefaultText bold style={SliderPersonal.personalDataBold}>{labels.getLabel(lang, header)}</DefaultText>
        <FlatList
          data={data}
          renderItem={({item,index}) => this.genData2Sub(item,index,highlight)}
          keyExtractor={(item) => item.id}
          extraData={this.props.language}
        />
      </View>
    )
  }

  genData2Sub = (item,index,highlight) => {
    let lang = this.props.language
    let data = lang=='th' ? item.delTH : item.delEN
    if(data!=null && data!=""){
      return (
        <View style={{paddingHorizontal:5,backgroundColor:highlight&&index%2!=0?'#F0F0F0':'white'}}>
          <DefaultText style={SliderPersonal.personalData}>     {lang=='th' ? item.delTH : item.delEN}</DefaultText>
        </View>
      )
    }
  }




  genBox = ({ item, index }) => {
    let lang = this.props.language
    if(item.MemberNo!=null){
      //console.log(item)
      let GTLDetl = ""
      let GADDetl = ""
      let GDADetl = ""
      let GRCDetl = ""
      let GPTDDetl = ""
      let GCIDetl = ""
      let companyName = ""

      let Age = ""
		  if(lang=="th"){
        GTLDetl = item.benefitInfo.GTLDetlTH
        GADDetl = item.benefitInfo.GADDetlTH
        GDADetl = item.benefitInfo.GDADetlTH
        GRCDetl = item.benefitInfo.GRCDetlTH
        GPTDDetl = item.benefitInfo.GPTDDetlTH
        GCIDetl = item.benefitInfo.GCIDetlTH
        companyName = item.CompanyName
        Age = item.AgeTH
      }else{
        GTLDetl = item.benefitInfo.GTLDetlEN
        GADDetl = item.benefitInfo.GADDetlEN
        GDADetl = item.benefitInfo.GDADetlEN
        GRCDetl = item.benefitInfo.GRCDetlEN
        GPTDDetl = item.benefitInfo.GPTDDetlEN
        GCIDetl = item.benefitInfo.GCIDetlEN
        companyName = item.CompanyNameEN
        Age = item.AgeEN
      }

      return (
        <View style={{justifyContent:"center",alignItems:'center'}}>
          <View style={[SliderPersonal.slideInnerContainer]}>
            { index !=0 ?
              <TouchableOpacity style={SliderPersonal.slideInnerSideContainer} onPress={() => this._slider1Ref.snapToPrev()}>
                <Icon
                  name='chevron-left'
                  type='evilicon'
                  color={Colors.bondiBlue}
                  size={40}
                />
              </TouchableOpacity> :
              <TouchableOpacity style={SliderPersonal.slideInnerSideContainer}>
                <DefaultText> </DefaultText>
              </TouchableOpacity>
            }
            <View style={SliderPersonal.slideInnerCenterContainer}>
                <DefaultText bold style={{color:Colors.bondiBlue,fontSize:17}}>{item.FName} {item.LName}</DefaultText>
            </View>
            { index != this.state.apiLength-1 ?
              <TouchableOpacity style={SliderPersonal.slideInnerSideContainer} onPress={() => {this._slider1Ref.snapToNext()}}>
                <Icon
                  name='chevron-right'
                  type='evilicon'
                  color={Colors.bondiBlue}
                  size={40}
                />
              </TouchableOpacity> :
              <TouchableOpacity style={SliderPersonal.slideInnerSideContainer}>
                <DefaultText> </DefaultText>
              </TouchableOpacity>
            }
          </View>

          <ScrollView style={{margin:5}}>
            <View style={[SliderPersonal.dataContainer,{backgroundColor:'white',padding:0}]}>
              {/*<View style={{justifyContent:"center",alignItems:"center"}}>
                <Image
                  style={{ width: 70, height: 70, opacity:0 }}
                  source={require("../assets/images/person.png")}
                />
              </View>*/}

              <View style={{flex:1,paddingVertical:10,paddingRight:10}}>
                {
                  //this.props.memberData['policyNo'] != null &&
                  this.genProfileData(labels.getLabel(lang, "Personal_policyNumber"),this.props.memberData['policyNo'])
                }

                {
                  //item.MemberNo != null &&
                  this.genProfileData(labels.getLabel(lang, "Personal_memberNo"),item.MemberNo)
                }

                {
                  //this.props.memberData['startDate'] != null && this.props.memberData['endDate'] != null &&
                  this.genProfileData(labels.getLabel(lang, "Personal_effectiveDate"),item.StartDate,item.EndDate)
                }

                {
                  //item.MemberNo.slice(-2) == "00" && this.props.memberData['companyNM'] != null &&
                  this.genProfileData(labels.getLabel(lang, "Personal_company"),companyName)
                }

                {/*
                  //item.FName != null &&
                  this.genProfileData(labels.getLabel(lang, "Personal_name"),item.FName + " " + item.LName)
                }

                {
                  //item.CitizenID != null &&
                  this.genProfileData(labels.getLabel(lang, "Personal_idNo"),item.CitizenID)
                }

                {
                  //item.Age != null &&
                  this.genProfileData(labels.getLabel(lang, "Personal_age"),Age)
                }

                {
                  //item.Sex != null &&
                  this.genProfileData(labels.getLabel(lang, "Personal_sex"),item.Sex)
                */}
              </View>
            </View>

            <View style={[SliderPersonal.dataContainer2,{marginTop:10}]}>

              { (
                  (item.benefitInfo.GTL != null && Number(item.benefitInfo.GTL.replace(/,/g, '')) > 1000 ) ||
                  item.benefitInfo.GAD != null ||
                  item.benefitInfo.GDA != null ||
                  item.benefitInfo.GRC != null ||
                  item.benefitInfo.GPTD != null ||
                  item.benefitInfo.GCI != null
                ) &&
                this.genHeader1(lang,item,GTLDetl,GADDetl,GDADetl,GRCDetl,GPTDDetl,GCIDetl)
              }

              { (
                  item.benefitInfo.IPDList != null ||
                  item.benefitInfo.OPDList != null ||
                  item.benefitInfo.DentalList != null ||
                  item.benefitInfo.AccidentList != null ||
                  item.benefitInfo.CriticalList != null ||
                  item.benefitInfo.MaternityList != null
                ) &&
                this.genHeader2(lang,item.benefitInfo.IPDList,item.benefitInfo.OPDList,item.benefitInfo.DentalList,item.benefitInfo.AccidentList,item.benefitInfo.CriticalList,item.benefitInfo.MaternityList)
              }

              <DefaultText bold style={[SliderPersonal.containerLabel,{marginTop:20}]}>{labels.getLabel(lang, "Personal_exception")}</DefaultText>

              <TouchableOpacity onPress={() => this.openWeb()}>
                <DefaultText style={[SliderPersonal.personalData,{marginTop:20,color:Colors.bondiBlue,textDecorationLine: 'underline',}]}>{labels.getLabel(lang, "Personal_exceptionDetail")}</DefaultText>
              </TouchableOpacity>

              <DefaultText style={[SliderPersonal.personalData,{marginTop:20,color:'red'}]}><DefaultText bold>{labels.getLabel(lang, "Personal_remark1")}</DefaultText> {labels.getLabel(lang, "Personal_remark2")}</DefaultText>

            </View>

            <View style={{paddingLeft: 10,marginTop:10,marginBottom:20}}>
              {/*<DefaultText style={[SliderPersonal.personalData]}>** {labels.getLabel(lang, "dataAsOf")} {utils.getToday(lang)}</DefaultText>*/}
            </View>
          </ScrollView>

          <Modal isVisible={this.state.isModalVisible} animationOutTiming={50} style={styles.fullModal}>
            <SafeAreaView style={{flex:1}}>
              <View style={styles.subModal}>
                {/* close */}
                <TouchableOpacity style={{position:'absolute',right:10,top:10}} onPress={() => this.setState({isModalVisible:false})}>
                  <Image
                    style={{ width: 22, height: 22 }}
                    source={require("../assets/icon/Close-eagle.png")}
                  />
                </TouchableOpacity>
                <View style={{flex:1,alignContent:'center',marginTop:40}}>

                  <WebView
                    source={{uri: this.state.uriPDF}}
                    style={styles.webview}
                    onLoadStart={() => this.setState({onLoad:true})}
                    onLoadEnd={() => this.setState({onLoad:false})}
                  />
                </View>
              </View>

              {/* loading indicator */}
              <View style={{position:'absolute',left:0,right:0,top:80,}}>
              {
                this.state.onLoad &&
                <ActivityIndicator size="large" color="#666" />
              }
              </View>
            </SafeAreaView>
          </Modal>
        </View>
      )
    }
  }

  openWeb = () => {
    let lang = this.props.language
    let attach = ''
    if(lang=='th'){
      attach = 'Except.pdf'
    }else{
      attach = 'Except_en.pdf'
    }
    let uriPDF = ''
    uriPDF = Constants.manifest.extra.urlFile + attach
    uriPDF = uriPDF.replace(".PDF", ".pdf")
    console.log("open : " + uriPDF)
    if(Platform.OS === "ios"){
      // this.setState({
      //   uriPDF,
      //   isModalVisible:true
      // })
      Linking.openURL(uriPDF)
    }else{
      if(Constants.appOwnership=='expo'){
        Alert.alert("This function not work on Android with Expo Client","Please use standalone version (apk)")
        console.log("This function not work on Android with Expo Client","Please use standalone version (apk)")
      }else{
        Linking.openURL(uriPDF)
      }
    }
  }


  render() {
    if (this.state.EnableView) {
    return (
      <View style={MainStyles.main}>
        <CustomUserInactivity onAction={() => this.props.navigation.navigate("Home")}>
          <View
            style={[
              MainStyles.container,
              { paddingTop: 5 }
            ]}
          >

          { this.props.memberData==null || this.props.memberData['memberInfo'].length==0 ?
            <NodataList/>
            :
            this.props.memberData!= null && this.state.apiData != null && this.state.indexStart != null &&
            <Carousel
              ref={c => (this._slider1Ref = c)}
              data={this.state.apiData}
              renderItem={this.genBox}
              sliderWidth={sliderWidth}
              itemWidth={sliderWidth}
              hasParallaxImages={true}
              firstItem={this.state.indexStart}
              inactiveSlideScale={1}
              inactiveSlideOpacity={0}
              loop={false}
              autoplay={false}
              onLayout={() => {this._slider1Ref.snapToItem(this.state.indexStart)}}
              onSnapToItem={index => {this.setState({ slider1ActiveSlide: index })}}
              windowSize={1}
              scrollEnabled={false}
              removeClippedSubviews={false}
              enableSnap={false}
            />
          }
            {/*this.genBox(0,1)*/}
          </View>

        </CustomUserInactivity>
      </View>
    );
    }else {
      return (<View></View>)
    }
  }
}

const styles = StyleSheet.create({
  fullModal: {
    margin: 0,
    flex:1
  },
  subModal: {
    height: Dimensions.get('window').height,
    width:Dimensions.get('window').width,
    flex:1,
    backgroundColor: "white",
    alignItems: "center",
  },
  webview: {
    width:Dimensions.get('window').width,
  }
});

const mapStateToProps = ({ auth }) => {
    const { language, lastScreen, loginStatus, memberData } = auth;
    return { language, lastScreen, loginStatus, memberData };
  };

export default connect(
  mapStateToProps,
  {
    languageChanged,
    lastScreenChanged,
    logOut,
  }
)(PersonalScreen);
