import React, { Component } from "react";
import {
  Alert,
  Image,
  PixelRatio,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import LoginStyles from "../styles/LoginStyle.style";
import * as labels from "../constants/label";
import * as utils from "../functions";
import DefaultText from "../components/DefaultText"

class ForgetPinTelScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      telError: "",
      localAuthentication: false,
      complete: false,
      changeTel: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {navigation.state.params.navigateBack()}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });

  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start ForgetPinTelScreen");
    let pin = this.props.navigation.getParam('pin', '');
    let localAuthentication = this.props.navigation.getParam('localAuthentication', '0');
    let passport = this.props.navigation.getParam('passport', '');
    this.setState({
      pin, 
      localAuthentication, 
      passport,
      complete: false,
      telError: "",
      changeTel: false,
      tel: "",
    })
    this.getData("policyNumberStore")
    this.getData("memberNoStore")
    this.getData("tokenStore")
    this.getData("lineIdStore")
    this.getData("telStore")
    this.getData("emailStore")


    this.setTitle(this.props.language)
  };

  navigateBack = () => {
    if(this.state.telChange){
      this.setState({
        telError:"",
        tel:"",
        telChange:false,
      })
    }else{
      utils.confirmBack(this.props.language,this.props.navigation,"Home")
    }
  };

  backToState1 = () => {
    this.setState({
      telError:"",
      tel:"",
      telChange:false,
    })
  }

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "forgetPin"),
      lang: lang,
      navigateBack: this.navigateBack,
    });
  }

  getData = async key => {
    let value = await utils.retrieveData(key)
    this.setState({[key]:value})
  }

  telChange = (text) => {
    this.setState({tel:text})
    if(text.length>0){
      this.setState({complete:true})
    }else{
      this.setState({complete:false})
    }
  }

  handleNextPressOldMobileNo = () => {
    this.props.navigation.navigate("ForgetPinOTP",{
      tel: this.state.telStore,
      pin: this.state.pin,
      localAuthentication: this.state.localAuthentication,
      policyNumber: this.state.policyNumberStore,
      memberNo: this.state.memberNoStore,
      passport: this.state.passport,
      token: this.state.tokenStore,
    })
  }

  handleNextPress = async () => {
    let errorFlag = false
    let lang = this.props.language

    let telError = labels.getError(this.props.language,utils.validateTel(this.state.tel));
    if(telError!=""){
      errorFlag = true
    }
    
    this.setState({ telError });
    if(errorFlag){

    }else{
      this.setState({complete:false})
      utils.setTel(this.state.tel)
      let result = ""
      let xmlBody = utils.UpdateMemberInfoBody(this.state.policyNumberStore,this.state.memberNoStore,this.state.tokenStore,this.state.emailStore,this.state.lineIdStore,this.state.tel);
      console.log("-------------------");
      console.log(`Request body: ${xmlBody}`);
      console.log("-------------------");
      let data = null
      await fetch(utils.urlMemberService, utils.genRequest(utils.UpdateMemberInfoSOAPAction,xmlBody) )
        .then(response => response.text())
        .then(response => {
          result = utils.getResult(response)
          // console.log(result)
          let errorCode = result["UpdateMemberInfoResponse"][0]["UpdateMemberInfoResult"][0]["a:errorCode"][0].toString()
          let errorDesc = ""
          if(lang=="th"){
            errorDesc = result["UpdateMemberInfoResponse"][0]["UpdateMemberInfoResult"][0]["a:errorDescTH"][0].toString()
          }else{
            errorDesc = result["UpdateMemberInfoResponse"][0]["UpdateMemberInfoResult"][0]["a:errorDescEN"][0].toString()
          }
          if(Number(errorCode)==0){
            this.props.navigation.navigate("ForgetPinOTP",{
              tel: this.state.tel,
              pin: this.state.pin,
              localAuthentication: this.state.localAuthentication,
              policyNumber: this.state.policyNumberStore,
              passport: this.state.passportStore,
              memberNo: this.state.memberNoStore,
              passport: this.state.passport,
              token: this.state.tokenStore,
            })
          }else{
            Alert.alert(labels.getLabel(lang,"error"),errorDesc)
          }
        })
        .catch(err => {
          console.log("fetch", err);
          Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
        });
      this.setState({complete:true})
    }
  }

  render() {
    let lang = this.props.language
    return (
      <View style={LoginStyles.main}>
        <View style={[LoginStyles.paddingStatus,{backgroundColor:"white"}]}>
          
          {this.state.telChange ? 
            (<View style={{marginTop:45,marginBottom:20,alignItems:'center'}}>
              <DefaultText style={{color: Colors.bondiBlue,fontSize:18}}>{labels.getLabel(lang,"forgetPinTelState2Header1")}</DefaultText>
              <DefaultText style={{color: Colors.bondiBlue,fontSize:18}}>{labels.getLabel(lang,"forgetPinTelState2Header2")}</DefaultText>
              <DefaultText style={{color: Colors.bondiBlue,fontSize:18}}>{labels.getLabel(lang,"forgetPinTelState2Header3")}</DefaultText>
            </View>)
          :
            (<View style={{marginTop:45,marginBottom:20,alignItems:'center'}}>
              <DefaultText style={{color: Colors.bondiBlue,fontSize:18}}>{labels.getLabel(lang,"forgetPinTelState1Header1")}</DefaultText>
              <DefaultText style={{color: Colors.bondiBlue,fontSize:18}}>{labels.getLabel(lang,"forgetPinTelState1Header2")}</DefaultText>
            </View>)
          }

            {this.state.telChange ? 
              (<View style={[{backgroundColor:'white',paddingTop:5,paddingHorizontal:75}]}>
                <TextInput 
                  style={[this.state.telError == "" ? styles.textBox : styles.textBoxError,{fontFamily:'Prompt'}]}
                  onChangeText={text => this.telChange(text)}
                  value={this.state.tel}
                  keyboardType={'numeric'}
                  maxLength={10}
                  returnKeyType={'done'}
                  allowFontScaling={false}
                />
                <View>
                  <DefaultText style={[styles.textError,{textAlign:"left"}]}>{this.state.telError}</DefaultText>
                </View>
              </View>)
            :
              (<View style={[{backgroundColor:'white',paddingTop:5,alignItems:"center",paddingHorizontal:75}]}>
                <DefaultText style={{color: Colors.bondiBlue,fontSize:18}}>{(this.state.telStore!=null && this.state.telStore.length==10) && "XXX XXX " + this.state.telStore.slice(-4)}</DefaultText>
              </View>)
            }

            

        </View>
        {this.state.telChange ? 
          (<View style={{flex:1,flexDirection:'row',paddingHorizontal:40, position:"absolute",left:0,right:0,bottom:50}}>
                <TouchableOpacity onPress={()=>this.navigateBack()}
                  style={[
                      LoginStyles.button,
                      { flex:1, marginRight:10 }
                  ]}
                >
                  <DefaultText bold style={{color: Colors.buttonTextActive, textAlign:'center' }}>{labels.getLabel(lang,"cancel")}</DefaultText>
                </TouchableOpacity>

                <TouchableOpacity onPress={this.handleNextPress}
                  style={[
                    this.state.complete ? LoginStyles.button : LoginStyles.buttonInactive,
                      { flex:1, marginLeft:10 }
                  ]}
                  disabled={!this.state.complete}
                >
                  <DefaultText bold style={{color: this.state.complete ? Colors.buttonTextActive : Colors.buttonTextInactive, textAlign:'center'}}>{labels.getLabel(lang,"next")}</DefaultText>
                </TouchableOpacity>
              </View>)
            :
            (<View style={{flexDirection:'row',paddingHorizontal:40, position:"absolute",left:0,right:0,bottom:50}}>
                <TouchableOpacity onPress={()=>this.setState({telChange:true})}
                  style={[
                      LoginStyles.button,
                      { flex:1, marginRight:10 }
                  ]}
                >
                  <DefaultText bold style={{color: Colors.buttonTextActive, textAlign:'center' }}>{labels.getLabel(lang,"forgetPinTelChange")}</DefaultText>
                </TouchableOpacity>

                <TouchableOpacity onPress={this.handleNextPressOldMobileNo} disabled={(this.state.telStore!=null && this.state.telStore.length==10) ? false:true}
                  style={[
                      (this.state.telStore!=null && this.state.telStore.length==10) ? LoginStyles.button : LoginStyles.buttonInactive,
                      { flex:1, marginLeft:10 }
                  ]}
                >
                  <DefaultText bold style={{color: Colors.buttonTextActive, textAlign:'center' }}>{labels.getLabel(lang,"confirm")}</DefaultText>
                </TouchableOpacity>
              </View>)
              
            }
      </View>
    );
  }
}


const styles = StyleSheet.create({
  textBox:{
    borderColor:'black',
    borderBottomWidth:1,
    paddingVertical:0,
    marginTop:30,
    fontSize:utils.getFontScale(20),
    textAlign:"center"
  },
  textBoxError:{
    borderColor:Colors.textBoxError,
    //color:Colors.textBoxError,
    borderBottomWidth:1,
    paddingVertical:0,
    marginTop:30,
    fontSize:utils.getFontScale(20),
    textAlign:"center"
  },
  textError:{
    fontSize:12,
    textAlign:'center',
    color:Colors.textBoxError
  }
});
const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus } = auth;
  return { language, lastScreen, loginStatus };
};

export default connect(
  mapStateToProps,
  {

  }
)(ForgetPinTelScreen);
