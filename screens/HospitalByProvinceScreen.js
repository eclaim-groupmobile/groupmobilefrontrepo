import React, { Component } from "react";
import {
  Alert,
  ActivityIndicator,
  AppState,
  Dimensions,
  FlatList,
  Image,
  Linking,
  PixelRatio,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import Constants from 'expo-constants';
import * as SQLite from 'expo-sqlite';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import { Icon, CheckBox, FormLabel } from "react-native-elements";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import CustomUserInactivity from "../components/CustomUserInactivity";
import * as labels from "../constants/label";
import Picker from "../components/picker";
import MainStyles from "../styles/Main.styles";
import HospitalStyles from "../styles/Hospital.style";
import * as utils from "../functions";
import HospitalCard from "../components/HospitalCard";
import NodataList from "../components/NoDataList"
import DefaultText from "../components/DefaultText"

class HospitalByProvinceScreen extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      apiData:[],
      search:"",
      search2:"",
      refreshing: false,
      location: null,
      permission:"false",
      locationEnable:"false",
      stateBangkok: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {navigation.navigate("Hospital")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });

  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start HospitalByProvinceScreen");
    let lang = this.props.language
    this.setState({
      apiData:[],
      search:"",
      search2:"",
      permission:false,
      locationEnable:false,
      stateBangkok:false,
    })
    this.setTitle(this.props.language)
    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({ permission: true });
      Alert.alert(
        labels.getLabel(lang,'error'),
        'This page will not work on android emulator',
        [
          {text: 'OK', onPress: () => this.props.navigation.navigate("Home")},
        ],
        { cancelable: false }
        )
    } else {
      this.getLocationAsync();
    }
  };

  getLocationAsync = async () => {
    let lang = this.props.language
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({ permission: false });
    }else{
      this.setState({ permission: true });
      let locationStatus = await Location.getProviderStatusAsync()
      if(locationStatus.locationServicesEnabled){
        this.setState({ locationEnable: true });
        let location = await Location.getCurrentPositionAsync({accuracy : 6});
        this.setState({lat: location.coords.latitude,long: location.coords.longitude})
      }else{
        this.setState({ locationEnable: false });
        Alert.alert(
          labels.getLabel(lang,'error'),
          labels.getLabel(lang,'locationServicesDisable'),
          [
            {text: labels.getLabel(lang,'cancel'), onPress: () => console.log("Disabled Location Service"), style: 'cancel'},
            {text: labels.getLabel(lang,'ok'), onPress: () => this.getLocationAsync()},
          ],
          { cancelable: false }
        )
      }    
    }
  };

  navigateBack = () => {
    this.props.navigation.navigate("Hospital");
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "hospitalByProvince"),
      lang: lang,
    });
  }

  onSearchHandle(){
    this.picker.toggle();
  }

  onSearchHandle2(){
    this.picker2.toggle();
  }

  onSearchComplete = (selectedValue) => {
    if(selectedValue.constructor !== Array){
      selectedValue = [selectedValue];
    }else{
      selectedValue = selectedValue[0]
    }
    this.onFetch(selectedValue)
  }

  onSearchComplete2 = (selectedValue) => {
    if(selectedValue.constructor !== Array){
      selectedValue = [selectedValue];
    }else{
      selectedValue = selectedValue[0]
    }
    this.onFetchCity(selectedValue)
  }

  onFetch = async selectedValue => {
    let lang = this.props.language
    const db = SQLite.openDatabase('db.MasterData');
    this.setState({refreshing: true});

    if(selectedValue==null || selectedValue==""){
      selectedValue = this.state.search
    }
    this.setState({search:selectedValue})
    let field = lang == 'th' ? 'provinceTH' : 'provinceEN'
    let name = lang == 'th' ? 'nameTH' : 'nameEN'
    let param = selectedValue.trim()
    if(param.includes("กรุงเทพ")) param="กรุงเทพฯ" //กรุงเทพฯ
    this.setState({apiData:null})
    if(param=='Bangkok' || param=='กรุงเทพฯ'){
      this.setState({stateBangkok:true})
    }else{
      this.setState({
        stateBangkok:false,
        refreshing:true,
        search2:"",
      })

      let paramMerge = utils.mergeWording(param)
      let sql = `select * from hospital where ${field} = '${param}' or ${field} = '${paramMerge}';`
      //console.log(sql)
      await db.transaction(tx => {
        tx.executeSql(
          sql,
          null,
          (_, { rows: { _array } }) => {
            this.setState({apiData: _array});
          }
        );
      })

      let sql2 = `select * from clinic where ${field} = '${param}' or ${field} = '${paramMerge}';`
      await db.transaction(tx => {
          tx.executeSql(
            sql2,
            null,
            (_, { rows: { _array } }) => {
              // console.log(_array)
              let tmpArr = this.state.apiData
              tmpArr = tmpArr.concat(_array).sort((a, b) => utils.compareObjectName(a,b,name))
              this.setState({apiData: tmpArr},() => this.setState({refreshing: false}));
            }
          );
      })
    }
  }

  onFetchCity = async selectedValue => {
    let lang = this.props.language
    const db = SQLite.openDatabase('db.MasterData');
    this.setState({refreshing: true});

    if(selectedValue==null || selectedValue==""){
      selectedValue = this.state.search2
    }
    // console.log(selectedValue)
    this.setState({search2:selectedValue})
    let field = lang == 'th' ? 'provinceTH' : 'provinceEN'
    let name = lang == 'th' ? 'nameTH' : 'nameEN'
    let param = this.state.search
    if(param.includes("กรุงเทพ")) param="กรุงเทพฯ" //กรุงเทพฯ
    let paramMerge = utils.mergeWording(param)

    let field2 = lang == 'th' ? 'cityTH' : 'cityEN'
    let param2 = selectedValue.trim()
    let paramMerge2 = utils.mergeWording(param2)

    let sql = `select * from hospital where (${field} = ? or ${field} = ?) and (${field2} = ? or ${field2} = ?);`
    // console.log(sql)
    await db.transaction(tx => {
        tx.executeSql(
          sql,
          [param,paramMerge,param2,paramMerge2],
          (_, { rows: { _array } }) => {
            this.setState({apiData: _array});
          }
        );
    })

    let sql2 = `select * from clinic where (${field} = ? or ${field} = ?) and (${field2} = ? or ${field2} = ?);`
    //console.log(sql2)
    await db.transaction(tx => {
          tx.executeSql(
            sql2,
            [param,paramMerge,param2,paramMerge2],
            (_, { rows: { _array } }) => {
              // console.log(_array)
              let tmpArr = this.state.apiData
              tmpArr = tmpArr.concat(_array).sort((a, b) => utils.compareObjectName(a,b,name))
              this.setState({apiData: tmpArr},() => this.setState({refreshing: false}));
            }
          );
      })
  }

  genBox = (item,index) => {
    return (
      <HospitalCard 
        item={item}
        index={index}
        lat={this.state.lat}
        long={this.state.long}
      />
    )
  }

  render() {
    let lang = this.props.language;
    let refreshing = this.state.refreshing;
    const deviceWidth = Dimensions.get("window").width;
    const deviceHeight = Dimensions.get("window").height;
    return (
      <View style={MainStyles.main}>
        <CustomUserInactivity onAction={() => this.props.navigation.navigate("Home")}>
          <View
            style={[
              MainStyles.container,
              { paddingLeft: 10, paddingRight: 10, paddingTop: 10 }
            ]}
          >
            <TouchableOpacity style={HospitalStyles.searchBox} onPress={this.onSearchHandle.bind(this)}>
              <DefaultText> </DefaultText>
              <View style={{justifyContent:'center'}}><DefaultText style={HospitalStyles.searchBoxContent}>{this.state.search=="" ? labels.getLabel(lang,"Hospital_searchProvince") : this.state.search}</DefaultText></View>
              <View style={{justifyContent:'center'}}><Icon type='entypo' name='chevron-down' color={Colors.bondiBlue} size={20} /></View>
            </TouchableOpacity>

            {this.state.stateBangkok ?
            <View style={{paddingBottom:128}}>
              <TouchableOpacity style={[HospitalStyles.searchBox,{marginHorizontal:10}]} onPress={this.onSearchHandle2.bind(this)}>
                <DefaultText> </DefaultText>
                <View style={{justifyContent:'center'}}><DefaultText style={HospitalStyles.searchBoxContent}>{this.state.search2=="" ? labels.getLabel(lang,"Hospital_searchDistrict") : this.state.search2}</DefaultText></View>
                <View style={{justifyContent:'center'}}><Icon type='entypo' name='chevron-down' color={Colors.bondiBlue} size={20} /></View>
              </TouchableOpacity>

              <FlatList
                data={this.state.apiData}
                renderItem={({item,index}) => this.genBox(item,index)}
                keyExtractor={(item) => (item.id.toString()+item.BType)}
                refreshing={this.state.refreshing}
                onRefresh={this.onFetch2}
                ListEmptyComponent={this.state.search2!="" && !this.state.refreshing && <NodataList/>}
              />
            </View>
            :
            <View style={{paddingBottom:64}}>
              <FlatList
                data={this.state.apiData}
                renderItem={({item,index}) => this.genBox(item,index)}
                keyExtractor={(item) => (item.id.toString()+item.BType)}
                refreshing={this.state.refreshing}
                onRefresh={this.onFetch}
                ListEmptyComponent={this.state.search!="" && !this.state.refreshing && <NodataList/>}
              />
            </View>
            }
            
          </View>

          <Picker
            ref={picker => this.picker = picker}
            deviceWidth={deviceWidth}
            deviceHeight={deviceHeight}
            showDuration={300}
            showMask={true}
            pickerData={this.props.provinceData[lang]}
            selectedValue={this.props.provinceData[lang][0]}
            onPickerDone={(pickedValue) => this.onSearchComplete(pickedValue)}
            pickerBtnText={labels.getLabel(lang,"done")}
            pickerCancelBtnText={labels.getLabel(lang,"cancel")}
          />

          <Picker
            ref={picker => this.picker2 = picker}
            deviceWidth={deviceWidth}
            deviceHeight={deviceHeight}
            showDuration={300}
            showMask={true}
            pickerData={this.props.cityData[lang]}
            selectedValue={this.props.cityData[lang][0]}
            onPickerDone={(pickedValue) => this.onSearchComplete2(pickedValue)}
            pickerBtnText={labels.getLabel(lang,"done")}
            pickerCancelBtnText={labels.getLabel(lang,"cancel")}
          />
        </CustomUserInactivity>
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
    const { language, lastScreen, provinceData, cityData} = auth;
    return { language, lastScreen, provinceData, cityData};
  };
  
export default connect(
  mapStateToProps,
  {
  }
)(HospitalByProvinceScreen);
