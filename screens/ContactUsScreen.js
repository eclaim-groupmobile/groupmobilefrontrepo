import React from "react";
import {
  Alert,
  Image,
  Linking,
  PixelRatio,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import * as labels from "../constants/label";
import CustomUserInactivity from "../components/CustomUserInactivity";
import LoginStyles from "../styles/LoginStyle.style";
import Colors from "../constants/Colors";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import * as utils from "../functions";
import DefaultText from "../components/DefaultText"

class ContactUsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {navigation.navigate("Home")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });


  constructor() {
    super();
    this.state={pinStore:""}
  }

  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start ContactUsScreen");
    this.setTitle(this.props.language)
  };

  navigateBack = () => {
    this.props.navigation.navigate("Home");
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "contactUs"),
      lang: lang,
    });
  }

  render() {
    let lang = this.props.language;
    return (
      <View style={LoginStyles.main}>
        <CustomUserInactivity onAction={() => this.props.navigation.navigate("Home")}>
          <View style={{marginTop:30,}}>
            <View style={[styles.center,]}>
              <Image
                style={[styles.logo,styles.center]}
                source={require('../assets/images/Core-Logo.png')}
              />
            </View>

            {/* <View style={[styles.row,{marginTop:50}]}>
                <DefaultText style={[styles.textAbout]}>{labels.getLabel(lang,"ContactUs_email")}: <DefaultText style={[styles.fontAnchor]} onPress={() => Linking.openURL('mailto:groupinfo@tokiomarinelife.co.th')}>groupinfo@tokiomarinelife.co.th</DefaultText></DefaultText>
            </View>
            
            <View style={[styles.row,{marginTop:30}]}>
                <DefaultText style={[styles.textAbout]}>{labels.getLabel(lang,"ContactUs_tel")}: <DefaultText style={[styles.fontAnchor]} onPress={() => Linking.openURL('tel:026501400')}>02-650-1400</DefaultText> {labels.getLabel(lang,"ContactUs_press")} 2</DefaultText>
            </View>

            <View style={[styles.row,{marginTop:30}]}>
                <DefaultText style={[styles.textAbout]}>{labels.getLabel(lang,"ContactUs_website")}: <DefaultText style={[styles.fontAnchor]} onPress={() => Linking.openURL('https://www.tokiomarine.com')}>www.tokiomarine.com</DefaultText></DefaultText>
            </View> */}
            <View style={{flexDirection:'row'}}>
              <View style={{flex:1}}></View>
              <View style={{flex:6}}>
                <View style={[styles.row,{marginTop:30}]}>
                    <DefaultText bold style={[styles.textAbout]}>{labels.getLabel(lang,"ContactUs_1")}</DefaultText>
                </View>

                <View style={[styles.row,{marginTop:5}]}>
                    <DefaultText style={[styles.textAbout]}> {labels.getLabel(lang,"ContactUs_2_1")} <DefaultText style={[styles.fontAnchor]} onPress={() => Linking.openURL('tel:026501400')}>0-2650-1400</DefaultText> {labels.getLabel(lang,"ContactUs_2_3")}</DefaultText>
                </View>

                <View style={[styles.row,{marginTop:5}]}>
                    <DefaultText style={[styles.textAbout]}> {labels.getLabel(lang,"ContactUs_3_1")} <DefaultText style={[styles.fontAnchor]} onPress={() => Linking.openURL('tel:026194080')}>0-2619-4080</DefaultText></DefaultText>
                </View>

                <View style={[styles.row,{marginTop:5}]}>
                    <DefaultText style={[styles.textAbout]}>{labels.getLabel(lang,"ContactUs_4")}</DefaultText>
                </View>

                <View style={[styles.row,{marginTop:5}]}>
                    <DefaultText style={[styles.textAbout]}>{labels.getLabel(lang,"ContactUs_5")}</DefaultText>
                </View>

                <View style={[styles.row,{marginTop:20}]}>
                    <DefaultText bold style={[styles.textAbout]}>{labels.getLabel(lang,"ContactUs_6")}</DefaultText>
                </View>

                <View style={[styles.row,{marginTop:5}]}>
                    <DefaultText style={[styles.textAbout]}> {labels.getLabel(lang,"ContactUs_7_1")} <DefaultText style={[styles.fontAnchor]} onPress={() => Linking.openURL('tel:026194072')}>0-2619-4072</DefaultText></DefaultText>
                </View>

                <View style={[styles.row,{marginTop:5}]}>
                    <DefaultText style={[styles.textAbout]}>{labels.getLabel(lang,"ContactUs_8")}</DefaultText>
                </View>

                <View style={[styles.row,{marginTop:5}]}>
                    <DefaultText style={[styles.textAbout]}>{labels.getLabel(lang,"ContactUs_9")}</DefaultText>
                </View>

                <View style={[styles.row,{marginTop:20}]}>
                    <DefaultText bold style={[styles.textAbout]}>{labels.getLabel(lang,"ContactUs_10_1")} <DefaultText style={[styles.fontAnchor]} onPress={() => Linking.openURL('mailto:groupinfo@tokiomarinelife.co.th')}>groupinfo@tokiomarinelife.co.th</DefaultText></DefaultText>
                </View>

                <View style={[styles.row,{marginTop:10}]}>
                    <DefaultText bold style={[styles.textAbout]}>{labels.getLabel(lang,"ContactUs_11_1")} <DefaultText style={[styles.fontAnchor]} onPress={() => Linking.openURL('https://www.tokiomarine.com')}>www.tokiomarine.com</DefaultText></DefaultText>
                </View>
              </View>
              <View style={{flex:1}}></View>
            </View>
          </View>
        </CustomUserInactivity>
      </View>
    );
  }

  
}

const styles = StyleSheet.create({
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    // alignItems: "center",
    // justifyContent: 'center',
    flexDirection: 'row',
  },
  fontAnchor: {
    textDecorationLine: 'underline',
    color: Colors.bondiBlue
  },
  logo: {
    width: 120,
    height: 120,
  },
  textAbout:{
    alignItems: "center",
    fontSize: 14,
    color: Colors.bondiBlue
  },
});

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen } = auth;
  return { language, lastScreen };
};

export default connect(
  mapStateToProps,
  {
  }
)(ContactUsScreen);
