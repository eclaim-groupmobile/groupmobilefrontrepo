import React, { Component } from "react";
import {
  Alert,
  Image,
  PixelRatio,
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import LoginStyles from "../styles/LoginStyle.style";
import * as labels from "../constants/label";
import PinView from "../components/PinView";
import * as utils from "../functions";
import DefaultText from "../components/DefaultText"
import * as LocalAuthentication from 'expo-local-authentication';

class ForgetPinSetUpScreen extends Component {
  constructor(props) {
    super(props);
    this.onComplete = this.onComplete.bind(this)
    this.state = { 
      successTextLabel: "",
      errorTextLabel: "",
      pinLabel: "",
      pin1st: "",
      pin2nd: "",
      localAuthentication: false,
    };
  }

  static navigationOptions = {
    header: null,
  };

  _checkLocalAuthentication = async () => {
    let result = await LocalAuthentication.hasHardwareAsync();
    let checkFingerprint
    if(Platform.OS === "ios"){
      checkFingerprint = await LocalAuthentication.supportedAuthenticationTypesAsync()
      if(checkFingerprint.indexOf(1)==-1){
        result = false
      }
    }
    if (result) {
		  this.setState({localAuthentication:true})
    }else{
      this.setState({localAuthentication:false})
    }
  };
  
  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
    
  }
  load = () => {
    utils.log("Start ForgetPinSetUpScreen");
    this._checkLocalAuthentication()
    this.setState({ pinLabel : "enterPin"})
    this.setState({ successTextLabel : ""})
    this.setState({ errorTextLabel : ""})
    let passport = this.props.navigation.getParam('passport', '');
    this.setState({passport})
  };

  navigateBack = () => {
    utils.confirmBack(this.props.language,this.props.navigation,"Home")
  };

  getData = async key => {
    let value = await utils.retrieveData(key)
    this.setState({[key]:value})
  }

  onComplete = (inputtedPin, clear) => {
    let lang = this.props.language
    if(this.state.pinLabel == "enterPin"){
      console.log("ForgetPinSetUpScreen 1st pin")
      this.setState({
        pin1st:inputtedPin,
        pinLabel:"reEnterPin",
        errorTextLabel:"",
      })
      clear();
      Alert.alert(labels.getLabel(lang,"confirmPin"))
    }else{
      console.log("ForgetPinSetUpScreen 2st pin")
      this.setState({
        pin2st:inputtedPin,
        pinLabel:"enterPin",
      })
      if (inputtedPin !== this.state.pin1st) {
        this.setState({
          pin1st:"",
          pin2nd:"",
          errorTextLabel:labels.getError(lang,"pinNotMatch")
        })
        clear();
      } else {
        //utils.setPin(inputtedPin)
        // clear();
        this.setState({
          errorTextLabel:"",
          successTextLabel:labels.getLabel(lang,"pinSuccessSetup")
        })
        if(this.state.localAuthentication){
          labelTouch = labels.getLabel(lang,"touchId")
          Alert.alert(
            '',
            labelTouch,
            [
              {text: labels.getLabel(lang,"cancel"), onPress: () => this.props.navigation.navigate("ForgetPinTel",{pin:inputtedPin,localAuthentication:"0",passport:this.state.passport}), style: 'cancel'},
              {text: labels.getLabel(lang,"enable"), onPress: () => this.setLocalAuthenticationStore(inputtedPin)},
            ],
            { cancelable: false }
          )
        }else{
          this.props.navigation.navigate("ForgetPinTel",{pin:inputtedPin,localAuthentication:"0",passport:this.state.passport})
        }
      }
    }
  }

  goToFirstPin = () => {
    this.setState({
      pinLabel:"enterPin",
      pin1st:"",
      pin2nd:"",
    })
    this.pinView.clear()
  }

  setLocalAuthenticationStore = (inputtedPin) => {
    //utils.setLocalAuthenticationStore("1")
    this.props.navigation.navigate("ForgetPinTel",{pin:inputtedPin,localAuthentication:"1",passport:this.state.passport})
    // this.props.navigation.navigate("LoginPin")
  }

  render() {
    let lang = this.props.language
    return (
      <View style={LoginStyles.main}>
        <View style={[LoginStyles.paddingStatus,{backgroundColor:"white"}]}>
          <View style={{marginTop:60,marginBottom:20,alignItems:'center'}}>
            <DefaultText style={{color: Colors.bondiBlue,fontSize:18}}>{labels.getLabel(lang,this.state.pinLabel)}</DefaultText>
          </View>
          <View style={{backgroundColor:'white',paddingTop:5}}>
            <PinView
              ref={pinView => this.pinView = pinView}
              onComplete={this.onComplete.bind(this)}
              inputBgOpacity={1}
              pinLength={6}
              buttonBgColor={'white'}
              inputBgColor={'white'}
              inputActiveBgColor={Colors.bondiBlue}
              buttonTextColor={'black'}
              successfulText={this.state.successTextLabel}
              errorText={this.state.errorTextLabel}
              deleteText={"DELICON"}
            />
          </View>
        </View>

        {/* back button */}
        {
          this.state.pinLabel != "enterPin" && 
          <TouchableOpacity style={{position:'absolute',left:10,top:40}} onPress={() => this.goToFirstPin()}>
            <Image
              style={{ width: 22, height: 22 }}
              source={require("../assets/icon/Back-button-alt.png")}
            />
          </TouchableOpacity>
        }
        
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus } = auth;
  return { language, lastScreen, loginStatus };
};

export default connect(
  mapStateToProps,
  {

  }
)(ForgetPinSetUpScreen);
