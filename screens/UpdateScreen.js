import React from "react";
import {
  Alert,
  BackHandler,
  Dimensions,
  Image,
  Linking,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View
} from "react-native";
import Constants from 'expo-constants';
import { connect } from "react-redux";
import { languageChanged, lastScreenChanged, logOut } from "../actions";
import * as labels from "../constants/label";
import Layout from "../constants/Layout";
import Colors from "../constants/Colors";
import * as utils from "../functions";
import DefaultText from "../components/DefaultText"
import LoginStyles from "../styles/LoginStyle.style";
import { Icon } from "react-native-elements";

const width = Layout.window.width; //Dimensions.get('window').width

class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
  }

  openStore = () => {
    BackHandler.exitApp();
    if (Constants.platform.android) {
      Linking.openURL(Constants.manifest.android.playStoreUrl)
    } else {
      Linking.openURL(Constants.manifest.ios.appStoreUrl)
    }
  }

  render() {
    let lang = this.props.language;
    return (
      <View style={styles.container}>
          <View style={{paddingHorizontal:20,alignItems: 'center'}}>
            <Icon
                name='update'
                type='material'
                color='#727272'
                size={80}
            />
            <DefaultText style={{alignItems: 'center'}}>{labels.getLabel(lang,"UpdateText")}{/*There is an updated version available on the {appString}. Click Update to open {appString} for update*/}</DefaultText>
          </View>

          <View style={{position:"absolute",bottom:30,left:0,right:0, justifyContent: 'center', alignItems: 'center'}}>
            <TouchableOpacity onPress={this.openStore}
                style={[
                  LoginStyles.button,
                    { flex:1, width:width-20 }
                ]}
            >
              <DefaultText bold style={{color: Colors.buttonTextActive,fontSize:18 }}>{labels.getLabel(lang,"Update")}</DefaultText>
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
});

const mapStateToProps = ({ auth }) => {
  const { language } = auth;
  return { language };
};

export default connect(
  mapStateToProps,
  {
    languageChanged,
  }
)(HomeScreen);
