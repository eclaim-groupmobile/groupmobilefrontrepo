import React from "react";
import {
  BackHandler,
  Dimensions,
  Image,
  Linking,
  PixelRatio,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Modal,
  TouchableHighlight
} from "react-native";
import Constants from "expo-constants";
import { connect } from "react-redux";
import { languageChanged, lastScreenChanged, logOut } from "../actions";
import * as labels from "../constants/label";
import Layout from "../constants/Layout";
import Colors from "../constants/Colors";
import CustomUserInactivity from "../components/CustomUserInactivity";
import * as utils from "../functions";
import DefaultText from "../components/DefaultText";

const width = Layout.window.width; //Dimensions.get('window').width
const fontScale =
  Platform.OS === "ios" ? parseInt(width / 32) : parseInt(width / 35);
const imageSizeFull = parseInt(width / 3) - 5;
const imageSize = parseInt(((width / 3) * 60) / 100);
const imageSize2 = parseInt(((width / 3) * 65) / 100);

class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  // state = {
  //   show: true
  // };

  constructor(props) {
    super(props);

    var today = new Date(
      new Date().getFullYear(),
      new Date().getMonth(),
      new Date().getDate(),
      0,
      0,
      0,
      0
    );
    var target = new Date(2020, 4, 28, 0, 0, 0, 0);

    console.log(today);
    console.log(target);

    if (today > target) {
      this.state = {
        show: false
      };
    } else {
      this.state = {
        show: true
      };
    }
  }

  componentWillMount() {
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackButton.bind(this)
    );
  }

  componentDidMount() {
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    this.onScreenChanged();
    utils.log("Start HomeScreen");
  };

  onScreenChanged() {
    this.props.lastScreenChanged("Home");
  }

  handleBackButton = () => {
    BackHandler.exitApp();
    return true;
  };

  openPrivacy = () => {
    let lang = this.props.language;

    if (lang == "th") {
      Linking.openURL(
        "https://www.tokiomarine.com/th/th-life/global/privacy-policy.html"
      );
    } else {
      Linking.openURL(
        "https://www.tokiomarine.com/th/en-life/global/privacy-policy.html"
      );
    }
  };

  handleCloseModal() {
    this.setState({ show: false });
  }

  render() {
    let lang = this.props.language;
    return (
      <View style={styles.container}>
        <CustomUserInactivity
          onAction={() => this.props.navigation.navigate("Home")}
        >
          <Modal
            animationType={"slide"}
            transparent={true}
            visible={this.state.show}
          >
            <View
              style={{
                backgroundColor: "#000000aa",
                flex: 1,
                flexDirection: "column",
                justifyContent: "center"
              }}
            >
              <View
                style={{
                  backgroundColor: "#fff",
                  margin: 40,
                  padding: 30,
                  borderRadius: 10,
                  height: 600
                }}
              >
                <Text
                  style={{
                    color: "#0096a9",
                    fontSize: 20,
                    fontWeight: "bold",
                    marginBottom: 10
                  }}
                >
                  {labels.getLabel(lang, "Settings_privacy")}
                </Text>
                <TouchableHighlight onPress={() => this.openPrivacy()}>
                  <Image
                    style={{
                      width: "100%",
                      height: 400,
                      resizeMode: "stretch"
                    }}
                    source={require("../assets/images/pdp.png")}
                  />
                </TouchableHighlight>

                <TouchableHighlight
                  style={styles.touchableButton}
                  onPress={() => {
                    this.handleCloseModal();
                  }}
                >
                  <Text style={styles.touchableText}>
                    {labels.getLabel(lang, "ok")}
                  </Text>
                </TouchableHighlight>
              </View>
            </View>
          </Modal>

          {/* header */}
          <View
            style={{
              height: 100,
              backgroundColor: Colors.tile0
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                borderBottomWidth: 5,
                borderColor: Colors.bondiBlue
              }}
            >
              <View style={{ flex: 2, flexDirection: "row" }}>
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Image
                    //style={{ width: imageSize/87*97, height: imageSize }} 95*95
                    style={{ width: 80, height: 80 }}
                    source={require("../assets/images/Core-Logo-mini.png")}
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                ></View>
              </View>
              <View style={{ flex: 1 }}>
                <View
                  style={{
                    flex: 1,
                    alignItems: "flex-end",
                    justifyContent: "flex-end",
                    marginRight: 10
                  }}
                >
                  <Image
                    //style={{ width: imageSize2/14*18, height: imageSize2 }}
                    style={{ width: (95 / 14) * 18, height: 95 }}
                    source={require("../assets/images/TMLTH.png")}
                  />
                </View>
              </View>
            </View>
          </View>

          {/* body */}
          <View style={{ flex: 1, backgroundColor: "white" }}>
            {/* row1 */}
            <View style={{ flex: 1 }}>
              <TouchableOpacity
                style={{
                  backgroundColor: Colors.tile1,
                  flex: 1
                }}
                onPress={() => this.props.navigation.navigate("HowToClaim")}
                activeOpacity={1}
              >
                <View style={{ flex: 1, justifyContent: "center" }}>
                  <View style={{ flexDirection: "row" }}>
                    <View style={{ flex: 8, paddingLeft: 20 }} />
                    <View style={{ flex: 5, alignItems: "center" }}>
                      <DefaultText bold style={[styles.fontHeader]}>
                        {labels.getLabel(
                          this.props.language,
                          "Main_howToClaimHeader"
                        )}
                      </DefaultText>
                    </View>
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <View
                      style={{
                        flex: 8,
                        justifyContent: "center",
                        paddingLeft: 20
                      }}
                    >
                      <DefaultText
                        style={[styles.fontDetail, { marginBottom: 5 }]}
                      >
                        {labels.getLabel(
                          this.props.language,
                          "Main_howToClaimDetail1"
                        )}
                        {"\n"}
                        {labels.getLabel(
                          this.props.language,
                          "Main_howToClaimDetail2"
                        )}
                        {"\n"}
                        {labels.getLabel(
                          this.props.language,
                          "Main_howToClaimDetail3"
                        )}
                      </DefaultText>
                    </View>
                    <View style={{ flex: 5, alignItems: "center" }}>
                      <Image
                        style={{ width: imageSize, height: imageSize }}
                        source={require("../assets/images/tile-1.png")}
                      />
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            {/* row2 */}
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={{ flex: 2 }}>
                <TouchableOpacity
                  style={{
                    backgroundColor: Colors.tile2,
                    flex: 1
                  }}
                  onPress={() => this.props.navigation.navigate("Hospital")}
                  activeOpacity={1}
                >
                  <View style={{ flex: 1, justifyContent: "center" }}>
                    <View style={{ marginLeft: 20, flexDirection: "row" }}>
                      <DefaultText bold style={styles.fontHeader}>
                        {labels.getLabel(
                          this.props.language,
                          "Main_networkHospitalHeader"
                        )}
                      </DefaultText>
                    </View>

                    <View style={{ flexDirection: "row" }}>
                      <View style={{ flex: 1, alignItems: "center" }}>
                        <Image
                          style={{ width: imageSize, height: imageSize }}
                          source={require("../assets/images/tile-2.png")}
                        />
                      </View>
                      <View
                        style={{
                          flex: 1,
                          paddingRight: 20,
                          justifyContent: "center"
                        }}
                      >
                        <DefaultText
                          style={[styles.fontDetail, { textAlign: "right" }]}
                        >
                          {labels.getLabel(
                            this.props.language,
                            "Main_networkHospitalDetail1"
                          )}
                          {"\n"}
                          {labels.getLabel(
                            this.props.language,
                            "Main_networkHospitalDetail2"
                          )}
                          {"\n"}
                          {labels.getLabel(
                            this.props.language,
                            "Main_networkHospitalDetail3"
                          )}
                        </DefaultText>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={{ flex: 1 }}>
                <TouchableOpacity
                  style={{
                    backgroundColor: Colors.tile3,
                    flex: 1,
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                  onPress={() => this.props.navigation.navigate("HealthTips")}
                  activeOpacity={1}
                >
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <DefaultText
                      bold
                      style={[styles.fontHeader, { color: "black" }]}
                    >
                      {labels.getLabel(this.props.language, "Main_healthTips")}
                    </DefaultText>
                    <Image
                      style={{ width: imageSize, height: imageSize }}
                      source={require("../assets/images/tile-3.png")}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            </View>

            {/* row3 */}
            <View
              style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
            >
              <View style={{ flex: 1 }}>
                <TouchableOpacity
                  style={{
                    backgroundColor: Colors.tile4,
                    flex: 1,
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                  onPress={() => this.props.navigation.navigate("ContactUs")}
                  activeOpacity={1}
                >
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <DefaultText
                      bold
                      style={[styles.fontHeader, { color: "black" }]}
                    >
                      {labels.getLabel(this.props.language, "Main_contactUs")}
                    </DefaultText>
                    <Image
                      style={{ width: imageSize, height: imageSize }}
                      source={require("../assets/images/tile-4.png")}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{ flex: 2 }}>
                <TouchableOpacity
                  style={{
                    backgroundColor: Colors.tile5,
                    flex: 1
                  }}
                  onPress={() => this.props.navigation.navigate("News")}
                  activeOpacity={1}
                >
                  <View style={{ flex: 1, justifyContent: "center" }}>
                    <View style={{ marginLeft: 20, flexDirection: "row" }}>
                      <View style={{ flex: 11 }} />
                      <View style={{ flex: 9, alignItems: "center" }}>
                        <DefaultText
                          bold
                          style={[styles.fontHeader, { color: "black" }]}
                        >
                          {labels.getLabel(
                            this.props.language,
                            "Main_newsHeader"
                          )}
                        </DefaultText>
                      </View>
                    </View>

                    <View style={{ flexDirection: "row" }}>
                      <View
                        style={{
                          flex: 11,
                          paddingLeft: 20,
                          justifyContent: "center"
                        }}
                      >
                        <DefaultText
                          style={[
                            styles.fontDetail,
                            { marginBottom: 10, color: "black" }
                          ]}
                        >
                          {labels.getLabel(
                            this.props.language,
                            "Main_newsDetail1"
                          )}
                          {"\n"}
                          {labels.getLabel(
                            this.props.language,
                            "Main_newsDetail2"
                          )}
                        </DefaultText>
                      </View>
                      <View style={{ flex: 9, alignItems: "center" }}>
                        <Image
                          style={{ width: imageSize, height: imageSize }}
                          source={require("../assets/images/tile-5.png")}
                        />
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </View>

            {/* row4 */}
            <View style={{ flex: 1 }}>
              <TouchableOpacity
                style={{
                  backgroundColor: Colors.tile6,
                  flex: 1
                }}
                onPress={() => this.props.navigation.navigate("Links")}
                activeOpacity={1}
              >
                <View style={{ flex: 1, justifyContent: "center" }}>
                  <View style={{ flexDirection: "row" }}>
                    <View style={{ flex: 1, alignItems: "center" }}>
                      <DefaultText
                        bold
                        style={[styles.fontHeader, { color: "black" }]}
                      >
                        {labels.getLabel(
                          this.props.language,
                          "Main_linksHeader"
                        )}
                      </DefaultText>
                    </View>
                    <View style={{ flex: 2, paddingLeft: 20 }} />
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <View style={{ flex: 1, alignItems: "center" }}>
                      <Image
                        style={{ width: imageSize, height: imageSize }}
                        source={require("../assets/images/tile-6.png")}
                      />
                    </View>
                    <View
                      style={{
                        flex: 2,
                        justifyContent: "center",
                        paddingLeft: 20
                      }}
                    >
                      <DefaultText
                        style={[styles.fontDetail, { color: "black" }]}
                      >
                        {labels.getLabel(
                          this.props.language,
                          "Main_linksDetail1"
                        )}
                        {"\n"}
                        {labels.getLabel(
                          this.props.language,
                          "Main_linksDetail2"
                        )}
                      </DefaultText>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </CustomUserInactivity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.tile0,
    paddingTop: (Constants.statusBarHeight = 0 ? 30 : Constants.statusBarHeight)
  },
  fontDetail: {
    fontSize: fontScale,
    color: "white"
  },
  fontHeader: {
    color: "white",
    //fontWeight: "bold",
    fontSize: fontScale * 1.2,
    marginBottom: 5
  },
  touchableText: {
    color: "#d2d711",
    fontSize: 20,
    textAlign: "center"
  },
  touchableButton: {
    width: "100%",
    padding: 10,
    backgroundColor: "#0096a9",
    marginBottom: 10,
    marginTop: 30
  }
});

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus } = auth;
  return { language, lastScreen, loginStatus };
};

export default connect(
  mapStateToProps,
  {
    languageChanged,
    lastScreenChanged,
    logOut
  }
)(HomeScreen);
