import React, { Component } from "react";
import {
  Alert,
  Image,
  Dimensions,
  Keyboard,
  KeyboardAvoidingView,
  PixelRatio,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Header } from "react-navigation";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import LoginStyles from "../styles/LoginStyle.style";
import Picker from "../components/picker";
import * as labels from "../constants/label";
import { TextField } from "react-native-materialui-textfield";
import * as utils from "../functions";
import DefaultText from "../components/DefaultText"

const now = new Date()

class ForgetPinScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      policyNumber: "G",
      memberNo: "",
      memberNo2: "",
      passport: "",
      dob: "",
      dateDefault: this.props.language == 'th' ? ["2447","มกราคม","01"] : ["1904","January","01"],
      dobFocus: false,
      errors: {},
    }

    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitPolicyNumber = this.onSubmitPolicyNumber.bind(this);
    this.onSubmitMemberNo = this.onSubmitMemberNo.bind(this);
    this.onSubmitMemberNo2 = this.onSubmitMemberNo2.bind(this);
    this.onSubmitPassport = this.onSubmitPassport.bind(this);
    this.onSubmitDob = this.onSubmitDob.bind(this);

    this.policyNumberRef = this.updateRef.bind(this, 'policyNumber');
    this.memberNoRef = this.updateRef.bind(this, 'memberNo');
    this.memberNo2Ref = this.updateRef.bind(this, 'memberNo2');
    this.passportRef = this.updateRef.bind(this, 'passport');
    this.dobRef = this.updateRef.bind(this, 'dob');
  }
  
  policyNumber = ''
  memberNo = ''

  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {navigation.navigate("LoginPin")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });
  
  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start ForgetPinScreen");
    this.changeDateDefault(now.getFullYear(),now.getMonth(),now.getDate(),true)
    this.setTitle(this.props.language)
    this.getData("policyNumberStore")
    this.getData("memberNoStore")
    this.setState({
      policyNumber: "G",
      memberNo: "",
      memberNo2: "",
      passport: "",
      dob: "",
      dobFocus: false,
      errors: {},
    })
  };

  navigateBack = () => {
    // if(this.picker.isPickerShow()){
    //   this.onCalendarHandle();
    // }else{
      this.props.navigation.navigate("LoginPin");
    // }
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "forgetPinConfirmUser"),
      lang: lang,
    });
  }

  getData = async key => {
    let value = await utils.retrieveData(key)
    this.setState({[key]:value})
  }

  onCalendarHandle(){
    Keyboard.dismiss();
    this.setState({KeyboardAvoidingView:true})
    this.picker.toggle();
    if(this.state.dobFocus) {
      this.setState({dobFocus:false})
    }else {
      this.setState({dobFocus:true})
    }
    this.state = { dob: "" };
  }

  handleNextPress = async () => {
    let lang = this.props.language
    let errorFlag = false
    let errors = {};

    let policyNumberError = labels.getError(this.props.language,utils.validatePolicyNumber(this.state.policyNumber,this.state.policyNumberStore));
    if(policyNumberError!=""){
      errorFlag = true
      errors['policyNumber'] = policyNumberError
    }

    let memberNoError = labels.getError(this.props.language,utils.validateMemberNo(this.state.memberNo  + "-" + this.state.memberNo2,this.state.memberNoStore));
    if(memberNoError!=""){
      errorFlag = true
      errors['memberNo'] = memberNoError
      errors['memberNo2'] = ' '
    }

    let passportError = labels.getError(this.props.language,utils.validatePassport(this.state.passport));
    if(passportError!=""){
      errorFlag = true
      errors['passport'] = passportError
    }

    let dobError = labels.getError(this.props.language,utils.validateDate(this.state.dob));
    if(dobError!=""){
      errorFlag = true
      errors['dob'] = dobError
    }
    
    this.setState({ errors });
    if(errorFlag){
      
    }else{
      this.setState({complete:false})
      let result = ""
      let memberNo = this.state.memberNo + "-" + this.state.memberNo2
      let xmlBody = utils.ChkMemberForgetPINBody(this.state.policyNumber,memberNo,this.state.passport,utils.getDateServiceFormat(lang,this.state.dob));
      console.log(`Request body: ${xmlBody}`);
      await fetch(utils.urlUserService, utils.genRequest(utils.ChkMemberForgetPINSOAPAction,xmlBody) )
        .then(response => response.text())
        .then(response => {
          result = utils.getResult(response)
          let errorCode = result["ChkMemberForgetPINResponse"][0]["ChkMemberForgetPINResult"][0]["a:errorCode"][0].toString()
          let errorDesc = ""
          if(lang=="th"){
            errorDesc = result["ChkMemberForgetPINResponse"][0]["ChkMemberForgetPINResult"][0]["a:errorDescTH"][0].toString()
          }else{
            errorDesc = result["ChkMemberForgetPINResponse"][0]["ChkMemberForgetPINResult"][0]["a:errorDescEN"][0].toString()
          }
          if(Number(errorCode)==0){
            this.props.navigation.navigate("ForgetPinSetup",{
              policyNumber: this.state.policyNumber,
              memberNo: memberNo,
              passport: this.state.passport,
              dob: this.state.dob,
            })
          }else{
            Alert.alert(labels.getLabel(lang,"error"),errorDesc)
          }
        })
        .catch(err => {
          console.log("fetch", err);
          Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
        });
      this.setState({complete:true})
      
    }
  }

  handleCheckCompleteField = (text,name) => {
    let count = 0

    if(name!='policyNumber'){
      let policyNumber = this.state.policyNumber
      if(policyNumber==null) policyNumber = ""
      if(policyNumber.length>0) count = count + 1
    }

    if(name!='memberNo'){
      let memberNo = this.state.memberNo
      if(memberNo==null) memberNo = ""
      if(memberNo.length>0) count = count + 1
    }

    if(name!='memberNo2'){
      let memberNo2 = this.state.memberNo2
      if(memberNo2==null) memberNo2 = ""
      if(memberNo2.length>0) count = count + 1
    }

    if(name!='passport'){
      let passport = this.state.passport
      if(passport==null) passport = ""
      if(passport.length>0) count = count + 1
    }

    if(name!='dob'){
      let dob = this.state.dob
      if(dob==null) dob = ""
      if(dob.length>0) count = count + 1
    }

    if(text==null) text = ""
    if(text.length>0) count = count + 1
    
    if(count>=5) this.setState({complete: true})
    else this.setState({complete:false})
  }

  changeDateDefault = (year,month,day,flagYear) => {
    let dateDefault = []
    if(this.props.language=='th')
      if(flagYear){
        dateDefault = [(year+543)+"", utils.monthNameTh[month]+"", ("0" + day + "").slice(-2)]
      }else{
        dateDefault = [(year)+"", utils.monthNameTh[month]+"", ("0" + day + "").slice(-2)]
      }
      
    else
      dateDefault = [year+"", utils.monthNameEn[month]+"", ("0" +  day + "").slice(-2)]
    this.setState({dateDefault})
  }

  dobChange = (text) => {
    text = text + ""
    let value = utils.changeDateFormat(this.props.language,text)
    let year = text.substring(0, 4);
    let month = text.substring(5, text.length - 3);
    let monthIndex = utils.getMonthIndex(this.props.language,month)
    let day = text.substring(text.length - 2);
    
    this.setState({dob: value},this.handleCheckCompleteField(utils.changeDateFormat(this.props.language,text),'dob'))
    this.setState({dobFocus:false})
    this.changeDateDefault(year,monthIndex,day,false)
    this.onSubmitDob()
  }

  onChangeText(text) {
    let errors = this.state.errors;
    ['policyNumber', 'memberNo2', 'memberNo', 'passport', 'dob']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          if(name=='policyNumber' && (text == null || text == "")) text = "G"
          // if(name=='memberNo') text = utils.formatMemberNo2(text)
          if(name=='memberNo'){
            errors['memberNo2'] = ''
          }
          if(name=='memberNo2'){
            errors['memberNo'] = ''
          }
          errors[name] = ''
          this.setState({ [name]: text, errors });
          this.handleCheckCompleteField(text,name)
          if(name=='memberNo'){
            if(text.length==5) this.memberNo2.focus();
          }
        }
      });
  }

  onSubmitPolicyNumber() {
    this.memberNo.focus();
  }

  onSubmitMemberNo() {
    this.memberNo2.focus();
  }

  onSubmitMemberNo2() {
    this.passport.focus();
  }

  onSubmitPassport() {
    this.passport.blur();
    this.onCalendarHandle();
  }

  onSubmitDob() {
    this.dob.blur();
  }
  
  updateRef(name, ref) {
    this[name] = ref;
  }

  render() {
    // const _osBehavior = Platform.OS === "ios" ? "position" : null;
    let { errors = {}, ...data } = this.state;
    let lang = this.props.language;
    const deviceWidth = Dimensions.get("window").width;
    const deviceHeight = Dimensions.get("window").height;

    return (
      
      <View style={LoginStyles.main}>
      <ScrollView keyboardShouldPersistTaps='always'>
      <KeyboardAvoidingView style={[LoginStyles.container,{marginBottom:26,marginTop:20}]} behavior="position" enabled={this.state.KeyboardAvoidingView} keyboardVerticalOffset={Header.HEIGHT + 20}>
          <TextField
            ref={this.policyNumberRef}
            value={data.policyNumber}
            maxLength={8}
            inputContainerPadding={2}
            lineWidth={2}
            autoCorrect={false}
            enablesReturnKeyAutomatically={true}
            onChangeText={this.onChangeText}
            onSubmitEditing={this.onSubmitPolicyNumber}
            returnKeyType='done'
            label={labels.getLabel(lang,"policyNumber")}
            error={errors.policyNumber}
            tintColor={Colors.bondiBlue}
            baseColor={Colors.bondiBlue}
            placeholder={' '}
            onFocus={()=>this.setState({KeyboardAvoidingView:false})}
            helpersNumberOfLines={1.2}
            style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
            labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            labelFontSize={utils.getFontScale(14)}
            containerStyle={{marginTop:10}}
            labelPadding={20}
          />

          <View style={{flexDirection:'row'}}>
            <TextField
              ref={this.memberNoRef}
              value={data.memberNo}
              maxLength={5}
              inputContainerPadding={2}
              lineWidth={2}
              keyboardType={'numeric'}
              autoCorrect={false}
              enablesReturnKeyAutomatically={true}
              onChangeText={this.onChangeText}
              onSubmitEditing={this.onSubmitMemberNo}
              returnKeyType='done'
              label={labels.getLabel(lang,"memberNo")}
              error={errors.memberNo}
              tintColor={Colors.bondiBlue}
              baseColor={Colors.bondiBlue}
              placeholder={' '}
              // onEndEditing={(e) => {
              //   this.setState({memberNo: utils.formatMemberNo(this.state.memberNo)});
              // }}
              // onBlur={()=>this.setState({memberNo: utils.formatMemberNo(this.state.memberNo)})}
              onFocus={()=>this.setState({KeyboardAvoidingView:false})}
              helpersNumberOfLines={1.2}
              style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
              labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              labelFontSize={utils.getFontScale(14)}
              containerStyle={{flex:10,marginTop:10}}
              labelPadding={20}
            />
            <TextField
              value={" "} //slash member no
              maxLength={1}
              inputContainerPadding={2}
              lineWidth={0}
              enablesReturnKeyAutomatically={true}
              label={' '}
              tintColor={Colors.bondiBlue}
              baseColor={Colors.white}
              editable={false}
              helpersNumberOfLines={1.2}
              style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),textAlign:'center',color:Colors.bondiBlue}}
              labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              labelFontSize={utils.getFontScale(14)}
              containerStyle={{flex:1,marginTop:10}}
              labelPadding={20}
            />
            <TextField
              ref={this.memberNo2Ref}
              value={data.memberNo2}
              maxLength={2}
              inputContainerPadding={2}
              lineWidth={2}
              keyboardType={'numeric'}
              autoCorrect={false}
              enablesReturnKeyAutomatically={true}
              onChangeText={this.onChangeText}
              onSubmitEditing={this.onSubmitMemberNo2}
              returnKeyType='done'
              label={' '}
              error={errors.memberNo2}
              tintColor={Colors.bondiBlue}
              baseColor={Colors.bondiBlue}
              placeholder={' '}
              helpersNumberOfLines={1.2}
              style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
              labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              labelFontSize={utils.getFontScale(14)}
              containerStyle={{flex:2,marginTop:10}}
              labelPadding={20}
            />
          </View>

          <TextField
            ref={this.passportRef}
            value={data.passport}
            maxLength={30}
            inputContainerPadding={2}
            lineWidth={2}
            autoCorrect={false}
            enablesReturnKeyAutomatically={true}
            onChangeText={this.onChangeText}
            onSubmitEditing={this.onSubmitPassport}
            returnKeyType='done'
            label={labels.getLabel(lang,"passport")}
            error={errors.passport}
            tintColor={Colors.bondiBlue}
            baseColor={Colors.bondiBlue}
            placeholder={' '}
            onFocus={()=>this.setState({KeyboardAvoidingView:true})}
            helpersNumberOfLines={2.2}
            style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
            labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            labelFontSize={utils.getFontScale(13)}
            containerStyle={{marginTop:10}}
            labelPadding={20}
          />
          
          <TouchableOpacity style={{marginTop: 5}} onPress={this.onCalendarHandle.bind(this)}>
            <TextField
              ref={this.dobRef}
              value={data.dob}
              inputContainerPadding={2}
              lineWidth={2}
              disabledLineWidth={2}
              disabledLineType={'solid'}
              autoCorrect={false}
              enablesReturnKeyAutomatically={true}
              //onChangeText={this.onChangeText}
              returnKeyType='done'
              label={labels.getLabel(lang,"dob")}
              error={errors.dob}
              tintColor={Colors.bondiBlue}
              baseColor={this.state.dobFocus ? Colors.bondiBlue : Colors.bondiBlue}
              placeholder={' '}
              editable={false}
              helpersNumberOfLines={1.2}
              style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
              labelTextStyle={{color:this.state.dobFocus ? Colors.bondiBlue : Colors.eagle,fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              labelFontSize={utils.getFontScale(14)}
              containerStyle={{marginTop:10}}
              labelPadding={20}
            />
          </TouchableOpacity>

          <TouchableOpacity onPress={this.handleNextPress}
            style={[
              this.state.complete ? LoginStyles.button : LoginStyles.buttonInactive,
                { marginTop: 50 }
            ]}
            disabled={!this.state.complete}
          >
            <DefaultText bold style={{color: this.state.complete ? Colors.buttonTextActive : Colors.buttonTextInactive}}>{labels.getLabel(lang,"next")}</DefaultText>
          </TouchableOpacity>
        </KeyboardAvoidingView>
        </ScrollView>
        <Picker
          ref={picker => this.picker = picker}
          deviceWidth={deviceWidth}
          deviceHeight={deviceHeight}
					showDuration={300}
          showMask={true}
          //onBackButtonPress={() => this.onCalendarHandle()}
          //onBackdropPress={() => this.onCalendarHandle()}
					pickerData={utils.createDateData(lang)}
					selectedValue={this.state.dateDefault}
					onPickerDone={(pickedValue) => this.dobChange(pickedValue)}
          onPickerCancel={() => this.setState({dobFocus:false})}
				/>
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus } = auth;
  return { language, lastScreen, loginStatus };
};

export default connect(
  mapStateToProps,
  {

  }
)(ForgetPinScreen);
