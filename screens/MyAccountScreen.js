import React from "react";
import { Alert, FlatList, Image, KeyboardAvoidingView, PixelRatio ,ScrollView, StyleSheet, Text, TextInput, View, TouchableOpacity } from "react-native";
import { Header } from 'react-navigation';
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { languageChanged, lastScreenChanged, logOut } from "../actions";
import * as labels from "../constants/label";
import CustomUserInactivity from "../components/CustomUserInactivity"
import LoginStyles from "../styles/LoginStyle.style"
import Colors from "../constants/Colors";
import SliderPersonal, { sliderWidth, itemWidth } from "../styles/SliderPersonal.style";
import * as utils from "../functions"
import DefaultText from "../components/DefaultText"

/*
  procState
  0 = normal
  1 = change line id
  2 = change mobile number
  3 = change e-mail
  4 = change password (input old password)
  5 = change password (set new password)
*/

class MyAccountScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {navigation.navigate("Settings")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });

  constructor() {
    super();
    this.state={
      policyNumberStore:"",
      memberNoStore:"",
      tokenStore:"",
      nameStore:"",
      lineIdStore:"",
      telStore:"",
      emailStore:"",
      procState:"",
      nextState:"",
      
      lineId:"",
      tel:"",
      email:"",
      oldPassword:"",
      newPassword:"",
      reNewPassword:"",
      
      lineIdError:"",
      telError:"",
      emailError:"",
      oldPasswordError:"",
      newPasswordError:"",
      reNewPasswordError:"",
      allPasswordError:"",
      resultLabel:"",

      KeyboardAvoidingView:false,
      EnableView:false,
    }
  }

  componentDidMount() {
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start MyAccountScreen");
    this.onScreenChanged()
    this.getLoginData()

    this.setState({
      procState:0,
      nextState:0,
      lineId:"",
      tel:"",
      email:"",
      oldPassword:"",
      newPassword:"",
      reNewPassword:"",
    
      lineIdError:"",
      telError:"",
      emailError:"",
      oldPasswordError:"",
      newPasswordError:"",
      reNewPasswordError:"",
      allPasswordError:"",
      resultLabel:"",
      KeyboardAvoidingView:false,
    })

    this.setTitle(this.props.language)
  };

  onScreenChanged() {
    this.props.lastScreenChanged("MyAccount");
  }

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "myAccount"),
      lang: lang,
    });
  }

  getData = async key => {
    let value = await utils.retrieveData(key)
    this.setState({[key]:value})
  }

  getLoginData = async key => {
    if(!this.props.loginStatus){
      this.setState({EnableView:false})
      let pinStoreVal = await utils.retrieveData("pinStore")
      if(pinStoreVal.length>0) this.props.navigation.navigate('LoginPin')
      else this.props.navigation.navigate('Login')
    }else{
      this.getData("policyNumberStore")
      this.getData("memberNoStore")
      this.getData("tokenStore")
      this.getData("nameStore")
      this.getData("lineIdStore")
      this.getData("telStore")
      this.getData("emailStore")
      this.setState({EnableView:true})
    }
  }

  genUtils = () => {
    let lang = this.props.language
    if(this.state.procState == 0){
      return (
        <View style={{alignItems:'center'}}>
          <TouchableOpacity onPress={() => this.setState({procState:4})} style={styles.container}>
            <DefaultText style={styles.anchor}>{labels.getLabel(lang,"MyAccount_changePassword")}</DefaultText>
          </TouchableOpacity>
        </View>
      )
    }else if(this.state.procState == 99){
      return (
        <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:20}}>
          <TouchableOpacity onPress={() => this.setState({procState:0})}>
            <DefaultText style={styles.anchor}> </DefaultText>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.doneClickClearState()}>
            <DefaultText style={styles.anchor}>{labels.getLabel(lang,"done")}</DefaultText>
          </TouchableOpacity>
        </View>
      )
    }else{
      return (
        <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:20}}>
          <TouchableOpacity 
            onPress={
              () => this.setState({
                procState:0,
                oldPassword:"",
                newPassword:"",
                reNewPassword:"",
                lineId:"",
                tel:"",
                email:"",
                lineIdError:"",
                telError:"",
                emailError:"",
                oldPasswordError:"",
                newPasswordError:"",
                reNewPasswordError:"",
                allPasswordError:"",
                resultLabel:"",
              })}>
            <DefaultText style={[{color:Colors.bondiBlue}]}>{labels.getLabel(lang,"cancel")}</DefaultText>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.doneClick()}>
            <DefaultText style={[{color:Colors.bondiBlue}]}>{labels.getLabel(lang,"done")}</DefaultText>
          </TouchableOpacity>
        </View>
      )
    }
  }
  
  genLineId = () => {
    let label = labels.getLabel(this.props.language,"MyAccount_lineId")
    if(this.state.procState == 1){
      return(
        <View>
          <View style={styles.container}>
            <View style={{flex:2,justifyContent:'center'}}>
              <DefaultText style={SliderPersonal.personalLabelMyAccount}>{label} :</DefaultText>
            </View>
            <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',height:30,paddingLeft:5}}>
              <TextInput 
                style={[SliderPersonal.personalData,this.state.lineIdError == "" ? styles.textBox : styles.textBoxError,{fontFamily:'Prompt'}]}
                onChangeText={text => this.lineIdChange(text)}
                value={this.state.lineId}
                maxLength={20}
                returnKeyType={'done'}
                onFocus={()=>this.setState({KeyboardAvoidingView:true})}
              />
            </View>
          </View>
          { this.state.lineIdError!="" && 
            <View style={styles.container2}>
              <View style={{flex:2,justifyContent:'center'}}>
                <DefaultText> </DefaultText>
              </View>
              <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',paddingLeft:5}}>
                <DefaultText style={styles.textError}>{this.state.lineIdError}</DefaultText>
              </View>
            </View>
          }
        </View>
      )
    }else{
      return (
        <View style={styles.container}>
          <DefaultText style={SliderPersonal.personalLabelMyAccount}>{label} :</DefaultText>
          <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',paddingLeft:5}}>
            <DefaultText style={SliderPersonal.personalDataNoFlexMyAccount}>{this.state.lineIdStore}</DefaultText>
            {this.state.procState==0 &&
            <Icon 
              type='feather'
              name='edit-3'
              size={16}
              color={Colors.bondiBlue}
              onPress={() => this.setState({procState:1})}
            />
            }
          </View>
        </View>
      )
    }
  }

  genTel = () => {
    let label = labels.getLabel(this.props.language,"MyAccount_tel")
    if(this.state.procState == 2){
      return(
        <View>
          <View style={styles.container}>
            <View style={{flex:2,justifyContent:'center'}}>
              <DefaultText style={SliderPersonal.personalLabelMyAccount}>{label} <DefaultText style={{color:'red'}}>*</DefaultText> :</DefaultText>
            </View>
            <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',height:30,paddingLeft:5}}>
              <TextInput 
                style={[this.state.telError == "" ? styles.textBox : styles.textBoxError,{fontFamily:'Prompt',width:100}]}
                onChangeText={text => this.telChange(text)}
                value={this.state.tel}
                keyboardType={'numeric'}
                maxLength={10}
                returnKeyType={'done'}
                onFocus={()=>this.setState({KeyboardAvoidingView:true})}
              />
            </View>
          </View>
          { this.state.telError!="" && 
            <View style={styles.container2}>
              <View style={{flex:2}}>
                <DefaultText> </DefaultText>
              </View>
              <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',paddingLeft:5}}>
                <DefaultText style={styles.textError}>{this.state.telError}</DefaultText>
              </View>
            </View>
          }
        </View>
      )
    }else{
      return (
        <View style={styles.container}>
          <DefaultText style={SliderPersonal.personalLabelMyAccount}>{label}<DefaultText style={{color:'red'}}>*</DefaultText> :</DefaultText>
          <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',paddingLeft:5}}>
            <DefaultText style={SliderPersonal.personalDataNoFlexMyAccount}>{this.state.telStore}</DefaultText>
            {this.state.procState==0 &&
            <Icon 
              type='feather'
              name='edit-3'
              size={16}
              color={Colors.bondiBlue}
              onPress={() => this.setState({procState:2})}
            />
            }
          </View>
        </View>
      )
    }
  }

  genEmail = () => {
    let label = labels.getLabel(this.props.language,"MyAccount_email")
    if(this.state.procState == 3){
      return(
        <View>
          <View style={styles.container}>
            <View style={{flex:2,justifyContent:'flex-start'}}>
              <DefaultText style={SliderPersonal.personalLabelMyAccount}>{label} :</DefaultText>
            </View>
            <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',height:30,paddingLeft:5}}>
              <TextInput 
                style={[SliderPersonal.personalData,this.state.emailError == "" ? styles.textBox : styles.textBoxError,{fontFamily:'Prompt'}]}
                onChangeText={text => this.emailChange(text)}
                value={this.state.email}
                returnKeyType={'done'}
                onFocus={()=>this.setState({KeyboardAvoidingView:true})}
              />
            </View>
          </View>
          { this.state.emailError!="" && 
            <View style={styles.container2}>
              <View style={{flex:2,justifyContent:'center'}}>
                <DefaultText> </DefaultText>
              </View>
              <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',paddingLeft:5}}>
                <DefaultText style={styles.textError}>{this.state.emailError}</DefaultText>
              </View>
            </View>
          }
        </View>
      )
    }else{
      return (
        <View style={styles.container}>
          <DefaultText style={SliderPersonal.personalLabelMyAccount}>{label} :</DefaultText>
          <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',paddingLeft:5}}>
            <DefaultText style={SliderPersonal.personalDataNoFlexMyAccount}>{this.state.emailStore}</DefaultText>
            {this.state.procState==0 &&
            <Icon 
              type='feather'
              name='edit-3'
              size={16}
              color={Colors.bondiBlue}
              onPress={() => this.setState({procState:3})}
            />
            }
          </View>
        </View>
      )
    }
  }

  genPassword = () => {
    let label = labels.getLabel(this.props.language,"MyAccount_oldPassword")
    let label2 = labels.getLabel(this.props.language,"MyAccount_newPassword")
    let label3 = labels.getLabel(this.props.language,"MyAccount_reNewPassword")
    if(this.state.procState == 4){
      return(
        <View>
          {/* old password */}
          <View style={styles.container}>
            <View style={{flex:2,justifyContent:'center'}}>
              <DefaultText style={SliderPersonal.personalLabelMyAccount}>{label} :</DefaultText>
            </View>
            <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',height:30,paddingLeft:5}}>
              <TextInput 
                style={[SliderPersonal.personalData,this.state.oldPasswordError == "" ? styles.textBox : styles.textBoxError,{fontFamily:'Prompt',width:250}]}
                onChangeText={text => this.oldPasswordChange(text)}
                value={this.state.oldPassword}
                maxLength={128}
                returnKeyType={'done'}
                secureTextEntry={true}
                onFocus={()=>this.setState({KeyboardAvoidingView:true})}
              />
            </View>
          </View>
          { this.state.oldPasswordError!="" && 
            <View style={styles.container2}>
              <View style={{flex:2,justifyContent:'center'}}>
                <DefaultText> </DefaultText>
              </View>
              <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',paddingLeft:5}}>
                <DefaultText style={styles.textError}>{this.state.oldPasswordError}</DefaultText>
              </View>
            </View>
          }

          {/* new password */}
          <View style={styles.container}>
            <View style={{flex:2,justifyContent:'center'}}>
              <DefaultText style={SliderPersonal.personalLabelMyAccount}>{label2} :</DefaultText>
            </View>
            <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',height:30,paddingLeft:5}}>
              <TextInput 
                style={[SliderPersonal.personalData,this.state.newPasswordError == "" && this.state.allPasswordError == "" ? styles.textBox : styles.textBoxError,{fontFamily:'Prompt'}]}
                onChangeText={text => this.newPasswordChange(text)}
                value={this.state.newPassword}
                maxLength={128}
                returnKeyType={'done'}
                secureTextEntry={true}
                onFocus={()=>this.setState({KeyboardAvoidingView:true})}
              />
            </View>
          </View>
          { this.state.newPasswordError!="" && 
            <View style={styles.container2}>
              <View style={{flex:2,justifyContent:'center'}}>
                <DefaultText> </DefaultText>
              </View>
              <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',paddingLeft:5}}>
                <DefaultText style={styles.textError}>{this.state.newPasswordError}</DefaultText>
              </View>
            </View>
          }

          {/* re-enter new password */}
          <View style={styles.container}>
            <View style={{flex:2,justifyContent:'center'}}>
              <DefaultText style={SliderPersonal.personalLabelMyAccount}>{label3} :</DefaultText>
            </View>
            <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',height:30,paddingLeft:5}}>
              <TextInput 
                style={[SliderPersonal.personalData,this.state.reNewPasswordError == "" && this.state.allPasswordError == ""  ? styles.textBox : styles.textBoxError,{fontFamily:'Prompt'}]}
                onChangeText={text => this.reNewPasswordChange(text)}
                value={this.state.reNewPassword}
                maxLength={128}
                returnKeyType={'done'}
                secureTextEntry={true}
                onFocus={()=>this.setState({KeyboardAvoidingView:true})}
              />
            </View>
          </View>
          { this.state.reNewPasswordError!="" && 
            <View style={styles.container2}>
              <View style={{flex:2,justifyContent:'center'}}>
                <DefaultText> </DefaultText>
              </View>
              <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',paddingLeft:5}}>
                <DefaultText style={styles.textError}>{this.state.reNewPasswordError}</DefaultText>
              </View>
            </View>
          }

          { this.state.allPasswordError!="" && 
            <View style={styles.container2}>
              <View style={{flex:2,justifyContent:'center'}}>
                <DefaultText> </DefaultText>
              </View>
              <View style={{flex:3,flexDirection:'row',justifyContent:'space-between',paddingLeft:5}}>
                <DefaultText style={styles.textError}>{this.state.allPasswordError}</DefaultText>
              </View>
            </View>
          }


        </View>
      )
    }
  }

  updateMemberInfo = async (type) => {
    let lang = this.props.language
    let result = ""
    let email = ""
    let lineId = ""
    let tel = ""
    if(type=="email"){
      email = this.state.email
    }else{
      email = this.state.emailStore
    }
    if(type=="lineId"){
      lineId = this.state.lineId
    }else{
      lineId = this.state.lineIdStore
    }
    if(type=="tel"){
      tel = this.state.tel
    }else{
      tel = this.state.telStore
    }
    let xmlBody = utils.UpdateMemberInfoBody(this.state.policyNumberStore,this.state.memberNoStore,this.state.tokenStore,email,lineId,tel);
    console.log("-------------------");
    console.log(`Request body: ${xmlBody}`);
    console.log("-------------------");
    let data = null
    await fetch(utils.urlMemberService, utils.genRequest(utils.UpdateMemberInfoSOAPAction,xmlBody) )
      .then(response => response.text())
      .then(response => {
        result = utils.getResult(response)
        // console.log(result)
        let errorCode = result["UpdateMemberInfoResponse"][0]["UpdateMemberInfoResult"][0]["a:errorCode"][0].toString()
        let errorDesc = ""
        if(lang=="th"){
          errorDesc = result["UpdateMemberInfoResponse"][0]["UpdateMemberInfoResult"][0]["a:errorDescTH"][0].toString()
        }else{
          errorDesc = result["UpdateMemberInfoResponse"][0]["UpdateMemberInfoResult"][0]["a:errorDescEN"][0].toString()
        }
        if(Number(errorCode)==0){
          if(type=="email"){
            this.setState({
              emailError:"",
              procState:99,
              email:"",
              emailStore:this.state.email,
              resultLabel:labels.getLabel(this.props.language,"MyAccount_emailSuccess")
            })
          }else if(type=="lineId"){
            this.setState({
              lineIdError:"",
              procState:99,
              lineId:"",
              lineIdStore:this.state.lineId,
              resultLabel:labels.getLabel(this.props.language,"MyAccount_lineIdSuccess")
            })
          }else if(type=="tel"){
            this.setState({
              telError:"",
              procState:99,
              tel:"",
              telStore:this.state.tel,
              resultLabel:labels.getLabel(this.props.language,"MyAccount_telSuccess")
            })
          }
        }else{
          Alert.alert(labels.getLabel(lang,"error"),errorDesc)
        }
      })
      .catch(err => {
        console.log("fetch", err);
        Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
      });
  }

  updatePassword = async () => {
    let lang = this.props.language
    let result = ""
    let xmlBody = utils.UpdPwdBody("RESETPWD",this.state.policyNumberStore,this.state.memberNoStore,this.state.newPassword,this.state.oldPassword,this.state.tokenStore);
    console.log(`Request body: ${xmlBody}`);
    await fetch(utils.urlUserService, utils.genRequest(utils.UpdPwdSOAPAction,xmlBody) )
      .then(response => response.text())
      .then(response => {
        result = utils.getResult(response)
        let errorCode = result["UpdPwdResponse"][0]["UpdPwdResult"][0]["a:errorCode"][0].toString()
        let errorDesc = ""
        if(lang=="th"){
          errorDesc = result["UpdPwdResponse"][0]["UpdPwdResult"][0]["a:errorDescTH"][0].toString()
        }else{
          errorDesc = result["UpdPwdResponse"][0]["UpdPwdResult"][0]["a:errorDescEN"][0].toString()
        }
        if(Number(errorCode)==0){
          this.setState({
            newPasswordError:"",
            reNewPasswordError:"",
            allPasswordError:"",
            procState:99,
            oldPassword:"",
            newPassword:"",
            reNewPassword:"",
            resultLabel:labels.getLabel(this.props.language,"MyAccount_passwordSuccess")
          })
        }else{
          Alert.alert(labels.getLabel(lang,"error"),errorDesc)
        }
      })
      .catch(err => {
        console.log("fetch", err);
        Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
      });
  }

  lineIdChange = (text) => {
    this.setState({lineId:text, lineIdError:''})
  }

  lineIdEdit = () => {
    this.setState({result:""})
    let textError = labels.getError(this.props.language,utils.validateLineId(this.state.lineId));
    this.setState({lineIdError:textError})
    if(textError==""){
      //update lineId
      this.updateMemberInfo("lineId")
      utils.setLineId(this.state.lineId)
    }
  }

  telChange = (text) => {
    this.setState({tel:text, telError:''})
  }

  telEdit = () => {
    this.setState({result:""})
    let textError = labels.getError(this.props.language,utils.validateTel(this.state.tel));
    this.setState({telError:textError})
    if(textError==""){
      //update tel
      this.updateMemberInfo("tel")
      utils.setTel(this.state.tel)
    }
  }

  emailChange = (text) => {
    this.setState({email:text, emailError:''})
  }

  emailEdit = () => {
    this.setState({result:""})
    let textError = labels.getError(this.props.language,utils.validateEmail(this.state.email));
    this.setState({emailError:textError})
    if(textError==""){
      //update email
      this.updateMemberInfo("email")
      utils.setEmail(this.state.email)
    }
  }

  oldPasswordChange = (text) => {
    this.setState({oldPassword:text, oldPasswordError:'', allPasswordError:''})
  }

  newPasswordChange = (text) => {
    this.setState({newPassword:text, newPasswordError:'', allPasswordError:''})
  }

  reNewPasswordChange = (text) => {
    this.setState({reNewPassword:text, reNewPasswordError:'', allPasswordError:''})
  }

  passwordEdit = () => {
    this.setState({result:""})
    let textError = ""
    let textError2 = ""
    let textError3 = ""
    let textError4 = ""

    if(textError == "") textError = labels.getError(this.props.language,utils.validatePassword(this.state.oldPassword));
    if(textError2 == "") textError2 = labels.getError(this.props.language,utils.validatePassword(this.state.newPassword));
    if(textError3 == "") textError3 = labels.getError(this.props.language,utils.validatePassword(this.state.reNewPassword));
    if(textError == "" && textError2 == "" && textError3 == "" && this.state.newPassword != this.state.reNewPassword){
      textError4 = labels.getError(this.props.language,"passwordNotMatch");
    }

    this.setState({oldPasswordError:textError})
    this.setState({newPasswordError:textError2})
    this.setState({reNewPasswordError:textError3})
    this.setState({allPasswordError:textError4})
    if(textError=="" && textError2=="" && textError3=="" && textError4==""){
      this.updatePassword()
    }
  }

  doneClick = () => {
    if(this.state.procState==1){
      this.lineIdEdit()
    }else if(this.state.procState==2){
      this.telEdit()
    }else if(this.state.procState==3){
      this.emailEdit()
    }else if(this.state.procState==4){
      this.passwordEdit()
    }else{
      this.setState({procState:0})
    }
  }

  doneClickClearState = () => {
    this.setState({procState:0})
    this.setState({nextState:0})
    this.setState({tel:""})
    this.setState({email:""})
    this.setState({oldPassword:""})
    this.setState({newPassword:""})
    this.setState({reNewPassword:""})

    this.setState({telError:""})
    this.setState({emailError:""})
    this.setState({oldPasswordError:""})
    this.setState({newPasswordError:""})
    this.setState({reNewPasswordError:""})
    this.setState({allPasswordError:""})
    this.setState({resultLabel:""})
  }

  render() {
    if (this.state.EnableView) {
    return (
      <View style={LoginStyles.main}>
        <CustomUserInactivity onAction={() => this.props.navigation.navigate("Home")}>
          <ScrollView contentContainerStyle={{justifyContent:"center",alignItems:'center'}}>
            <KeyboardAvoidingView style={[SliderPersonal.dataContainer2,{marginTop:10}]} behavior="position" enabled={this.state.KeyboardAvoidingView} keyboardVerticalOffset={Header.HEIGHT + 20}>
              <DefaultText bold style={[SliderPersonal.containerLabel2]}>{labels.getLabel(this.props.language,"MyAccount_userInformation")}</DefaultText>
              <View style={[SliderPersonal.containerInsideBox,{paddingLeft:20,paddingRight:20,marginTop:10}]}>

                <View style={[styles.container]}>
                  <DefaultText style={SliderPersonal.personalLabelMyAccount}>{labels.getLabel(this.props.language,"MyAccount_name")} :</DefaultText>
                  <DefaultText style={SliderPersonal.personalDataMyAccount}>{this.state.nameStore}</DefaultText>
                </View>

                <View style={styles.container}>
                  <DefaultText style={SliderPersonal.personalLabelMyAccount}>{labels.getLabel(this.props.language,"MyAccount_policyNumber")} :</DefaultText>
                  <DefaultText style={SliderPersonal.personalDataMyAccount}>{this.state.policyNumberStore}</DefaultText>
                </View>

                <View style={styles.container}>
                  <DefaultText style={SliderPersonal.personalLabelMyAccount}>{labels.getLabel(this.props.language,"MyAccount_memberNo")} :</DefaultText>
                  <DefaultText style={SliderPersonal.personalDataMyAccount}>{this.state.memberNoStore}</DefaultText>
                </View>

                { this.genTel() }

                { this.genLineId() }

                { this.genEmail() }

                <View style={{marginTop:20}}></View>

                { this.genPassword() }

                { this.state.procState==99 && 
                  <View style={{alignItems:'center'}}>
                    <DefaultText style={{color:Colors.bondiBlue}}>{this.state.resultLabel}</DefaultText>
                  </View>}
                { this.genUtils() }
              </View>
            </KeyboardAvoidingView>
          </ScrollView>
        </CustomUserInactivity>
      </View>
    );
    }else {
      return (<View></View>)
    }
  }

  
}

const styles = StyleSheet.create({
  textBox:{
    borderColor:Colors.bondiBlue,
    borderBottomWidth:1,
    paddingVertical:0,
    paddingHorizontal:5,
    fontSize:utils.getFontScale(12),
    color:Colors.bondiBlue,
  },
  textBoxError:{
    borderColor:Colors.textBoxError,
    // color:Colors.textBoxError,
    color:Colors.bondiBlue,
    borderBottomWidth:1,
    paddingVertical:0,
    paddingHorizontal:5,
    fontSize:utils.getFontScale(12),
  },
  container:{
    flexDirection:'row',
    marginTop:5,
    height:30,
    justifyContent:'center',
  },
  container2:{
    flexDirection:'row',
    justifyContent:'center',
  },
  label:{
    fontSize:12,
    textAlign:'right'
  },
  anchor:{
    fontSize:12,
    color:Colors.bondiBlue,
  },
  textError:{
    fontSize:12,
    color:Colors.textBoxError
  }
});

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus } = auth;
  return { language, lastScreen, loginStatus };
};

export default connect(
  mapStateToProps,
  {
    languageChanged,
    lastScreenChanged,
    logOut
  }
)(MyAccountScreen);
