import React, { Component } from "react";
import {
  Alert,
  Image,
  PixelRatio,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  YellowBox
} from "react-native";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import LoginStyles from "../styles/LoginStyle.style";
import * as labels from "../constants/label";
import * as utils from "../functions";
import DefaultText from "../components/DefaultText"
import _ from 'lodash';

class RegisterTermScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      policyNumber: "",
      memberNo: "",
      passport: "",
      dob: "",
      tel: "",
      email: "",
      lineId: "",
      version:"",
      title:"",
      desc:"",
    }

    YellowBox.ignoreWarnings(["Warning: Each"]);
    const _console = _.clone(console);
    console.warn = message => {
      if (message.indexOf('Each child') <= -1) {
        _console.warn(message);
      }
    };
  }
  
  policyNumber = '';
  memberNo = '';
  passport = '';
  dob = '';
  tel = '';
  email = '';
  lineId = '';

  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {utils.confirmBack(navigation.state.params.lang,navigation,"Home")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });
  
  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start RegisterTermScreen");
    policyNumber = this.props.navigation.getParam('policyNumber', '');
    memberNo = utils.formatMemberNo(this.props.navigation.getParam('memberNo', ''));
    passport = this.props.navigation.getParam('passport', '');
    dob = this.props.navigation.getParam('dob', '');
    tel = this.props.navigation.getParam('tel', '');
    email = this.props.navigation.getParam('email', '');
    lineId = this.props.navigation.getParam('lineId', '');
    this.setState({
      policyNumber,
      memberNo,
      passport,
      dob,
      tel,
      email,
      lineId,
      version:"",
      title:"",
      desc:"",
    })
    this.getTerm(policyNumber,memberNo)
    this.setTitle(this.props.language)
  };

  navigateBack = () => {
    utils.confirmBack(this.props.language,this.props.navigation,"Home")
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "term"),
      lang: lang,
    });
  }

  getTerm = async (policyNumber,memberNo) => {
    let lang = this.props.language
    let result = ""
    let xmlBody = utils.GetMasterAgreementBody(policyNumber,memberNo);
    console.log(`Request body: ${xmlBody}`);
    await fetch(utils.urlMemberService, utils.genRequest(utils.GetMasterAgreementSOAPAction,xmlBody) )
      .then(response => response.text())
      .then(response => {
        result = utils.getResult(response)
        let errorCode = result["GetMasterAgreementResponse"][0]["GetMasterAgreementResult"][0]["a:errorCode"][0].toString()
        let errorDesc = ""
        if(lang=="th"){
          errorDesc = result["GetMasterAgreementResponse"][0]["GetMasterAgreementResult"][0]["a:errorDescTH"][0].toString()
        }else{
          errorDesc = result["GetMasterAgreementResponse"][0]["GetMasterAgreementResult"][0]["a:errorDescEN"][0].toString()
        }
        if(Number(errorCode)==0){
          let title = ''
          let desc = ''
          let version = ''
          if(lang=="th"){
            title = result["GetMasterAgreementResponse"][0]["GetMasterAgreementResult"][0]["a:titleTH"][0]
            desc = result["GetMasterAgreementResponse"][0]["GetMasterAgreementResult"][0]["a:descTH"][0]
          }else{
            title = result["GetMasterAgreementResponse"][0]["GetMasterAgreementResult"][0]["a:titleEN"][0]
            desc = result["GetMasterAgreementResponse"][0]["GetMasterAgreementResult"][0]["a:descEN"][0]
          }
          version = result["GetMasterAgreementResponse"][0]["GetMasterAgreementResult"][0]["a:version"][0]
          desc = desc.replace(/\\n/g, "\n");
          this.setState({title,desc,version})
        }else{
          Alert.alert(labels.getLabel(lang,"error"),errorDesc)
        }
      })
      .catch(err => {
        console.log("fetch", err);
        Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
      });
  }

  handleAcceptPress = async () => {
    let result = ""
    let xmlBody = utils.UpdateTransAgreementBody(this.state.policyNumber,this.state.memberNo,"-",this.state.version);
    console.log(`Request body: ${xmlBody}`);
    await fetch(utils.urlMemberService, utils.genRequest(utils.UpdateTransAgreementSOAPAction,xmlBody) )
      .then(response => response.text())
      .then(response => {
        result = utils.getResult(response)
        let errorCode = result["UpdateTransAgreementResponse"][0]["UpdateTransAgreementResult"][0]["a:errorCode"][0].toString()
        let errorDesc = ""
        if(lang=="th"){
          errorDesc = result["UpdateTransAgreementResponse"][0]["UpdateTransAgreementResult"][0]["a:errorDescTH"][0].toString()
        }else{
          errorDesc = result["UpdateTransAgreementResponse"][0]["UpdateTransAgreementResult"][0]["a:errorDescEN"][0].toString()
        }
        if(Number(errorCode)==0){
          this.props.navigation.navigate("RegisterOTP",{
            policyNumber: this.state.policyNumber,
            memberNo: this.state.memberNo,
            passport: this.state.passport,
            dob: this.state.dob,
            tel: this.state.tel,
            email: this.state.email,
            lineId: this.state.lineId,
          })
        }else{
          Alert.alert(labels.getLabel(lang,"error"),errorDesc)
        }
      })
      .catch(err => {
        console.log("fetch", err);
        Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
      });
  }

  handleDeclinePress = () => {
    this.props.navigation.navigate("Home")
  }

  render() {
    let lang = this.props.language
    return (
      <View style={{flex:1}}>
        <ScrollView style={[LoginStyles.mainTerm,{backgroundColor:'white'}]}>
            <View style={{alignItems:'center', marginTop:20}}><DefaultText bold style={{color:Colors.TMFreshGrey,fontSize:24}}>{this.state.title}</DefaultText></View>
            {/*<DefaultText style={{marginTop:10,marginBottom:10,color:Colors.TMFreshGrey}}>{this.state.desc}</DefaultText>*/}
            <View style={{marginTop:10,marginBottom:10}}>
              {utils.customTag(this.state.desc)}
            </View>
            

            <View style={{flexDirection:'row', marginBottom:20}}>
                <TouchableOpacity onPress={this.handleDeclinePress}
                    style={[
                        LoginStyles.button,
                        { flex:1, marginRight:10 }
                    ]}
                >
                  <DefaultText bold style={{color: Colors.buttonTextActive }}>{labels.getLabel(lang,"decline")}</DefaultText>
                </TouchableOpacity>

                <TouchableOpacity onPress={this.handleAcceptPress}
                    style={[
                      this.state.desc=='' ? LoginStyles.buttonInactive : LoginStyles.button,
                        { flex:1, marginLeft:10 }
                    ]}
                    disabled={this.state.desc=='' ? true : false}
                >
                  <DefaultText bold style={{color: Colors.buttonTextActive }}>{labels.getLabel(lang,"accept")}</DefaultText>
                </TouchableOpacity>
            </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen } = auth;
  return { language, lastScreen };
};

export default connect(
  mapStateToProps,
  {

  }
)(RegisterTermScreen);
