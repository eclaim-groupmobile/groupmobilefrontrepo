import React from "react";
import {
  ActivityIndicator,
  Dimensions,
  Alert,
  FlatList,
  Image,
  Linking,
  PixelRatio,
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  WebView,
  YellowBox
} from "react-native";
import { ScreenOrientation  } from "expo";
import Constants from 'expo-constants';
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { languageChanged, lastScreenChanged, logOut } from "../actions";
import * as labels from "../constants/label";
import CustomUserInactivity from "../components/CustomUserInactivity";
import LoginStyles from "../styles/LoginStyle.style";
import Colors from "../constants/Colors";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import * as utils from "../functions";
import { Card, Title, Paragraph } from 'react-native-paper';
import Modal from "react-native-modal";
import PinchZoomView from 'react-native-pinch-zoom-view';
import ScaledImage from '../components/ScaledImage'
import DefaultText from "../components/DefaultText"
import _ from 'lodash';

const { height, width } = Dimensions.get("window");

class HealthTipsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {navigation.navigate("Home")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });

  constructor(props) {
    super(props);
    this.state = { 
      onLoad:false,
      layout: {
        height: height,
        width: width
      }
    };
    
    YellowBox.ignoreWarnings(["Can't call setState"]);
    const _console = _.clone(console);
    console.warn = message => {
      if (message.indexOf('Setting a timer') <= -1) {
        _console.warn(message);
      }
    };
  }

  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    // console.log(dataMock.result)
    utils.log("Start HealthTipsScreen");
    this.setTitle(this.props.language)
    this.setState({
      typeModal:null,
      isModalVisible:false,
      onLoad:false,
      apiData: this.props.healthTipsList,
    })
  };

  navigateBack = () => {
    this.props.navigation.navigate("Home");
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "healthTips"),
      lang: lang,
    });
  }

  _onLayout = event => {
    // console.log(
    //     "------------------------------------------------" +
    //         JSON.stringify(event.nativeEvent.layout)
    // );

    this.setState({
        layout: {
            height: event.nativeEvent.layout.height > event.nativeEvent.layout.width ? height : width,
            width: event.nativeEvent.layout.height > event.nativeEvent.layout.width ? width : height
        }
    });
  };

  getData = async (fileId) => {
    if(this.state.onLoad==false){
      let lang = this.props.language
      // GetHealthTipDetail
      this.setState({onLoad:true})
      let result = ""
      let xmlBody = utils.GetHealthTipDetailBody(fileId);
      console.log("-------------------");
      console.log("Request body: " + xmlBody);
      console.log("-------------------");
      let data = null
      await fetch(utils.urlMemberService, utils.genRequest(utils.GetHealthTipDetailSOAPAction,xmlBody) )
        .then(response => response.text())
        .then(response => {
          result = utils.getResult(response)
          // console.log(result)
          let errorCode = result["GetHealthTipDetailResponse"][0]["GetHealthTipDetailResult"][0]["a:errorCode"][0].toString()
          let errorDesc = ""
          if(lang=="th"){
            errorDesc = result["GetHealthTipDetailResponse"][0]["GetHealthTipDetailResult"][0]["a:errorDescTH"][0].toString()
          }else{
            errorDesc = result["GetHealthTipDetailResponse"][0]["GetHealthTipDetailResult"][0]["a:errorDescEN"][0].toString()
          }
          if(errorDesc=="[object Object]"){
            errorDesc = labels.getLabel(lang,"errorNetwork")
          }
          if(Number(errorCode)==0){
            /////////////////////
            let fileType = result["GetHealthTipDetailResponse"][0]["GetHealthTipDetailResult"][0]["a:fileType"][0].toString()
            let filePath = Constants.manifest.extra.urlFile2 + result["GetHealthTipDetailResponse"][0]["GetHealthTipDetailResult"][0]["a:filePath"][0].toString()
            filePath = filePath.replace(".PDF", ".pdf")
            if(filePath!=null && filePath !=''){
              if(fileType=='pdf'){
                if(Platform.OS === "ios"){
                  // ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.ALL);
                  // this.setState({uri:filePath,isModalVisible:true,typeModal:"WebView"})
                  Linking.openURL(filePath)
                }else{
                  if(Constants.appOwnership=='expo'){
                    Alert.alert("This function not work on Android with Expo Client","Please use standalone version (apk)")
                    console.log("This function not work on Android with Expo Client","Please use standalone version (apk)")
                  }else{
                    Linking.openURL(filePath)
                  }
                }
              }else{
                if(fileType=='jpg' || fileType=='png' || fileType=='gif'){
                  // Linking.openURL(filePath)
                  // console.log(filePath)
                  ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.ALL);
                  this.setState({uri:filePath,isModalVisible:true,typeModal:"Image"})
                }
              }
            }
            
          }else{
            Alert.alert(labels.getLabel(lang,"error"),errorDesc)
          }
          this.setState({onLoad:false})
        })
        .catch(err => {
          console.log("fetch", err);
          Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
          this.setState({onLoad:false})
        });
    }
  }

  genBox = (item,index) => {
    let lang = this.props.language
    if(item.fileID!=null){
      return (
        <TouchableOpacity onPress={() => this.getData(item.fileID)} style={[styles.link]}>
          <Image
            source={require("../assets/icon/doc.png")}
            style={{width: 25, height: 25}}
          />
          <DefaultText style={{marginLeft:10,color:Colors.bondiBlue,flex:1,flexWrap: 'wrap'}}>{item.fileName + ': ' + item.fileDescription}</DefaultText>
        </TouchableOpacity>
      )
    }
  }

  render() {
    let lang = this.props.language;
    return (
      <View style={LoginStyles.main} onLayout={this._onLayout}>
        <CustomUserInactivity onAction={() => this.props.navigation.navigate("Home")}>
          <View style={styles.option}>
            {this.state.apiData!=null &&
            <FlatList
              data={this.state.apiData}
              renderItem={({item,index}) => this.genBox(item,index)}
              keyExtractor={(item) => item.fileID}
              extraData={this.props.language}
            />
            }
          </View>
          {
            this.state.onLoad &&
            <View style={{position:'absolute',left:0,right:0,top:30}}>
              <ActivityIndicator size="large" color="#666" />
            </View>
            
          }

          <Modal isVisible={this.state.isModalVisible} animationOutTiming={50} style={styles.fullModal} supportedOrientations={['portrait', 'landscape']}>
            <SafeAreaView style={{flex:1}}>
              <View style={[styles.subModal,{width:this.state.layout.width,height:this.state.layout.height}]}>
                {/* close */}
                <TouchableOpacity style={{position:'absolute',right:10,top:10}} onPress={() => {this.setState({isModalVisible:false});ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT_UP);}}>
                  <Image
                    style={{ width: 22, height: 22 }}
                    source={require("../assets/icon/Close-eagle.png")}
                  />
                </TouchableOpacity>
                <View style={{alignContent:'center',marginTop:40,overflow:'hidden',width:this.state.layout.width,height:this.state.layout.height,alignItems:'center'}}>
                  
                  { this.state.typeModal=='WebView' ?
                    <WebView
                      source={{uri: this.state.uri}}
                      style={[styles.webview,{width:this.state.layout.width}]}
                      onLoadStart={() => this.setState({onLoad:true})}
                      onLoadEnd={() => this.setState({onLoad:false})}
                    />
                    :
                    <ScaledImage
                      uri={this.state.uri}
                      width={this.state.layout.width}
                    />
                  }
                </View>
              </View>

              {/* loading indicator */}
              <View style={{position:'absolute',left:0,right:0,top:80,}}>
              {
                this.state.onLoad &&
                <ActivityIndicator size="large" color="#666" />
              }
              </View>
            </SafeAreaView>
          </Modal>
        </CustomUserInactivity>
      </View>
    );
  }

  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
  },
  row:{
    flexDirection: "row", 
    justifyContent: "space-between",
    padding:10
  },
  option:{
    justifyContent:'center',
  },
  link:{
    flexDirection:'row',
    textAlign:'center',
    marginLeft:20,
    marginRight:20,
    marginTop:20,
  },
  fullModal: {
    margin: 0,
  },
  subModal: {
    backgroundColor: "white",
    alignItems: "center",
  },
  webview: { 

  },
});

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus, healthTipsList } = auth;
  return { language, lastScreen, loginStatus, healthTipsList };
};

export default connect(
  mapStateToProps,
  {
    languageChanged,
    lastScreenChanged,
    logOut
  }
)(HealthTipsScreen);
