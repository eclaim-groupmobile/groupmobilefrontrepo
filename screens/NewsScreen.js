import React from "react";
import {
  ActivityIndicator,
  Dimensions,
  Alert,
  FlatList,
  Image,
  Linking,
  PixelRatio,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from "react-native";
import Constants from 'expo-constants';
import { connect } from "react-redux";
import { languageChanged, lastScreenChanged, logOut } from "../actions";
import * as labels from "../constants/label";
import CustomUserInactivity from "../components/CustomUserInactivity";
import LoginStyles from "../styles/LoginStyle.style";
import Colors from "../constants/Colors";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import * as utils from "../functions";
import Carousel from "react-native-snap-carousel";
import SliderEntry from "../components/SliderEntry";
import { sliderWidth, itemWidth } from "../styles/SliderEntry.style";
import ScaledImage from '../components/ScaledImage2'
import DefaultText from "../components/DefaultText"

class NewsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {navigation.navigate("Home")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });

  constructor(props) {
    super(props);
    this.state = { 
      onLoad:true,
      slider1ActiveSlide:0,
    };
  }

  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start NewsScreen");
    this.setTitle(this.props.language)
    this.setState({
      isModalVisible:false,
      onLoad:true,
      slider1ActiveSlide:0
    })
  };

  navigateBack = () => {
    this.props.navigation.navigate("Home");
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "news"),
      lang: lang,
    });
  }

  renderItemWithParallax ({item, index}, parallaxProps) {
    return (
        <SliderEntry
          data={item}
          even={(index + 1) % 2 === 0}
          parallax={true}
          parallaxProps={parallaxProps}
        />
    );
}

  render() {
    let lang = this.props.language;
    let picUri = this.props.newsList[this.state.slider1ActiveSlide]['pictureLink']!=null ? Constants.manifest.extra.urlFile2 + this.props.newsList[this.state.slider1ActiveSlide]['pictureLink'] : null
    return (
      <View style={LoginStyles.main}>
        <CustomUserInactivity onAction={() => this.props.navigation.navigate("Home")}>
          <ScrollView contentContainerStyle={styles.option}>
            {this.props.newsList!=null &&
            <View>
              <View style={{backgroundColor:Colors.header,marginBottom:5,paddingTop:10}}>
                <Carousel
                  ref={c => this._slider1Ref = c}
                  data={this.props.newsList}
                  renderItem={this.renderItemWithParallax}
                  sliderWidth={sliderWidth}
                  itemWidth={itemWidth}
                  hasParallaxImages={true}
                  firstItem={0}
                  inactiveSlideScale={0.94}
                  inactiveSlideOpacity={0.7}
                  // inactiveSlideShift={20}
                  containerCustomStyle={styles.slider}
                  contentContainerCustomStyle={styles.sliderContentContainer}
                  loop={false}
                  autoplay={false}
                  onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
                  removeClippedSubviews={false}
                />
              </View>
              <View style={[styles.scview,{justifyContent:'center',alignItems: 'center',marginHorizontal:15}]} >
                {this.props.newsList[this.state.slider1ActiveSlide]['pictureLink']!=null && 
                  <View style={{paddingTop:10,marginBottom:5,height:170,alignSelf: "stretch"}}>
                    <View style={{justifyContent:'center',alignItems: 'center'}}>
                      <Image 
                        source={{uri: Constants.manifest.extra.urlFile2 + this.props.newsList[this.state.slider1ActiveSlide]['pictureLink']}}
                        style={{width:itemWidth-10,height:150}}
                      />
                    </View>
                  </View>
                }
                <View style={{paddingHorizontal:10,marginBottom:10,backgroundColor:'white'}}>
                  <DefaultText bold style={{lineHeight:18}}>{this.props.newsList[this.state.slider1ActiveSlide]['title']}</DefaultText>
                  <DefaultText style={{lineHeight:18}}>       {this.props.newsList[this.state.slider1ActiveSlide]['detail']}</DefaultText>
                </View>
              </View>
            </View>
            }
          </ScrollView>
        </CustomUserInactivity>
      </View>
    );
  }

  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
  },
  option:{
    paddingBottom:0,
    justifyContent:'center',
  },
  scview: {
    marginTop:5,
    marginBottom:10,
    borderWidth:1,
    borderColor:Colors.bondiBlue,
    borderRadius:5,
    justifyContent:'center',
  }
});

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus, newsList } = auth;
  return { language, lastScreen, loginStatus, newsList };
};

export default connect(
  mapStateToProps,
  {
    languageChanged,
    lastScreenChanged,
    logOut
  }
)(NewsScreen);
