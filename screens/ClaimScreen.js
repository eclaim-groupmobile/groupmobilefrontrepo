import React, { Component } from "react";
import {
  Alert,
  BackHandler,
  FlatList,
  Image,
  ImageBackground,
  PixelRatio,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Icon } from 'react-native-elements';
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import { languageChanged,lastScreenChanged,notificationChanged,notificationListChanged, logOut } from "../actions";
import CustomUserInactivity from "../components/CustomUserInactivity";
import SliderPersonal,{ sliderWidth, itemWidth } from "../styles/SliderPersonal.style";
import Carousel from "react-native-snap-carousel";
import * as labels from "../constants/label";
import MainStyles from "../styles/Main.styles"
import * as utils from "../functions"
import NodataList from "../components/NoDataList"
import DefaultText from "../components/DefaultText"

/*  stat
    0 - show all
    1 - IPD
    2 - OPD
    3 - Lab / X-ray
    4 - Dental
    5 - AME
*/
class ClaimScreen extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      pinStore: "",
      apiData:[],
      data:null,
      apiLength:0,
      slider1ActiveSlide: 0,
      stat: 0,
      EnableView:false,
      refreshing:false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
  });

  componentDidMount() {
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start ClaimScreen");
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton.bind(this));
    this.onScreenChanged()
    this.getLoginData()
    this.setTitle(this.props.language)
    // console.log(this.props.notificationList)
  };
  handleBackButton = () => {
    if(this.state.stat!=0){
      this.setState({stat:0})
    }else{
      BackHandler.exitApp();
    }
    return true;
  };
  
  onScreenChanged() {
    this.props.lastScreenChanged("Claim");
  }

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "claim"),
      lang: lang,
    });
  }

  getData = async key => {
    let value = await utils.retrieveData(key)
    this.setState({[key]:value})
  }

  getLoginData = async key => {
    if(!this.props.loginStatus){
      this.setState({EnableView:false})
      let pinStoreVal = await utils.retrieveData("pinStore")
      if(pinStoreVal.length>0) this.props.navigation.navigate('LoginPin')
      else this.props.navigation.navigate('Login')
    }else{
      this.setState({EnableView:true})
      //load data
      await this.getData("policyNumberStore")
      await this.getData("memberNoStore")
      await this.getData("tokenStore")
      if(this.props.memberData!=null){
        this.setState({
          apiData: this.props.memberData['memberInfo'],
          apiLength: this.props.memberData['memberInfo'].length,
          slider1ActiveSlide:0,
          stat:0,
        },() => this._slider1Ref.snapToItem(0))
      }
    }
  }

  _notificationChanged(noti){
    this.props.notificationChanged(noti)
  }

  _notificationListChanged(list){
    this.props.notificationListChanged(list)
  }

  onBeforeSnapToItem = index => {
    this.setState({ 
      slider1ActiveSlide: index,
      stat: 0
    })
  }

  onSnapToItem = index => {
    // this.setState({ 
    //   slider1ActiveSlide: index,
    //   stat: 0
    // })
  }

  fetchDetail = async (policyNumber,memberNo,type) => {
    let lang = this.props.language
    let data = null
    let result = ""
    let xmlBody = ""
    let SOAPAction = null
    let responseLabel = ''
    let resultLabel = ''
    let nodeData = ''
    let stat = 0
    let loginedMemberNo = this.state.memberNoStore
    if(type=="IPD"){
      xmlBody = utils.GetIPDClaimDetailBody(policyNumber,loginedMemberNo,memberNo,this.state.tokenStore);
      SOAPAction = utils.GetIPDClaimDetailSOAPAction
      responseLabel = 'GetIPDClaimDetailResponse'
      resultLabel = 'GetIPDClaimDetailResult'
      nodeData = 'ClaimIPDNotify'
      stat = 1
    }else if(type=="OPD"){
      xmlBody = utils.GetOPDClaimDetailBody(policyNumber,loginedMemberNo,memberNo,this.state.tokenStore);
      SOAPAction = utils.GetOPDClaimDetailSOAPAction
      responseLabel = 'GetOPDClaimDetailResponse'
      resultLabel = 'GetOPDClaimDetailResult'
      nodeData = 'ClaimOPDNotify'
      stat = 2
    }else if(type=="LAB"){
      xmlBody = utils.GetLABClaimDetailBody(policyNumber,loginedMemberNo,memberNo,this.state.tokenStore);
      SOAPAction = utils.GetLABClaimDetailSOAPAction
      responseLabel = 'GetLABClaimDetailResponse'
      resultLabel = 'GetLABClaimDetailResult'
      nodeData = 'ClaimLabsNotify'
      stat = 3
    }else if(type=="DEN"){
      xmlBody = utils.GetDENClaimDetailBody(policyNumber,loginedMemberNo,memberNo,this.state.tokenStore);
      SOAPAction = utils.GetDENClaimDetailSOAPAction
      responseLabel = 'GetDENClaimDetailResponse'
      resultLabel = 'GetDENClaimDetailResult'
      nodeData = 'ClaimDentalNotify'
      stat = 4
    }else if(type=="AME"){
      xmlBody = utils.GetAMEClaimDetailBody(policyNumber,loginedMemberNo,memberNo,this.state.tokenStore);
      SOAPAction = utils.GetAMEClaimDetailSOAPAction
      responseLabel = 'GetAMEClaimDetailResponse'
      resultLabel = 'GetAMEClaimDetailResult'
      nodeData = 'ClaimAMENotify'
      stat = 5
    }
    this.setState({stat,refreshing:true})
    console.log("-------------------");
    console.log("Request body: " + xmlBody);
    console.log("-------------------");
    await fetch(utils.urlMemberService, utils.genRequest(SOAPAction,xmlBody) )
      .then(response => response.text())
      .then(response => {
        result = utils.getResult(response)
        // console.log(result)
        let errorCode = result[responseLabel][0][resultLabel][0]["a:errorCode"][0].toString()
        let errorDesc = ""
        if(lang=="th"){
          errorDesc = result[responseLabel][0][resultLabel][0]["a:errorDescTH"][0].toString()
        }else{
          errorDesc = result[responseLabel][0][resultLabel][0]["a:errorDescEN"][0].toString()
        }
        // console.log(result)
        if(Number(errorCode)==0){
          /////////////////////
          let data = null
          if(type=="IPD"){
            data = utils.getIPDInfo(result)
          }else if(type=="OPD"){
            data = utils.getOPDInfo(result)
          }else if(type=="LAB"){
            data = utils.getLABInfo(result)
          }else if(type=="DEN"){
            data = utils.getDENInfo(result)
          }else if(type=="AME"){
            data = utils.getAMEInfo(result)
          }
          this.setState({data,type})
          let xmlBody2 = utils.UpdateClaimNotifyKeyBody(policyNumber,loginedMemberNo,memberNo,type,this.state.tokenStore);
          console.log("-------------------");
          console.log("Request body: " + xmlBody2);
          console.log("-------------------");
          fetch(utils.urlMemberService, utils.genRequest(utils.UpdateClaimNotifyKeySOAPAction,xmlBody2))
            .then(response => response.text())
            .then(response => {
              result2 = utils.getResult(response)
              let errorCode2 = result2["UpdateClaimNotifyKeyResponse"][0]["UpdateClaimNotifyKeyResult"][0]["a:errorCode"][0].toString()
              let errorDesc2 = ""
              if(lang=="th"){
                errorDesc2 = result2["UpdateClaimNotifyKeyResponse"][0]["UpdateClaimNotifyKeyResult"][0]["a:errorDescTH"][0].toString()
              }else{
                errorDesc2 = result2["UpdateClaimNotifyKeyResponse"][0]["UpdateClaimNotifyKeyResult"][0]["a:errorDescEN"][0].toString()
              }
              if(Number(errorCode2)==0){
                let notificationList = this.props.notificationList
                let notification = Number(this.props.notification)
                let index = this.state.slider1ActiveSlide
                let notificationDetailCount = Number(notificationList[index][nodeData])
                notification = notification - notificationDetailCount
                notificationList[index][nodeData] = "0"
                this._notificationChanged(notification+"")
                this._notificationListChanged(notificationList)
              }else{
                Alert.alert(labels.getLabel(lang,"error"),errorDesc2)
                this.setState({stat:0})
              }
            })
            .catch(err => {
              console.log("fetch", err);
              Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
              this.setState({stat:0})
            });
        }else{
          Alert.alert(labels.getLabel(lang,"error"),errorDesc)
          this.setState({stat:0})
        }
        this.setState({refreshing:false})
      })
      .catch(err => {
        console.log("fetch", err);
        Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
        this.setState({stat:0,refreshing:false})
      });
  }

  genBox = ({ item, index }) => {
    let lang = this.props.language;

    return (
      <View style={{justifyContent:"center",alignItems:'center'}}>
        <View style={[SliderPersonal.slideInnerContainer]}>
          { index !=0 ?
            <TouchableOpacity style={SliderPersonal.slideInnerSideContainer} onPress={() => this._slider1Ref.snapToPrev()}>        
              <Icon
                name='chevron-left'
                type='evilicon'
                color={Colors.bondiBlue}
                size={40}
              />
            </TouchableOpacity> : 
            <TouchableOpacity style={SliderPersonal.slideInnerSideContainer}>        
              <DefaultText> </DefaultText>
            </TouchableOpacity>
          }
          <View style={SliderPersonal.slideInnerCenterContainer}>
              <DefaultText bold style={{color:Colors.bondiBlue,fontSize:17}}>{item.FName} {item.LName}</DefaultText>
          </View>
          { index != this.state.apiLength-1 ?
            <TouchableOpacity style={SliderPersonal.slideInnerSideContainer} onPress={() => this._slider1Ref.snapToNext()}>      
              <Icon
                name='chevron-right'
                type='evilicon'
                color={Colors.bondiBlue}
                size={40}
              />
            </TouchableOpacity> : 
            <TouchableOpacity style={SliderPersonal.slideInnerSideContainer}>        
              <DefaultText> </DefaultText>
            </TouchableOpacity>
          }
        </View>

        <ScrollView style={SliderPersonal.scrollView}>
          <View style={{marginBottom:5}}>
            <DefaultText style={{color:Colors.bondiBlue}}>{labels.getLabel(lang,"Claim_yearPolicy")} : {(item.StartDate != null && item.EndDate != null) && item.StartDate + " - " + item.EndDate}</DefaultText>
          </View>

          {/* IPD */
          (this.state.stat==0 && item.HasIPD=="TRUE") &&
          <View style={SliderPersonal.dataContainer3}>
            { Number(this.props.notificationList[this.state.slider1ActiveSlide]['ClaimIPDNotify'])>0 &&
            <View style={styles.noti}>
              <DefaultText bold style={{textAlign:'center',color:Colors.cardBorder}}>{this.props.notificationList[this.state.slider1ActiveSlide]['ClaimIPDNotify']}</DefaultText>
            </View>
            }
            <TouchableOpacity style={{flex:1}} onPress={() => this.fetchDetail(this.props.memberData['policyNo'],item.MemberNo,"IPD")}> 
              <View style={styles.dataContainerSub}>
                <View style={{justifyContent:'center'}}>
                  <DefaultText bold style={styles.dataHead}>{labels.getLabel(lang,"Claim_labelIPD")}{/* this.setState({stat:1}) */}</DefaultText>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          }
          {/* IPD data */
            this.state.stat==1 && this.genIPDBox()
          }

          {/* OPD */
            (this.state.stat==0 && item.HasOPD=="TRUE") &&
          <View style={[SliderPersonal.dataContainer3,{marginTop:10}]}>
            { Number(this.props.notificationList[this.state.slider1ActiveSlide]['ClaimOPDNotify'])>0 &&
            <View style={styles.noti}>
              <DefaultText bold style={{textAlign:'center',color:Colors.cardBorder}}>{this.props.notificationList[this.state.slider1ActiveSlide]['ClaimOPDNotify']}</DefaultText>
            </View>
            }
            <TouchableOpacity style={{flex:1}} onPress={() => this.fetchDetail(this.props.memberData['policyNo'],item.MemberNo,"OPD")}> 
              <View style={styles.dataContainerSub}>
                <View style={{justifyContent:'center'}}>
                  <DefaultText bold style={styles.dataHead}>{labels.getLabel(lang,"Claim_labelOPD")}{/* this.setState({stat:2}) */}</DefaultText>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          }
          {/* OPD data */
          this.state.stat==2 && this.genOPDBox()
          }

          {/* Lab */
          (this.state.stat==0 && item.HasLAB=="TRUE") &&
          <View style={[SliderPersonal.dataContainer3,{marginTop:10}]}>
            { Number(this.props.notificationList[this.state.slider1ActiveSlide]['ClaimLabsNotify'])>0 &&
            <View style={styles.noti}>
              <DefaultText bold style={{textAlign:'center',color:Colors.cardBorder}}>{this.props.notificationList[this.state.slider1ActiveSlide]['ClaimLabsNotify']}</DefaultText>
            </View>
            }
            <TouchableOpacity style={{flex:1}} onPress={() => this.fetchDetail(this.props.memberData['policyNo'],item.MemberNo,"LAB")}> 
              <View style={styles.dataContainerSub}>
                <View style={{justifyContent:'center'}}>
                  <DefaultText bold style={styles.dataHead}>{labels.getLabel(lang,"Claim_labelLab")}{/* this.setState({stat:3}) */}</DefaultText>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          }
          {/* Lab data */
          this.state.stat==3 && this.genLABBox()
          }

          {/* Dental */
            (this.state.stat==0 && item.HasDEN=="TRUE") &&
          <View style={[SliderPersonal.dataContainer3,{marginTop:10}]}>
            { Number(this.props.notificationList[this.state.slider1ActiveSlide]['ClaimDentalNotify'])>0 &&
            <View style={styles.noti}>
              <DefaultText bold style={{textAlign:'center',color:Colors.cardBorder}}>{this.props.notificationList[this.state.slider1ActiveSlide]['ClaimDentalNotify']}</DefaultText>
            </View>
            }
            <TouchableOpacity style={{flex:1}} onPress={() => this.fetchDetail(this.props.memberData['policyNo'],item.MemberNo,"DEN")}> 
              <View style={styles.dataContainerSub}>
                <View style={{justifyContent:'center'}}>
                  <DefaultText bold style={styles.dataHead}>{labels.getLabel(lang,"Claim_labelDental")}{/* this.setState({stat:4}) */}</DefaultText>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          }
          {/* Dental data */
            this.state.stat==4 && this.genDENBox()
          }

          {/* AME */
          (this.state.stat==0 && item.HasAME=="TRUE") &&
          <View style={[SliderPersonal.dataContainer3,{marginTop:10}]}>
            { Number(this.props.notificationList[this.state.slider1ActiveSlide]['ClaimAMENotify'])>0 &&
            <View style={styles.noti}>
              <DefaultText bold style={{textAlign:'center',color:Colors.cardBorder}}>{this.props.notificationList[this.state.slider1ActiveSlide]['ClaimAMENotify']}</DefaultText>
            </View>
            }
            <TouchableOpacity style={{flex:1}} onPress={() => this.fetchDetail(this.props.memberData['policyNo'],item.MemberNo,"AME")}> 
              <View style={styles.dataContainerSub}>
                <View style={{justifyContent:'center'}}>
                  <DefaultText bold style={styles.dataHead}>{labels.getLabel(lang,"Claim_labelAME")}{/* this.setState({stat:5}) */}</DefaultText>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          }
          {/* AME data */
            this.state.stat==5 && this.genAMEBox()
          }

          {
            (this.state.stat==0 && item.HasIPD!="TRUE" && item.HasOPD!="TRUE" && item.HasLAB!="TRUE" && item.HasDEN!="TRUE" && item.HasAME!="TRUE") &&
            <NodataList noIcon/>
          }
          
          {/* remark */
          (this.state.stat==1 && this.state.data!=null && this.state.data.claimInfo.length>0) ?
          <DefaultText style={[SliderPersonal.personalRemark,{marginTop:5}]}><DefaultText style={{textDecorationLine:'underline'}}>{labels.getLabel(lang,"Claim_IPD_remark1")}</DefaultText> {labels.getLabel(lang,"Claim_IPD_remark2")}</DefaultText>
          : (this.state.stat==2 && this.state.data!=null && this.state.data.claimInfo.length>0) ?
          <DefaultText style={[SliderPersonal.personalRemark,{marginTop:5}]}><DefaultText style={{textDecorationLine:'underline'}}>{labels.getLabel(lang,"Claim_OPD_remark1")}</DefaultText> {labels.getLabel(lang,"Claim_OPD_remark2")}</DefaultText>
          : (this.state.stat==3 && this.state.data!=null && this.state.data.claimInfo.length>0) ?
          <DefaultText style={[SliderPersonal.personalRemark,{marginTop:5}]}><DefaultText style={{textDecorationLine:'underline'}}>{labels.getLabel(lang,"Claim_LAB_remark1")}</DefaultText> {labels.getLabel(lang,"Claim_LAB_remark2")}</DefaultText>
          : (this.state.stat==4 && this.state.data!=null && this.state.data.claimInfo.length>0) ?
          <DefaultText style={[SliderPersonal.personalRemark,{marginTop:5}]}><DefaultText style={{textDecorationLine:'underline'}}>{labels.getLabel(lang,"Claim_DEN_remark1")}</DefaultText> {labels.getLabel(lang,"Claim_DEN_remark2")}</DefaultText>
          : (this.state.stat==5 && this.state.data!=null && this.state.data.claimInfo.length>0) ?
          <DefaultText style={[SliderPersonal.personalRemark,{marginTop:5}]}><DefaultText style={{textDecorationLine:'underline'}}>{labels.getLabel(lang,"Claim_AME_remark1")}</DefaultText> {labels.getLabel(lang,"Claim_AME_remark2")}</DefaultText>
          : <DefaultText></DefaultText>
          }

          <View style={{paddingLeft: 10,marginTop:5,marginBottom:10}}>
            <DefaultText>{/* footer */} </DefaultText> 
          </View>
        </ScrollView>
      </View>
    )
  }

  genIPDBox = () => {
    let lang = this.props.language;
    let data = this.state.data
    return(
      <View>
        {(!this.state.refreshing && data!=null) &&
        <View style={styles.dataContainerSub2}>
          <TouchableOpacity style={{flexDirection:'row'}} onPress={() => this.setState({stat:0,data:null})}>
            <Icon
              type="entypo"
              name="chevron-left"
              containerStyle={{justifyContent:'flex-end'}}
            />
            <View style={{justifyContent:'flex-end'}}><DefaultText bold style={styles.dataHead}>{data.visityType}</DefaultText></View>
          </TouchableOpacity>
          { data.claimInfo.length > 0 &&
          <View style={{justifyContent:'center',flex:1,flexGrow:1,paddingLeft:20}}>
            <DefaultText bold style={[styles.data]}>{labels.getLabel(lang,"Claim_IPD_info1")}</DefaultText>
            {/*<View>
              <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_IPD_info2_1")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimTotNo} {labels.getLabel(lang,"Claim_IPD_info2_2")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimTotAmt} {labels.getLabel(lang,"baht")}</DefaultText></View>
              </View>
              <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_IPD_info3_1")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimOverTotNo} {labels.getLabel(lang,"Claim_IPD_info3_2")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimOverTotAmt} {labels.getLabel(lang,"baht")}</DefaultText></View>
              </View>
              <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_IPD_info4_1")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimMatTotNo} {labels.getLabel(lang,"Claim_IPD_info4_2")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimMatTotAmt} {labels.getLabel(lang,"baht")}</DefaultText></View>
              </View>
            </View>*/}
            <View style={{flexDirection:'row',alignSelf:'flex-end'}}>
              <View style={{ alignSelf: 'stretch' }}>
                <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_IPD_info2_1")}</DefaultText></View>
                {/* <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_IPD_info5_1")}</DefaultText></View> */}
                <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_IPD_info3_1")}</DefaultText></View>
                <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_IPD_info4_1")}</DefaultText></View>
              </View>
              <View style={{ alignSelf: 'stretch', marginLeft:2 }}>
                <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimTotNo} {labels.getLabel(lang,"Claim_IPD_info2_2")}</DefaultText></View>
                {/* <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimTotNo} {labels.getLabel(lang,"Claim_IPD_info5_2")}</DefaultText></View> */}
                <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimOverTotNo} {labels.getLabel(lang,"Claim_IPD_info3_2")}</DefaultText></View>
                <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimMatTotNo} {labels.getLabel(lang,"Claim_IPD_info4_2")}</DefaultText></View>
                
              </View>
              <View style={{ alignSelf: 'stretch', marginLeft:2 }}>
                <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimTotAmt} {labels.getLabel(lang,"baht")}</DefaultText></View>
                {/* <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimTotAmt} {labels.getLabel(lang,"baht")}</DefaultText></View> */}
                <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimOverTotAmt} {labels.getLabel(lang,"baht")}</DefaultText></View>
                <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimMatTotAmt} {labels.getLabel(lang,"baht")}</DefaultText></View>
              </View>
            </View>
          </View>
          }
        </View>
        }
        {data != null && 
          <FlatList
            data={data.claimInfo}
            renderItem={({item,index}) => this.genSubBox(item,index,"IPD")}
            keyExtractor={(item) => item.id}
            refreshing={this.state.refreshing}
            ListEmptyComponent={!this.state.refreshing && <NodataList noIcon/>}
          />
        }
      </View>
    )
  }

  genOPDBox = () => {
    let lang = this.props.language;
    let data = this.state.data
    return(
      <View>
        {(!this.state.refreshing && data!=null) &&
        <View style={styles.dataContainerSub2}>
          <TouchableOpacity style={{flexDirection:'row'}} onPress={() => this.setState({stat:0,data:null})}>
            <Icon
              type="entypo"
              name="chevron-left"
              containerStyle={{justifyContent:'flex-end'}}
            />
            <View style={{justifyContent:'flex-end'}}><DefaultText bold style={styles.dataHead}>{data.visityType}</DefaultText></View>
          </TouchableOpacity>
          { data.claimInfo.length > 0 &&
          <View style={{justifyContent:'center',flex:1,flexGrow:1,paddingLeft:20}}>
            <DefaultText bold style={[styles.data]}>{labels.getLabel(lang,"Claim_OPD_info1")}</DefaultText>
            {/*<View>
              <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_OPD_info2_1")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimAS400No} {labels.getLabel(lang,"Claim_OPD_info2_2")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimAS400Amt} {labels.getLabel(lang,"baht")}</DefaultText></View>
              </View>
              <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_OPD_info3_1")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimWebHospNo} {labels.getLabel(lang,"Claim_OPD_info3_2")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimWebHospAmt} {labels.getLabel(lang,"baht")}</DefaultText></View>
              </View>
              <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                {((data.claimRemainNo != null && data.claimRemainNo != "" && Number(data.claimRemainNo.replace(/,/g, '')) != 0) || (data.claimRemainAmt != null && data.claimRemainAmt != "" && Number(data.claimRemainAmt.replace(/,/g, '')) != 0)) &&
                  <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_OPD_info4_1")}</DefaultText></View>
                }
                {(data.claimRemainNo != null && data.claimRemainNo != "" && Number(data.claimRemainNo.replace(/,/g, '')) != 0 && data.claimRemainAmt != null && data.claimRemainAmt != "" && Number(data.claimRemainAmt.replace(/,/g, '')) != 0) ? 
                  <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainNo} {labels.getLabel(lang,"Claim_OPD_info4_2")} /</DefaultText></View>
                  : (data.claimRemainNo != null && data.claimRemainNo != "" && Number(data.claimRemainNo.replace(/,/g, '')) != 0) ?
                  <View style={{ flex: 2, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainNo} {labels.getLabel(lang,"Claim_OPD_info4_2")}</DefaultText></View>
                  : (data.claimRemainAmt != null && data.claimRemainAmt != "" && Number(data.claimRemainAmt.replace(/,/g, '')) != 0) &&
                  <View style={{ flex: 2, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainAmt} {labels.getLabel(lang,"Claim_OPD_info4_3")}</DefaultText></View>
                }
                {(data.claimRemainNo != null && data.claimRemainNo != "" && Number(data.claimRemainNo.replace(/,/g, '')) != 0 && data.claimRemainAmt != null && data.claimRemainAmt != "" && Number(data.claimRemainAmt.replace(/,/g, '')) != 0) && 
                  <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainAmt} {labels.getLabel(lang,"Claim_OPD_info4_3")}</DefaultText></View>
                }
              </View>*/}
            <View style={{flexDirection:'row',alignSelf:'flex-end'}}>
              <View style={{ alignSelf: 'stretch' }}>
                <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_OPD_info2_1")}</DefaultText></View>
                <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_OPD_info3_1")}</DefaultText></View>
                {((data.claimRemainNo != null && data.claimRemainNo != "") || (data.claimRemainAmt != null && data.claimRemainAmt != "")) &&
                  <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_OPD_info4_1")}</DefaultText></View>
                } 
              </View>
              <View style={{ alignSelf: 'stretch', marginLeft:2 }}>
                <View style={{flexDirection:'row',alignSelf:'flex-end'}}>
                  <View style={{ alignSelf: 'stretch' }}>
                    <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimAS400No} {labels.getLabel(lang,"Claim_OPD_info2_2")}</DefaultText></View>
                    <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimWebHospNo} {labels.getLabel(lang,"Claim_OPD_info3_2")}</DefaultText></View>
                    {(data.claimRemainNo != null && data.claimRemainNo != ""&& data.claimRemainAmt != null && data.claimRemainAmt != "") &&
                      <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainNo} {labels.getLabel(lang,"Claim_OPD_info4_2")} /</DefaultText></View>
                    }
                  </View>
                  <View style={{ alignSelf: 'stretch', marginLeft:2 }}>
                    <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimAS400Amt} {labels.getLabel(lang,"baht")}</DefaultText></View>
                    <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimWebHospAmt} {labels.getLabel(lang,"baht")}</DefaultText></View>
                    {(data.claimRemainNo != null && data.claimRemainNo != ""&& data.claimRemainAmt != null && data.claimRemainAmt != "") &&
                      <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainAmt} {labels.getLabel(lang,"Claim_OPD_info4_3")}</DefaultText></View>
                    }
                  </View>
                </View>
                {((data.claimRemainAmt == null || data.claimRemainAmt == "") && data.claimRemainNo != null && data.claimRemainNo != "") &&
                  <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainNo} {labels.getLabel(lang,"Claim_OPD_info4_2")}</DefaultText></View>
                }
                {((data.claimRemainNo == null || data.claimRemainNo == "") && data.claimRemainAmt != null && data.claimRemainAmt != "") &&
                  <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainAmt} {labels.getLabel(lang,"Claim_OPD_info4_3")}</DefaultText></View>
                }
              </View>
            </View>
          </View>
          }
        </View>
        }
        {data != null && 
          <FlatList
            data={data.claimInfo}
            renderItem={({item,index}) => this.genSubBox(item,index,"OPD")}
            keyExtractor={(item) => item.id}
            refreshing={this.state.refreshing}
            ListEmptyComponent={!this.state.refreshing && <NodataList noIcon/>}
          />
        }
      </View>
    )
  }

  genLABBox = () => {
    let lang = this.props.language;
    let data = this.state.data
    return(
      <View>
        {(!this.state.refreshing && data!=null) &&
        <View style={styles.dataContainerSub2}>
          <TouchableOpacity style={{flexDirection:'row'}} onPress={() => this.setState({stat:0,data:null})}>
            <Icon
              type="entypo"
              name="chevron-left"
              containerStyle={{justifyContent:'flex-end'}}
            />
            <View style={{justifyContent:'flex-end'}}><DefaultText bold style={styles.dataHead}>{data.visityType}</DefaultText></View>
          </TouchableOpacity>
          { data.claimInfo.length > 0 &&
          <View style={{justifyContent:'center',flex:1,flexGrow:1,paddingLeft:20}}>
            <DefaultText bold style={[styles.data]}>{labels.getLabel(lang,"Claim_LAB_info1")}</DefaultText>
            {/*<View>
              <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_LAB_info2_1")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimAS400No} {labels.getLabel(lang,"Claim_LAB_info2_2")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimAS400Amt} {labels.getLabel(lang,"baht")}</DefaultText></View>
              </View>
              <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_LAB_info3_1")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimWebHospNo} {labels.getLabel(lang,"Claim_LAB_info3_2")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimWebHospAmt} {labels.getLabel(lang,"baht")}</DefaultText></View>
              </View>
              <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                {((data.claimRemainNo != null && data.claimRemainNo != "" && Number(data.claimRemainNo.replace(/,/g, '')) != 0) || (data.claimRemainAmt != null && data.claimRemainAmt != "" && Number(data.claimRemainAmt.replace(/,/g, '')) != 0)) && 
                  <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_LAB_info4_1")}</DefaultText></View>
                }
                {(data.claimRemainNo != null && data.claimRemainNo != "" && Number(data.claimRemainNo.replace(/,/g, '')) != 0 && data.claimRemainAmt != null && data.claimRemainAmt != "" && Number(data.claimRemainAmt.replace(/,/g, '')) != 0) ? 
                  <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainNo} {labels.getLabel(lang,"Claim_LAB_info4_2")} /</DefaultText></View>
                  : (data.claimRemainNo != null && data.claimRemainNo != "" && Number(data.claimRemainNo.replace(/,/g, '')) != 0) ?
                  <View style={{ flex: 2, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainNo} {labels.getLabel(lang,"Claim_LAB_info4_2")}</DefaultText></View>
                  : (data.claimRemainAmt != null && data.claimRemainAmt != "" && Number(data.claimRemainAmt.replace(/,/g, '')) != 0) &&
                  <View style={{ flex: 2, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainAmt} {labels.getLabel(lang,"Claim_LAB_info4_3")}</DefaultText></View>
                }
                {(data.claimRemainNo != null && data.claimRemainNo != "" && Number(data.claimRemainNo.replace(/,/g, '')) != 0 && data.claimRemainAmt != null && data.claimRemainAmt != "" && Number(data.claimRemainAmt.replace(/,/g, '')) != 0) && 
                  <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainAmt} {labels.getLabel(lang,"Claim_LAB_info4_3")}</DefaultText></View>
                }
              </View>
            </View>*/}
            <View style={{flexDirection:'row',alignSelf:'flex-end'}}>
              <View style={{ alignSelf: 'stretch' }}>
                <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_LAB_info2_1")}</DefaultText></View>
                <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_LAB_info3_1")}</DefaultText></View>
                {((data.claimRemainNo != null && data.claimRemainNo != "") || (data.claimRemainAmt != null && data.claimRemainAmt != "")) &&
                  <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_LAB_info4_1")}</DefaultText></View>
                } 
              </View>
              <View style={{ alignSelf: 'stretch', marginLeft:2 }}>
                <View style={{flexDirection:'row',alignSelf:'flex-end'}}>
                  <View style={{ alignSelf: 'stretch' }}>
                    <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimAS400No} {labels.getLabel(lang,"Claim_LAB_info2_2")}</DefaultText></View>
                    <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimWebHospNo} {labels.getLabel(lang,"Claim_LAB_info3_2")}</DefaultText></View>
                    {(data.claimRemainNo != null && data.claimRemainNo != "" && data.claimRemainAmt != null && data.claimRemainAmt != "") &&
                      <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainNo} {labels.getLabel(lang,"Claim_LAB_info4_2")} /</DefaultText></View>
                    }
                  </View>
                  <View style={{ alignSelf: 'stretch', marginLeft:2 }}>
                    <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimAS400Amt} {labels.getLabel(lang,"baht")}</DefaultText></View>
                    <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimWebHospAmt} {labels.getLabel(lang,"baht")}</DefaultText></View>
                    {(data.claimRemainNo != null && data.claimRemainNo != "" && data.claimRemainAmt != null && data.claimRemainAmt != "") &&
                      <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainAmt} {labels.getLabel(lang,"Claim_LAB_info4_3")}</DefaultText></View>
                    }
                  </View>
                </View>
                {((data.claimRemainAmt == null || data.claimRemainAmt == "") && data.claimRemainNo != null && data.claimRemainNo != "") &&
                  <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainNo} {labels.getLabel(lang,"Claim_LAB_info4_2")}</DefaultText></View>
                }
                {((data.claimRemainNo == null || data.claimRemainNo == "") && data.claimRemainAmt != null && data.claimRemainAmt != "") &&
                  <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainAmt} {labels.getLabel(lang,"Claim_LAB_info4_3")}</DefaultText></View>
                }
              </View>
            </View>
          </View>
          }
        </View>
        }
        {data != null && 
          <FlatList
            data={data.claimInfo}
            renderItem={({item,index}) => this.genSubBox(item,index,"LAB")}
            keyExtractor={(item) => item.id}
            refreshing={this.state.refreshing}
            ListEmptyComponent={!this.state.refreshing && <NodataList noIcon/>}
          />
        }
      </View>
    )
  }

  genDENBox = () => {
    let lang = this.props.language;
    let data = this.state.data
    return(
      <View>
        {(!this.state.refreshing && data!=null) &&
        <View style={styles.dataContainerSub2}>
          <TouchableOpacity style={{flexDirection:'row'}} onPress={() => this.setState({stat:0,data:null})}>
            <Icon
              type="entypo"
              name="chevron-left"
              containerStyle={{justifyContent:'flex-end'}}
            />
            <View style={{justifyContent:'flex-end'}}><DefaultText bold style={styles.dataHead}>{data.visityType}</DefaultText></View>
          </TouchableOpacity>
          { data.claimInfo.length > 0 &&
          <View style={{justifyContent:'center',flex:1,flexGrow:1,paddingLeft:20}}>
            <DefaultText bold style={[styles.data]}>{labels.getLabel(lang,"Claim_DEN_info1")}</DefaultText>
            {/*<View>
              <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_DEN_info2_1")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimAS400No}  {labels.getLabel(lang,"Claim_DEN_info2_2")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimAS400Amt} {labels.getLabel(lang,"baht")}</DefaultText></View>
              </View>
              <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_DEN_info3_1")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimWebHospNo} {labels.getLabel(lang,"Claim_DEN_info3_2")}</DefaultText></View>
                <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimWebHospAmt} {labels.getLabel(lang,"baht")}</DefaultText></View>
              </View>
              <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                {((data.claimRemainNo != null && data.claimRemainNo != "" && Number(data.claimRemainNo.replace(/,/g, '')) != 0) || (data.claimRemainAmt != null && data.claimRemainAmt != "" && Number(data.claimRemainAmt.replace(/,/g, '')) != 0)) && 
                  <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_DEN_info4_1")}</DefaultText></View>
                }
                {(data.claimRemainNo != null && data.claimRemainNo != "" && Number(data.claimRemainNo.replace(/,/g, '')) != 0 && data.claimRemainAmt != null && data.claimRemainAmt != "" && Number(data.claimRemainAmt.replace(/,/g, '')) != 0) ? 
                  <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainNo} {labels.getLabel(lang,"Claim_DEN_info4_2")} /</DefaultText></View>
                  : (data.claimRemainNo != null && data.claimRemainNo != "" && Number(data.claimRemainNo.replace(/,/g, '')) != 0) ?
                  <View style={{ flex: 2, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainNo} {labels.getLabel(lang,"Claim_DEN_info4_2")}</DefaultText></View>
                  : (data.claimRemainAmt != null && data.claimRemainAmt != "" && Number(data.claimRemainAmt.replace(/,/g, '')) != 0) &&
                  <View style={{ flex: 2, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainAmt} {labels.getLabel(lang,"Claim_DEN_info4_3")}</DefaultText></View>
                }
                {(data.claimRemainNo != null && data.claimRemainNo != "" && Number(data.claimRemainNo.replace(/,/g, '')) != 0 && data.claimRemainAmt != null && data.claimRemainAmt != "" && Number(data.claimRemainAmt.replace(/,/g, '')) != 0) && 
                  <View style={{ flex: 1, alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainAmt} {labels.getLabel(lang,"Claim_DEN_info4_3")}</DefaultText></View>
                }
              </View>
            </View>*/}
            <View style={{flexDirection:'row',alignSelf:'flex-end'}}>
              <View style={{ alignSelf: 'stretch' }}>
                <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_DEN_info2_1")}</DefaultText></View>
                <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_DEN_info3_1")}</DefaultText></View>
                {((data.claimRemainNo != null && data.claimRemainNo != "") || (data.claimRemainAmt != null && data.claimRemainAmt != "")) &&
                  <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{labels.getLabel(lang,"Claim_DEN_info4_1")}</DefaultText></View>
                } 
              </View>
              <View style={{ alignSelf: 'stretch', marginLeft:2 }}>
                <View style={{flexDirection:'row',alignSelf:'flex-end'}}>
                  <View style={{ alignSelf: 'stretch' }}>
                    <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimAS400No} {labels.getLabel(lang,"Claim_DEN_info2_2")}</DefaultText></View>
                    <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimWebHospNo} {labels.getLabel(lang,"Claim_DEN_info3_2")}</DefaultText></View>
                    {(data.claimRemainNo != null && data.claimRemainNo != "" && data.claimRemainAmt != null && data.claimRemainAmt != "") &&
                      <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainNo} {labels.getLabel(lang,"Claim_DEN_info4_2")} /</DefaultText></View>
                    }
                  </View>
                  <View style={{ alignSelf: 'stretch', marginLeft:2 }}>
                    <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimAS400Amt} {labels.getLabel(lang,"baht")}</DefaultText></View>
                    <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimWebHospAmt} {labels.getLabel(lang,"baht")}</DefaultText></View>
                    {(data.claimRemainNo != null && data.claimRemainNo != "" && data.claimRemainAmt != null && data.claimRemainAmt != "") &&
                      <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainAmt} {labels.getLabel(lang,"Claim_DEN_info4_3")}</DefaultText></View>
                    }
                  </View>
                </View>
                {((data.claimRemainAmt == null || data.claimRemainAmt == "") && data.claimRemainNo != null && data.claimRemainNo != "") &&
                  <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainNo} {labels.getLabel(lang,"Claim_DEN_info4_2")}</DefaultText></View>
                }
                {((data.claimRemainNo == null || data.claimRemainNo == "") && data.claimRemainAmt != null && data.claimRemainAmt != "") &&
                  <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.claimRemainAmt} {labels.getLabel(lang,"Claim_DEN_info4_3")}</DefaultText></View>
                }
              </View>
            </View>
          </View>
          }
        </View>
        }
        {data != null && 
          <FlatList
            data={data.claimInfo}
            renderItem={({item,index}) => this.genSubBox(item,index,"DEN")}
            keyExtractor={(item) => item.id}
            refreshing={this.state.refreshing}
            ListEmptyComponent={!this.state.refreshing && <NodataList noIcon/>}
          />
        }
      </View>
    )
  }

  genAMEBox = () => {
    let lang = this.props.language;
    let data = this.state.data
    return(
      <View>
        {(!this.state.refreshing && data!=null) &&
          <FlatList
            data={data.claimHead}
            renderItem={({item,index}) => this.genAMEHeadBox(item,index,data.visityType,data.claimHead.length)}
            keyExtractor={(item) => item.id}
            ListEmptyComponent={(
              <View style={styles.dataContainerSub2}>
                <TouchableOpacity style={{flexDirection:'row'}} onPress={() => this.setState({stat:0,data:null})}>
                  <Icon
                    type="entypo"
                    name="chevron-left"
                    containerStyle={{justifyContent:'flex-end'}}
                  />
                  <View style={{justifyContent:'flex-end'}}><DefaultText bold style={styles.dataHead}>{data.visityType}</DefaultText></View>
                </TouchableOpacity>
              </View>
            )}
          />
        }
        {data != null && 
          <FlatList
            data={data.claimInfo}
            renderItem={({item,index}) => this.genSubBox(item,index,"AME")}
            keyExtractor={(item) => item.id}
            refreshing={this.state.refreshing}
            ListEmptyComponent={!this.state.refreshing && <NodataList noIcon/>}
          />
        }
      </View>
    )
  }

  genAMEHeadBox = (data,index,visityType,dataCount) => {
    let lang = this.props.language;
    let dataLabel = []
    let dataDate = []
    let dataAmt = []
    let count = data.visitDTAmt.length
    let objLabel = {}
    let objDate = {}
    let objAmt = {}
    objLabel['id'] = index+"L0"
    objDate['id'] = index+"D0"
    objAmt['id'] = index+"A0"
    objLabel['label'] = labels.getLabel(lang,"Claim_AME_info2_1")
    objDate['date'] = data.incurredDate
    objAmt['amt'] = " "
    dataLabel.push(objLabel)
    dataDate.push(objDate)
    dataAmt.push(objAmt)
    
    for(let i=0;i<count;i++){
      let objLabel2 = {}
      let objDate2 = {}
      let objAmt2 = {}
      objLabel2['id'] = index+"L" + (i+1)
      objDate2['id'] = index+"D" + (i+1)
      objAmt2['id'] = index+"A" + (i+1)
      if(i==0){
        objLabel2['label'] = labels.getLabel(lang,"Claim_AME_info3_1")
      }else{
        objLabel2['label'] = " "
      }
      objDate2['date'] = data.visitDTAmt[i].visitDate
      objAmt2['amt'] = data.visitDTAmt[i].incurredAmt + " " + labels.getLabel(lang,"Claim_AME_info3_2")
      dataLabel.push(objLabel2)
      dataDate.push(objDate2)
      dataAmt.push(objAmt2)
    }

    let objLabel3 = {}
    let objDate3 = {}
    let objAmt3 = {}
    
    objLabel3['id'] = index+"L" + (count+1)
    objDate3['id'] = index+"D" + (count+1)
    objAmt3['id'] = index+"A" + (count+1)
    objLabel3['label'] = labels.getLabel(lang,"Claim_AME_info4_1")
    objDate3['date'] = " "
    objAmt3['amt'] = data.totalAmt + " " + labels.getLabel(lang,"Claim_AME_info4_2")
    dataLabel.push(objLabel3)
    dataDate.push(objDate3)
    dataAmt.push(objAmt3)

    let objLabel4 = {}
    let objDate4 = {}
    let objAmt4 = {}
    objLabel4['id'] = index+"L" + (count+2)
    objDate4['id'] = index+"D" + (count+2)
    objAmt4['id'] = index+"D" + (count+2)
    objLabel4['label'] = labels.getLabel(lang,"Claim_AME_info5_1")
    objDate4['date'] = " "
    objAmt4['amt'] = data.remainAmt + " " + labels.getLabel(lang,"Claim_AME_info5_2")
    dataLabel.push(objLabel4)
    dataDate.push(objDate4)
    dataAmt.push(objAmt4)
    
    return(
      <View style={styles.dataContainerSub2}>
        { index==0 ?
        <TouchableOpacity style={{flexDirection:'row'}} onPress={() => this.setState({stat:0,data:null})}>
          <Icon
            type="entypo"
            name="chevron-left"
            containerStyle={{justifyContent:'flex-start'}}
          />
          <View style={{justifyContent:'flex-start'}}><DefaultText bold style={styles.dataHead}>{visityType}</DefaultText></View>
        </TouchableOpacity>
        :
        <View><DefaultText> </DefaultText></View>
        }
        { data != null &&
        <View style={{justifyContent:'center',flex:1,flexGrow:1,paddingLeft:20}}>
          { index==0 &&
            <DefaultText bold style={[styles.data]}>{labels.getLabel(lang,"Claim_AME_info1")}</DefaultText>
          }
          {data.visitDTAmt.length > 0 &&
            <View style={{flexDirection:'row',alignSelf:'flex-end'}}>
              <View style={{ alignSelf: 'stretch' }}>
                <FlatList
                  data={dataLabel}
                  renderItem={({item,index}) => this.genAMELabelSubBox(item,index)}
                  keyExtractor={(item) => item.id}
                />
              </View>
              <View style={{flexDirection:'row'}}>
                <View style={{flexDirection:'row',alignSelf:'flex-end'}}>
                  <View style={{ alignSelf: 'stretch', marginLeft:2 }}>
                    <FlatList
                      data={dataDate}
                      renderItem={({item,index}) => this.genAMEDateSubBox(item,index,count)}
                      keyExtractor={(item) => item.id}
                      listKey={(item) => item.id}
                    />
                  </View>
                  <View style={{ alignSelf: 'stretch', marginLeft:2 }}>
                    <FlatList
                      data={dataDate}
                      renderItem={({item,index}) => this.genAMESeparateSubBox(item,index,count)}
                      keyExtractor={(item) => item.id}
                      listKey={(item) => "S"+ item.id}
                    />
                  </View>
                  <View style={{ alignSelf: 'stretch', marginLeft:2 }}>
                    <FlatList
                      data={dataAmt}
                      renderItem={({item,index}) => this.genAMEAmtSubBox(item,index,count)}
                      keyExtractor={(item) => item.id}
                      listKey={(item) => item.id}
                    />
                  </View>
                </View>
              </View>
            </View>
          }
          { index!=dataCount-1 &&
            <DefaultText bold style={[styles.data]}>--------------------------------</DefaultText>
          }
        </View>
        }
      </View>
    )
  }

  genAMELabelSubBox = (data,index) => {
    let lang = this.props.language;
    return(
      <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.label}</DefaultText></View>
    )
  }
  
  genAMEDateSubBox = (data,index,count) => {
    let lang = this.props.language;
    return(
      <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.date}</DefaultText></View>
    )
  }

  genAMESeparateSubBox = (data,index,count) => {
    let lang = this.props.language;
    if(index==0||index==count+1||index==count+2)
    return(
      <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}> </DefaultText></View>
    )
    else
    return(
      <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>:</DefaultText></View>
    )
  }
  
  genAMEAmtSubBox = (data,index,count) => {
    let lang = this.props.language;
    return(
      <View style={{ alignSelf: 'stretch' }} ><DefaultText style={styles.data}>{data.amt}</DefaultText></View>
    )
  }
  
  genSubBox = (data,index,type) =>{
    let lang = this.props.language;
    let claimDiagnose = lang =='th' ? data.claimDiagnoseTH : data.claimDiagnoseEN
    return(
    <View style={[SliderPersonal.dataContainer3,{marginTop:10}]}>
      <View style={{flex:1,padding:10}}>

        <View style={[styles.dataContainerSub3]}>
            <DefaultText style={SliderPersonal.personalLabel}>{labels.getLabel(lang,"Claim_product")} :</DefaultText>
            <DefaultText style={SliderPersonal.personalData}>{data.productCode}</DefaultText>
        </View>

        {type=="AME" &&
        <View style={[styles.dataContainerSub3]}>
            <DefaultText style={SliderPersonal.personalLabel}>{labels.getLabel(lang,"Claim_incurredDate")} :</DefaultText>
            <DefaultText style={SliderPersonal.personalData}>{data.incurredDate}</DefaultText>
        </View>
        }
        {type=="IPD" ?
        <View style={[styles.dataContainerSub3]}>
            <DefaultText style={SliderPersonal.personalLabel}>{labels.getLabel(lang,"Claim_date")} :</DefaultText>
            <DefaultText style={SliderPersonal.personalData}>{data.startDate} {(data.endDate != null && data.endDate != "") && "-"} {data.endDate}</DefaultText>
        </View>
        : type=="AME" ?
        <View style={[styles.dataContainerSub3]}>
            <DefaultText style={SliderPersonal.personalLabel}>{labels.getLabel(lang,"Claim_date")} :</DefaultText>
            <DefaultText style={SliderPersonal.personalData}>{data.visitDate}</DefaultText>
        </View>
        :
        <View style={[styles.dataContainerSub3]}>
            <DefaultText style={SliderPersonal.personalLabel}>{labels.getLabel(lang,"Claim_date")} :</DefaultText>
            <DefaultText style={SliderPersonal.personalData}>{data.startDate}</DefaultText>
        </View>
        }

        <View style={[styles.dataContainerSub3]}>
            <DefaultText style={SliderPersonal.personalLabel}>{labels.getLabel(lang,"Claim_hospital")} :</DefaultText>
            <DefaultText style={SliderPersonal.personalData}>{lang =='th' ? data.claimHospitalTH : data.claimHospitalEN}</DefaultText>
        </View>

        <View style={[styles.dataContainerSub3]}>
            <DefaultText style={SliderPersonal.personalLabel}>{labels.getLabel(lang,"Claim_diagnosis")} :</DefaultText>
            <DefaultText style={[SliderPersonal.personalData]}>{claimDiagnose}</DefaultText>
        </View>

        <View style={[styles.dataContainerSub3]}>
            <DefaultText style={SliderPersonal.personalLabel}>{labels.getLabel(lang,"Claim_claimType")} :</DefaultText>
            <DefaultText style={SliderPersonal.personalData}>{data.claimTypeDesc}</DefaultText>
        </View>

        <View style={[styles.dataContainerSub3]}>
            <DefaultText style={SliderPersonal.personalLabel}>{labels.getLabel(lang,"Claim_authorizedDate")} :</DefaultText>
            <DefaultText style={SliderPersonal.personalData}>{data.claimAuthDT == "99/99/9999" ? " " : data.claimAuthDT}</DefaultText>
        </View>

        <View style={[styles.dataContainerSub3]}>
            <DefaultText style={SliderPersonal.personalLabel}>{labels.getLabel(lang,"Claim_status")} :</DefaultText>
            <DefaultText style={SliderPersonal.personalData}>{lang =='th' ? data.claimStatTH : data.claimStatEN}</DefaultText>
        </View>

        <View style={[styles.dataContainerSub3]}>
            <DefaultText style={SliderPersonal.personalLabel}>{labels.getLabel(lang,"Claim_claim")} :</DefaultText>
            <DefaultText style={SliderPersonal.personalData}>{data.claimAmt}</DefaultText>
        </View>

        <View style={[styles.dataContainerSub3]}>
            <DefaultText style={SliderPersonal.personalLabel}>{labels.getLabel(lang,"Claim_approve")} :</DefaultText>
            <DefaultText style={SliderPersonal.personalData}>{data.claimApprvAmt}</DefaultText>
        </View>

        <View style={[styles.dataContainerSub3]}>
            <DefaultText style={SliderPersonal.personalLabel}>{labels.getLabel(lang,"Claim_surplus")} :</DefaultText>
            <DefaultText style={SliderPersonal.personalData}>{data.claimOverAmt}</DefaultText>
        </View>
        {((type=="IPD"||type=="OPD"||type=="LAB"||type=="DEN"||type=="AME") && data.claimType=="REG") &&
          <View style={[styles.dataContainerSub3]}>
              <DefaultText style={SliderPersonal.personalRemark}>{labels.getLabel(lang,"Claim_remarkCash")}</DefaultText>
          </View>
        }
      </View>

    </View>
    )
  }

  render() {
    let lang = this.props.language;
    if (this.state.EnableView) {
    return (
      <View style={MainStyles.main}>
        <CustomUserInactivity onAction={() => this.props.navigation.navigate("Home")}>
          <View
            style={[
              MainStyles.container,
              { paddingTop: 5 }
            ]}
          >
          { this.props.memberData==null || this.props.memberData['memberInfo'].length==0 ?
            <NodataList/>
            :
            this.state.apiData != null &&
            <Carousel
              ref={c => (this._slider1Ref = c)}
              data={this.state.apiData}
              renderItem={this.genBox}
              sliderWidth={sliderWidth}
              itemWidth={sliderWidth}
              hasParallaxImages={true}
              firstItem={0}
              inactiveSlideScale={1}
              inactiveSlideOpacity={0}
              loop={false}
              autoplay={false}
              onBeforeSnapToItem={index => this.onBeforeSnapToItem(index)}
              onSnapToItem={index => this.onSnapToItem(index)}
              windowSize={1}
              scrollEnabled={false}
              removeClippedSubviews={false}
              enableSnap={false}
            />
          }
            {/*this.genBox(0,1)*/}
          </View>
          
        </CustomUserInactivity>
      </View>
    );
    }else {
      return (<View></View>)
    }
  }
}

const mapStateToProps = ({ auth }) => {
    const { language, lastScreen, loginStatus, notification, notificationList, memberData } = auth;
    return { language, lastScreen, loginStatus, notification, notificationList, memberData };
  };
  
  export default connect(
    mapStateToProps,
    {
      languageChanged,
      lastScreenChanged,
      notificationChanged,
      notificationListChanged,
      logOut,
    }
  )(ClaimScreen);

  
const styles = StyleSheet.create({
  noti:{
    position:'absolute',
    right:-1,
    top:-1,
    justifyContent:'center',
    alignContent:'center',
    backgroundColor:'white',
    width:25,
    height:25,
    borderBottomLeftRadius:5,
    borderLeftWidth: 1,
    borderBottomWidth: 1,
    borderLeftColor: Colors.cardBorder,
    borderBottomColor: Colors.cardBorder,
  },
  dataContainerSub:{
    paddingTop:20,
    paddingLeft:5,
    paddingRight:5,
    paddingBottom:20,
    //flexDirection:"row",
    //justifyContent:"space-between",
  },
  dataContainerSub2:{
    paddingLeft:0,
    paddingRight:0,
    flexDirection:"row",
    justifyContent:"space-between",
    flex:1
  },
  dataContainerSub3:{
    flexDirection:"row",
    justifyContent:"space-between"
  },
  data:{
    flex:1,
    textAlign:'right',
    fontSize:12,
    flexWrap: 'wrap'
  },
  dataHead:{
    // fontWeight:'500',
    color:Colors.cardLabel,
    fontSize:20,
    textAlign:'center',
  }
});
