import React, { Component } from "react";
import {
  ActivityIndicator,
  Alert,
  Dimensions,
  Image,
  PixelRatio,
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  YellowBox
} from "react-native";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import LoginStyles from "../styles/LoginStyle.style";
import * as labels from "../constants/label";
import PinView from "../components/PinView";
import { Icon } from "react-native-elements";
import Modal from "react-native-modal";
import {
  eCardDataClear,
  logIn,
  logOut,
  memberDataChanged,
  notificationChanged,
  notificationListChanged,
  hasECardChanged,
  pinFailed,
} from "../actions";
import * as utils from "../functions";
import DefaultText from "../components/DefaultText"
import _ from 'lodash';
import Constants from 'expo-constants';
import * as LocalAuthentication from 'expo-local-authentication';

class LoginPinScreen extends Component {
  constructor(props) {
    super(props);
    this.onPinComplete = this.onPinComplete.bind(this)
    this.state = {
      labelPin: "",
      policyNumberStore: "",
      memberNoStore: "",
      tokenStore: "",
      pinStore: "",
      loginProcess: false,

      localAuthentication: false,
      isModalVisible: false,
    };

    YellowBox.ignoreWarnings(["Can't call setState"]);
    const _console = _.clone(console);
    console.warn = message => {
      if (message.indexOf('Setting a timer') <= -1) {
        _console.warn(message);
      }
    };
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    handleAndroidBackButton(this.navigateHome);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start LoginPinSetUpScreen");
    this.setState({loginProcess: false})
    this.checkLocalAuthentication()
    this.getData("policyNumberStore")
    this.getData("memberNoStore")
    this.getData("passwordStore")
    this.getData("tokenStore")
    this.getData("pinStore")
  };

  checkLocalAuthentication = async () => {
		let result = await LocalAuthentication.hasHardwareAsync();
    let result2 = await LocalAuthentication.isEnrolledAsync();
    let result3 = await utils.retrieveData("localAuthenticationStore")
		if (result && result2 && result3 == "1") {
      this.handleTouchPress()
      // this.toggleModal()
      this.setState({localAuthentication:true})
    }else{
      this.setState({localAuthentication:false})
    }
  };

  navigateHome = () => {
    if(!this.state.loginProcess){
      this.props.navigation.navigate("Home");
    }
  };

  getData = async key => {
    let value = await utils.retrieveData(key)
    this.setState({[key]:value})
  }

  _login = async (clear) => {
    let lang = this.props.language
    let result=""
    let result2=""
    let token=""
    let fName=""
    let lName=""
    let name=""
    let tel=""
    let mail=""
    let lineId=""
    let xmlBody = utils.LoginBody(this.state.policyNumberStore,this.state.memberNoStore,"",this.state.tokenStore);
    let term = "FALSE"
    this.setState({loginProcess:true})
    console.log(`Request body: ${xmlBody}`);
    await fetch(utils.urlUserService, utils.genRequest(utils.LoginSOAPAction,xmlBody) )
      .then(response => response.text())
      .then(response => {
        result = utils.getResult(response)
        let errorCode = result["LoginResponse"][0]["LoginResult"][0]["a:errorCode"][0].toString()
        let errorDesc = ""
        if(lang=="th"){
          errorDesc = result["LoginResponse"][0]["LoginResult"][0]["a:errorDescTH"][0].toString()
        }else{
          errorDesc = result["LoginResponse"][0]["LoginResult"][0]["a:errorDescEN"][0].toString()
        }
        let needReRegister = result["LoginResponse"][0]["LoginResult"][0]["a:needReRegister"][0].toString()
        if(Number(errorCode)==0){
          if(needReRegister=="Y"){
            let passport = result["LoginResponse"][0]["LoginResult"][0]["a:citizenID"][0].toString()
            let dob = result["LoginResponse"][0]["LoginResult"][0]["a:dob"][0].toString()
            this.props.navigation.navigate("ReRegister",{
              policyNumber: this.state.policyNumber,
              memberNo: memberNo,
              passport: passport,
              dob: dob,
            })
          }else{
            term=result["LoginResponse"][0]["LoginResult"][0]["a:forceAcceptAgreement"][0]
            token=result["LoginResponse"][0]["LoginResult"][0]["a:token"][0]
            fName=result["LoginResponse"][0]["LoginResult"][0]["a:firstName"][0]
            lName=result["LoginResponse"][0]["LoginResult"][0]["a:lastName"][0]
            name=fName.trim()+" "+lName.trim()
            tel=result["LoginResponse"][0]["LoginResult"][0]["a:mobileNo"][0]
            mail=result["LoginResponse"][0]["LoginResult"][0]["a:email"][0]
            lineId=result["LoginResponse"][0]["LoginResult"][0]["a:lineID"][0]
            //////////////////////////////////
            utils.logInWithoutResetData(this.state.policyNumberStore,this.state.memberNoStore,token,name,tel,mail,lineId)
            this.onLogIn()
            //////////////////////////////////
            let xmlBody2 = utils.GetMemberBenefitBody(this.state.policyNumberStore,this.state.memberNoStore,token);
            console.log(`Request body: ${xmlBody2}`);
            fetch(utils.urlMemberService, utils.genRequest(utils.GetMemberBenefitSOAPAction,xmlBody2) )
              .then(response => response.text())
              .then(response => {
                result2 = utils.getResult(response)
                let errorCode = result2["GetMemberBenefitResponse"][0]["GetMemberBenefitResult"][0]["a:errorCode"][0].toString()
                let errorDesc = ""
                if(lang=="th"){
                  errorDesc = result2["GetMemberBenefitResponse"][0]["GetMemberBenefitResult"][0]["a:errorDescTH"][0].toString()
                }else{
                  errorDesc = result2["GetMemberBenefitResponse"][0]["GetMemberBenefitResult"][0]["a:errorDescEN"][0].toString()
                }
                if(Number(errorCode)==0){
                  let memberData = utils.GetMemberBenefitData(result2)
                  let noti = Number(memberData['claimTotNotify'])
                  let notiList = memberData['notificationList']
                  let hasECard = memberData['hasECard']
                  this._memberDataChanged(memberData)
                  this._notificationChanged(noti)
                  this._notificationListChanged(notiList)
                  this._hasECardChanged(hasECard)
                  this._pinFailed(0)
                  if(term=="TRUE"){
                    setTimeout(() => {
                      this.props.navigation.navigate("LoginTerm",{
                        policyNumber: this.state.policyNumberStore,
                        memberNo: this.state.memberNoStore,
                        nextScreen: this.props.lastScreen,
                      });}
                      ,1000)
                  }else{
                    setTimeout(() => {
                      this.props.navigation.navigate(this.props.lastScreen);}
                      ,1000)
                  }
                }else{
                  this.setState({loginProcess:false})
                  this.setState({labelPin:""})
                  Alert.alert(labels.getLabel(lang,"error"),errorDesc)
                  clear();
                }
              })
              .catch(err => {
                this.setState({loginProcess:false})
                this.setState({labelPin:""})
                console.log("fetch", err);
                Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
                clear();
              });
          }
          /////////////////////////////////
        }else if(Number(errorCode)==9){
          this.setState({loginProcess:false})
          this.setState({labelPin:""})
          Alert.alert(labels.getLabel(lang,"error"),errorDesc)
          this.props.logOut();
          this.props.eCardDataClear();
          utils.logOut()
          this._notificationChanged(0)
          this.props.navigation.navigate("Login")
        }else{
          this.setState({loginProcess:false})
          this.setState({labelPin:""})
          Alert.alert(labels.getLabel(lang,"error"),errorDesc)
          clear();
        }
      })
      .catch(err => {
        this.setState({loginProcess:false})
        this.setState({labelPin:""})
        console.log("fetch", err);
        Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
        clear();
      });
  }

  _memberDataChanged = (data) => {
    this.props.memberDataChanged(data);
  }

  _notificationChanged(noti){
    this.props.notificationChanged(noti)
  }

  _notificationListChanged(noti){
    this.props.notificationListChanged(noti)
  }

  _hasECardChanged = (data) => {
    this.props.hasECardChanged(data)
  }

  _pinFailed = (data) => {
    this.props.pinFailed(data)
  }

  onLogIn() {
    this.props.logIn();
  }

  onPinComplete = (inputtedPin, clear) => {
    if(inputtedPin == this.state.pinStore){
      //success
      this._login(clear)
      this.setState({labelPin:"touchIdSuccess"})
      //clear();
    }else{
      this.setState({labelPin:"enterPinFailed"})
      clear();
      if(this.props.numberOfPinFail == 4){
        this.props.logOut();
        this.props.eCardDataClear();
        utils.logOut()
        this._notificationChanged(0)
        this.props.navigation.navigate("Login")
      }else{
        this._pinFailed(this.props.numberOfPinFail+1)
      }
    }
  }

  handleTouchPress = () => {
    if (Constants.platform.android) {
      this._showAndroidAlert();
    } else {
      this.scanBiometrics();
    }
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible,labelPin:"" })
  }

  _showAndroidAlert = () => {
    this.setState({ isModalVisible: true,labelPin:"" })
    this.scanBiometrics();
  };

  _cancelAuthenticate = () => {
    if(Platform.OS === "android"){
      LocalAuthentication.cancelAuthenticate()
    }
  }

  scanBiometrics = async () => {
    let result = await LocalAuthentication.authenticateAsync({promptMessage :'Biometric Scan for setup loggin.'});
    //console.log(result)
    if (result.success) {
      // Alert.alert('authentication success');
      this.setState({labelPin:"touchIdSuccess"})
      utils.setLocalAuthenticationStore("1")
      this._login();
    } else {
      console.log(result.error)
      if(!result.error.includes("cancel") && !result.error.includes("unknown")){
        this.setState({labelPin:"touchIdFailed"})
        this._cancelAuthenticate()
        if(this.props.numberOfPinFail == 4){
          this.setState({ isModalVisible: false,labelPin:"" })
          this.props.logOut();
          this.props.eCardDataClear();
          utils.logOut()
          this._notificationChanged(0)
          this.props.navigation.navigate("Login")
        }else{
          this._pinFailed(this.props.numberOfPinFail+1)
          this.scanBiometrics()
        }
      }else if(result.error.includes("unknown")){
        this.setState({ isModalVisible: false,labelPin:"" })
      }else{
        // this.setState({isModalVisible: false,labelPin:""})
      }
    }
  };

  render() {
    let lang = this.props.language;

    return (
      <View style={LoginStyles.main}>
       <View style={[LoginStyles.paddingStatus,{backgroundColor:"white"}]}>
          <View style={{marginTop:60,marginBottom:20,alignItems:'center'}}>
            <DefaultText style={{color: Colors.bondiBlue,fontSize:18}}>{labels.getLabel(lang,"putPin")}</DefaultText>
          </View>
          <View style={{backgroundColor:'white',paddingTop:5}}>
            <PinView
              onComplete={this.onPinComplete.bind(this)}
              inputBgOpacity={1}
              pinLength={6}
              buttonBgColor={'white'}
              inputBgColor={'white'}
              inputActiveBgColor={Colors.bondiBlue}
              buttonTextColor={'black'}
              successfulText={labels.getLabel(lang,this.state.labelPin)}
              errorText={labels.getError(lang,this.state.labelPin)}
              deleteText={"DELICON"}
              fingerTouch={this.state.localAuthentication}
              onFingerTouch={() => this.handleTouchPress()}
            />
          </View>
          {/* close button */}
          <TouchableOpacity style={{position:'absolute',right:10,top:45}} onPress={() => {this.setState({isModalVisible:false,labelPin:""});this._cancelAuthenticate();this.props.navigation.navigate('Home')}} disabled={this.state.loginProcess}>
            <Image
              style={{ width: 22, height: 22 }}
              source={require("../assets/icon/Close.png")}
            />
          </TouchableOpacity>

        </View>

        {/* not you */}
        <TouchableOpacity style={{position:'absolute',left:0,right:0,bottom:55}} onPress={() => {this._cancelAuthenticate();this.props.navigation.navigate('Login')}} disabled={this.state.loginProcess}>
          <DefaultText style={{color:Colors.bondiBlue,textAlign:'center',fontSize:20}}>{labels.getLabel(lang,"notYou")}</DefaultText>
        </TouchableOpacity>

        {/* forget pin */}
        <TouchableOpacity style={{position:'absolute',left:0,right:0,bottom:15}} onPress={() => this.props.navigation.navigate('ForgetPin')} disabled={this.state.loginProcess}>
          <DefaultText style={{color:Colors.bondiBlue,textAlign:'center',fontSize:20}}>{labels.getLabel(lang,"forgetPin")}</DefaultText>
        </TouchableOpacity>

        {/* modal touch */}
        <Modal isVisible={this.state.isModalVisible} onModalHide={() => this._cancelAuthenticate()} animationOutTiming={50} style={styles.fullModal} onModalShow={() => console.log("Show Touch")}>
          <SafeAreaView style={{flex:1}}>
            <View style={styles.subModal}>
              <View style={{flex:1,alignContent:'center',marginTop:50}}>
                <DefaultText style={{color:Colors.bondiBlue,textAlign:'center',fontSize:20}}>{labels.getLabel(lang,"fingerprintModal1")}</DefaultText>
                <DefaultText style={{color:Colors.bondiBlue,textAlign:'center',fontSize:20}}>{labels.getLabel(lang,"fingerprintModal2")}</DefaultText>
                <View style={{marginTop:100}}>
                  <Icon
                    name='ios-finger-print'
                    type='ionicon'
                    color= {this.state.labelPin=="touchIdSuccess"  ? Colors.bondiBlue : this.state.labelPin=="touchIdFailed" ? Colors.textBoxError : Colors.eagle}
                    size={80}
                  />
                </View>
                <DefaultText style={{marginTop:20,color:this.state.labelPin=="touchIdFailed" ? 'red' : Colors.bondiBlue,textAlign:'center',fontSize:16}}>
                  {labels.getLabel(lang,this.state.labelPin)}

                </DefaultText>
                { this.state.labelPin=="touchIdSuccess" &&
                  <ActivityIndicator size="large" color={Colors.bondiBlue} />
                }

                <TouchableOpacity style={{position:'absolute',left:0,right:0,bottom:80}} onPress={() => {this._cancelAuthenticate();this.props.navigation.navigate('Login')}} disabled={this.state.loginProcess}>
                  <DefaultText style={{color:Colors.bondiBlue,textAlign:'center',fontSize:20}}>{labels.getLabel(lang,"notYou")}</DefaultText>
                </TouchableOpacity>

                <TouchableOpacity style={{position:'absolute',left:0,right:0,bottom:30}} onPress={() => this.setState({isModalVisible:false,labelPin:""})} disabled={this.state.loginProcess}>
                  <DefaultText style={{color:Colors.bondiBlue,textAlign:'center',fontSize:20}}>{labels.getLabel(lang,"usePin")}</DefaultText>
                </TouchableOpacity>
              </View>
              {/* close */}
              <TouchableOpacity style={{position:'absolute',right:10,top:10}} onPress={() => this.props.navigation.navigate('Home')} disabled={this.state.loginProcess}>
                <Image
                  style={{ width: 22, height: 22 }}
                  source={require("../assets/icon/Close-eagle.png")}
                />
              </TouchableOpacity>
            </View>


          </SafeAreaView>
        </Modal>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  fullModal: {
    margin: 0
  },
  subModal: {
    height: Dimensions.get('window').height,
    backgroundColor: "white",
    alignItems: "center",
  },
});

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus, numberOfPinFail } = auth;
  return { language, lastScreen, loginStatus, numberOfPinFail };
};

export default connect(
  mapStateToProps,
  {
    eCardDataClear,
    logIn,
    logOut,
    memberDataChanged,
    notificationChanged,
    notificationListChanged,
    hasECardChanged,
    pinFailed,
  }
)(LoginPinScreen);
