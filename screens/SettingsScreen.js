import React from "react";
import { Alert, BackHandler, FlatList, Linking, PixelRatio, StyleSheet, Text, View, TouchableOpacity } from "react-native";
import Constants from 'expo-constants';
import { Icon, Divider  } from "react-native-elements";
import { Switch } from "../components/Switch";
import { connect } from "react-redux";
import { notificationChanged, eCardDataClear, languageChanged, lastScreenChanged, logOut, hasECardChanged } from "../actions";
import * as labels from "../constants/label";
import CustomUserInactivity from "../components/CustomUserInactivity"
import LoginStyles from "../styles/LoginStyle.style"
import Colors from "../constants/Colors";
import * as utils from "../functions"
import { StylesText } from "../components/StyledText";
import DefaultText from "../components/DefaultText"

class SettingsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
  });


  constructor() {
    super();
    this.state={pinStore:""}
  }

  componentDidMount() {
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start SettingsScreen");
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton.bind(this));
    this.onScreenChanged();
    this.getData("policyNumberStore")
    this.getData("memberNoStore")
    this.getData("tokenStore")
    this.getData("pinStore")
    this.setTitle(this.props.language)
  };
  handleBackButton = () => {
    BackHandler.exitApp();
    return true;
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "settings"),
      lang: lang,
    });
  }

  getData = async key => {
    let value = await utils.retrieveData(key)
    this.setState({[key]:value})
  }

  toggleSwitch1 = value => {
    if (value) {
      this.onLanguageChangedTh();
    } else {
      this.onLanguageChangedEn();
    }
  };

  onLanguageChangedEn() {
    this.onLanguageChanged("en");
    labels.setLang("en");
    // this.props.navigation.setParams({
    //   title: labels.getLabel("en", "settings")
    // });
    this.setTitle("en")
  }

  onLanguageChangedTh() {
    this.onLanguageChanged("th");
    labels.setLang("th");
    // this.props.navigation.setParams({
    //   title: labels.getLabel("th", "settings")
    // });
    this.setTitle("th")
  }

  onLanguageChanged(text) {
    this.props.languageChanged(text);
  }

  onScreenChanged() {
    this.props.lastScreenChanged("Settings");
  }

  handleLogoutPress = async () => {
    let lang = this.props.language
    let data = null
    let result = ""
    let xmlBody = utils.LogoutBody(this.state.policyNumberStore,this.state.memberNoStore,this.state.tokenStore);
    console.log("Request body: " + xmlBody);
    await fetch(utils.urlUserService, utils.genRequest(utils.LogoutSOAPAction,xmlBody) )
      .then(response => response.text())
      .then(response => {
        result = utils.getResult(response)
        // console.log(result)
        let errorCode = result["LogoutResponse"][0]["LogoutResult"][0]["a:errorCode"][0].toString()
        let errorDesc = ""
        if(lang=="th"){
          errorDesc = result["LogoutResponse"][0]["LogoutResult"][0]["a:errorDescTH"][0].toString()
        }else{
          errorDesc = result["LogoutResponse"][0]["LogoutResult"][0]["a:errorDescEN"][0].toString()
        }
        if(Number(errorCode)==0){
          console.log("logout")
        }else{
          console.log("fetch", errorDesc);
          // Alert.alert(labels.getLabel(lang,"error"),errorDesc)
        }
      })
      .catch(err => {
        console.log("fetch", err);
        //Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
      });
    this.props.logOut();
    // this.props.notificationChanged(0)
    // this.props.eCardDataClear()
    // utils.logOut()
    // this.props.hasECardChanged("FALSE")
    Alert.alert(labels.getLabel(this.props.language, "Settings_logOut"))
    this.props.navigation.navigate("Home")
  };

  openPrivacy = () => {
    let lang = this.props.language;

    if(lang=="th"){
      Linking.openURL('https://www.tokiomarine.com/th/th-life/global/privacy-policy.html')
    }else{
      Linking.openURL('https://www.tokiomarine.com/th/en-life/global/privacy-policy.html')
    }
  }

  render() {
    let lang = this.props.language;
    return (
      <View style={LoginStyles.main}>
        <CustomUserInactivity onAction={() => this.props.navigation.navigate("Home")}>
          <View style={styles.option}>

            {
            this.state.pinStore.length>0 &&
            <TouchableOpacity style={styles.row} onPress={() => this.props.navigation.navigate("MyAccount")}>
              {/* <View style={styles.optionTextContainer}>
                <DefaultText style={styles.optionText}> </DefaultText>
              </View> */}
              <View style={styles.optionTextContainer2}>
                <DefaultText style={styles.optionText}>{labels.getLabel(lang, "Settings_myAccount")}</DefaultText>
              </View>
              <View style={[styles.optionTextContainer,{alignItems:'flex-end'}]}>
                <Icon
                  name='chevron-right'
                  type='evilicon'
                  color={Colors.bondiBlue}
                  size={40}
                />
              </View>
            </TouchableOpacity>
            }
            <Divider style={{ backgroundColor: Colors.header }} />
            <View style={styles.row}>
              {/* <View style={styles.optionTextContainer}>
                <DefaultText style={styles.optionText}> </DefaultText>
              </View> */}
              <View style={styles.optionTextContainer2}>
                <DefaultText style={styles.optionText}>Language/ภาษา</DefaultText>
              </View>
              <View style={[styles.optionTextContainer,{alignItems:'flex-end'}]}>
                <Switch
                  value={lang == "en" ? false : true}
                  onValueChange={this.toggleSwitch1.bind(this)}
                  disabled={false}
                  activeText={"TH"}
                  inActiveText={"EN"}
                  circleBorderWidth={1}
                  backgroundActive={Colors.bondiBlue}
                  backgroundInactive={Colors.bondiBlue}
                  circleActiveBorderColor={Colors.bondiBlue}
                  circleInactiveBorderColor={Colors.bondiBlue}
                  circleActiveColor={"#fff"}
                  circleInActiveColor={"#fff"}
                  changeValueImmediately={true}
                  innerCircleStyle={{
                    alignItems: "center",
                    justifyContent: "center"
                  }} // style for inner animated circle for what you (may) be rendering inside the circle
                  outerCircleStyle={{}} // style for outer animated circle
                  renderActiveText={true}
                  renderInActiveText={true}
                  switchLeftPx={4} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
                  switchRightPx={4} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
                />
              </View>
            </View>
            <Divider style={{ backgroundColor: Colors.header }} />
            <TouchableOpacity style={styles.row} onPress={() => this.openPrivacy()}>
              <View style={styles.optionTextContainer2}>
                <DefaultText style={styles.optionText}>{labels.getLabel(lang, "Settings_privacy")}</DefaultText>
              </View>
            </TouchableOpacity>
            <Divider style={{ backgroundColor: Colors.header }} />
            {
            this.state.pinStore.length>0 &&
            <View style={[styles.row]}>
              <View style={styles.optionTextContainer}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={this.handleLogoutPress}
                >
                  <DefaultText bold style={[styles.buttonText,{paddingHorizontal:20}]}>
                    {labels.getLabel(lang, "Settings_logOut")}
                  </DefaultText>
                </TouchableOpacity>
              </View>
            </View>
            }
          </View>
          <View style={{position:"absolute",right:3,bottom:3}}><DefaultText style={{color:"#E8E8E8"}}>Version {Constants.manifest.version}</DefaultText></View>
        </CustomUserInactivity>
      </View>
    );
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
  },
  row:{
    flexDirection: "row",
    justifyContent: "space-between",
    padding:10
  },
  optionsTitleText: {
    fontSize: 16,
    marginLeft: 15,
    marginTop: 9,
    marginBottom: 12
  },
  option: {
    paddingHorizontal: 5,
    //paddingVertical: 20,
  },
  button: {
    paddingHorizontal: 5,
    paddingVertical: 20,
    backgroundColor:Colors.cardBorder,
    borderRadius:5,
  },
  optionTextContainer: {
    flex:1,
    alignItems:'center',
    padding:10
  },
  optionTextContainer2: {
    flex:2,
    //alignItems:'center',
    padding:10
  },
  optionText: {
    fontSize: 16,
    color:Colors.bondiBlue,
  },
  buttonText: {
    fontSize: 16,
    color:Colors.bondiBlue,
    // fontWeight:'bold',
  }
});

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus } = auth;
  return { language, lastScreen, loginStatus };
};

export default connect(
  mapStateToProps,
  {
    eCardDataClear,
    notificationChanged,
    languageChanged,
    lastScreenChanged,
    logOut,
    hasECardChanged
  }
)(SettingsScreen);
