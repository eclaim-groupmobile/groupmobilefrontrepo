import React from "react";
import {
  Alert,
  FlatList,
  Image,
  PixelRatio,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from "react-native";
import { Icon,Divider } from "react-native-elements";
import { connect } from "react-redux";
import { languageChanged, lastScreenChanged, logOut } from "../actions";
import * as labels from "../constants/label";
import CustomUserInactivity from "../components/CustomUserInactivity";
import LoginStyles from "../styles/LoginStyle.style";
import Colors from "../constants/Colors";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import * as utils from "../functions";
import DefaultText from "../components/DefaultText"

class HospitalScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {navigation.navigate("Home")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });

  constructor() {
    super();
  }

  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start HospitalScreen");
    this.setTitle(this.props.language)
  };

  navigateBack = () => {
    this.props.navigation.navigate("Home");
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "hospital"),
      lang: lang,
    });
  }

  render() {
    let lang = this.props.language;
    return (
      <View style={LoginStyles.main}>
        <CustomUserInactivity onAction={() => this.props.navigation.navigate("Home")}>
          <View style={styles.option}>

            {/* Near */}
            <TouchableOpacity style={styles.row} onPress={() => this.props.navigation.navigate("HospitalByNear")}>
              <View style={{flex:1,flexDirection:'row',paddingHorizontal:20}}>
                <Image
                    style={{ width: 60, height: 60, borderWidth:1,borderRadius:30,borderColor:Colors.cardBorder}}
                    source={require("../assets/icon/iconfinder_worldwide_location.png")}
                  />
                <View style={{flexGrow:1,marginLeft:10}}>
                  <View style={{flex:1,flexDirection:'row',justifyContent:'space-between'}}>
                      <View style={{justifyContent:"center"}}>
                        <DefaultText bold style={styles.optionText}>
                          {labels.getLabel(lang, "Hospital_hospitalByNear")}
                        </DefaultText>
                      </View>
                    <Icon
                      name='chevron-right'
                      type='evilicon'
                      color={Colors.bondiBlue}
                      size={40}
                    />
                  </View>
                  <View style={{paddingRight:25}}>
                    <Divider style={{ backgroundColor: Colors.bondiBlue, height: 0.5 }} />
                  </View>
                </View>
              </View>
            </TouchableOpacity>

            {/* Province */}
            <TouchableOpacity style={styles.row} onPress={() => this.props.navigation.navigate("HospitalByProvince")}>
              <View style={{flex:1,flexDirection:'row',paddingHorizontal:20}}>
                <Image
                    style={{ width: 60, height: 60, borderWidth:1,borderRadius:30,borderColor:Colors.cardBorder}}
                    source={require("../assets/icon/iconfinder_city.png")}
                  />
                <View style={{flexGrow:1,marginLeft:10}}>
                  <View style={{flex:1,flexDirection:'row',justifyContent:'space-between'}}>
                      <View style={{justifyContent:"center"}}>
                        <DefaultText bold style={styles.optionText}>
                          {labels.getLabel(lang, "Hospital_hospitalByProvince")}
                        </DefaultText>
                      </View>
                    <Icon
                      name='chevron-right'
                      type='evilicon'
                      color={Colors.bondiBlue}
                      size={40}
                    />
                  </View>
                  <View style={{paddingRight:25}}>
                    <Divider style={{ backgroundColor: Colors.bondiBlue, height: 0.5 }} />
                  </View>
                </View>
              </View>
            </TouchableOpacity>

            {/* Hospital */}
            <TouchableOpacity style={styles.row} onPress={() => this.props.navigation.navigate("HospitalByHospital")}>
              <View style={{flex:1,flexDirection:'row',paddingHorizontal:20}}>
                <Image
                    style={{ width: 60, height: 60, borderWidth:1,borderRadius:30,borderColor:Colors.cardBorder}}
                    source={require("../assets/icon/iconmonstr-building.png")}
                  />
                <View style={{flexGrow:1,marginLeft:10}}>
                  <View style={{flex:1,flexDirection:'row',justifyContent:'space-between'}}>
                      <View style={{justifyContent:"center"}}>
                        <DefaultText bold style={styles.optionText}>
                          {labels.getLabel(lang, "Hospital_hospitalByHospital")}
                        </DefaultText>
                      </View>
                    <Icon
                      name='chevron-right'
                      type='evilicon'
                      color={Colors.bondiBlue}
                      size={40}
                    />
                  </View>
                  <View style={{paddingRight:25}}>
                    <Divider style={{ backgroundColor: Colors.bondiBlue, height: 0.5 }} />
                  </View>
                </View>
              </View>
            </TouchableOpacity>
            

            {/* Clinic */}
            <TouchableOpacity style={styles.row} onPress={() => this.props.navigation.navigate("HospitalByClinic")}>
              <View style={{flex:1,flexDirection:'row',paddingHorizontal:20}}>
                <Image
                    style={{ width: 60, height: 60, borderWidth:1,borderRadius:30,borderColor:Colors.cardBorder}}
                    source={require("../assets/icon/iconfinder_11.png")}
                  />
                <View style={{flexGrow:1,marginLeft:10}}>
                  <View style={{flex:1,flexDirection:'row',justifyContent:'space-between'}}>
                      <View style={{justifyContent:"center"}}>
                        <DefaultText bold style={styles.optionText}>
                          {labels.getLabel(lang, "Hospital_hospitalByClinic")}
                        </DefaultText>
                      </View>
                    <Icon
                      name='chevron-right'
                      type='evilicon'
                      color={Colors.bondiBlue}
                      size={40}
                    />
                  </View>
                  <View style={{paddingRight:25}}>
                    <Divider style={{ backgroundColor: Colors.bondiBlue, height: 0.5 }} />
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{position:'absolute',bottom:10,flexDirection:'row',paddingHorizontal:10}}>
                <DefaultText style={{color:'red'}}>
                  {labels.getLabel(lang, "Hospital_remark1")}
                </DefaultText>
                <DefaultText style={{color:'red',marginLeft:10,flex:1,flexWrap: "wrap"}}>
                  {labels.getLabel(lang, "Hospital_remark2")}
                </DefaultText>
            </View>
        </CustomUserInactivity>
      </View>
    );
  }

  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
  },
  row:{
    flexDirection: "row", 
    justifyContent: "space-between",
    padding:10
  },
  optionsTitleText: {
    fontSize: 16,
    marginLeft: 15,
    marginTop: 9,
    marginBottom: 12
  },
  option: {
    paddingHorizontal: 5,
    paddingVertical: 20,
  },
  optionTextContainer: {
    justifyContent:'center',
    alignItems:'center',
    //paddingHorizontal:10
  },
  optionText: {
    fontSize:16,
    color:Colors.bondiBlue,
    // fontWeight:'bold',
    textAlign:'left',
    justifyContent:"center",
  },
});

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus } = auth;
  return { language, lastScreen, loginStatus };
};

export default connect(
  mapStateToProps,
  {
    languageChanged,
    lastScreenChanged,
    logOut
  }
)(HospitalScreen);
