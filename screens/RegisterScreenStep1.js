import React, { Component } from "react";
import {
  Alert,
  Image,
  PixelRatio,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import { Icon } from "react-native-elements";
import LoginStyles from "../styles/LoginStyle.style";
import * as labels from "../constants/label";
import { TextField } from "react-native-materialui-textfield";
import * as utils from "../functions";
import DefaultText from "../components/DefaultText"

class RegisterScreenStep1 extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      policyNumber: "",
      memberNo: "",
      memberNo2: "",
      errors: {},
    };

    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitPolicyNumber = this.onSubmitPolicyNumber.bind(this);
    this.onSubmitMemberNo = this.onSubmitMemberNo.bind(this);
    this.onSubmitMemberNo2 = this.onSubmitMemberNo2.bind(this);

    this.policyNumberRef = this.updateRef.bind(this, 'policyNumber');
    this.memberNoRef = this.updateRef.bind(this, 'memberNo');
    this.memberNo2Ref = this.updateRef.bind(this, 'memberNo2');
  }

  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {navigation.navigate("Login")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });

  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start RegisterScreenStep1");
    this.setTitle(this.props.language)
    this.setState({
      policyNumber:"G",
      memberNo:"",
      memberNo2: "",
      errors: {},
    })
  };

  navigateBack = () => {
    this.props.navigation.navigate("Login");
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "register"),
      lang: lang,
    });
  }

  handleNextPress = async () => {
    let errorFlag = false
    let errors = {};
    let lang = this.props.language

    let policyNumberError = labels.getError(this.props.language,utils.validatePolicyNumber(this.state.policyNumber));
    if(policyNumberError!=""){
      errorFlag = true
      errors['policyNumber'] = policyNumberError
    }

    let memberNoError = labels.getError(this.props.language,utils.validateMemberNo(this.state.memberNo + "-" + this.state.memberNo2));
    if(memberNoError!=""){
      errorFlag = true
      errors['memberNo'] = memberNoError
      errors['memberNo2'] = ' '
    }
    
    this.setState({ errors });
    if(errorFlag){

    }else{
      this.setState({complete:false})
      let result = ""
      let memberNo = this.state.memberNo + "-" + this.state.memberNo2
      let xmlBody = utils.ChkMemberBody(this.state.policyNumber,memberNo);
      console.log(`Request body: ${xmlBody}`);
      await fetch(utils.urlUserService, utils.genRequest(utils.ChkMemberSOAPAction,xmlBody) )
        .then(response => response.text())
        .then(response => {
          result = utils.getResult(response)
          let errorCode = result["ChkMemberResponse"][0]["ChkMemberResult"][0]["a:errorCode"][0].toString()
          let errorDesc = ""
          if(lang=="th"){
            errorDesc = result["ChkMemberResponse"][0]["ChkMemberResult"][0]["a:errorDescTH"][0].toString()
          }else{
            errorDesc = result["ChkMemberResponse"][0]["ChkMemberResult"][0]["a:errorDescEN"][0].toString()
          }
          if(Number(errorCode)==0){
            this.props.navigation.navigate("RegisterStep2",{
              policyNumber: this.state.policyNumber,
              memberNo: memberNo,
            })
          }else{
            Alert.alert(labels.getLabel(lang,"error"),errorDesc)
          }
        })
        .catch(err => {
          console.log("fetch", err);
          Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
        });
      this.setState({complete:true})
    }
  }

  handleCheckCompleteField = (text,name) => {
    let count = 0
    
    if(name!='policyNumber'){
      let policyNumber = this.state.policyNumber
      if(policyNumber==null) policyNumber = ""
      if(policyNumber.length>0) count = count + 1
    }

    if(name!='memberNo'){
      let memberNo = this.state.memberNo
      if(memberNo==null) memberNo = ""
      if(memberNo.length>0) count = count + 1
    }

    if(name!='memberNo2'){
      let memberNo2 = this.state.memberNo2
      if(memberNo2==null) memberNo2 = ""
      if(memberNo2.length>0) count = count + 1
    }

    if(text==null) text = ""
    if(text.length>0) count = count + 1

    if(count>=3) this.setState({complete: true})
    else this.setState({complete:false})
  }

  onChangeText(text) {
    let errors = this.state.errors;
    ['policyNumber', 'memberNo2', 'memberNo']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          if(name=='policyNumber' && (text == null || text == "")) text = "G"
          // if(name=='memberNo') text = utils.formatMemberNo2(text)
          if(name=='memberNo'){
            errors['memberNo2'] = ''
          }
          if(name=='memberNo2'){
            errors['memberNo'] = ''
          }
          errors[name] = ''
          this.setState({ [name]: text, errors });
          this.handleCheckCompleteField(text,name)
          if(name=='memberNo'){
            if(text.length==5) this.memberNo2.focus();
          }
        }
      });
  }

  onSubmitPolicyNumber() {
    this.memberNo.focus();
  }

  onSubmitMemberNo() {
    this.memberNo2.focus();
  }

  onSubmitMemberNo2() {
    this.memberNo2.blur();
  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  render() {
    let { errors = {}, ...data } = this.state;
    let lang = this.props.language;

    return (
      <View style={LoginStyles.main}>
        <View style={[LoginStyles.container,{flex:1,justifyContent:'center'}]}>
          <TextField
            ref={this.policyNumberRef}
            value={data.policyNumber}
            maxLength={8}
            inputContainerPadding={2}
            lineWidth={2}
            autoCorrect={false}
            enablesReturnKeyAutomatically={true}
            onChangeText={this.onChangeText}
            onSubmitEditing={this.onSubmitPolicyNumber}
            returnKeyType='done'
            label={labels.getLabel(lang,"policyNumber")}
            error={errors.policyNumber}
            tintColor={Colors.bondiBlue}
            baseColor={Colors.bondiBlue}
            placeholder={' '}
            helpersNumberOfLines={1.2}
            style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
            labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            labelFontSize={utils.getFontScale(14)}
            containerStyle={{marginTop:10}}
            labelPadding={20}
          />

          <View style={{flexDirection:'row'}}>
            <TextField
              ref={this.memberNoRef}
              value={data.memberNo}
              maxLength={5}
              inputContainerPadding={2}
              lineWidth={2}
              keyboardType={'numeric'}
              autoCorrect={false}
              enablesReturnKeyAutomatically={true}
              onChangeText={this.onChangeText}
              onSubmitEditing={this.onSubmitMemberNo}
              returnKeyType='done'
              label={labels.getLabel(lang,"memberNo")}
              error={errors.memberNo}
              tintColor={Colors.bondiBlue}
              baseColor={Colors.bondiBlue}
              placeholder={' '}
              // onEndEditing={(e) => {
              //   this.setState({memberNo: utils.formatMemberNo(this.state.memberNo)});
              // }}
              // onBlur={()=>this.setState({memberNo: utils.formatMemberNo(this.state.memberNo)})}
              helpersNumberOfLines={1.2}
              style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
              labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              labelFontSize={utils.getFontScale(14)}
              containerStyle={{flex:10,marginTop:10}}
              labelPadding={20}
            />
            <TextField
              value={" "} //slash member no
              maxLength={1}
              inputContainerPadding={2}
              lineWidth={0}
              enablesReturnKeyAutomatically={true}
              label={' '}
              tintColor={Colors.bondiBlue}
              baseColor={Colors.white}
              editable={false}
              helpersNumberOfLines={1.2}
              style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),textAlign:'center',color:Colors.bondiBlue}}
              labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              labelFontSize={utils.getFontScale(14)}
              containerStyle={{flex:1,marginTop:10}}
              labelPadding={20}
            />
            <TextField
              ref={this.memberNo2Ref}
              value={data.memberNo2}
              maxLength={2}
              inputContainerPadding={2}
              lineWidth={2}
              keyboardType={'numeric'}
              autoCorrect={false}
              enablesReturnKeyAutomatically={true}
              onChangeText={this.onChangeText}
              onSubmitEditing={this.onSubmitMemberNo2}
              returnKeyType='done'
              label={' '}
              error={errors.memberNo2}
              tintColor={Colors.bondiBlue}
              baseColor={Colors.bondiBlue}
              placeholder={' '}
              helpersNumberOfLines={1.2}
              style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
              labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              labelFontSize={utils.getFontScale(14)}
              containerStyle={{flex:2,marginTop:10}}
              labelPadding={20}
            />
          </View>
            
          <TouchableOpacity onPress={this.handleNextPress}
            style={[
              this.state.complete ? LoginStyles.button : LoginStyles.buttonInactive,
                { marginTop: 200 }
            ]}
            disabled={!this.state.complete}
          >
            <DefaultText bold style={{color: this.state.complete ? Colors.buttonTextActive : Colors.buttonTextInactive}}>{labels.getLabel(lang,"next")}</DefaultText>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus } = auth;
  return { language, lastScreen, loginStatus };
};

export default connect(
  mapStateToProps,
  {

  }
)(RegisterScreenStep1);
