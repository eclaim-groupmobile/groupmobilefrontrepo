import React, { Component } from "react";
import {
  Image,
  Dimensions,
  Keyboard,
  KeyboardAvoidingView,
  PixelRatio,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Header } from "react-navigation";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import LoginStyles from "../styles/LoginStyle.style";
import Picker from "../components/picker";
import * as labels from "../constants/label";
import { TextField } from "react-native-materialui-textfield";
import { TextFieldCustom } from "../components/react-native-materialui-textfield-custom";
import * as utils from "../functions";
import DefaultText from "../components/DefaultText"

const now = new Date()

class RegisterScreenStep2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      policyNumber: "",
      memberNo: "",
      passport: "",
      dob: "",
      tel: "",
      email: "",
      lineId: "",
      dateDefault: this.props.language == 'th' ? ["2447","มกราคม","01"] : ["1904","January","01"],
      dobFocus: false,
      KeyboardAvoidingView:false,
      errors: {},
    }

    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitPassport = this.onSubmitPassport.bind(this);
    this.onSubmitDob = this.onSubmitDob.bind(this);
    this.onSubmitTel = this.onSubmitTel.bind(this);
    this.onSubmitEmail = this.onSubmitEmail.bind(this);
    this.onSubmitLineId = this.onSubmitLineId.bind(this);

    this.passportRef = this.updateRef.bind(this, 'passport');
    this.dobRef = this.updateRef.bind(this, 'dob');
    this.telRef = this.updateRef.bind(this, 'tel');
    this.emailRef = this.updateRef.bind(this, 'email');
    this.lineIdRef = this.updateRef.bind(this, 'lineId');
  }

  policyNumber = ''
  memberNo = ''

  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {utils.confirmBack(navigation.state.params.lang,navigation,"Home")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });

  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start RegisterScreenStep2");
    policyNumber = this.props.navigation.getParam('policyNumber', '');
    memberNo = utils.formatMemberNo(this.props.navigation.getParam('memberNo', ''));
    passport = this.props.navigation.getParam('passport', '');
    dob = this.props.navigation.getParam('dob', '');
    tel = this.props.navigation.getParam('tel', '');
    email = this.props.navigation.getParam('email', '');
    lineId = this.props.navigation.getParam('lineId', '');
    this.setState({
      policyNumber,
      memberNo,
      passport,
      dob,
      tel,
      email,
      lineId,
      KeyboardAvoidingView:false,
      errors: {},
    })
    this.changeDateDefault(now.getFullYear(),now.getMonth(),now.getDate(),true)
    this.setTitle(this.props.language)
  };

  navigateBack = () => {
    // if(this.picker.isPickerShow()){
    //   this._onCalendarHandle();
    // }else{
      // this.props.navigation.navigate("RegisterStep1");
      utils.confirmBack(this.props.language,this.props.navigation,"Home")
    // }
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "register"),
      lang: lang,
    });
  }

  _onCalendarHandle(){
    Keyboard.dismiss();
    this.setState({KeyboardAvoidingView:false})
    this.picker.toggle();
    if(this.state.dobFocus) {
      this.setState({dobFocus:false})
    }else {
      this.setState({dobFocus:true})
    }
    this.state = { dob: "" };
  }

  handleNextPress = () => {
    let errorFlag = false
    let errors = {};

    let passportError = labels.getError(this.props.language,utils.validatePassport(this.state.passport));
    if(passportError!=""){
      errorFlag = true
      errors['passport'] = passportError
    }

    let dobError = labels.getError(this.props.language,utils.validateDate(this.state.dob));
    if(dobError!=""){
      errorFlag = true
      errors['dob'] = dobError
    }

    let telError = labels.getError(this.props.language,utils.validateTel(this.state.tel));
    if(telError!=""){
      errorFlag = true
      errors['tel'] = telError
    }

    let emailError = labels.getError(this.props.language,utils.validateEmail(this.state.email));
    if(emailError!=""){
      errorFlag = true
      errors['email'] = emailError
    }

    let lineIdError = labels.getError(this.props.language,utils.validateLineId(this.state.lineId));
    if(lineIdError!=""){
      errorFlag = true
      errors['lineId'] = lineIdError
    }

    this.setState({ errors });
    if(errorFlag){

    }else{
      this.props.navigation.navigate("RegisterTerm",{
        policyNumber: this.state.policyNumber,
        memberNo: this.state.memberNo,
        passport: this.state.passport,
        dob: this.state.dob,
        tel:this.state.tel,
        email:this.state.email,
        lineId:this.state.lineId,
      })
    }
  }

  handleCheckCompleteField = (text,name) => {
    let count = 0

    if(name!='passport'){
      let passport = this.state.passport
      if(passport==null) passport = ""
      if(passport.length>0) count = count + 1
    }

    if(name!='dob'){
      let dob = this.state.dob
      if(dob==null) dob = ""
      if(dob.length>0) count = count + 1
    }

    if(name!='tel'){
      let tel = this.state.tel
      if(tel==null) tel = ""
      if(tel.length>0) count = count + 1
    }

    // if(name!='email'){
    //   let email = this.state.email
    //   if(email==null) email = ""
    //   if(email.length>0) count = count + 1
    // }

    // if(name!='lineId'){
    //   let lineId = this.state.lineId
    //   if(lineId==null) lineId = ""
    //   if(lineId.length>0) count = count + 1
    // }

    if(text==null) text = ""
    if(text.length>0) count = count + 1

    if(count>=3) this.setState({complete: true})
    else this.setState({complete:false})
  }

  changeDateDefault = (year,month,day,flagYear) => {
    let dateDefault = []
    if(this.props.language=='th')
      if(flagYear){
        dateDefault = [(year+543)+"", utils.monthNameTh[month]+"", ("0" + day + "").slice(-2)]
      }else{
        dateDefault = [(year)+"", utils.monthNameTh[month]+"", ("0" + day + "").slice(-2)]
      }

    else
      dateDefault = [year+"", utils.monthNameEn[month]+"", ("0" +  day + "").slice(-2)]
    this.setState({dateDefault})
  }

  dobChange = (text) => {
    text = text + ""
    let value = utils.changeDateFormat(this.props.language,text)
    let year = text.substring(0, 4);
    let month = text.substring(5, text.length - 3);
    let monthIndex = utils.getMonthIndex(this.props.language,month)
    let day = text.substring(text.length - 2);

    this.setState({dob: value},this.handleCheckCompleteField(utils.changeDateFormat(this.props.language,text),'dob'))
    this.setState({dobFocus:false})
    this.changeDateDefault(year,monthIndex,day,false)
    this.onSubmitDob()
  }

  onChangeText(text) {
    let errors = this.state.errors;
    ['passport', 'dob', 'tel', 'email', 'lineId']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          errors[name] = ''
          this.setState({ [name]: text, errors });
          this.handleCheckCompleteField(text,name)
        }
      });
  }

  onSubmitPassport() {
    this.passport.blur();
    this._onCalendarHandle();
  }

  onSubmitDob() {
    this.dob.blur();
    this.tel.focus();
  }

  onSubmitTel() {
    this.email.focus();
  }

  onSubmitEmail() {
    this.lineId.focus();
  }

  onSubmitLineId() {
    this.lineId.blur();
  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  render() {
    // const _osBehavior = Platform.OS === "ios" ? "position" : null;
    let { errors = {}, ...data } = this.state;
    let lang = this.props.language;
    const deviceWidth = Dimensions.get("window").width;
    const deviceHeight = Dimensions.get("window").height;

    return (

      <View style={LoginStyles.main}>
      <ScrollView keyboardShouldPersistTaps='always'>
        <KeyboardAvoidingView style={[LoginStyles.container,{marginBottom:26,marginTop:20}]} behavior="position" enabled={this.state.KeyboardAvoidingView} keyboardVerticalOffset={Header.HEIGHT + 20}>
          <TextFieldCustom
            ref={this.passportRef}
            value={data.passport}
            maxLength={30}
            inputContainerPadding={2}
            lineWidth={2}
            autoCorrect={false}
            enablesReturnKeyAutomatically={true}
            onChangeText={this.onChangeText}
            onSubmitEditing={this.onSubmitPassport}
            returnKeyType='done'
            label={labels.getLabel(lang,"passport")}
            error={errors.passport}
            tintColor={Colors.bondiBlue}
            baseColor={Colors.bondiBlue}
            placeholder={' '}
            onFocus={()=>this.setState({KeyboardAvoidingView:false})}
            helpersNumberOfLines={2.2}
            style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
            labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            labelFontSize={utils.getFontScale(13)}
            containerStyle={{marginTop:10}}
            labelPadding={20}
            star={true}
          />

          <TouchableOpacity style={{marginTop: 5}} onPress={this._onCalendarHandle.bind(this)}>
            <TextFieldCustom
              ref={this.dobRef}
              value={data.dob}
              inputContainerPadding={2}
              lineWidth={2}
              disabledLineWidth={2}
              disabledLineType={'solid'}
              autoCorrect={false}
              enablesReturnKeyAutomatically={true}
              //onChangeText={this.onChangeText}
              returnKeyType='done'
              label={labels.getLabel(lang,"dob")}
              error={errors.dob}
              tintColor={Colors.bondiBlue}
              baseColor={this.state.dobFocus ? Colors.bondiBlue : Colors.bondiBlue}
              placeholder={' '}
              editable={false}
              helpersNumberOfLines={1.2}
              style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
              labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              labelFontSize={utils.getFontScale(14)}
              containerStyle={{marginTop:10}}
              labelPadding={20}
              star={true}
            />
          </TouchableOpacity>

          <TextFieldCustom
            ref={this.telRef}
            value={data.tel}
            maxLength={10}
            inputContainerPadding={2}
            lineWidth={2}
            autoCorrect={false}
            enablesReturnKeyAutomatically={true}
            onChangeText={this.onChangeText}
            onSubmitEditing={this.onSubmitTel}
            returnKeyType='done'
            label={labels.getLabel(lang,"tel")}
            error={errors.tel}
            tintColor={Colors.bondiBlue}
            baseColor={Colors.bondiBlue}
            placeholder={' '}
            keyboardType="phone-pad"
            onFocus={()=>this.setState({KeyboardAvoidingView:true})}
            helpersNumberOfLines={1.2}
            style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
            labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            labelFontSize={utils.getFontScale(14)}
            containerStyle={{marginTop:10}}
            labelPadding={20}
            star={true}
          />

          <TextField
            ref={this.emailRef}
            value={data.email}
            maxLength={128}
            inputContainerPadding={2}
            lineWidth={2}
            autoCorrect={false}
            enablesReturnKeyAutomatically={true}
            onChangeText={this.onChangeText}
            onSubmitEditing={this.onSubmitEmail}
            returnKeyType='done'
            label={labels.getLabel(lang,"email")}
            error={errors.email}
            tintColor={Colors.bondiBlue}
            baseColor={Colors.bondiBlue}
            placeholder={' '}
            keyboardType="email-address"
            onFocus={()=>this.setState({KeyboardAvoidingView:true})}
            helpersNumberOfLines={1.2}
            style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
            labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            labelFontSize={utils.getFontScale(14)}
            containerStyle={{marginTop:10}}
            labelPadding={20}
          />

          <TextField
            ref={this.lineIdRef}
            value={data.lineId}
            maxLength={20}
            inputContainerPadding={2}
            lineWidth={2}
            labelFontSize={14}
            autoCorrect={false}
            enablesReturnKeyAutomatically={true}
            onChangeText={this.onChangeText}
            onSubmitEditing={this.onSubmitLineId}
            returnKeyType='done'
            label={labels.getLabel(lang,"lineId")}
            error={errors.lineId}
            tintColor={Colors.bondiBlue}
            baseColor={Colors.bondiBlue}
            placeholder={' '}
            onFocus={()=>this.setState({KeyboardAvoidingView:true})}
            helpersNumberOfLines={1.2}
            style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
            labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            labelFontSize={utils.getFontScale(14)}
            containerStyle={{marginTop:10}}
            labelPadding={20}
          />

          <TouchableOpacity onPress={this.handleNextPress}
            style={[
              this.state.complete ? LoginStyles.button : LoginStyles.buttonInactive,
                { marginTop: 50 }
            ]}
            disabled={!this.state.complete}
          >
            <DefaultText bold style={{color: this.state.complete ? Colors.buttonTextActive : Colors.buttonTextInactive}}>{labels.getLabel(lang,"next")}</DefaultText>
          </TouchableOpacity>
        </KeyboardAvoidingView>
        </ScrollView>
        <Picker
          ref={picker => this.picker = picker}
          deviceWidth={deviceWidth}
          deviceHeight={deviceHeight}
					showDuration={300}
          showMask={true}
          //onBackButtonPress={() => this._onCalendarHandle()}
          //onBackdropPress={() => this._onCalendarHandle()}
					pickerData={utils.createDateData(lang)}
					selectedValue={this.state.dateDefault}
					onPickerDone={(pickedValue) => this.dobChange(pickedValue)}
          onPickerCancel={() => this.setState({dobFocus:false})}
          pickerBtnText={labels.getLabel(lang,"done")}
          pickerCancelBtnText={labels.getLabel(lang,"cancel")}
				/>
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus } = auth;
  return { language, lastScreen, loginStatus };
};

export default connect(
  mapStateToProps,
  {

  }
)(RegisterScreenStep2);
