import React, { Component } from "react";
import {
  BackHandler,
  FlatList,
  Image,
  ImageBackground,
  PixelRatio,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import { languageChanged, lastScreenChanged, logOut } from "../actions";
import CustomUserInactivity from "../components/CustomUserInactivity";
import * as labels from "../constants/label";
import MainStyles from "../styles/Main.styles";
import * as utils from "../functions";
import NodataList from "../components/NoDataList"
import DefaultText from "../components/DefaultText"

class MemberScreen extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      pinStore: "",
      apiData: [],
      memberNoMain: "",
      EnableView:false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
  });

  componentDidMount() {
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start MemberScreen");
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton.bind(this));
    this.onScreenChanged()
    this.getLoginData()
    if(this.props.memberData!=null){
      this.setState({
        apiData: this.props.memberData['memberInfo'],
      })
    }
    this.setTitle(this.props.language)
  };
  handleBackButton = () => {
    BackHandler.exitApp();
    return true;
  };

  onScreenChanged() {
    this.props.lastScreenChanged("Member");
  }

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "member"),
      lang: lang,
    });
  }

  getData = async key => {
    let value = await utils.retrieveData(key)
    this.setState({[key]:value})
  }

  getLoginData = async key => {
    if(!this.props.loginStatus){
      this.setState({EnableView:false})
      let pinStoreVal = await utils.retrieveData("pinStore")
      if(pinStoreVal.length>0) this.props.navigation.navigate('LoginPin')
      else this.props.navigation.navigate('Login')
    }else{
      this.setState({EnableView:true})
    }
  }

  genBox = (item,index) => {
    let lang = this.props.language
    if(item.MemberNo!=null){
      return (
        <View style={{borderColor:Colors.cardBorder,borderWidth:1,justifyContent:"center",marginBottom:20,borderRadius:5}}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate("Personal",{indexStart:index})}>
            <View style={{padding: 20,flexDirection:"row",justifyContent:"space-between"}}>
              <DefaultText bold style={{color:Colors.bondiBlue}}>{item.MemberNo.slice(-2) + "      " + item.FName.trim() + " " + item.LName}</DefaultText>
              <DefaultText bold style={{color:Colors.bondiBlue}}>{lang == 'th' ? item.memberTypeTh : item.memberTypeEn}</DefaultText>
            </View>
          
          </TouchableOpacity>
        </View>
      )
    }
    
  }

  render() {
    let lang = this.props.language;

    if (this.state.EnableView) {
    return (
      <View style={MainStyles.main}>
        <CustomUserInactivity onAction={() => this.props.navigation.navigate("Home")}>
          <View
            style={[
              MainStyles.container,
              { paddingLeft: 10, paddingRight: 10, paddingTop: 10 }
            ]}
          >
            
            { this.props.memberData==null || this.props.memberData['memberInfo'].length==0 ?
            <NodataList/>
            :
            (this.props.memberData!= null && this.state.apiData != null) &&
            <View>
              <DefaultText bold style={{color:Colors.cardLabel,textAlign:'center',marginBottom:20,fontSize:16}}>{labels.getLabel(lang,"Member_header")}: {this.props.memberData['memberInfo'][0]['MemberNo'].substring(0,5)}</DefaultText>
              <FlatList
                data={this.state.apiData}
                renderItem={({item,index}) => this.genBox(item,index)}
                keyExtractor={(item) => item.MemberNo}
                extraData={this.props.language}
              />
            </View>
            }
          </View>
          <View style={{paddingLeft: 10,marginTop:5,marginBottom:5}}>
            <DefaultText style={{color:'red'}}>** {labels.getLabel(lang, "dataAsOf")} {utils.getDay(lang,-1)}</DefaultText>
          </View>
        </CustomUserInactivity>
      </View>
    );
    }else {
      return (<View></View>)
    }
  }
}

const mapStateToProps = ({ auth }) => {
    const { language, lastScreen, loginStatus, memberData } = auth;
    return { language, lastScreen, loginStatus, memberData };
  };
  
  export default connect(
    mapStateToProps,
    {
      languageChanged,
      lastScreenChanged,
      logOut,
    }
  )(MemberScreen);

  
const styles = StyleSheet.create({
  
});
