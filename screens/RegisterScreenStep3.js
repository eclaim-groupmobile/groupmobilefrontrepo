import React, { Component } from "react";
import {
  Alert,
  Image,
  Keyboard,
  KeyboardAvoidingView,
  PixelRatio,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import LoginStyles from "../styles/LoginStyle.style";
import * as labels from "../constants/label";
import { TextField } from "react-native-materialui-textfield";
import * as utils from "../functions";
import DefaultText from "../components/DefaultText"

const now = new Date()

class RegisterScreenStep3 extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      policyNumber: "",
      memberNo: "",
      passport: "",
      dob: "",
      tel: "",
      email: "",
      lineId: "",
      password: "",
      rePassword: "",
      errors: {},
    }

    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitPassword = this.onSubmitPassword.bind(this);
    this.onSubmitRePassword = this.onSubmitRePassword.bind(this);

    this.passwordRef = this.updateRef.bind(this, 'password');
    this.rePasswordRef = this.updateRef.bind(this, 'rePassword');
  }
  
  policyNumber = '';
  memberNo = '';
  passport = '';
  dob = '';
  tel = '';
  email = '';
  lineId = '';

  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {utils.confirmBack(navigation.state.params.lang,navigation,"Home")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });
  
  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
    Keyboard.dismiss()
  }
  load = () => {
    utils.log("Start RegisterScreenStep3");
    policyNumber = this.props.navigation.getParam('policyNumber', '');
    memberNo = utils.formatMemberNo(this.props.navigation.getParam('memberNo', ''));
    passport = this.props.navigation.getParam('passport', '');
    dob = this.props.navigation.getParam('dob', '');
    tel = this.props.navigation.getParam('tel', '');
    email = this.props.navigation.getParam('email', '');
    lineId = this.props.navigation.getParam('lineId', '');
    this.setState({
      policyNumber,
      memberNo,
      passport,
      dob,
      tel,
      email,
      lineId,
      errors: {},
    })
    this.setTitle(this.props.language)
    // Keyboard.dismiss()
    // this.password.focus();
  };

  navigateBack = () => {
    utils.confirmBack(this.props.language,this.props.navigation,"Home")
    // this.props.navigation.navigate("RegisterStep2",{
    //   policyNumber: this.state.policyNumber,
    //   memberNo: this.state.memberNo,
    //   passport: this.state.passport,
    //   dob: this.state.dob,
    //   tel:this.state.tel,
    //   email:this.state.email,
    //   lineId:this.state.lineId
    // })
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "register"),
      lang: lang,
    });
  }

  handleSubmitPress = async () => {
    let errorFlag = false
    let errors = {};
    let lang = this.props.language

    let passwordError = labels.getError(this.props.language,utils.validatePassword(this.state.password));
    if(passwordError!=""){
      errorFlag = true
      errors['password'] = passwordError
    }

    let rePasswordError = labels.getError(this.props.language,utils.validatePassword(this.state.rePassword));
    if(rePasswordError!=""){
      errorFlag = true
      errors['rePassword'] = rePasswordError
    }

    if(errorFlag==false && this.state.password!=this.state.rePassword){
      rePasswordError=labels.getError(this.props.language,"passwordNotMatch");
      errorFlag = true
      errors['rePassword'] = rePasswordError
    }
    
    this.setState({ errors });
    if(errorFlag){
      
    }else{
      this.setState({complete:false})
      let result = ""
      let xmlBody = utils.UpdPwdBody("REGISTER",this.state.policyNumber,this.state.memberNo,this.state.password,"","");
      console.log(`Request body: ${xmlBody}`);
      await fetch(utils.urlUserService, utils.genRequest(utils.UpdPwdSOAPAction,xmlBody) )
        .then(response => response.text())
        .then(response => {
          result = utils.getResult(response)
          let errorCode = result["UpdPwdResponse"][0]["UpdPwdResult"][0]["a:errorCode"][0].toString()
          let errorDesc = ""
          if(lang=="th"){
            errorDesc = result["UpdPwdResponse"][0]["UpdPwdResult"][0]["a:errorDescTH"][0].toString()
          }else{
            errorDesc = result["UpdPwdResponse"][0]["UpdPwdResult"][0]["a:errorDescEN"][0].toString()
          }
          if(Number(errorCode)==0){
            Alert.alert(labels.getLabel(lang,"createSuccess"))
            this.props.navigation.navigate("Login")
          }else{
            Alert.alert(labels.getLabel(lang,"error"),errorDesc)
          }
        })
        .catch(err => {
          console.log("fetch", err);
          Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
        });
      this.setState({complete:true})
      
    }
  }

  handleCheckCompleteField = (text,name) => {
    let count = 0

    if(name!='password'){
      let password = this.state.password
      if(password==null) password = ""
      if(password.length>0) count = count + 1
    }

    if(name!='rePassword'){
      let rePassword = this.state.rePassword
      if(rePassword==null) rePassword = ""
      if(rePassword.length>0) count = count + 1
    }

    if(text==null) text = ""
    if(text.length>0) count = count + 1
    
    if(count>=2) this.setState({complete: true})
    else this.setState({complete:false})
  }

  onChangeText(text) {
    let errors = this.state.errors;
    ['password', 'rePassword']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          errors[name] = ''
          this.setState({ [name]: text, errors });
          this.handleCheckCompleteField(text,name)
        }
      });
  }

  onSubmitPassword() {
    this.rePassword.focus();
  }

  onSubmitRePassword() {
    this.rePassword.blur();
  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  render() {
    let { errors = {}, ...data } = this.state;
    let lang = this.props.language;

    // const _osBehavior = Platform.OS === "ios" ? "position" : null;
    return (
      <View style={LoginStyles.main}>
        <View style={[LoginStyles.container,{flex:1,justifyContent:'center'}]}>
          <TextField
            ref={this.passwordRef}
            value={data.password}
            maxLength={128}
            inputContainerPadding={2}
            lineWidth={2}
            enablesReturnKeyAutomatically={true}
            onChangeText={this.onChangeText}
            onSubmitEditing={this.onSubmitPassword}
            clearTextOnFocus={false}
            returnKeyType='done'
            label={labels.getLabel(lang,"password")}
            error={errors.password}
            tintColor={Colors.bondiBlue}
            baseColor={Colors.bondiBlue}
            placeholder={' '}
            secureTextEntry={true}
            helpersNumberOfLines={lang=='th'?5:4}
            style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
            labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            labelFontSize={utils.getFontScale(14)}
            containerStyle={{marginTop:10}}
            labelPadding={20}
          />

          <TextField
            ref={this.rePasswordRef}
            value={data.rePassword}
            maxLength={128}
            inputContainerPadding={2}
            lineWidth={2}
            enablesReturnKeyAutomatically={true}
            onChangeText={this.onChangeText}
            onSubmitEditing={this.onSubmitRePassword}
            clearTextOnFocus={false}
            returnKeyType='done'
            label={labels.getLabel(lang,"rePassword")}
            error={errors.rePassword}
            tintColor={Colors.bondiBlue}
            baseColor={Colors.bondiBlue}
            placeholder={' '}
            secureTextEntry={true}
            helpersNumberOfLines={lang=='th'?5:4}
            style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
            labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
            labelFontSize={utils.getFontScale(14)}
            containerStyle={{marginTop:10}}
            labelPadding={20}
          />

          <TouchableOpacity onPress={this.handleSubmitPress}
            style={[
              this.state.complete ? LoginStyles.button : LoginStyles.buttonInactive,
                { marginTop: 200 }
            ]}
            disabled={!this.state.complete}
          >
            <DefaultText bold style={{color: this.state.complete ? Colors.buttonTextActive : Colors.buttonTextInactive}}>{labels.getLabel(lang,"submit")}</DefaultText>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus } = auth;
  return { language, lastScreen, loginStatus };
};

export default connect(
  mapStateToProps,
  {

  }
)(RegisterScreenStep3);
