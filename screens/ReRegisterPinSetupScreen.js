import React, { Component } from "react";
import {
  Alert,
  Image,
  PixelRatio,
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import LoginStyles from "../styles/LoginStyle.style";
import * as labels from "../constants/label";
import PinView from "../components/PinView";
import * as utils from "../functions";
import DefaultText from "../components/DefaultText"
import {
  logIn,
  languageChanged,
  memberDataChanged,
  notificationChanged,
  notificationListChanged,
  hasECardChanged,
} from "../actions";
import * as LocalAuthentication from 'expo-local-authentication';

class ReRegisterPinSetupScreen extends Component {
  constructor(props) {
    super(props);
    this.onComplete = this.onComplete.bind(this)
    this.state = { 
      policyNumber: "",
      memberNo: "",
      password: "",
      passport: "",
      dob: "",
      tel: "",
      email: "",
      lineId: "",
      successTextLabel: "",
      errorTextLabel: "",
      pinLabel: "",
      pin1st: "",
      pin2nd: "",
      localAuthentication: false,
    };
  }

  policyNumber = '';
  memberNo = '';
  password = '';
  dob = '';
  passport = '';
  tel = '';
  email = '';
  lineId = '';

  static navigationOptions = {
    header: null,
  };

  _checkLocalAuthentication = async () => {
		let result = await LocalAuthentication.hasHardwareAsync();
    let checkFingerprint
    if(Platform.OS === "ios"){
      checkFingerprint = await LocalAuthentication.supportedAuthenticationTypesAsync()
      if(checkFingerprint.indexOf(1)==-1){
        result = false
      }
    }
    if (result) {
		  this.setState({localAuthentication:true})
    }else{
      this.setState({localAuthentication:false})
    }
  };
  
  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
    
  }
  load = () => {
    utils.log("Start ReRegisterPinSetupScreen");
    policyNumber = this.props.navigation.getParam('policyNumber', '');
    memberNo = utils.formatMemberNo(this.props.navigation.getParam('memberNo', ''));
    password = this.props.navigation.getParam('password', '');
    passport = this.props.navigation.getParam('passport', '');
    dob = this.props.navigation.getParam('dob', '');
    tel = this.props.navigation.getParam('tel', '');
    email = this.props.navigation.getParam('email', '');
    lineId = this.props.navigation.getParam('lineId', '');
    // console.log(policyNumber,memberNo,password,passport,dob,tel,email,lineId)
    this.setState({
      policyNumber,
      memberNo,
      password,
      passport,
      dob,
      tel,
      email,
      lineId,
    })
    this._checkLocalAuthentication()
    this.setState({ pinLabel : "enterPin"})
    this.setState({ successTextLabel : ""})
    this.setState({ errorTextLabel : ""})
  };
  
  navigateBack = () => {
    if(this.state.pinLabel == "reEnterPin"){
      this.goToFirstPin()
    }else{
      utils.confirmBack(this.props.language,this.props.navigation,"Home")
    }
    // console.log("Back button is disabled")
  };

  getData = async key => {
    let value = await utils.retrieveData(key)
    this.setState({[key]:value})
  }

  onComplete = (inputtedPin, clear) => {
    let lang = this.props.language
    if(this.state.pinLabel == "enterPin"){
      console.log("ReRegisterPinSetupScreen 1st pin")
      this.setState({
        pin1st:inputtedPin,
        pinLabel:"reEnterPin",
        errorTextLabel:"",
      })
      clear();
      Alert.alert(labels.getLabel(lang,"confirmPin"))
    }else{
      console.log("ReRegisterPinSetupScreen 2st pin")
      this.setState({
        pin2st:inputtedPin,
        pinLabel:"enterPin",
      })
      if (inputtedPin !== this.state.pin1st) {
        this.setState({
          pin1st:"",
          pin2nd:"",
          errorTextLabel:labels.getError(lang,"pinNotMatch")
        })
        clear();
      } else {
        // utils.setPin(inputtedPin)
        // clear();
        this.setState({
          errorTextLabel:"",
          successTextLabel:labels.getLabel(lang,"pinSuccessSetup")
        })
        if(this.state.localAuthentication){
          labelTouch = labels.getLabel(lang,"touchId")
          Alert.alert(
            '',
            labelTouch,
            [
              {text: labels.getLabel(lang,"cancel"), onPress: () => this._onUpdateData(inputtedPin), style: 'cancel'},
              {text: labels.getLabel(lang,"enable"), onPress: () => this._setLocalAuthenticationStore(inputtedPin)},
            ],
            { cancelable: false }
          )
        }else{
          this._onUpdateData(inputtedPin)
        }
      }
    }
  }

  _setLocalAuthenticationStore = (inputtedPin) => {
    utils.setLocalAuthenticationStore("1")
    this._onUpdateData(inputtedPin)
  }

  _onUpdateData = async (inputtedPin) => {
    this.setState({
      pinLabel:"loginNow",
    })
    this._onLogIn(inputtedPin)
  }

  _onLogIn = async (inputtedPin) => {
    this.setState({complete:false})
    let lang = this.props.language
    let result=""
    let result2=""
    let token=""
    let fName=""
    let lName=""
    let name=""
    let tel=""
    let mail=""
    let lineId=""
    let memberNo = this.state.memberNo
    let xmlBody = utils.LoginBody(this.state.policyNumber,memberNo,this.state.password);
    this.setState({loginProcess:true})
    console.log(`Request body: ${xmlBody}`);
    await fetch(utils.urlUserService, utils.genRequest(utils.LoginSOAPAction,xmlBody) )
      .then(response => response.text())
      .then(response => {
        result = utils.getResult(response)
        let errorCode = result["LoginResponse"][0]["LoginResult"][0]["a:errorCode"][0].toString()
        let errorDesc = ""
        if(lang=="th"){
          errorDesc = result["LoginResponse"][0]["LoginResult"][0]["a:errorDescTH"][0].toString()
        }else{
          errorDesc = result["LoginResponse"][0]["LoginResult"][0]["a:errorDescEN"][0].toString()
        }
        if(Number(errorCode)==0){
          token=result["LoginResponse"][0]["LoginResult"][0]["a:token"][0]
          fName=result["LoginResponse"][0]["LoginResult"][0]["a:firstName"][0]
          lName=result["LoginResponse"][0]["LoginResult"][0]["a:lastName"][0]
          name=fName.trim()+" "+lName.trim()
          tel=result["LoginResponse"][0]["LoginResult"][0]["a:mobileNo"][0]
          mail=result["LoginResponse"][0]["LoginResult"][0]["a:email"][0]
          lineId=result["LoginResponse"][0]["LoginResult"][0]["a:lineID"][0]
          //////////////////////////////////
          utils.logIn(this.state.policyNumber,memberNo,token,name,tel,mail,lineId)
          this.props.logIn();
          utils.setPin(inputtedPin)
          //////////////////////////////////
          let xmlBody2 = utils.GetMemberBenefitBody(this.state.policyNumber,memberNo,token);
          console.log(`Request body: ${xmlBody2}`);
          fetch(utils.urlMemberService, utils.genRequest(utils.GetMemberBenefitSOAPAction,xmlBody2) )
            .then(response => response.text())
            .then(response => {
              result2 = utils.getResult(response)
              let errorCode = result2["GetMemberBenefitResponse"][0]["GetMemberBenefitResult"][0]["a:errorCode"][0].toString()
              let errorDesc = ""
              if(lang=="th"){
                errorDesc = result2["GetMemberBenefitResponse"][0]["GetMemberBenefitResult"][0]["a:errorDescTH"][0].toString()
              }else{
                errorDesc = result2["GetMemberBenefitResponse"][0]["GetMemberBenefitResult"][0]["a:errorDescEN"][0].toString()
              }
              if(Number(errorCode)==0){
                let memberData = utils.GetMemberBenefitData(result2)
                let noti = Number(memberData['claimTotNotify'])
                let notiList = memberData['notificationList']
                let hasECard = memberData['hasECard']
                this._memberDataChanged(memberData)
                this._notificationChanged(noti)
                this._notificationListChanged(notiList)
                this._hasECardChanged(hasECard)
                // setTimeout(() => {
                this.props.navigation.navigate(this.props.lastScreen);
                // },1000)
              }else{
                this.goToFirstPin()
                Alert.alert(labels.getLabel(lang,"error"),errorDesc)
              }
            })
            .catch(err => {
              this.setState({loginProcess:false})
              console.log("fetch", err);
              Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
              this.goToFirstPin()
            });
          /////////////////////////////////
        }else{
          this.setState({loginProcess:false})
          Alert.alert(labels.getLabel(lang,"error"),errorDesc)
          this.goToFirstPin()
        }
      })
      .catch(err => {
        this.setState({loginProcess:false})
        console.log("fetch", err);
        Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
        this.goToFirstPin()
      });
  }

  _memberDataChanged = (data) => {
    this.props.memberDataChanged(data);
  }

  _notificationChanged(noti){
    this.props.notificationChanged(noti)
  }

  _notificationListChanged(noti){
    this.props.notificationListChanged(noti)
  }

  _hasECardChanged = (data) => {
    this.props.hasECardChanged(data)
  }

  goToFirstPin = () => {
    this.setState({
      pinLabel:"enterPin",
      pin1st:"",
      pin2nd:"",
      successTextLabel:"",
      errorTextLabel:"",
    })
    this.pinView.clear()
  }

  render() {
    return (
      <View style={LoginStyles.main}>
        <View style={[LoginStyles.paddingStatus,{backgroundColor:"white"}]}>
          <View style={{marginTop:60,marginBottom:20,alignItems:'center'}}>
            <DefaultText style={{color: Colors.bondiBlue,fontSize:18}}>{labels.getLabel(this.props.language,this.state.pinLabel)}</DefaultText>
          </View>
          <View style={{backgroundColor:'white',paddingTop:5}}>
            <PinView
              ref={pinView => this.pinView = pinView}
              onComplete={this.onComplete.bind(this)}
              inputBgOpacity={1}
              pinLength={6}
              buttonBgColor={'white'}
              inputBgColor={'white'}
              inputActiveBgColor={Colors.bondiBlue}
              buttonTextColor={'black'}
              successfulText={this.state.successTextLabel}
              errorText={this.state.errorTextLabel}
              deleteText={"DELICON"}
            />
          </View>
        </View>

        {/* back button */}
          <TouchableOpacity style={{position:'absolute',left:10,top:45}} onPress={() => this.navigateBack()}>
            <Image
              style={{ width: 22, height: 22 }}
              source={require("../assets/icon/Back-button-alt.png")}
            />
          </TouchableOpacity>
        
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus } = auth;
  return { language, lastScreen, loginStatus };
};

export default connect(
  mapStateToProps,
  {
    logIn,
    languageChanged,
    memberDataChanged,
    notificationChanged,
    notificationListChanged,
    hasECardChanged,
  }
)(ReRegisterPinSetupScreen);
