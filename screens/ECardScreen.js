import React, { Component } from "react";
import {
  ActivityIndicator,
  Alert,
  BackHandler,
  Dimensions,
  Image,
  ImageBackground,
  PanResponder,
  PixelRatio,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import Constants from "expo-constants";
import {
  useKeepAwake,
  activateKeepAwake,
  deactivateKeepAwake
} from "expo-keep-awake";
import { Icon } from "react-native-elements";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import {
  languageChanged,
  lastScreenChanged,
  eCardDataChanged,
  logOut
} from "../actions";
import CustomUserInactivity from "../components/CustomUserInactivity";
import SliderPersonal, {
  sliderWidth,
  itemWidth
} from "../styles/SliderPersonal.style";
import Carousel, { Pagination } from "react-native-snap-carousel";
import * as labels from "../constants/label";
import MainStyles from "../styles/Main.styles";
import * as utils from "../functions";
import { StylesText } from "../components/StyledText";
import PinchZoomView from "react-native-pinch-zoom-view";
import NodataList from "../components/NoDataList";
import DefaultText from "../components/DefaultText";
import { Header } from "react-navigation";

// const BASE_SIZE = { width: 456, height: 292 };
// const BASE_SIZE = { width: 228, height: 146 }; // /2
const BASE_SIZE = { width: 1014, height: 639 };
// const BASE_SIZE = { width: 230, height: 145 }; // /4.5
const imageBackground = require("../assets/images/card-full.png");
const imageBackground2th = require("../assets/images/card-back-full-th.png");
const imageBackground2en = require("../assets/images/card-back-full-en.png");
const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

class ECardScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pinStore: "",
      apiLength: 0,
      slider1ActiveSlide: 0,
      EnableView: false,
      cardFront: true
    };
  }

  indexStart = 0;

  static navigationOptions = ({ navigation }) => ({
    title:
      typeof navigation.state.params === "undefined" ||
      typeof navigation.state.params.title === "undefined"
        ? ""
        : navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header
    },
    headerTitleStyle: {
      fontFamily: "Prompt_bold",
      fontWeight: "200",
      fontSize: 20 / PixelRatio.getFontScale()
    }
  });

  componentDidMount() {
    this.props.navigation.addListener("willFocus", this.load);
    this.props.navigation.addListener("willBlur", this.blur);
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (e, gestureState) => {
        return true;
      },
      onPanResponderMove: (e, gestureState) => {
        const { dy, dx } = gestureState;

        this.setState({
          topTransition: dy,
          leftTransition: dx
        });
      },
      onPanResponderRelease: (e, gestureState) => {
        const { top, left } = this.state;
        const { dy, dx } = gestureState;

        this.setState({
          cardFront: !this.state.cardFront
        });
      }
    });
  }
  load = () => {
    utils.log("Start ECardScreen");
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackButton.bind(this)
    );
    this.onScreenChanged();
    activateKeepAwake();
    this.getLoginData();
    if (this.props.memberData != null) {
      this.setState({
        slider1ActiveSlide: 0,
        cardFront: true
      });
    }
    this.setTitle(this.props.language);
  };
  blur = () => {
    deactivateKeepAwake();
  };
  handleBackButton = () => {
    BackHandler.exitApp();
    return true;
  };

  onScreenChanged() {
    this.props.lastScreenChanged("ECard");
  }

  setTitle = lang => {
    if (lang == "") lang = "en";
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "eCard"),
      lang: lang
    });
  };

  getData = async key => {
    let value = await utils.retrieveData(key);
    this.setState({ [key]: value });
  };

  getLoginData = async key => {
    if (!this.props.loginStatus) {
      this.setState({ EnableView: false });
      let pinStoreVal = await utils.retrieveData("pinStore");
      if (pinStoreVal.length > 0) this.props.navigation.navigate("LoginPin");
      else this.props.navigation.navigate("Login");
    } else {
      await this.getData("policyNumberStore");
      await this.getData("memberNoStore");
      await this.getData("tokenStore");
      await this.setState({ EnableView: true });
      if (this.props.eCardLastDate != utils.getToday("en")) {
        this.fetchData();
      }
    }
  };

  fetchData = async () => {
    let lang = this.props.language;
    let data = null;
    let result = "";
    let xmlBody = utils.GetECardBody(
      this.state.policyNumberStore,
      this.state.memberNoStore,
      this.state.tokenStore
    );
    console.log("-------------------");
    console.log("Request body: " + xmlBody);
    console.log("-------------------");
    await fetch(
      utils.urlMemberService,
      utils.genRequest(utils.GetECardSOAPAction, xmlBody)
    )
      .then(response => response.text())
      .then(response => {
        result = utils.getResult(response);
        // console.log(result)
        let errorCode = result["GetECardResponse"][0]["GetECardResult"][0][
          "a:errorCode"
        ][0].toString();
        let errorDesc = "";
        if (lang == "th") {
          errorDesc = result["GetECardResponse"][0]["GetECardResult"][0][
            "a:errorDescTH"
          ][0].toString();
        } else {
          errorDesc = result["GetECardResponse"][0]["GetECardResult"][0][
            "a:errorDescEN"
          ][0].toString();
        }
        // console.log(errorCode)
        if (Number(errorCode) == 0) {
          /////////////////////
          let eCardData = utils.getECardInfo(result);
          this.setState({ apiLength: eCardData["eCardInfo"].length });
          this.props.eCardDataChanged(eCardData);
        } else if (Number(errorCode) == 14) {
          Alert.alert(labels.getLabel(lang, "error"), errorDesc);
          let eCardData = {};
          let emptyArr = [];
          eCardData["eCardInfo"] = emptyArr;
          this.props.eCardDataChanged(eCardData);
        } else {
          console.log("fetch", errorDesc);
          // Alert.alert(labels.getLabel(lang,"error"),errorDesc)
        }
      })
      .catch(err => {
        console.log("fetch", err);
        Alert.alert(
          labels.getLabel(lang, "error"),
          labels.getLabel(lang, "errorNetwork")
        );
      });
  };

  genBox = ({ item, index }) => {
    let lang =
      item.countryCode == null || item.countryCode == "TH" ? "th" : "en";
    let langReverse = lang == "th" ? "en" : "th";
    let leftData = lang == "th" ? 250 : 280;
    let leftData2 = lang == "th" ? 300 : 280;
    let rightData = lang == "th" ? 330 : 250;
    //const containerSize = { ...BASE_SIZE, height: BASE_SIZE.height * scale };

    let claim = [];
    let bottomLeft = [];
    let remark = [];
    if (
      item.genPage01 == "CARD01" ||
      item.genPage01 == "CARD02" ||
      item.genPage01 == "CARD03" ||
      item.genPage01 == "CARD04" ||
      item.genPage01 == "CARD05"
    ) {
      remark[1] = item.genPage03;
      remark[2] = item.genPage04;
      remark[3] = item.genPage05;
      remark[4] = "";
      remark[5] = "";
      remark[6] = "";
      bottomLeft[1] = "";
      bottomLeft[2] = "";
      bottomLeft[3] = item.genPage02;
    } else if (item.genPage01 == "CARD06") {
      remark[1] = item.genPage03;
      remark[2] = item.genPage04;
      remark[3] = item.genPage05;
      remark[4] = item.genPage06;
      remark[5] = item.genPage07;
      remark[6] = item.genPage08;
      bottomLeft[1] = "";
      bottomLeft[2] = "";
      bottomLeft[3] = item.genPage02;
    } else if (item.genPage01 == "CARD07") {
      remark[1] = item.genPage03;
      remark[2] = item.genPage04;
      remark[3] = item.genPage05;
      remark[4] = "";
      remark[5] = "";
      remark[6] = "";
      bottomLeft[1] = "";
      bottomLeft[2] = "";
      bottomLeft[3] = item.genPage02;
    } else if (item.genPage01 == "CARD08") {
      remark[1] = item.genPage02;
      remark[2] = item.genPage03;
      remark[3] = item.genPage04;
      remark[4] = item.genPage05;
      remark[5] = "";
      remark[6] = "";
      bottomLeft[1] = item.genPage06;
      bottomLeft[2] = item.genPage07;
      bottomLeft[3] = item.genPage08;
    } else if (item.genPage01 == "CARD11") {
      remark[1] = "";
      remark[2] = "";
      remark[3] = "";
      remark[4] = "";
      remark[5] = "";
      remark[6] = "";
      bottomLeft[1] = "";
      bottomLeft[2] = "";
      bottomLeft[3] = item.genPage02;
    }

    let claims = "";

    // for(let j=1;j<=7;j++){
    //   claim[j] = ""
    // }
    // let i = 1
    let countClaims = 0;
    if (item.claimLifeGTL != null && item.claimLifeGTL != "") {
      // claim[i] = item.claimLifeGTL
      claims =
        claims == "" ? item.claimLifeGTL : claims + "\n" + item.claimLifeGTL;
      countClaims++;
    }
    if (item.claimLifeGPTD != null && item.claimLifeGPTD != "") {
      // claim[i] = item.claimLifeGPTD
      claims =
        claims == "" ? item.claimLifeGPTD : claims + "\n" + item.claimLifeGPTD;
      countClaims++;
    }
    if (item.claimBenefit1 != null && item.claimBenefit1 != "") {
      // claim[i] = item.claimBenefit1
      claims =
        claims == "" ? item.claimBenefit1 : claims + "\n" + item.claimBenefit1;
      countClaims++;
    }
    if (item.claimBenefit2 != null && item.claimBenefit2 != "") {
      // claim[i] = item.claimBenefit2
      claims =
        claims == "" ? item.claimBenefit2 : claims + "\n" + item.claimBenefit2;
      countClaims++;
    }
    if (item.genPage01 == "CARD11") {
      if (item.claimBenefit8 != null && item.claimBenefit8 != "") {
        claims =
          claims == ""
            ? item.claimBenefit8
            : claims + "\n" + item.claimBenefit8;
        countClaims++;
      }
    }
    if (item.claimBenefit3 != null && item.claimBenefit3 != "") {
      // claim[i] = item.claimBenefit3
      claims =
        claims == "" ? item.claimBenefit3 : claims + "\n" + item.claimBenefit3;
      countClaims++;
    }
    if (item.claimBenefit4 != null && item.claimBenefit4 != "") {
      // claim[i] = item.claimBenefit4
      claims =
        claims == "" ? item.claimBenefit4 : claims + "\n" + item.claimBenefit4;
      countClaims++;
    }
    if (item.claimBenefit5 != null && item.claimBenefit5 != "") {
      // claim[i] = item.claimBenefit5
      claims =
        claims == "" ? item.claimBenefit5 : claims + "\n" + item.claimBenefit5;
      countClaims++;
    }
    if (item.claimBenefit6 != null && item.claimBenefit6 != "") {
      // claim[i] = item.claimBenefit6
      claims =
        claims == "" ? item.claimBenefit6 : claims + "\n" + item.claimBenefit6;
      countClaims++;
    }
    if (item.genPage01 == "CARD09" || item.genPage01 == "CARD10") {
      if (item.claimBenefit7 != null && item.claimBenefit7 != "") {
        claims =
          claims == ""
            ? item.claimBenefit7
            : claims + "\n" + item.claimBenefit7;
        countClaims++;
      }
    }

    let remarks = "";
    for (let k = 1; k <= 6; k++) {
      if (remark[k] != null && remark[k] != "") {
        if (remarks == "") {
          remarks = remark[k];
        } else {
          remarks = remarks + "\n" + remark[k];
        }
      }
    }

    //set fontsize claim
    let customFontsize = 0;
    // for debug
    //countClaims = 4
    //claims = "GTL: 1,252,8000 บาท\nGAD: 1,879,200 บาท\nIPD Plan002: (R&B:3,000 บาท/วัน)\nIPD Plan002: (R&B:3,000 บาท/วัน)\nOPD: 2,000 บาท/ครั้ง/วัน\nIPD Plan002: (R&B:3,000 บาท/วัน)\nIPD Plan002: (R&B:3,000 บาท/วัน)"
    //claims = "GTL/GPTD: 500,000 บาท\nGAD: 500,000 บาท\nIPD: Plan 002: (R&B:3,000 บาท/วัน)\nER: 20,000 บาท/ครั้ง\nCPD: 2,000 บาท/ครั้ง/วัน"
    let customTop1 = 420;
    switch (countClaims) {
      case 4:
        customFontsize = 33;
        break;
      case 5:
        customFontsize = 30;
        break;
      case 6:
        customFontsize = 25;
        break;
      case 7:
        customFontsize = 21;
        break;
      case 8:
        customFontsize = 18;
        break;
      default:
        customFontsize = 35;
    }
    /*let customTop1 = lang=="th" ? 445 : 425
    switch(countClaims) {
      case 4:
        customFontsize = lang=="th" ? 37 : 35
        break;
      case 5:
        customFontsize = lang=="th" ? 32 : 35
        break;
      case 6:
        customFontsize = lang=="th" ? 28 : 30
        break;
      case 7:
        customFontsize = lang=="th" ? 24 : 26
        break;
      default:
        customFontsize = lang=="th" ? 37 : 37
    }*/

    let remarkLeft = 0;
    let remarkTop = 0;
    let remarkWidth = 0;
    let remarkFont = 0;
    if (item.genPage01 == "CARD08") {
      remarkLeft = 670;
      remarkTop = 270;
      remarkWidth = 330;
      remarkFont = 24;
    } else {
      remarkLeft = 700;
      remarkTop = 350;
      remarkWidth = 300;
      remarkFont = 28;
    }

    let maxWidthBody = width - 40;
    let bottomHeight = 48;
    let headerBody = lang == "th" ? 135 : 150; // orange box + date + pagnition
    let heightBody =
      height -
      Constants.statusBarHeight -
      Header.HEIGHT -
      headerBody -
      bottomHeight;
    let scale = heightBody / BASE_SIZE.width;

    if (BASE_SIZE.height * scale > maxWidthBody) {
      scale = maxWidthBody / BASE_SIZE.height;
    }

    let diff = (BASE_SIZE.width - BASE_SIZE.height) / 2;
    let cardMarginTop =
      diff + ((BASE_SIZE.width - BASE_SIZE.width * scale) / 2) * -1 + 5; //space

    const transform = { transform: [{ scale }, { rotate: "90deg" }] };

    let uri =
      Constants.manifest.extra.urlFile2 + this.props.eCardData["signaturePic"];
    let policyHolder = this.props.eCardData["policyHolder"] + "";
    let insuredName = item.FName + " " + item.LName + " " + item.insuranceNo;

    let policyTop = 0;
    //set line height holder
    let customLineHeight = 0;
    if (policyHolder.length > 40) {
      customLineHeight = 35;
      policyTop = 230;
    } else {
      customLineHeight = 45;
      policyTop = 250;
    }

    return (
      <ScrollView style={{ margin: 5 }}>
        <View style={{ alignItems: "center", flex: 1, height }}>
          <View
            style={[SliderPersonal.slideInnerContainer, { marginBottom: 5 }]}
          >
            {index != 0 ? (
              <TouchableOpacity
                style={SliderPersonal.slideInnerSideContainer}
                onPress={() => {
                  this._slider1Ref.snapToPrev();
                }}
              >
                <Icon
                  name="chevron-left"
                  type="evilicon"
                  color={Colors.bondiBlue}
                  size={40}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity style={SliderPersonal.slideInnerSideContainer}>
                <DefaultText> </DefaultText>
              </TouchableOpacity>
            )}
            <View style={SliderPersonal.slideInnerCenterContainer}>
              <DefaultText
                bold
                style={{ color: Colors.bondiBlue, fontSize: 17 }}
              >
                {item.FName} {item.LName}
              </DefaultText>
            </View>
            {index != this.state.apiLength - 1 ? (
              <TouchableOpacity
                style={SliderPersonal.slideInnerSideContainer}
                onPress={() => {
                  this._slider1Ref.snapToNext();
                }}
              >
                <Icon
                  name="chevron-right"
                  type="evilicon"
                  color={Colors.bondiBlue}
                  size={40}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity style={SliderPersonal.slideInnerSideContainer}>
                <DefaultText> </DefaultText>
              </TouchableOpacity>
            )}
          </View>

          <View
            style={{ width: width - 2, alignItems: "center" }}
            {...this.panResponder.panHandlers}
          >
            {/* card */}
            {/* <View style={{overflow:'hidden',backgroundColor: 'black',width:width-20}}> */}
            <View style={{ marginTop: 0 }}>
              <DefaultText
                style={
                  this.props.language == "th"
                    ? styles.fontDateTh
                    : styles.fontDateEn
                }
              >
                {labels.getLabel(this.props.language, "ECard_labelDate")}{" "}
                {utils.getToday(this.props.language)}{" "}
                {labels.getLabel(this.props.language, "ECard_labelDate2")}
              </DefaultText>
            </View>

            <TouchableOpacity
              onPress={() =>
                this.setState({ cardFront: !this.state.cardFront })
              }
              activeOpacity={0.8}
            >
              <Pagination
                dotsLength={2}
                activeDotIndex={this.state.cardFront ? 0 : 1}
                containerStyle={{ paddingVertical: 2 }}
                dotStyle={{
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  marginHorizontal: 0,
                  backgroundColor: "rgba(0, 0, 0, 0.75)"
                }}
                inactiveDotStyle={
                  {
                    // Define styles for inactive dots here
                  }
                }
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
              />
              {this.state.cardFront ? (
                <ImageBackground
                  style={[
                    BASE_SIZE,
                    transform,
                    { marginTop: cardMarginTop, marginBottom: cardMarginTop }
                  ]}
                  source={imageBackground}
                >
                  <StylesText
                    bold
                    style={[
                      styles.cardHeader,
                      {
                        position: "absolute",
                        left: 0,
                        right: 0,
                        top: 120,
                        textAlign: "center"
                      }
                    ]}
                  >
                    {labels.getLabel(lang, "ECard_CardHeader")}
                  </StylesText>
                  <StylesText
                    style={[
                      styles.cardHeaderSub,
                      {
                        position: "absolute",
                        left: 0,
                        right: 0,
                        top: 165,
                        textAlign: "center"
                      }
                    ]}
                  >
                    {labels.getLabel(langReverse, "ECard_CardHeader")}
                  </StylesText>

                  <StylesText
                    bold
                    style={[
                      lang == "th" ? styles.cardText2 : styles.cardText,
                      { position: "absolute", left: 50, top: 195 }
                    ]}
                  >
                    {labels.getLabel(lang, "ECard_PolicyNo")}
                  </StylesText>
                  <StylesText
                    style={[
                      styles.cardTextSub,
                      { position: "absolute", left: 50, top: 225 }
                    ]}
                  >
                    {labels.getLabel(langReverse, "ECard_PolicyNo")}
                  </StylesText>
                  <StylesText
                    bold
                    style={[
                      styles.cardText,
                      { position: "absolute", left: leftData, top: 195 }
                    ]}
                  >
                    {this.props.eCardData["policyNo"]}
                  </StylesText>

                  <StylesText
                    bold
                    style={[
                      lang == "th" ? styles.cardText2 : styles.cardText,
                      { position: "absolute", left: 507, top: 195 }
                    ]}
                  >
                    {labels.getLabel(lang, "ECard_MemberNo")}
                  </StylesText>
                  <StylesText
                    style={[
                      styles.cardTextSub,
                      { position: "absolute", left: 507, top: 225 }
                    ]}
                  >
                    {labels.getLabel(langReverse, "ECard_MemberNo")}
                  </StylesText>
                  <StylesText
                    bold
                    style={[
                      styles.cardText,
                      { position: "absolute", left: 507 + rightData, top: 195 }
                    ]}
                  >
                    {item.MemberNo}
                  </StylesText>

                  <StylesText
                    bold
                    style={[
                      lang == "th" ? styles.cardText2 : styles.cardText,
                      { position: "absolute", left: 50, top: 250 }
                    ]}
                  >
                    {labels.getLabel(lang, "ECard_Policyholder")}
                  </StylesText>
                  <StylesText
                    style={[
                      styles.cardTextSub,
                      { position: "absolute", left: 50, top: 280 }
                    ]}
                  >
                    {labels.getLabel(langReverse, "ECard_Policyholder")}
                  </StylesText>
                  {/* <StylesText bold style={[policyHolder.length>40?styles.cardText2:styles.cardText,{position:'absolute',left:policyTop,top:250,width:1014-leftData,lineHeight:customLineHeight}]}>{policyHolder}</StylesText> */}
                  <StylesText
                    bold
                    style={[
                      item.genPage01 == "CARD08"
                        ? styles.cardText3
                        : policyHolder.length > 40
                        ? styles.cardText2
                        : styles.cardText,
                      {
                        position: "absolute",
                        left: policyTop,
                        top: 250,
                        width: 1014 - leftData,
                        lineHeight: customLineHeight
                      }
                    ]}
                  >
                    {policyHolder}
                  </StylesText>

                  <StylesText
                    bold
                    style={[
                      lang == "th" ? styles.cardText2 : styles.cardText,
                      { position: "absolute", left: 50, top: 305 }
                    ]}
                  >
                    {labels.getLabel(lang, "ECard_InsuredName")}
                  </StylesText>
                  <StylesText
                    style={[
                      styles.cardTextSub,
                      { position: "absolute", left: 50, top: 335 }
                    ]}
                  >
                    {labels.getLabel(langReverse, "ECard_InsuredName")}
                  </StylesText>
                  {/* <StylesText bold style={[insuredName.length>40?styles.cardText2:styles.cardText,{position:'absolute',left:leftData,top:305}]}>{insuredName}</StylesText> */}
                  <StylesText
                    bold
                    style={[
                      item.genPage01 == "CARD08"
                        ? styles.cardText3
                        : insuredName.length > 40
                        ? styles.cardText2
                        : styles.cardText,
                      { position: "absolute", left: leftData, top: 305 }
                    ]}
                  >
                    {insuredName}
                  </StylesText>

                  <StylesText
                    bold
                    style={[
                      lang == "th" ? styles.cardText2 : styles.cardText,
                      { position: "absolute", left: 50, top: 360 }
                    ]}
                  >
                    {labels.getLabel(lang, "ECard_Efd")}
                  </StylesText>
                  <StylesText
                    style={[
                      styles.cardTextSub,
                      { position: "absolute", left: 50, top: 390 }
                    ]}
                  >
                    {labels.getLabel(langReverse, "ECard_Efd")}
                  </StylesText>
                  <StylesText
                    bold
                    style={[
                      styles.cardText,
                      { position: "absolute", left: leftData, top: 360 }
                    ]}
                  >
                    {item.policyStartDT != null &&
                      item.policyEndDT != null &&
                      item.policyStartDT + " - " + item.policyEndDT}
                  </StylesText>

                  <StylesText
                    bold
                    style={[
                      lang == "th" ? styles.cardText2 : styles.cardText,
                      { position: "absolute", left: 50, top: 415 }
                    ]}
                  >
                    {labels.getLabel(lang, "ECard_BenefitCoverage")}
                  </StylesText>
                  {lang == "th" && (
                    <StylesText
                      bold
                      style={[
                        lang == "th" ? styles.cardText2 : styles.cardText,
                        { position: "absolute", left: 50, top: 440 }
                      ]}
                    >
                      {labels.getLabel(lang, "ECard_BenefitCoverageSub")}
                    </StylesText>
                  )}
                  <StylesText
                    style={[
                      styles.cardTextSub,
                      {
                        position: "absolute",
                        left: 50,
                        top: lang == "th" ? 465 : 445
                      }
                    ]}
                  >
                    {labels.getLabel(langReverse, "ECard_BenefitCoverage")}
                  </StylesText>
                  <StylesText
                    bold
                    style={[
                      {
                        fontSize: customFontsize,
                        position: "absolute",
                        left: leftData2,
                        top: customTop1
                      }
                    ]}
                  >
                    {claims}
                  </StylesText>

                  {/* sign */}
                  <Image
                    source={{ uri }}
                    style={{
                      width: 191,
                      height: 56,
                      position: "absolute",
                      left: 795,
                      bottom: 25
                    }}
                  />
                  <StylesText
                    bold
                    style={[
                      lang == "th" ? styles.cardText2 : styles.cardText,
                      { position: "absolute", left: 670, bottom: 40 }
                    ]}
                  >
                    {labels.getLabel(lang, "ECard_IssuedBy")}
                  </StylesText>
                  <StylesText
                    style={[
                      styles.cardTextSub,
                      { position: "absolute", left: 670, bottom: 10 }
                    ]}
                  >
                    {labels.getLabel(langReverse, "ECard_IssuedBy")}
                    ..................................................
                  </StylesText>

                  {/* remark */}
                  {remarks != "" && (
                    <View
                      style={[
                        {
                          position: "absolute",
                          left: remarkLeft,
                          top: remarkTop,
                          width: remarkWidth,
                          borderWidth: 2,
                          padding: 5
                        }
                      ]}
                    >
                      <StylesText
                        bold
                        style={[styles.cardTextSub2, { fontSize: remarkFont }]}
                      >
                        {remarks}
                      </StylesText>
                    </View>
                  )}

                  {/* bottomLeft */}
                  <View
                    style={[
                      {
                        position: "absolute",
                        left: item.genPage01 == "CARD08" ? 18 : 50,
                        bottom: 0
                      }
                    ]}
                  >
                    <StylesText
                      bold
                      style={[
                        item.genPage01 == "CARD08"
                          ? styles.cardTextSub2
                          : styles.cardTextSub,
                        { position: "absolute", bottom: 60 }
                      ]}
                    >
                      {bottomLeft[1]}
                    </StylesText>
                    <StylesText
                      bold
                      style={[
                        item.genPage01 == "CARD08"
                          ? styles.cardTextSub2
                          : styles.cardTextSub,
                        { position: "absolute", bottom: 35 }
                      ]}
                    >
                      {bottomLeft[2]}
                    </StylesText>
                    <StylesText
                      bold
                      style={[
                        item.genPage01 == "CARD08"
                          ? styles.cardTextSub2
                          : styles.cardTextSub,
                        { position: "absolute", bottom: 10 }
                      ]}
                    >
                      {bottomLeft[3]}
                    </StylesText>
                  </View>
                </ImageBackground>
              ) : (
                <ImageBackground
                  style={[
                    BASE_SIZE,
                    transform,
                    { marginTop: cardMarginTop, marginBottom: cardMarginTop }
                  ]}
                  source={
                    lang == "th" ? imageBackground2th : imageBackground2en
                  }
                ></ImageBackground>
              )}
            </TouchableOpacity>

            {/* </View> */}
          </View>
        </View>
      </ScrollView>
    );
  };

  genCard = ({ item, index }) => {
    if (index == 0) {
    } else {
      return (
        <ImageBackground
          style={[
            BASE_SIZE,
            transform,
            { marginTop: cardMarginTop + 10, marginBottom: cardMarginTop + 10 }
          ]}
          source={lang == "th" ? imageBackground2th : imageBackground2en}
        ></ImageBackground>
      );
    }
  };

  render() {
    if (this.state.EnableView) {
      return (
        <View style={MainStyles.main}>
          <View style={[MainStyles.container, { paddingTop: 5 }]}>
            {this.props.eCardData != null ? (
              this.props.eCardData["eCardInfo"].length == 0 ? (
                <NodataList />
              ) : (
                <Carousel
                  ref={c => (this._slider1Ref = c)}
                  data={this.props.eCardData["eCardInfo"]}
                  renderItem={this.genBox}
                  sliderWidth={sliderWidth}
                  itemWidth={sliderWidth}
                  hasParallaxImages={true}
                  firstItem={0}
                  inactiveSlideScale={1}
                  inactiveSlideOpacity={0}
                  loop={false}
                  autoplay={false}
                  onSnapToItem={index =>
                    this.setState({
                      slider1ActiveSlide: index,
                      cardFront: true
                    })
                  }
                  windowSize={1}
                  scrollEnabled={false}
                  removeClippedSubviews={false}
                  enableSnap={false}
                />
              )
            ) : (
              <ActivityIndicator size="large" color="#666" />
            )}
            {/*this.genBox(0,1)*/}
          </View>
          <View>
            <DefaultText style={styles.ecardRemark}>{labels.getLabel(lang, "Ecard_Remark")}</DefaultText>
          </View>
        </View>
      );
    } else {
      return <View></View>;
    }
  }
}

const styles = StyleSheet.create({
  cardHeader: {
    fontSize: 45
  },
  cardHeaderSub: {
    fontSize: 30
  },
  cardHeaderSub2: {
    fontSize: 40
  },
  cardText: {
    fontSize: 40
  },
  cardText2: {
    fontSize: 35
  },
  cardText3: {
    fontSize: 30
  },
  cardTextSub: {
    fontSize: 30
  },
  cardTextSub2: {
    fontSize: 24
  },
  cardTextSub3: {
    fontSize: 17
  },
  fontDateEn: {
    fontSize: 14,
    color: Colors.bondiBlue,
    textAlign: "center"
  },
  fontDateTh: {
    fontSize: 16,
    color: Colors.bondiBlue,
    textAlign: "center"
  },
  ecardRemark: {
    color: "red",
    fontSize: 14
  }
});

const mapStateToProps = ({ auth }) => {
  const {
    language,
    lastScreen,
    loginStatus,
    memberData,
    eCardData,
    eCardLastDate
  } = auth;
  return {
    language,
    lastScreen,
    loginStatus,
    memberData,
    eCardData,
    eCardLastDate
  };
};

export default connect(
  mapStateToProps,
  {
    languageChanged,
    lastScreenChanged,
    eCardDataChanged,
    logOut
  }
)(ECardScreen);
