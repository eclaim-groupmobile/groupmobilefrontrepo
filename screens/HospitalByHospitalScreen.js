import React, { Component } from "react";
import {
  Alert,
  ActivityIndicator,
  AppState,
  Dimensions,
  FlatList,
  Image,
  Linking,
  PixelRatio,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import Constants from 'expo-constants';
import * as SQLite from 'expo-sqlite';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import { Icon, CheckBox, FormLabel, SearchBar } from "react-native-elements";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import CustomUserInactivity from "../components/CustomUserInactivity";
import * as labels from "../constants/label";
import Picker from "../components/picker";
import MainStyles from "../styles/Main.styles";
import HospitalStyles from "../styles/Hospital.style";
import * as utils from "../functions";
import HospitalCard from "../components/HospitalCard";
import NodataList from "../components/NoDataList"
import DefaultText from "../components/DefaultText"

class HospitalByHospitalScreen extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      apiData:[],
      search:"",
      refreshing: false,
      location: null,
      permission:"false",
      locationEnable:"false",
      searchBox: '',
      searchBoxUse:false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    headerTintColor: Colors.bondiBlue,
    headerStyle: {
      backgroundColor: Colors.header,
    },
    headerTitleStyle: { fontFamily:'Prompt_bold',fontWeight:'200',fontSize:20/PixelRatio.getFontScale()},
    headerLeft: <TouchableOpacity onPress={ () => {navigation.navigate("Hospital")}} style={{marginLeft:15}}><Image style={{ width: 22, height: 22 }} source={require("../assets/icon/Back-button-alt2.png")} /></TouchableOpacity>,
  });

  componentDidMount() {
    handleAndroidBackButton(this.navigateBack);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start HospitalByHospitalScreen");
    let lang = this.props.language
    this.setState({
      apiData:[],
      search:this.props.hospitalData[lang][0],
      permission:"false",
      locationEnable:"false",
    })
    this.setTitle(this.props.language)
    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({ permission: true });
      Alert.alert(
        labels.getLabel(lang,'error'),
        'This page will not work on android emulator',
        [
          {text: 'OK', onPress: () => this.props.navigation.navigate("Home")},
        ],
        { cancelable: false }
        )
    } else {
      this.getLocationAsync();
    }
  };

  getLocationAsync = async () => {
    let lang = this.props.language
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({ permission: false });
    }else{
      this.setState({ permission: true });
      let locationStatus = await Location.getProviderStatusAsync()
      if(locationStatus.locationServicesEnabled){
        this.setState({ locationEnable: true });
        let location = await Location.getCurrentPositionAsync({accuracy : 6});
        this.setState({lat: location.coords.latitude,long: location.coords.longitude})
      }else{
        this.setState({ locationEnable: false });
        Alert.alert(
          labels.getLabel(lang,'error'),
          labels.getLabel(lang,'locationServicesDisable'),
          [
            {text: labels.getLabel(lang,'cancel'), onPress: () => console.log("Disabled Location Service"), style: 'cancel'},
            {text: labels.getLabel(lang,'ok'), onPress: () => this.getLocationAsync()},
          ],
          { cancelable: false }
        )
      }
    }
  };

  navigateBack = () => {
    this.props.navigation.navigate("Hospital");
  };

  setTitle = (lang) => {
    if(lang=="") lang='en'
    this.props.navigation.setParams({
      title: labels.getTitle(lang, "hospitalByHospital"),
      lang: lang,
    });
  }

  onSearchHandle(){
    this.picker.toggle();
  }

  onSearchComplete = (selectedValue) => {
    if(selectedValue.constructor !== Array){
      selectedValue = [selectedValue];
    }else{
      selectedValue = selectedValue[0]
    }
    this.onFetch(selectedValue)
  }

  onFetch = selectedValue => {
    let lang = this.props.language
    const db = SQLite.openDatabase('db.MasterData');

    if(selectedValue==null || selectedValue==""){
      selectedValue = this.state.search
    }else{
      this.setState({searchBox:""})
      this.setState({searchBoxUse:false})
    }
    this.setState({search:selectedValue})
    
    let param = selectedValue.trim()
    if (param!="") {
      this.setState({refreshing: true});
      let field = lang == 'th' ? 'nameTH' : 'nameEN'
      let sql = `select * from hospital where ${field} = ?;`
      // console.log(sql)
      db.transaction(tx => {
          tx.executeSql(
            sql,
            [param],
            (_, { rows: { _array } }) => {
              let tmpArr = _array.sort((a, b) => utils.compareObjectName(a,b,field))
              this.setState({apiData: tmpArr},() => this.setState({refreshing: false}));
            }
          );
      })
    }
  }

  updateSearchBox = searchBox  => {
    this.setState({ searchBox });
    if(searchBox==""){
      this.setState({searchBoxUse:false})
    }
  }

  onSearchBoxComplete = () => {
    let lang = this.props.language
    const db = SQLite.openDatabase('db.MasterData');
    
    selectedValue = this.state.searchBox
    let param = selectedValue.trim()

    if (param!="") {
      this.setState({refreshing: true});
      this.setState({search:""})
      this.setState({searchBoxUse:true})
      let field = lang == 'th' ? 'nameTH' : 'nameEN'
      let sql = `select * from hospital where ${field} like ?;`
      // console.log(sql)
      db.transaction(tx => {
          tx.executeSql(
            sql,
            ["%"+param+"%"],
            (_, { rows: { _array } }) => {
              let tmpArr = _array.sort((a, b) => utils.compareObjectName(a,b,field))
              this.setState({apiData: tmpArr},() => this.setState({refreshing: false}));
            }
          );
      })
    }
  }

  genBox = (item,index) => {
    return (
      <HospitalCard 
        item={item}
        index={index}
        lat={this.state.lat}
        long={this.state.long}
      />
    )
  }

  render() {
    let lang = this.props.language;
    let refreshing = this.state.refreshing;
    const deviceWidth = Dimensions.get("window").width;
    const deviceHeight = Dimensions.get("window").height;
    const { searchBox } = this.state;
    return (
      <View style={MainStyles.main}>
        <CustomUserInactivity onAction={() => this.props.navigation.navigate("Home")}>
          <View
            style={[
              MainStyles.container,
              { paddingLeft: 10, paddingRight: 10, paddingTop: 10 }
            ]}
          >
            <TouchableOpacity style={[HospitalStyles.searchBox,{marginBottom:10}]} onPress={this.onSearchHandle.bind(this)}>
              <DefaultText> </DefaultText>
              <View style={{justifyContent:'center'}}><DefaultText style={HospitalStyles.searchBoxContent}>{this.state.search=="" ? labels.getLabel(lang,"Hospital_searchHospital") : this.state.search}</DefaultText></View>
              <View style={{justifyContent:'center'}}><Icon type='entypo' name='chevron-down' color={Colors.bondiBlue} size={20} /></View>
            </TouchableOpacity>

            <SearchBar
              placeholder=""
              onChangeText={this.updateSearchBox}
              value={searchBox}
              lightTheme={true}
              clearIcon={true}
              containerStyle={{backgroundColor:'white',borderColor:'white',borderTopColor:'white',borderBottomColor:'white',marginBottom:10}}
              inputStyle={{backgroundColor:'#eee',fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              onSubmitEditing={this.onSearchBoxComplete}
            />

            { /*refreshing ? 
            <ActivityIndicator size="large" color="grey" />
              : */}
            <FlatList
              data={this.state.apiData}
              renderItem={({item,index}) => this.genBox(item,index)}
              keyExtractor={(item) => item.id.toString()}
              refreshing={this.state.refreshing}
              onRefresh={this.onFetch}
              ListEmptyComponent={(this.state.search!=""||this.state.searchBoxUse) && !this.state.refreshing && <NodataList noIcon/>}
            />
            
          </View>

          <Picker
            ref={picker => this.picker = picker}
            deviceWidth={deviceWidth}
            deviceHeight={deviceHeight}
            showDuration={300}
            showMask={true}
            pickerData={this.props.hospitalData[lang]}
            selectedValue={this.state.search}
            onPickerDone={(pickedValue) => this.onSearchComplete(pickedValue)}
            pickerBtnText={labels.getLabel(lang,"done")}
            pickerCancelBtnText={labels.getLabel(lang,"cancel")}
          />
        </CustomUserInactivity>
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
    const { language, lastScreen, hospitalData} = auth;
    return { language, lastScreen, hospitalData};
  };
  
export default connect(
  mapStateToProps,
  {
  }
)(HospitalByHospitalScreen);
