import React, { Component } from "react";
import {
  Alert,
  Dimensions,
  Image,
  PixelRatio,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  YellowBox
} from "react-native";
import { handleAndroidBackButton } from "../components/handleAndroidBackButton";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import LoginStyles from "../styles/LoginStyle.style";
import * as labels from "../constants/label";
import {
  logIn,
  eCardDataClear,
  languageChanged,
  memberDataChanged,
  notificationChanged,
  notificationListChanged,
  hasECardChanged,
  logOut,
  pinFailed,
} from "../actions";
import { TextField } from "react-native-materialui-textfield";
import * as utils from "../functions";
import { Switch } from "../components/Switch";
import DefaultText from "../components/DefaultText"
import _ from 'lodash';
import Constants from 'expo-constants';

const deviceHeight = Dimensions.get('window').height
const deviceWidth = Dimensions.get('window').width

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      policyNumber: "G",
      memberNo: "",
      memberNo2: "",
      password: "",
      errorTextLabel: "",
      complete: false,
      loginProcess: false,
      errors: {},
      imageHeight: 0,
      firstLoad: true,
    };

    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitPolicyNumber = this.onSubmitPolicyNumber.bind(this);
    this.onSubmitMemberNo = this.onSubmitMemberNo.bind(this);
    this.onSubmitMemberNo2 = this.onSubmitMemberNo2.bind(this);
    this.onSubmitPassword = this.onSubmitPassword.bind(this);

    this.policyNumberRef = this.updateRef.bind(this, 'policyNumber');
    this.memberNoRef = this.updateRef.bind(this, 'memberNo');
    this.memberNo2Ref = this.updateRef.bind(this, 'memberNo2');
    this.passwordRef = this.updateRef.bind(this, 'password');

    YellowBox.ignoreWarnings(["Can't call setState"]);
    const _console = _.clone(console);
    console.warn = message => {
      if (message.indexOf('Setting a timer') <= -1) {
        _console.warn(message);
      }
    };
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    handleAndroidBackButton(this.navigateHome);
    this.props.navigation.addListener("willFocus", this.load);
  }
  load = () => {
    utils.log("Start LoginScreen");
    this.setState({
      policyNumber: "G",
      memberNo: "",
      memberNo2: "",
      password: "",
      errorTextLabel: "",
      complete: false,
      loginProcess: false,
      errors: {},
      imageHeight: 0,
      firstLoad: true,
    })
  };

  navigateHome = () => {
    if(!this.state.loginProcess){
      this.props.navigation.navigate("Home");
    }
  };

  handleLoginPress = () => {
    let errorFlag = false
    let errors = {};

    let policyNumberError = utils.validatePolicyNumber(this.state.policyNumber);
    this.setState({policyNumberError:policyNumberError})
    if(policyNumberError!=""){
      errorFlag = true
      errors['policyNumber'] = policyNumberError
    }

    let memberNoError = utils.validateMemberNo(this.state.memberNo + "-" + this.state.memberNo2);
    if(memberNoError!=""){
      errorFlag = true
      errors['memberNo'] = memberNoError
      errors['memberNo2'] = ' '
    }

    let passwordError = utils.validatePassword(this.state.password);
    if(passwordError!=""){
      errorFlag = true
      errors['password'] = passwordError
    }

    this.setState({ errors });
    if(errorFlag){
      
    }else{
      this._onLogIn()
    }
  }

  _onLogIn = async () => {
    this.setState({complete:false})
    let lang = this.props.language
    let result=""
    let result2=""
    let token=""
    let fName=""
    let lName=""
    let name=""
    let tel=""
    let mail=""
    let lineId=""
    let memberNo = this.state.memberNo + "-" + this.state.memberNo2
    let xmlBody = utils.LoginBody(this.state.policyNumber,memberNo,this.state.password);
    let term = "FALSE"
    this.setState({loginProcess:true})
    console.log(`Request body: ${xmlBody}`);
    await fetch(utils.urlUserService, utils.genRequest(utils.LoginSOAPAction,xmlBody) )
      .then(response => response.text())
      .then(response => {
        result = utils.getResult(response)
        let errorCode = result["LoginResponse"][0]["LoginResult"][0]["a:errorCode"][0].toString()
        let errorDesc = ""
        if(lang=="th"){
          errorDesc = result["LoginResponse"][0]["LoginResult"][0]["a:errorDescTH"][0].toString()
        }else{
          errorDesc = result["LoginResponse"][0]["LoginResult"][0]["a:errorDescEN"][0].toString()
        }
        let needReRegister = result["LoginResponse"][0]["LoginResult"][0]["a:needReRegister"][0].toString()
        if(Number(errorCode)==0){
          if(needReRegister=="Y"){
            let passport = result["LoginResponse"][0]["LoginResult"][0]["a:citizenID"][0].toString()
            let dob = utils.getValue(result["LoginResponse"][0]["LoginResult"][0]["a:dob"][0])
            let tel = utils.getValue(result["LoginResponse"][0]["LoginResult"][0]["a:mobileNo"][0])
            let email = utils.getValue(result["LoginResponse"][0]["LoginResult"][0]["a:email"][0])
            let lineId = utils.getValue(result["LoginResponse"][0]["LoginResult"][0]["a:lineID"][0])
            this.props.navigation.navigate("ReRegister",{
              policyNumber: this.state.policyNumber,
              memberNo: memberNo,
              passport: passport,
              dob: dob,
              tel: tel,
              email: email,
              lineId: lineId,
            })
          }else{
            //log out old user
            this.props.logOut();
            this._notificationChanged(0)
            this.props.eCardDataClear()
            utils.logOut()
            this._hasECardChanged("FALSE")

            term=result["LoginResponse"][0]["LoginResult"][0]["a:forceAcceptAgreement"][0]
            token=result["LoginResponse"][0]["LoginResult"][0]["a:token"][0]
            fName=result["LoginResponse"][0]["LoginResult"][0]["a:firstName"][0]
            lName=result["LoginResponse"][0]["LoginResult"][0]["a:lastName"][0]
            name=fName.trim()+" "+lName.trim()
            tel=result["LoginResponse"][0]["LoginResult"][0]["a:mobileNo"][0]
            mail=result["LoginResponse"][0]["LoginResult"][0]["a:email"][0]
            lineId=result["LoginResponse"][0]["LoginResult"][0]["a:lineID"][0]
            //////////////////////////////////
            utils.logIn(this.state.policyNumber,memberNo,token,name,tel,mail,lineId)
            this.props.logIn();
            //////////////////////////////////
            let xmlBody2 = utils.GetMemberBenefitBody(this.state.policyNumber,memberNo,token);
            console.log(`Request body: ${xmlBody2}`);
            fetch(utils.urlMemberService, utils.genRequest(utils.GetMemberBenefitSOAPAction,xmlBody2) )
              .then(response => response.text())
              .then(response => {
                result2 = utils.getResult(response)
                let errorCode = result2["GetMemberBenefitResponse"][0]["GetMemberBenefitResult"][0]["a:errorCode"][0].toString()
                let errorDesc = ""
                if(lang=="th"){
                  errorDesc = result2["GetMemberBenefitResponse"][0]["GetMemberBenefitResult"][0]["a:errorDescTH"][0].toString()
                }else{
                  errorDesc = result2["GetMemberBenefitResponse"][0]["GetMemberBenefitResult"][0]["a:errorDescEN"][0].toString()
                }
                if(Number(errorCode)==0){
                  let memberData = utils.GetMemberBenefitData(result2)
                  let noti = Number(memberData['claimTotNotify'])
                  let notiList = memberData['notificationList']
                  let hasECard = memberData['hasECard']
                  this._memberDataChanged(memberData)
                  this._notificationChanged(noti)
                  this._notificationListChanged(notiList)
                  this._hasECardChanged(hasECard)
                  this._pinFailed(0)
                  // setTimeout(() => {
                  if(term=="TRUE"){
                    this.props.navigation.navigate("LoginTerm",{
                      policyNumber: this.state.policyNumber,
                      memberNo,
                      nextScreen: "LoginPinSetUp",
                    });
                  }else{
                    this.props.navigation.navigate("LoginPinSetUp");
                  }
                  
                  // },1000)
                }else{
                  this.setState({loginProcess:false})
                  Alert.alert(labels.getLabel(lang,"error"),errorDesc)
                }
                this.setState({complete:true})
              })
              .catch(err => {
                this.setState({loginProcess:false})
                console.log("fetch", err);
                Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
                this.setState({complete:true})
              });
          }
          /////////////////////////////////
        // }else if(Number(errorCode)==99){
        //   this.props.navigation.navigate("ReLogin",{
        //     policyNumber: this.state.policyNumber,
        //     memberNo: memberNo,
        //     password: this.state.password,
        //   })
        }else{
          this.setState({loginProcess:false})
          Alert.alert(labels.getLabel(lang,"error"),errorDesc)
          this.setState({complete:true})
        }
      })
      .catch(err => {
        this.setState({loginProcess:false})
        console.log("fetch", err);
        Alert.alert(labels.getLabel(lang,"error"),labels.getLabel(lang,"errorNetwork"))
        this.setState({complete:true})
      });
  }

  _memberDataChanged = (data) => {
    this.props.memberDataChanged(data);
  }

  _notificationChanged(noti){
    this.props.notificationChanged(noti)
  }

  _notificationListChanged(noti){
    this.props.notificationListChanged(noti)
  }

  _hasECardChanged = (data) => {
    this.props.hasECardChanged(data)
  }

  _pinFailed = (data) => {
    this.props.pinFailed(data)
  }

  handleCheckCompleteField = (text,name) => {
    let count = 0

    if(name!='policyNumber'){
      let policyNumber = this.state.policyNumber
      if(policyNumber==null) policyNumber = ""
      if(policyNumber.length>0) count = count + 1
    }

    if(name!='memberNo'){
      let memberNo = this.state.memberNo
      if(memberNo==null) memberNo = ""
      if(memberNo.length>0) count = count + 1
    }

    if(name!='memberNo2'){
      let memberNo2 = this.state.memberNo2
      if(memberNo2==null) memberNo2 = ""
      if(memberNo2.length>0) count = count + 1
    }

    if(name!='password'){
      let password = this.state.password
      if(password==null) password = ""
      if(password.length>0) count = count + 1
    }

    if(text==null) text = ""
    if(text.length>0) count = count + 1
    
    if(count>=4) this.setState({complete: true})
    else this.setState({complete:false})
  }

  handleRegisterPress = () => {
    this.props.navigation.navigate('RegisterStep1')
  }

  handleForgetPasswordPress = () => {
    this.props.navigation.navigate('ForgetPasswordStep1')
  }

  getData = async key => {
    let value = await utils.retrieveData(key)
    this.setState({[key]:value})
  }

  onChangeText(text) {
    let errors = this.state.errors;
    ['policyNumber', 'memberNo2', 'memberNo', 'password']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          if(name=='policyNumber' && (text == null || text == "")) text = "G"
          //if(name=='memberNo') text = utils.formatMemberNo2(text)
          if(name=='memberNo'){
            errors['memberNo2'] = ''
          }
          if(name=='memberNo2'){
            errors['memberNo'] = ''
          }
          errors[name] = ''
          this.setState({ [name]: text, errors });
          
          this.handleCheckCompleteField(text,name)
          if(name=='memberNo'){
            if(text.length==5) this.memberNo2.focus();
          }
        }
      });
  }

  onSubmitPolicyNumber() {
    this.memberNo.focus();
  }

  onSubmitMemberNo() {
    this.memberNo2.focus();
  }

  onSubmitMemberNo2() {
    this.password.focus();
  }

  onSubmitPassword() {
    this.password.blur();
  }
  updateRef(name, ref) {
    this[name] = ref;
  }

  toggleSwitch1 = value => {
    if (value) {
      this.onLanguageChangedTh();
    } else {
      this.onLanguageChangedEn();
    }
  };

  onLanguageChangedEn() {
    this.onLanguageChanged("en");
    labels.setLang("en");
  }

  onLanguageChangedTh() {
    this.onLanguageChanged("th");
    labels.setLang("th");
  }

  onLanguageChanged(text) {
    this.props.languageChanged(text);
  }

  render() {
    let { errors = {}, ...data } = this.state;
    let lang = this.props.language;
    let height = 0
    return (
      <View style={[LoginStyles.main,{}]}>
        
        <View style={[LoginStyles.paddingStatus,{backgroundColor:Colors.header}]}>
          {/* <View style={[LoginStyles.center]}> */}
          <View style={[{marginLeft:20}]}>
            <Image
              style={{ width: 97*4/5, height: 87*4/5, marginTop:10, marginBottom:0 }}
              source={require("../assets/images/Core-Logo-mini.png")}
            />
          </View>
          <View style={[{backgroundColor:'white',paddingLeft: 75,paddingRight: 75,marginTop:10}]}>
            <TextField
              ref={this.policyNumberRef}
              value={data.policyNumber}
              maxLength={8}
              inputContainerPadding={2}
              lineWidth={2}
              autoCorrect={false}
              enablesReturnKeyAutomatically={true}
              onChangeText={this.onChangeText}
              onSubmitEditing={this.onSubmitPolicyNumber}
              returnKeyType='done'
              label={labels.getLabel(lang,"policyNumber")}
              error={labels.getError(this.props.language,errors.policyNumber)}
              tintColor={Colors.bondiBlue}
              baseColor={Colors.bondiBlue}
              placeholder={' '}
              helpersNumberOfLines={1.2}
              style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
              labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              labelFontSize={utils.getFontScale(14)}
              containerStyle={{marginTop:20}}
              labelPadding={20}
            />

            <View style={{flexDirection:'row'}}>
              <TextField
                ref={this.memberNoRef}
                value={data.memberNo}
                maxLength={5}
                inputContainerPadding={2}
                lineWidth={2}
                keyboardType={'numeric'}
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                onChangeText={this.onChangeText}
                onSubmitEditing={this.onSubmitMemberNo}
                returnKeyType='done'
                label={labels.getLabel(lang,"memberNo")}
                error={labels.getError(this.props.language,errors.memberNo)}
                tintColor={Colors.bondiBlue}
                baseColor={Colors.bondiBlue}
                placeholder={' '}
                // onEndEditing={(e) => {
                //   this.setState({memberNo: utils.formatMemberNo(this.state.memberNo)});
                // }}
                // onBlur={()=>this.setState({memberNo: utils.formatMemberNo(this.state.memberNo)})}
                helpersNumberOfLines={1.2}
                style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
                labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
                titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
                affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
                labelFontSize={utils.getFontScale(14)}
                containerStyle={{flex:10,marginTop:10}}
                labelPadding={20}
              />
              <TextField
                value={" "} //slash member no
                maxLength={1}
                inputContainerPadding={2}
                lineWidth={0}
                enablesReturnKeyAutomatically={true}
                label={' '}
                tintColor={Colors.bondiBlue}
                baseColor={Colors.white}
                editable={false}
                helpersNumberOfLines={1.2}
                style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),textAlign:'center',color:Colors.bondiBlue}}
                labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
                titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
                affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
                labelFontSize={utils.getFontScale(14)}
                containerStyle={{flex:1,marginTop:10}}
                labelPadding={20}
              />
              <TextField
                ref={this.memberNo2Ref}
                value={data.memberNo2}
                maxLength={2}
                inputContainerPadding={2}
                lineWidth={2}
                keyboardType={'numeric'}
                autoCorrect={false}
                enablesReturnKeyAutomatically={true}
                onChangeText={this.onChangeText}
                onSubmitEditing={this.onSubmitMemberNo2}
                returnKeyType='done'
                label={' '}
                error={errors.memberNo2}
                tintColor={Colors.bondiBlue}
                baseColor={Colors.bondiBlue}
                placeholder={' '}
                helpersNumberOfLines={1.2}
                style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
                labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
                titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
                affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
                labelFontSize={utils.getFontScale(14)}
                containerStyle={{flex:2,marginTop:10}}
                labelPadding={20}
              />
            </View>
            

            <TextField
              ref={this.passwordRef}
              value={data.password}
              maxLength={128}
              inputContainerPadding={2}
              lineWidth={2}
              enablesReturnKeyAutomatically={true}
              onChangeText={this.onChangeText}
              onSubmitEditing={this.onSubmitPassword}
              clearTextOnFocus={false}
              returnKeyType='done'
              label={labels.getLabel(lang,"password")}
              error={labels.getError(this.props.language,errors.password)}
              tintColor={Colors.bondiBlue}
              baseColor={Colors.bondiBlue}
              placeholder={' '}
              secureTextEntry={true}
              helpersNumberOfLines={lang=='th'?5:4}
              style={{fontFamily:'Prompt',fontSize:utils.getFontScale(14),color:Colors.bondiBlue}}
              labelTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              titleTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              affixTextStyle={{fontFamily:'Prompt',fontSize:utils.getFontScale(14)}}
              labelFontSize={utils.getFontScale(14)}
              containerStyle={{marginTop:10}}
              labelPadding={20}
            />
            
            
            {/* <TouchableOpacity onPress={this.handleRegisterPress} style={{alignItems:'flex-end', marginTop: 10}} disabled={this.state.loginProcess}>
              <DefaultText style={{color:Colors.bondiBlue}}>{labels.getLabel(lang,"register")}</DefaultText>
            </TouchableOpacity>

            <TouchableOpacity onPress={this.handleForgetPasswordPress} style={{alignItems:'flex-end', marginTop: 10}} disabled={this.state.loginProcess}>
              <DefaultText style={{color:Colors.bondiBlue}}>{labels.getLabel(lang,"forgetPassword")}</DefaultText>
            </TouchableOpacity> */}
            
            <TouchableOpacity onPress={this.handleLoginPress}
              style={[
                this.state.complete ? LoginStyles.button : LoginStyles.buttonInactive,
                { marginTop: 30 }
              ]}
              disabled={!this.state.complete}
            >
              <DefaultText bold style={{color: this.state.complete ? Colors.buttonTextActive : Colors.buttonTextInactive}}>{labels.getLabel(lang,"logIn")}</DefaultText>
            </TouchableOpacity>

            <View style={{marginTop:20,flexDirection:'row',alignItems: "center",justifyContent:'center'}}>
              <TouchableOpacity onPress={this.handleRegisterPress} disabled={this.state.loginProcess}>
                <DefaultText style={{color:Colors.bondiBlue}}>{labels.getLabel(lang,"register")}</DefaultText>
              </TouchableOpacity>
              <DefaultText style={{marginHorizontal:5}}>|</DefaultText>
              <TouchableOpacity onPress={this.handleForgetPasswordPress} disabled={this.state.loginProcess}>
                <DefaultText style={{color:Colors.bondiBlue}}>{labels.getLabel(lang,"forgetPassword")}</DefaultText>
              </TouchableOpacity>
            </View>
          </View>
          
        </View>
        <View 
          style={{backgroundColor:'white', marginVertical:20,marginHorizontal:20,flexGrow:1,borderBottomColor:Colors.bondiBlue,borderBottomWidth:10}}
          onLayout={(event) => {if(this.state.firstLoad==true){height = event.nativeEvent.layout.height;this.setState({firstLoad:false,imageHeight:height});}}}>
          <View style={{flexDirection:'row',justifyContent:'center',position:"absolute",left:0,right:0,bottom:0}}>
            {/*<Image
              style={{ 
                height:this.state.imageHeight>100?100:this.state.imageHeight,
                width:this.state.imageHeight>100?100*18/14:this.state.imageHeight*18/14}}
              source={require("../assets/images/TMLTH.png")}
            />*/}
            <Image
              style={{ 
                height:100,
                width:100*18/14,}}
              source={require("../assets/images/TMLTH.png")}
            />
          </View>
          
        </View>

        {/* back button */}
        <TouchableOpacity style={{position:'absolute',left:10,top: (87*4/5)+10+Constants.statusBarHeight+11+5}} onPress={() => this.props.navigation.navigate('Home')} disabled={this.state.loginProcess}>
          <Image
            style={{ width: 22, height: 22 }}
            source={require("../assets/icon/Back-button-alt2.png")}
          />
        </TouchableOpacity>

        {/* language */}
        <View  style={{position:'absolute',right:10,top:50,flexDirection:'row'}} >
          {/*<TouchableOpacity onPress={() => this.onLanguageChangedTh()}>
            <DefaultText style={{color:'white'}}>TH</DefaultText>
          </TouchableOpacity>

          <DefaultText style={{color:'white'}}> / </DefaultText>

          <TouchableOpacity onPress={() => this.onLanguageChangedEn()}>
            <DefaultText style={{color:'white'}}>EN</DefaultText>
          </TouchableOpacity>*/}

          <Switch
            value={lang == "en" ? false : true}
            onValueChange={this.toggleSwitch1.bind(this)}
            disabled={false}
            activeText={"TH"}
            inActiveText={"EN"}
            circleBorderWidth={1}
            backgroundActive={Colors.bondiBlue}
            backgroundInactive={Colors.bondiBlue}
            circleActiveBorderColor={Colors.bondiBlue}
            circleInactiveBorderColor={Colors.bondiBlue}
            circleActiveColor={"#fff"}
            circleInActiveColor={"#fff"}
            changeValueImmediately={true}
            innerCircleStyle={{
              alignItems: "center",
              justifyContent: "center"
            }} // style for inner animated circle for what you (may) be rendering inside the circle
            outerCircleStyle={{}} // style for outer animated circle
            renderActiveText={true}
            renderInActiveText={true}
            switchLeftPx={4} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
            switchRightPx={4} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { language, lastScreen, loginStatus, numberOfPinFail } = auth;
  return { language, lastScreen, loginStatus, numberOfPinFail };
};

export default connect(
  mapStateToProps,
  {
    logIn,
    eCardDataClear,
    languageChanged,
    memberDataChanged,
    notificationChanged,
    notificationListChanged,
    hasECardChanged,
    logOut,
    pinFailed,
  }
)(LoginScreen);
