import React from "react";
import {
  Alert,
  AsyncStorage,
  BackHandler,
  Linking,
  Platform,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  View
} from "react-native";
import { AppLoading } from "expo";
import * as Font from 'expo-font';
import { Asset } from 'expo-asset';
import { Ionicons } from '@expo/vector-icons';
import * as SQLite from 'expo-sqlite';
import AppNavigator from "./navigation/AppNavigator";
import { connect } from "react-redux";
import { hospitalDataChanged, clinicDataChanged, provinceDataChanged, cityDataChanged, languageChanged, logOut, linkListChanged, howToClaimListChanged, healthTipsListChanged, newsListChanged } from "./actions";
import * as labels from "./constants/label";
import * as utils from './functions'
import Constants from 'expo-constants';
import UpdateScreen from './screens/UpdateScreen'

const db = SQLite.openDatabase('db.MasterData');

class Loading extends React.Component {
  state = {
    isLoadingComplete: false,
    loadingMasterData: false,
    forceUpdate: false,
  };

  //start get language
  getLang = async () => {
    let lang = await labels.getLang();
    if (lang === "en") {
      this.onLanguageChangedEn();
    } else {
      this.onLanguageChangedTh();
    }
  };

  //getMasterData
  getMasterData = async () => {
    
    if(!this.state.isLoadingComplete && !this.props.skipLoadingScreen){
      let result = ""
      let xmlBody = utils.GetMasterDataBody();
      console.log("-------------------");
      console.log("Request body: " + xmlBody);
      console.log("-------------------");
      let data = null
      await fetch(utils.urlMemberService, utils.genRequest(utils.GetMasterDataSOAPAction,xmlBody) )
        .then(response => response.text())
        .then(response => {
          result = utils.getResult(response)
          data = utils.GetMasterDataInfo(result)
          return data
        }).then(data => {
          //console.log(data['appVersionNeedForceUpdate'])
          //data['appVersionNeedForceUpdate']="Y"
          if(data['appVersionNeedForceUpdate']=="Y"){
            this.setState({forceUpdate:true})
          }else{
            this.props.hospitalDataChanged(data['hospital'])
            this.props.clinicDataChanged(data['clinic'])
            this.props.provinceDataChanged(data['province'])
            this.props.cityDataChanged(data['city'])
            this.props.linkListChanged(data['linkList'])
            this.props.howToClaimListChanged(data['howToClaimList'])
            this.props.healthTipsListChanged(data['healthTipsList'])
            this.props.newsListChanged(data['newsList'])
            this.setState({loadingMasterData:true})
          }
        })
        .catch(err => {
          console.log("fetch", err);
          Alert.alert(
            "Error",
            'Cannot retrieve data, click OK to try again',
            [
              {text: 'OK', onPress: () => this.getMasterData()},
            ],
            { cancelable: false }
            )
        });
    }
  }

  onLanguageChangedEn() {
    this.onLanguageChanged("en");
  }

  onLanguageChangedTh() {
    this.onLanguageChanged("th");
  }

  onLanguageChanged(text) {
    this.props.languageChanged(text);
  }

  //timeout
  _handleTimeoutPress = (active) => {
    //console.log(active)
    if(this.props.loginStatus && active == false ){
      this.props.logOut();
      Alert.alert(labels.getLabel(this.props.language, "logOut"))
    }
  };

  render() {
    if(this.state.forceUpdate){
      return (
        <UpdateScreen />
      );
    }else if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen && ! this.state.loadingMasterData) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <View style={styles.container}>
            {Platform.OS === "ios" && <StatusBar barStyle="default" />}
            <AppNavigator />
        </View>
      );
    }
  }

  _loadResourcesAsync = async () => {
    this.getLang()
    return Promise.all([
      Asset.loadAsync([
        require("./assets/images/tile-1.png"),
        require("./assets/images/tile-2.png"),
        require("./assets/images/tile-3.png"),
        require("./assets/images/tile-4.png"),
        require("./assets/images/tile-5.png"),
        require("./assets/images/tile-6.png"),
        require("./assets/images/Core-Logo.png"),
        require("./assets/images/tm-logo-white.png"),
        require('./assets/images/TMLTH.png'),
        require("./assets/images/card-full.png"),
        require("./assets/images/card-back-full-th.png"),
        require("./assets/images/card-back-full-en.png"),
        require("./assets/images/person.png"),
        require('./assets/icon/Claim.png'),
        require('./assets/icon/Claim-Selected.png'),
        require('./assets/icon/E-Card.png'),
        require('./assets/icon/E-Card-Selected.png'),
        require('./assets/icon/Home-button.png'),
        require('./assets/icon/Home-button-selected.png'),
        require('./assets/icon/Members.png'),
        require('./assets/icon/Members-Selected.png'),
        require('./assets/icon/Personal.png'),
        require('./assets/icon/Personal-Selected.png'),
        require('./assets/icon/Setting-icon.png'),
        require('./assets/icon/Setting-icon-selected.png'),
        require('./assets/icon/Globe.png'),
        require('./assets/icon/doc.png'),
        require("./assets/icon/Close-eagle.png"),
        require("./assets/icon/Back-button.png"),
        require("./assets/icon/Back-button-alt.png"),
        require("./assets/icon/Back-button-alt2.png"),

        require("./assets/icon/iconfinder_11.png"),
        require("./assets/icon/iconfinder_city.png"),
        require("./assets/icon/iconfinder_worldwide_location.png"),
        require("./assets/icon/iconmonstr-building.png"),
      ]),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Ionicons.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        thsarabun_bold: require("./assets/fonts/thsarabun_bold.ttf"),
        thsarabun: require("./assets/fonts/thsarabun.ttf"),
        Prompt_bold: require("./assets/fonts/Prompt_bold.ttf"),
        Prompt_italic: require("./assets/fonts/Prompt_italic.ttf"),
        Prompt: require("./assets/fonts/Prompt.ttf")
      }),
      this.getMasterData()
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}

const mapStateToProps = ({ auth }) => {
  const { loginStatus } = auth;
  return { loginStatus };
};

export default connect(
  mapStateToProps,
  {
    hospitalDataChanged,
    clinicDataChanged,
    provinceDataChanged,
    cityDataChanged,
    languageChanged,
    logOut,
    linkListChanged, 
    howToClaimListChanged,
    healthTipsListChanged,
    newsListChanged,
  }
)(Loading);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  }
});
