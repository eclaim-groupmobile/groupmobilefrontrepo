import { AsyncStorage } from "react-native";
import * as labelExtra from "./labelExtra";

const label_en_ = "";
const label_th_ = "";

const applyLetterSpacing = (string, count = 1) => {
  return string.split("").join("\u200A".repeat(count));
};

//menu
const menu = {
  en: {
    Home: "Home",
    Member: "Members",
    Personal: "Personal Info",
    Claim: "Claim",
    ECard: "Card",
    Settings: ""
  },
  th: {
    Home: "หน้าหลัก",
    Member: "สมาชิก",
    Personal: "ข้อมูลบุคคล",
    Claim: "สินไหม",
    ECard: "บัตรประกัน",
    Settings: ""
  }
};
//Title
const title = {
  en: {
    register: applyLetterSpacing("Registration"),
    login: applyLetterSpacing("Login"),
    forgetPassword: applyLetterSpacing("Forget Password"),
    otp: applyLetterSpacing("OTP"),
    term: applyLetterSpacing("Term and Conditions"),
    forgetPinConfirmUser: applyLetterSpacing("Forget PIN"),
    forgetPin: applyLetterSpacing("Forget PIN"),
    howToClaim: applyLetterSpacing("How to Claim"),
    hospital: applyLetterSpacing("Hospital Network"),
    hospitalByNear: "Search Nearby Hospitals/Clinics",
    //hospitalByNear: applyLetterSpacing("Search Nearby Hospitals/Clinics"),
    hospitalByProvince: applyLetterSpacing("Search By Province"),
    hospitalByHospital: applyLetterSpacing("Search By Hospital"),
    hospitalByClinic: applyLetterSpacing("Search By Clinic"),
    healthTips: applyLetterSpacing("Health Tips"),
    contactUs: applyLetterSpacing("Contact Us"),
    news: applyLetterSpacing("News"),
    links: applyLetterSpacing("Links"),
    claim: applyLetterSpacing("Claim Summary"),
    personal: applyLetterSpacing("Personal Information"),
    member: applyLetterSpacing("Members"),
    settings: applyLetterSpacing("Settings"),
    myAccount: applyLetterSpacing("My Account"),
    eCard: applyLetterSpacing("Insurance Card")
  },
  th: {
    register: "สมัครใช้งาน",
    login: "ลงชื่อเข้าใช้ ",
    forgetPassword: "ลืมรหัสผ่าน",
    otp: "ยืนยันเบอร์มือถือ",
    term: "ข้อกำหนด และ เงื่อนไข",
    forgetPinConfirmUser: "ยืนยันตัวตน",
    forgetPin: "ลืมรหัส PIN",
    howToClaim: "วิธีการเรียกร้องสินไหม",
    hospital: "ค้นหาโรงพยาบาลเครือข่าย",
    hospitalByNear: "ค้นหาโรงพยาบาลและคลินิกใกล้เคียง",
    hospitalByProvince: "ค้นหาตามจังหวัด",
    hospitalByHospital: "ค้นหาตามรายชื่อโรงพยาบาล",
    hospitalByClinic: "ค้นหาตามรายชื่อคลินิค",
    healthTips: "ข้อมูลส่งเสริมสุขภาพ",
    contactUs: "ติดต่อเรา",
    news: "ข่าวสาร",
    links: "ลิงค์น่ารู้ ",
    claim: "ประวัติการเคลม",
    personal: "ข้อมูลส่วนบุคคล",
    member: "รายชื่อสมาชิก",
    settings: "ตั้งค่า",
    myAccount: "บัญชีของฉัน",
    eCard: "บัตรประกัน"
  }
};

//data
const dataExt = {
  en: {
    termHead: labelExtra.data_en_termHead,
    termDetail: labelExtra.data_en_termDetail,
    cardBack: labelExtra.data_en_cardBack,
    cardBack1: labelExtra.data_en_cardBack1,
    cardBack2: labelExtra.data_en_cardBack2,
    cardBack3: labelExtra.data_en_cardBack3,
    cardBack4: labelExtra.data_en_cardBack4,
    cardBack5: labelExtra.data_en_cardBack5,
    cardBack5Red: labelExtra.data_en_cardBack5Red
  },
  th: {
    termHead: labelExtra.data_th_termHead,
    termDetail: labelExtra.data_th_termDetail,
    cardBack: labelExtra.data_th_cardBack,
    cardBack1: labelExtra.data_th_cardBack1,
    cardBack2: labelExtra.data_th_cardBack2,
    cardBack3: labelExtra.data_th_cardBack3,
    cardBack4: labelExtra.data_th_cardBack4,
    cardBack5: labelExtra.data_th_cardBack5,
    cardBack5Red: labelExtra.data_th_cardBack5Red
  }
};

//error
const error = {
  en: {
    invalidLogin: "Invalid data",
    empty: "Empty", //*
    invalidLength: "Invalid Length", //*
    invalidPattern: "Invalid Pattern",
    otp: "OTP is invalid, please Try Again ",
    passwordNotMatch: "Password that you enter are not match",
    invalidPolicyNumber: "Policy Number invalid",
    invalidMemberNo: "Member Number invalid",
    invalidPassport: "Citizen ID invalid",
    invalidDate: "Date of Birth invalid",
    invalidTel: "Mobile No invalid",
    invalidEmail: "Email invalid",
    invalidLineId: "Line ID invalid",
    invalidPassword:
      "Password must be at least 8 characters long. Must contain at least one uppercase (A-Z), one lowercase (a-z) and one digit (0-9).",
    invalidPassword2: "Password should be at least 8 characters",
    enterPinFailed: "Invalid PIN please try again",
    pinNotMatch: "PIN does not match please try again",
    noData: "No data found",
    noDataNear: "No data found",
    cannotFindYourCurrentLocation: "Cannot find your current location",
    otpInvalid: "Invalid OTP please try again",
    otpExpire: "OTP is expired"
  },
  th: {
    invalidLogin: "ข้อมูลไม่ถูกต้อง",
    empty: "Empty", //*
    invalidLength: "Invalid Length", //*
    invalidPattern: "ระบุข้อมูลไม่ถูกต้อง",
    otp: "รหัส OTP ไม่ถูกต้อง",
    passwordNotMatch: "รหัสผ่านไม่ตรงกัน",
    invalidPolicyNumber: "เลขที่กรมธรรม์ไม่ถูกต้อง",
    invalidMemberNo: "เลขที่สมาชิกไม่ถูกต้อง",
    invalidPassport: "บัตรประจำตัวประชาชน หรือ หนังสือเดินทาง ไม่ถูกต้อง",
    invalidDate: "วันเดือนปีเกิดไม่ถูกต้อง",
    invalidTel: "หมายเลขโทรศัพท์ไม่ถูกต้อง",
    invalidEmail: "อีเมลไม่ถูกต้อง",
    invalidLineId: "Line ID ไม่ถูกต้อง",
    invalidPassword:
      "รหัสผ่านต้องมีอย่างน้อย 8 ตัวอักษร และต้องประกอบไปด้วยภาษาอังกฤษตัวพิมพ์ใหญ่, ตัวพิมพ์เล็ก และตัวเลขอย่างน้อยอย่างละ 1 ตัวอักษร",
    invalidPassword2: "รหัสผ่านควรมีความยาวอย่างน้อย 8 ตัวอักษร",
    enterPinFailed: "PIN ไม่ถูกต้อง โปรดลองใหม่อีกครั้ง",
    pinNotMatch: "ระบุ PIN ไม่ตรงกัน โปรดลองใหม่อีกครั้ง",
    noData: "ไม่พบข้อมูลที่ค้นหา",
    noDataNear: "บ้านฉันไม่มีโรงพยาบาลใกล้เคียง",
    cannotFindYourCurrentLocation: "ไม่สามารถหาตำแหน่งปัจจุบันของคุณได้",
    otpInvalid: "รหัส OTP ไม่ถูกต้อง โปรดลองใหม่อีกครั้ง",
    otpExpire: "รหัส OTP หมดอายุแล้ว"
  }
};

const label = {
  en: {
    done: "Done",
    ok: "OK",
    cancel: "Cancel",
    enable: "Enable",
    timeout: "Timeout",
    baht: "Baht",
    error: "Error",
    errorNetwork: "Something went wrong. Please try again later.",
    km: "kilometers",
    dataAsOf: "Data as of",
    locationServicesDisable:
      "Location Services is disable, please enable Location Services",
    confirmBack: "Do you really want to exit to home screen ?",
    submit: "SUBMIT",
    next: "NEXT",
    accept: "ACCEPT",
    decline: "DECLINE",
    confirm: "CONFIRM",
    skip: "SKIP",
    confirmPin: "Confirm PIN again",
    //Home
    Main_howToClaimHeader: "How to claim",
    Main_howToClaimDetail1: "WHAT IS THE PROCEDURE OF CLAIM ?",
    Main_howToClaimDetail2: "HOW CAN I KNOW THE CLAIM STATUS ?",
    Main_howToClaimDetail3: "HOW CAN I RECEIVE MY MONEY ?",
    Main_networkHospitalHeader: "Hospital Network",
    Main_networkHospitalDetail1: "FIND THE HOSPITAL",
    Main_networkHospitalDetail2: "NETWORK",
    Main_networkHospitalDetail3: "NEAR YOU.",
    Main_healthTips: "Health Tips",
    Main_contactUs: "Contact Us",
    Main_newsHeader: "News",
    Main_newsDetail1: "DON'T MISS THE LATEST",
    Main_newsDetail2: "UPDATE FROM US.",
    Main_linksHeader: "Links",
    Main_linksDetail1: "LEARN MORE ABOUT LIFE INSURANCE",
    Main_linksDetail2: "TO MAXIMIZE YOUR BENEFIT.",
    //how to claim
    HowToClaim_claim: "Support Documents Required for Claim: IPD and OPD",
    HowToClaim_healthcard: "How to use a Health Insurance Card",
    //links
    Links_link1: "Office of Insurance Commission",
    Links_link2: "The Thai Life Assurance Association",
    //hospital
    Hospital_hospitalByNear: "Search Nearby Hospitals/Clinics",
    Hospital_hospitalByProvince: "Search by Province",
    Hospital_hospitalByHospital: "Search by hospital name",
    Hospital_hospitalByClinic: "Search for clinic",
    Hospital_remark1: "Remark:",
    Hospital_remark2:
      "Hospital credit service will be apply only policies which agree to use benefits shown on Insurance card.",
    Hospital_searchProvince: "Please select the Province",
    Hospital_searchDistrict: "Please select the District",
    Hospital_searchHospital: "Please select the Hospital",
    Hospital_searchClinic: "Please select the Clinic",
    Hospital_map: "Map",
    Hospital_call: "Call",
    //Contact Us
    ContactUs_email: "Email",
    ContactUs_tel: "Phone",
    ContactUs_website: "Website",
    ContactUs_press: "Press",
    ContactUs_1: "Group Insurance Call Center",
    ContactUs_2_1: "Tel:",
    // ContactUs_2_2: "0-2650-1400",
    ContactUs_2_3: "press 2",
    ContactUs_3_1: "Fax:",
    // ContactUs_3_2: "0-2619-4080",
    ContactUs_4: "Working time:",
    ContactUs_5: "- Everyday 24 hrs.",
    ContactUs_6: "Claims - Group Insurance Department",
    ContactUs_7_1: "Fax:",
    // ContactUs_7_2: "0-2619-4072",
    ContactUs_8: "Working time:",
    ContactUs_9: "- Everyday 08:30 - 20:00 hrs.",
    ContactUs_10_1: "Email:",
    // ContactUs_10_2: "groupinfo@tokiomarinelife.co.th",
    ContactUs_11_1: "Web Site:",
    // ContactUs_11_2: "www.tokiomarine.com",
    //Log in
    policyNumber: "Policy Number",
    memberNo: "Member Number",
    password: "Password",
    logIn: "LOG IN",
    register: "Register",
    forgetPassword: "Forget Password",
    enterPin: "ENTER PIN FOR GROUP MOBILE",
    reEnterPin: "CONFIRM PIN FOR GROUP MOBILE",
    loginNow: "LOG IN...",
    putPin: "ENTER PIN FOR GROUP MOBILE",
    pinSuccessSetup: "PIN successfully set up",
    touchId: "DO YOU WANT TO ENABLE TOUCH ID ?",
    fingerprint: "DO YOU WANT TO ENABLE FINGERPRINT ?",
    fingerprintModal1: "LOG IN WITH TOUCH ID",
    fingerprintModal2: "PLACE YOUR FINGER ON CENSOR",
    touchIdSuccess: "Login Successful !",
    touchIdFailed: "Finger print not recognized!",
    usePin: "LOG IN USING PIN",
    notYou: "NOT YOU ?",
    //Forget Pin
    forgetPin: "FORGOT PIN",
    forgetPinTelState1Header1: "CONFIRM MOBILE NUMBER",
    forgetPinTelState1Header2: "OTP WILL SEND TO",
    forgetPinTelState2Header1: "CHANGE MOBILE NUMBER",
    forgetPinTelState2Header2: "IDENTIFY MOBILE NUMBER",
    forgetPinTelState2Header3: "TO RECEIVE OTP",
    forgetPinTelChange: "CHANGE MOBILE NUMBER",
    changePinSuccess: "Change PIN success",
    //Register
    passport: "Citizen ID / Passport Number",
    dob: "Date of Birth",
    tel: "Mobile Phone",
    email: "E-mail",
    lineId: "Line ID",
    rePassword: "Re-enter Password",
    otpHead1: "Enter the OTP we sent to",
    otpHead2: "Ref:",
    smsResend: "RE-SEND >",
    createSuccess: "Account Successfully Created",
    resetPasswordSuccess: "Password Successfully Reset",
    //member
    Member_header: "Dependents of Member No.",
    //personal
    Personal_policyNumber: "Policy Number",
    Personal_memberNo: "Member Number",
    Personal_effectiveDate: "Effective Date",
    Personal_company: "Company",
    Personal_name: "Full Name",
    Personal_idNo: "ID No",
    Personal_age: "Age",
    Personal_sex: "Sex",
    Personal_header1: "Life, Accident, Disability",
    Personal_groupTermLife: "Group Term Life",
    Personal_groupAccidentBenefit: "Group Accidental Benefit",
    Personal_doubleIndemnityBenefit: "Double Indemnity Benefit",
    Personal_extendInclude:
      "Extended to include murder/assault, strike/riot/civil commotion",
    Personal_groupPermanentBenefit: "Group Permanent Total",
    Personal_sumAssured: "Sum Assured",
    Personal_header2: "Health",
    Personal_healthIPD: "Group Health Benefit- In-patient Hospitalization",
    Personal_healthOPD: "Group Health Benefit- Out-patient or Clinical Benefit",
    Personal_healthDental: "Dental care",
    Personal_healthAccident: "Medical Treatment Expenses caused by Accident",
    Personal_healthCritical: "Critical Illness",
    Personal_healthMaternity: "Maternity Expense",
    Personal_exception: "Exclusions",
    Personal_exceptionDetail:
      "EXCEPTIONS FOR GROUP HEALTH INSURANCE AS IPD AND OPD.",
    Personal_remark1: "Remarks:",
    Personal_remark2:
      "The above screen does not show all benefits. For detailed coverage and condition, please refer to master policy.",
    //claim
    Claim_insured: "Insured",
    Claim_yearPolicy: "Policy Year",
    Claim_labelIPD: "In-patient Claim (IPD)",
    Claim_labelOPD: "Out-patient Claim (OPD)",
    Claim_labelLab: "Claim for Lab/X-ray (OPD Lab Test)",
    Claim_labelDental: "Dental",
    Claim_labelAME: "Accidental Medical Expense Claim (ME)",
    //IPD
    Claim_IPD_info1: "Usage Information",
    Claim_IPD_info2_1: "IPD:",
    Claim_IPD_info2_2: "visits / total",
    Claim_IPD_info3_1: "Major Medical:",
    Claim_IPD_info3_2: "visits / total",
    Claim_IPD_info4_1: "Maternity:",
    Claim_IPD_info4_2: "visits / total",
    Claim_IPD_info5_1: "GMJ:",
    Claim_IPD_info5_2: "visits / total",
    Claim_IPD_remark1: "Remarks:",
    Claim_IPD_remark2: "Claim information is 1-day back-date",
    //OPD
    Claim_OPD_info1: "Usage Information",
    Claim_OPD_info2_1: "Authorized transactions:",
    Claim_OPD_info2_2: "visits /",
    Claim_OPD_info3_1: "Waiting Hospital Invoice:",
    Claim_OPD_info3_2: "visits /",
    Claim_OPD_info4_1: "Remaining benefits:",
    Claim_OPD_info4_2: "visits",
    Claim_OPD_info4_3: "Baht",
    Claim_OPD_remark1: "Remarks:",
    Claim_OPD_remark2: "Claim information is 1-day back-date",
    //LAB
    Claim_LAB_info1: "Usage Information",
    Claim_LAB_info2_1: "Authorized transactions:",
    Claim_LAB_info2_2: "visits /",
    Claim_LAB_info3_1: "Waiting Hospital Invoice:",
    Claim_LAB_info3_2: "visits /",
    Claim_LAB_info4_1: "Remaining benefits:",
    Claim_LAB_info4_2: "visits",
    Claim_LAB_info4_3: "Baht",
    Claim_LAB_remark1: "Remarks:",
    Claim_LAB_remark2: "Claim information is 1-day back-date",
    //DEN
    Claim_DEN_info1: "Usage Information",
    Claim_DEN_info2_1: "Authorized transactions:",
    Claim_DEN_info2_2: "visits /",
    Claim_DEN_info3_1: "Waiting Hospital Invoices:",
    Claim_DEN_info3_2: "visits /",
    Claim_DEN_info4_1: "Remaining benefits:",
    Claim_DEN_info4_2: "visits",
    Claim_DEN_info4_3: "Baht",
    Claim_DEN_remark1: "Remarks:",
    Claim_DEN_remark2: "Claim information is 1-day back-date",
    //AME
    Claim_AME_info1: "Usage Information",
    Claim_AME_info2_1: "Accidental Date:",
    Claim_AME_info3_1: "Treatment date:",
    Claim_AME_info3_2: "Baht",
    Claim_AME_info4_1: "Total:",
    Claim_AME_info4_2: "Baht",
    Claim_AME_info5_1: "Remaining benefits:",
    Claim_AME_info5_2: "Baht",
    Claim_AME_remark1: "Remarks:",
    Claim_AME_remark2: "Claim information is 1-day back-date",
    Claim_product: "Product",
    Claim_incurredDate: "Accident Date",
    Claim_date: "Visit Date",
    Claim_hospital: "Hospital",
    Claim_diagnosis: "Diagnosis",
    Claim_claimType: "Claim type",
    Claim_authorizedDate: "Authorized Date",
    Claim_status: "Status",
    Claim_claim: "Claim Amount",
    Claim_approve: "Approve",
    Claim_surplus: "Surplus",
    Claim_remarkCash:
      "*You will receive reimbursement within 5 business days from authorized date.",
    //eCard
    ECard_labelDate: "Insurance card is applicable for visit date",
    ECard_labelDate2: "only",
    ECard_CardHeader: "GROUP INSURANCE CERTIFICATE",
    ECard_PolicyNo: "Policy No.",
    ECard_MemberNo: "Member No.",
    ECard_Policyholder: "Policyholder",
    ECard_InsuredName: "Insured Name",
    ECard_Efd: "Effective date",
    ECard_BenefitCoverage: "Benefit Coverage",
    ECard_BenefitCoverageSub: "",
    ECard_IssuedBy: "Issued By",
    ECard_remark1:
      "Remark: For OPD, employee has to pay for medical expense and reimburse with Insurer later, except for using SSO hospitals with Seagate, of which max. benefit for OPD is 500 Baht per day.",
    ECard_remark2: "",
    ECard_CustomerRel1: "ลูกค้าสัมพันธ์ ",
    ECard_CustomerRel2: "เทพารักษ์\tโทร.09-2251-0641 / 09-2251-0644",
    ECard_CustomerRel3: "โคราช\tโทร.08-1967-4636 / 08-1879-2542",
    //settings
    Settings_language: "Language",
    Settings_myAccount: "My Account",
    Settings_logOut: "  LOGOUT  ",
    Settings_privacy: "Personal Data Policy",
    //My Account
    MyAccount_userInformation: "User Information",
    MyAccount_name: "Name",
    MyAccount_policyNumber: "Policy Number",
    MyAccount_memberNo: "Member Number",
    MyAccount_lineId: "Line ID",
    MyAccount_tel: "Mobile Number",
    MyAccount_email: "E-mail",
    MyAccount_changePassword: "Change Password",
    MyAccount_oldPassword: "Old Password",
    MyAccount_newPassword: "New Password",
    MyAccount_reNewPassword: "Re-enter Password",
    MyAccount_lineIdSuccess: "Line ID has been changed successfully!",
    MyAccount_telSuccess: "Mobile Number has been changed successfully!",
    MyAccount_emailSuccess: "E-mail has been changed successfully!",
    MyAccount_passwordSuccess: "Your password has been changed successfully!",
    //Update
    Update: "Update",
    UpdateText:
      "There is newer version of this application available. Please update this application before using.",
    Ecard_Remark:
      "**When using E-card, no need to capture screen or copy of screen for billing.  Card information will be displayed on Confirmation sheet."
  },
  th: {
    done: "เสร็จ",
    ok: "ตกลง",
    cancel: "ยกเลิก",
    enable: "เปิดใช้งาน",
    timeout: "หมดเวลา",
    baht: "บาท",
    error: "ข้อผิดพลาด",
    errorNetwork: "มีปัญหาเกิดขึ้น โปรดลองใหม่อีกครั้ง",
    km: "กิโลเมตร",
    dataAsOf: "ข้อมูล ณ วันที่",
    locationServicesDisable:
      "Location Services ไม่ถูกเปิดใช้งาน โปรดเปิดใช้งาน Location Services",
    confirmBack: "กรุณายืนยันการกลับสู่หน้าหลัก ?",
    submit: "ตกลง",
    next: "ถัดไป",
    accept: "ยอมรับ",
    decline: "ปฎิเสธ",
    confirm: "ยืนยัน",
    skip: "ข้าม",
    confirmPin: "ยืนยัน PIN อีกครั้ง",
    //Home
    Main_howToClaimHeader: "วิธีการเรียกร้องสินไหม",
    Main_howToClaimDetail1: "ขั้นตอนการเรียกร้องสินไหม ?",
    Main_howToClaimDetail2: "ตรวจสอบสถานะของการเรียกร้องสินไหม ?",
    Main_howToClaimDetail3: "วิธีการรับสินไหม ?",
    Main_networkHospitalHeader: "ค้นหาโรงพยาบาลในเครือข่าย",
    Main_networkHospitalDetail1: "ค้นหาโรงพยาบาล",
    Main_networkHospitalDetail2: "ในเครือข่าย",
    Main_networkHospitalDetail3: "ที่ใกล้คุณ",
    Main_healthTips: "ข้อมูลส่งเสริมสุขภาพ",
    Main_contactUs: "ติดต่อเรา",
    Main_newsHeader: "ข่าวสาร",
    Main_newsDetail1: "อย่าพลาดข่าวสาร",
    Main_newsDetail2: "และข้อมูลล่าสุดจากเรา",
    Main_linksHeader: " ลิงค์น่ารู้ ",
    Main_linksDetail1: "ข้อมูลเพิ่มเติมเกี่ยวกับประกันชีวิต",
    Main_linksDetail2: "เพื่อผลประโยชน์สูงสุดของท่าน",
    //how to claim
    HowToClaim_claim:
      "เอกสารที่ใช้ประกอบพิจารณาเบิกสินไหม กรณีเข้ารับการรักษาแบบผู้ป่วยในและผู้ป่วยนอก",
    HowToClaim_healthcard: "วิธีการใช้บัตรประกันสุขภาพ",
    //links
    Links_link1: "สำนักงานคณะกรรมการกำกับและส่งเสริมการประกอบธุรกิจประกันภัย",
    Links_link2: "สมาคมประกันชีวิตไทย",
    //hospital
    Hospital_hospitalByNear: "ค้นหาโรงพยาบาลและคลินิกใกล้เคียง",
    Hospital_hospitalByProvince: "ค้นหาตามจังหวัด",
    Hospital_hospitalByHospital: "ค้นหาตามรายชื่อโรงพยาบาล",
    Hospital_hospitalByClinic: "ค้นหาตามรายชื่อคลินิค",
    Hospital_remark1: "หมายเหตุ:",
    Hospital_remark2:
      "สามารถใช้บริการ Credit โรงพยาบาลเฉพาะกรมธรรม์ที่มีการตกลงเปิดใช้ผลประโยชน์ที่กำหนดไว้บนหน้าบัตรประกันเท่านั้น",
    Hospital_searchProvince: "เลือกจังหวัด",
    Hospital_searchDistrict: "เลือกเขต",
    Hospital_searchHospital: "เลือกโรงพยาบาล",
    Hospital_searchClinic: "เลือกคลินิค",
    Hospital_map: "แผนที่",
    Hospital_call: "ติดต่อ",
    //Contact Us
    ContactUs_email: "อีเมล",
    ContactUs_tel: "เบอร์โทร",
    ContactUs_website: "Website",
    ContactUs_press: "กด",
    ContactUs_1: "ศูนย์บริการลูกค้าฝ่ายประกันกลุ่ม",
    ContactUs_2_1: "โทร:",
    // ContactUs_2_2: "0-2650-1400",
    ContactUs_2_3: "กด 2",
    ContactUs_3_1: "โทรสาร:",
    // ContactUs_3_2: "0-2619-4080",
    ContactUs_4: "เวลาทำการ:",
    ContactUs_5: "- ตลอด 24 ชั่วโมง",
    ContactUs_6: "ส่วนสินไหมฝ่ายการประกันกลุ่ม",
    ContactUs_7_1: "โทรสาร:",
    // ContactUs_7_2: "0-2619-4072",
    ContactUs_8: "เวลาทำการ:",
    ContactUs_9: "- ทุกวัน 08:30 - 20:00 น.",
    ContactUs_10_1: "อีเมล:",
    // ContactUs_10_2: "groupinfo@tokiomarinelife.co.th",
    ContactUs_11_1: "เว็บไซต์:",
    // ContactUs_11_2: "www.tokiomarine.com",
    //Log in
    policyNumber: "เลขที่กรมธรรม์ ",
    memberNo: "เลขที่สมาชิก",
    password: "รหัสผ่าน",
    logIn: "เข้าสู่ระบบ",
    register: "ลงทะเบียน",
    forgetPassword: "ลืมรหัสผ่าน",
    enterPin: "กำหนดรหัส PIN สำหรับ GROUP MOBILE",
    reEnterPin: "ยืนยันรหัส PIN สำหรับใช้งาน GROUP MOBILE",
    loginNow: "กำลังเข้าสู่ระบบ...",
    putPin: "ระบุรหัส PIN สำหรับใช้งาน GROUP MOBILE",
    pinSuccessSetup: "บันทึก PIN เรียบร้อยแล้ว", //*
    touchId: "ต้องการเปิดใช้งานแสกนลายนิ้วมือหรือไม่ ?", //*
    fingerprint: "ต้องการเปิดใช้งานแสกนลายนิ้วมือหรือไม่ ?", //*
    fingerprintModal1: "เข้าสู่ระบบด้วย Touch ID",
    fingerprintModal2: "โปรดวางนิ้วลงบนตัวแสกนลายนิ้วมือ",
    touchIdSuccess: "ล็อคอินสำเร็จ !", //*
    touchIdFailed: "ลายนิ้วมือไม่ถูกต้อง โปรดลองใหม่อีกครั้ง",
    usePin: "เปลี่ยนไปใช้ PIN", //*
    notYou: "ไม่ใช่คุณ ?",
    //Forget Pin
    forgetPin: "ลืมรหัส PIN",
    forgetPinTelState1Header1: "ยืนยันส่งเบอร์โทรศัพท์ ",
    forgetPinTelState1Header2: "ทีจะส่งรหัส OTP",
    forgetPinTelState2Header1: "เปลี่ยนเบอร์โทรศัพท์ ",
    forgetPinTelState2Header2: "โปรดระบุเบอร์โทรศัพท์ ",
    forgetPinTelState2Header3: "ที่ต้องการส่งรหัส OTP",
    forgetPinTelChange: "เปลี่ยนเบอร์โทรศัพท์ ",
    changePinSuccess: "เปลี่ยนรหัส PIN เรียบร้อยแล้ว",
    //Register
    passport: "บัตรประชาชน หรือ หนังสือเดินทาง",
    dob: "วันเดือนปีเกิด",
    tel: "หมายเลขโทรศัพท์ ",
    email: "อีเมล",
    lineId: "Line ID",
    rePassword: "ยืนยันรหัสผ่าน",
    otpHead1: "โปรดใส่รหัส OTP ที่ส่งไปยัง", //*
    otpHead2: "รหัสอ้างอิง:", //*
    smsResend: "ส่งอีกครั้ง >", //*
    createSuccess: "สร้างบัญชีเรียบร้อยแล้ว", //*
    resetPasswordSuccess: "เปลี่ยนรหัสผ่านใหม่เรียบร้อยแล้ว", //*
    //member
    Member_header: "รายชื่อสมาชิก ภายใต้หมายเลขสมาชิก",
    //personal
    Personal_policyNumber: "เลขที่กรมธรรม์",
    Personal_memberNo: "เลขที่สมาชิก",
    Personal_effectiveDate: "วันที่มีผลบังคับ",
    Personal_company: "ชื่อบริษัท",
    Personal_name: "ชื่อ-นามสกุล",
    Personal_idNo: "เลขที่บัตร",
    Personal_age: "อายุ",
    Personal_sex: "เพศ",
    Personal_header1: "ประกันชีวิต, อุบัติเหตุ, ทุพพลภาพ",
    Personal_groupTermLife: "การประกันเสียชีวิต",
    Personal_groupAccidentBenefit: "การประกันอบุติเหตุ",
    Personal_doubleIndemnityBenefit:
      "เสียชีวิตเนื่องจากอุบัติเหตุเกิดขึ้นแก่ยานพาหนะซึ่งผู้ทำการขนส่งสาธารณะ เป็นผู้รับจ้างทำการขนส่งบนเส้นทางขนส่งทางบก หรือลิฟท์ หรือเนื่องจากไฟไหม้โรงมหรสพ โรงแรม หรืออาคารสาธารณะ",
    Personal_extendInclude:
      "เพิ่มความคุ้มครอง การถูกฆาตกรรม ลอบทำร้ายร่างกาย, จลาจล ฯ",
    Personal_groupPermanentBenefit: "การประกันทุพพลภาพถาวรสิ้นเชิง",
    Personal_sumAssured: "ทุนประกัน",
    Personal_header2: "ประกันสุขภาพ",
    Personal_healthIPD: "การประกันสุขภาพ - ผู้ป่วยใน",
    Personal_healthOPD: "การประกันสุขภาพ - ผู้ป่วยนอก",
    Personal_healthDental: "การรักษาพยาบาลด้านทันตกรรม",
    Personal_healthAccident: "ค่ารักษาพยาบาลเนื่องจากอุบัติเหตุ",
    Personal_healthCritical: "การประกันภัยโรคร้ายแรง (Critical Illness)",
    Personal_healthMaternity: "ค่าคลอดบุตร (Maternity)",
    Personal_exception: "ข้อยกเว้น",
    Personal_exceptionDetail:
      "ข้อยกเว้นสำหรับการประกันสุขภาพในฐานะผู้ป่วยในและผู้ป่วยนอก",
    Personal_remark1: "หมายเหตุ:",
    Personal_remark2:
      "หน้าจอแสดงผลประโยชน์อย่างย่อเท่านั้น เงื่อนไขและความคุ้มครองโดยละเอียดให้เป็นไปตามกรมธรรม์หลักที่ออกให้ผู้ถือกรมธรรม์",
    //claim
    Claim_insured: "ผู้เอาประกัน",
    Claim_yearPolicy: "ปีกรมธรรม์ปัจจุบัน",
    Claim_labelIPD: "การเรียกร้องสินไหมผู้ป่วยใน (IPD)",
    Claim_labelOPD: "การเรียกร้องสินไหมผู้ป่วยนอก (OPD)",
    Claim_labelLab: "การเรียกร้องสินไหมค่าแล็บ/เอ็กซเรย์ (OPD Lab Test)",
    Claim_labelDental: "ทันตกรรม",
    Claim_labelAME:
      "ค่ารักษาพยาบาลเนื่องจากอุบัติเหตุ (Accidental Medical Expense)",
    //IPD
    Claim_IPD_info1: "ข้อมูลการใช้สิทธิ์ ",
    Claim_IPD_info2_1: "ผู้ป่วยใน จำนวน",
    Claim_IPD_info2_2: "ครั้ง /",
    Claim_IPD_info3_1: "ค่าใช้จ่ายส่วนเกิน IPD จำนวน",
    Claim_IPD_info3_2: "ครั้ง /",
    Claim_IPD_info4_1: "ค่าคลอดบุตร จำนวน",
    Claim_IPD_info4_2: "ครั้ง /",
    Claim_IPD_info5_1: "ค่าใช้จ่ายส่วนเกินจากผลประโยชน์ จำนวน",
    Claim_IPD_info5_2: "ครั้ง /",
    Claim_IPD_remark1: "หมายเหตุ",
    Claim_IPD_remark2: "ข้อมูลสินไหมจะเป็นข้อมูลย้อนหลัง 1 วัน",
    //OPD
    Claim_OPD_info1: "ข้อมูลการใช้สิทธิ์ ",
    Claim_OPD_info2_1: "เบิกบริษัทประกัน:",
    Claim_OPD_info2_2: "ครั้ง /",
    Claim_OPD_info3_1: "รอวางบิลจาก รพ.:",
    Claim_OPD_info3_2: "ครั้ง /",
    Claim_OPD_info4_1: "คงเหลือ:",
    Claim_OPD_info4_2: "ครั้ง",
    Claim_OPD_info4_3: "บาท",
    Claim_OPD_remark1: "หมายเหตุ",
    Claim_OPD_remark2: "ข้อมูลสินไหมจะเป็นข้อมูลย้อนหลัง 1 วัน",
    //LAB
    Claim_LAB_info1: "ข้อมูลการใช้สิทธิ์ ",
    Claim_LAB_info2_1: "เบิกบริษัทประกัน:",
    Claim_LAB_info2_2: "ครั้ง /",
    Claim_LAB_info3_1: "รอโรงพยาบาลวางบิล:",
    Claim_LAB_info3_2: "ครั้ง /",
    Claim_LAB_info4_1: "คงเหลือ",
    Claim_LAB_info4_2: "ครั้ง",
    Claim_LAB_info4_3: "บาท",
    Claim_LAB_remark1: "หมายเหตุ",
    Claim_LAB_remark2: "ข้อมูลสินไหมจะเป็นข้อมูลย้อนหลัง 1 วัน",
    //DEN
    Claim_DEN_info1: "ข้อมูลการใช้สิทธิ์ ",
    Claim_DEN_info2_1: "เบิกบริษัทประกัน:",
    Claim_DEN_info2_2: "ครั้ง /",
    Claim_DEN_info3_1: "รอโรงพยาบาลวางบิล:",
    Claim_DEN_info3_2: "ครั้ง /",
    Claim_DEN_info4_1: "คงเหลือ",
    Claim_DEN_info4_2: "ครั้ง",
    Claim_DEN_info4_3: "บาท",
    Claim_DEN_remark1: "หมายเหตุ",
    Claim_DEN_remark2: "ข้อมูลสินไหมจะเป็นข้อมูลย้อนหลัง 1 วัน",
    //AME
    Claim_AME_info1: "ข้อมูลการใช้สิทธิ์ ",
    Claim_AME_info2_1: "วันที่เกิดอุบัติเหตุ:",
    Claim_AME_info3_1: "วันที่รักษา:",
    Claim_AME_info3_2: "บาท",
    Claim_AME_info4_1: "รวม:",
    Claim_AME_info4_2: "บาท",
    Claim_AME_info5_1: "คงเหลือ:",
    Claim_AME_info5_2: "บาท",
    Claim_AME_remark1: "หมายเหตุ",
    Claim_AME_remark2: "ข้อมูลสินไหมจะเป็นข้อมูลย้อนหลัง 1 วัน",
    Claim_product: "ความคุ้มครอง",
    Claim_incurredDate: "วันที่เกิดอุบัติเหตุ",
    Claim_date: "วันที่รักษา",
    Claim_hospital: "โรงพยาบาล",
    Claim_diagnosis: "การวินิจฉัย",
    Claim_claimType: "วิธีการเรียกร้องสินไหม",
    Claim_authorizedDate: "วันที่พิจารณา",
    Claim_status: "สถานะ",
    Claim_claim: "เรียกร้อง",
    Claim_approve: "อนุมัติ",
    Claim_surplus: "ส่วนเกิน",
    Claim_remarkCash:
      "* ท่านจะได้รับเงินสินไหมภายใน 5 วันทำการ นับจากวันที่พิจารณา",

    //eCard
    ECard_labelDate: "บัตรประกันนี้ใช้ได้ในวันที่",
    ECard_labelDate2: "เท่านั้น",
    ECard_CardHeader: "ใบรับรองการเอาประกันภัยกลุ่ม",
    ECard_PolicyNo: "เลขที่กรมธรรม์ ",
    ECard_MemberNo: "เลขประจำตัวผู้เอาประกันภัย",
    ECard_Policyholder: "ผู้ถือกรมธรรม์ ",
    ECard_InsuredName: "ผู้เอาประกันภัย",
    ECard_Efd: "วันที่มีผลบังคับ",
    ECard_BenefitCoverage: "จำนวนเงินเอาประกันภัย",
    ECard_BenefitCoverageSub: "ความคุ้มครอง",
    ECard_IssuedBy: "ผู้ออกบัตร",
    ECard_remark1:
      "หมายเหตุ กรณีเบิกค่ารักษาแบบคนไข้นอก (OPD) พนักงานต้องสำรองจ่ายและนำมาเบิกคืนภายหลัง  ยกเว้นการใช้แผนร่วมในโรงพยาบาล ที่ร่วมโครงการการประกันสังคมของซีเกท ผ่านระบบ website ซึ่งผลประโยชน์สูงสุด OPD 500 บาทต่อครั้ง",
    ECard_remark2:
      "** กรณีการรักษาแบบผู้ป่วยใน ขอให้ทางโรงพยาบาลแจ้งโตเกียวมารีนฯ ทันที โทร. 02-619-4071 (8.30-20.00 น.) หลังเวลาดังกล่าวติดต่อวันถัดไป",
    ECard_CustomerRel1: "ลูกค้าสัมพันธ์ ",
    ECard_CustomerRel2: "เทพารักษ์\tโทร.09-2251-0641 / 09-2251-0644",
    ECard_CustomerRel3: "โคราช\tโทร.08-1967-4636 / 08-1879-2542",
    //settings
    Settings_language: "ภาษา",
    Settings_myAccount: "บัญชีของฉัน",
    Settings_logOut: "ออกจากระบบ",
    Settings_privacy: "นโยบายข้อมูลส่วนบุคคล",
    //My Account
    MyAccount_userInformation: "ข้อมูลผู้ใช้งาน",
    MyAccount_name: "ชื่อ-นามสกุล",
    MyAccount_policyNumber: "เลขที่กรมธรรม์",
    MyAccount_memberNo: "เลขที่ประจำตัว",
    MyAccount_lineId: "Line ID",
    MyAccount_tel: "เบอร์โทรศัพท์",
    MyAccount_email: "อีเมล",
    MyAccount_changePassword: "เปลี่ยนรหัสผ่าน",
    MyAccount_oldPassword: "รหัสผ่านเดิม", //*
    MyAccount_newPassword: "รหัสผ่านใหม่", //*
    MyAccount_reNewPassword: "ยืนยันรหัสผ่านใหม่", //*
    MyAccount_lineIdSuccess: "เปลี่ยนไอดีไลน์เรียบร้อยแล้ว", //*
    MyAccount_telSuccess: "เปลี่ยนเบอร์โทรเรียบร้อยแล้ว", //*
    MyAccount_emailSuccess: "เปลี่ยนอีเมลเรียบร้อยแล้ว", //*
    MyAccount_passwordSuccess: "เปลี่ยนรหัสผ่านของคุณเรียบร้อยแล้ว", //*
    //Update
    Update: "อัปเดต",
    UpdateText: "มีเวอร์ชั่นใหม่ของแอพพลิเคชัน โปรดทำการอัปเดตเวอร์ชั่น",
    Ecard_Remark:
      "**กรณี ถ้าผู้เอาประกันยื่นใช้ E-card  สถานพยาบาลไม่ต้องถ่ายหน้าจอ E-card หรือสำเนาบัตรประกันอีก  เนื่องจากมีข้อมูล E-card บนใบยืนยันสิทธิ์แล้ว"
  }
};

const link = {
  en: {
    news: "https://www.tokiomarine.com/th/en/personal/discover.html",
    healthTips: "https://www.tokiomarine.com/th/en/personal/discover.html"
  },
  th: {
    news: "https://www.tokiomarine.com/th/th/personal/discover.html",
    healthTips: "https://www.tokiomarine.com/th/th/personal/discover.html"
  }
};

export const getLabel = (lang, data) => {
  // if(data==null||data=="") return ""
  // let name = "label_" + lang + "_" + data;
  // return eval(name);
  if (data == null || data == "") return "";
  return label[lang][data];
};

export const getData = (lang, data) => {
  if (data == null || data == "") return "";
  return dataExt[lang][data];
};

export const getError = (lang, data) => {
  if (data == null || data == "") return "";
  return error[lang][data];
};

export const getTitle = (lang, data) => {
  if (data == null || data == "") return "";
  return title[lang][data];
};

export const getMenu = (lang, data) => {
  if (data == null || data == "") return "";
  return menu[lang][data];
};

export const getLink = (lang, data) => {
  if (data == null || data == "") return "";
  return link[lang][data];
};

export const setLang = async text => {
  try {
    await AsyncStorage.setItem("lang", text);
  } catch (error) {
    // Error saving data
  }
};

export const getLang = async () => {
  try {
    const value = await AsyncStorage.getItem("lang");
    if (value !== null) {
      return value.toString();
    } else {
      return "en";
    }
  } catch (error) {
    console.log(error);
    return "en";
  }
};
