export const data_en_termHead = "Terms of Use"
export const data_th_termHead = "เงื่อนไขของการใช้งาน"
export const data_en_termDetail = 'Acceptance of Terms\n\n' +
'The TMLTH Group Application (hereafter called the “Application”) is owned, maintained and administered by Tokio Marine Insurance (Thailand) Public Company Limited/Tokio Marine Life Insurance (Thailand) Public Company Limited (hereafter called the “Company”). “You” and “Your” means any one accessing this application.\n\n' +
'Your access to and use of this application are subject to the following Term of Use, as well as to all applicable laws and regulations. Please read the following terms and conditions carefully before accessing the Application and any information contained therein. By using the Application in any manner, you agree to all terms and conditions contained or referenced below. The content of the Application may change, modify, supplement or update at any time without prior notification. Unless otherwise indicated, any new products and services, content and software added to this application will also be subject to these Term of Use effective upon the date of any such addition.\n\n' +
'Limitation of Liability\n\n' +
'1.Although every effort is made to include accurate and up-to-date information on this Application, no warranties or representations are made as to the accuracy, completeness, or adequacy of such information. Users relying on information from this Application do so at their own risk.\n\n' +
'Cautionary Statement on Forward-Looking Information\n\n' +
'2.The information contained in this Application may include forward-looking statements that are based on the Company’s current plans, targets, expectations, assumptions, estimates and projections about its businesses and operations. These forward-looking statements are not guarantees of future performance and involve risks and uncertainties, and actual results may materially differ from those contained in the forward-looking statements as a result of various factors.\n\n' +
'Proprietary rights to content\n\n' +
'3.All content, information, designs, layouts, logos, tradenames, trademarks and other intellectual property contained in this Application are protected by copyrights, trademark rights and other intellectual property rights held by the Company or by other parties that have granted to the Company the license to make use of them. All persons accessing this Application agree to make use of its content in a manner that does not infringe those rights.\n\n' +
'Hyperlinks to Third-Party Web Sites\n\n' +
'4.The Application contains links and banners allowing users to access other application that are not administered by the Company, and the Company accepts no liability whatsoever for the use, content, etc., of such application. These links should not be construed as recommendations of the linked websites.\n\n' +
'If you wish to create a link to this Application, please contact us, and obtain prior approval from the Company.\n\n' +
'Disclaimer\n\n' +
'5.The Company and other group companies disclaim any liability, expressed or implied, for any direct, indirect, incidental, consequential, or special damages arising from the use of this Application or linked application or from the suspension of operation or changes to the content of this Application.\n\n' +
'6.The laws of Thailand will govern all matters related to the use of this Application.\n\n' +
'Please note that individual web pages may include notices other than those specified here.'

export const data_th_termDetail = 'การยอมรับเงื่อนไข\n\n' +
'แอปพลิเคชัน TMLTH Group (ต่อไปนี้จะเรียกว่า “แอปพลิเคชัน”) เป็นแอปพลิเคชัน ที่ บริษัท โตเกียวมารีนประกันภัย (ประเทศไทย) จำกัด (มหาชน) และ บริษัท /โตเกียวมารีนประกันชีวิต (ประเทศไทย) จำกัด  (มหาชน) (ต่อไปนี้จะเรียกว่า “บริษัท”) เป็นเจ้าของและดูแลจัดการ  และคำว่า “ท่านหรือคุณ” ที่ปรากฏในแอปพลิเคชันนี้ หมายถึงบุคคลใดที่ได้เข้ามายังหน้าแอปพลิเคชันนี้\n\n' +
'บุคคลใดที่เข้ามาหรือใช้งานในหน้าแอปพลิเคชันนี้ จะต้องยอมรับเงื่อนไขในการใช้งานที่ได้ระบุไว้ในที่นี้ รวมถึง  ปฏิบัติตามกฎหมายและระเบียบข้อบังคับที่เกี่ยวข้องทั้งหมด ดังนั้น กรุณาอ่านข้อกำหนดและเงื่อนไขต่อไปนี้อย่างละเอียดถี่ถ้วน ก่อนที่จะเข้ามา หรือ ใช้งาน หรือ ใช้ประโยชน์จากข้อมูลต่างๆที่ปรากฏในแอปพลิเคชันนี้\n\n' +
'การที่ท่านได้ใช้เว็บไซต์นี้ไม่ว่าในลักษณะใด หมายถึง ท่านได้ยอมรับข้อกำหนดและเงื่อนไขการใช้งานที่ระบุในที่นี้หรือที่อ้างถึงข้างท้ายนี้  เนื้อหาที่ปรากฏในแอปพลิเคชันอาจมีการเปลี่ยนแปลง แก้ไข เพิ่มเติม หรือ ปรับปรุงได้ตลอดเวลาโดยไม่ต้องแจ้งให้ทราบล่วงหน้า  อนึ่ง ผลิตภัณฑ์และการบริการใหม่ๆ รวมถึง เนื้อหาและซอฟแวร์ใหม่ที่บริษัทได้จัดหาเพิ่มเติมที่ปรากฏในแอปพลิเคชันนี้จะอยู่ภายใต้เงื่อนไขของการใช้งานฉบับนี้นับแต่วันที่ได้ทำการเพิ่มข้อมูลหรือเนื้อหาในเว็บไซต์ เว้นแต่ จะได้มีการระบุไว้เป็นอย่างอื่น\n\n' +
'การจำกัดความรับผิดชอบ\n\n' +
'1.แม้ว่าจะได้พยายามทุกวิถีทางอันจะทำให้ข้อมูลที่ปรากฏบนแอปพลิเคชันนี้ เป็นข้อมูลที่ถูกต้องและเป็นปัจจุบัน แต่อย่างไรก็ตาม บริษัทไม่รับประกันว่าข้อมูลที่ปรากฎนี้ ถูกต้อง สมบูรณ์ หรือ เพียงพอในการใช้งาน  ดังนั้น ผู้ที่ใช้ข้อมูลจากแอปพลิเคชันนี้จะต้องรับความเสี่ยงด้วยตัวเอง\n\n' +
'ข้อความเตือนเกี่ยวกับข้อมูลที่เป็นการคาดการณ์\n\n' +
'2.ข้อมูลที่ประกอบอยู่ในแอปพลิเคชันนี้ อาจจะประกอบด้วยเนื้อหาที่เกี่ยวข้องกับข้อมูลคาดการณ์ที่พิจารณาจากสภาวะและทิศทางการดำเนินธุรกิจในปัจจุบัน ประกอบกับ เป้าหมาย ความคาดหวัง สมมติฐานด้านต่างๆ การประมาณการ และการคาดคะเนทางธุรกิจและการปฏิบัติการของบริษัท ข้อมูลที่เป็นการคาดการณ์ดังกล่าวไม่ได้เป็นการรับประกันผลการดำเนินการในอนาคต และเป็นข้อมูลเกี่ยวข้องกับความเสี่ยงและความไม่แน่นอน ผลลัพธ์ที่เกิดจริงอาจแตกต่างไปจากการคาดการณ์อันเป็นผลจากปัจจัยที่หลากหลายแตกต่างกัน\n\n' +
'สิทธิความเป็นเจ้าของในเนื้อหา\n\n' +
'3.เนื้อหา ข้อมูล การออกแบบ รูปแบบ โลโก้ ชื่อทางการค้า เครื่องหมายการค้า และทรัพย์สินทางปัญญาอื่นๆ ที่ปรากฏอยู่ในแอปพลิเคชันนี้ได้รับการคุ้มครองในด้านลิขสิทธิ์ สิทธิบัตร เครื่องหมายการค้า และสิทธิในทรัพย์สินทางปัญญาอื่นๆ ซึ่งเป็นสิทธิของบริษัท หรือ บุคคลอื่นใดที่อนุญาตให้บริษัทใช้ประโยชน์จากงานเหล่านี้ บุคคลที่เข้าสู่เว็บไซต์นี้ตกลงที่นำเนื้อหาที่ปรากฏในแอปพลิเคชันนี้ไปใช้ในลักษณะที่ไม่ละเมิดสิทธิดังกล่าว\n\n' +
'การเข้าสู่แอปพลิเคชันอื่นๆจากแอปพลิเคชันนี้\n\n' +
'4.เว็บไซต์นี้อาจปรากฏเมนูการเชื่อมต่อ [Links] และหัวข้อ [Banners] ต่างๆ ที่อำนวยความสะดวกให้ผู้ใช้งานสามารถเชื่อมโยงจากเว็บไซต์นี้เข้าสู่แอปพลิเคชันอื่นๆที่บริษัทไม่ได้เป็นผู้ดูแลจัดการ แต่ทั้งนี้ บริษัทจะไม่รับผิดชอบสำหรับการใช้งาน เนื้อหา และอื่นใดในแอปพลิเคชันอื่นๆเหล่านั้น  และ การอำนวยความสะดวกในการเชื่อมต่อดังกล่าวไม่ใช่คำแนะนำให้ผู้ใช้งานเข้าสู่แอปพลิเคชันเหล่านั้นแต่อย่างใด\n\n' +
'ถ้าท่านประสงค์ที่จะสร้างเมนูการเชื่อมต่อ[Links] มายังเว็บไซต์นี้ กรุณาติดต่อบริษัท และขอความเห็นชอบจากบริษัทก่อนที่จะดำเนินการใดๆ\n\n' +
'คำจำกัดสิทธิ์ความรับผิดชอบ\n\n' +
'5.บริษัทและบริษัทในกลุ่มขอสงวนสิทธิในการไม่รับผิดชอบไม่ว่าจะโดยชัดแจ้งหรือโดยนัย ในเหตุการณ์อันสืบเนื่อง หรือความเสียหายเฉพาะใดๆที่เกี่ยวข้องโดยตรงหรือโดยอ้อมจากการที่ท่านใช้แอปพลิเคชันนี้หรือแอปพลิเคชันอื่นที่เชื่อมต่อจากแอปพลิเคชันนี้ หรือ จากการหยุดชะงักในด้านปฏิบัติการ หรือการเปลี่ยนแปลงเนื้อหาของแอปพลิเคชันนี้\n\n' +
'6.แอปพลิเคชันนี้อยู่ภายใต้การบังคับใช้ของกฎหมายไทย\n\n' +
'โปรดทราบว่าหน้าเว็บไซต์แต่ละหน้าอาจจะมีการประกาศอื่นๆนอกเหนือไปจากที่ระบุไว้ในที่นี้'

export const data_en_cardBack = 'Condition of Insurance Card:'
export const data_en_cardBack1 = '1. Insurance card is issued according to condition and treated as part of Group Insurance policy; therefore the cardholder will be covered under condition specified in the policy contract.'
export const data_en_cardBack2 = '2. Insurance card is individual personal right and unable to transfer to other persons.'
export const data_en_cardBack3 = '3. Insurance card must be used with Personal Identification card or other cards which can used as evidence instead of Personal Identification Card.'
export const data_en_cardBack4 = '4. Right to use Insurance Card will be ceased when the card expired or insured member is terminated or insurance coverage is terminated from whatever reasons. Insured member must immediately return Insurance Card to Policyholder or Tokio Marine Life Insurance Public Co., Ltd.'
export const data_en_cardBack5 = '5. The company shall reserve the right to cancel, withdrawn or change condition for services other than specified in insurance contract.'
export const data_en_cardBack5Red = 'In using OPD Credit at network hospitals/clinics, if there is excess amount from coverage of policy, Insured member must be responsible for that amount.  Moreover after credit has been granted and the company found out later that the treatment is not covered by insurance policy or the expense is over insurance limit, the company will call back this amount from the Insured.'

export const data_th_cardBack = 'ข้อกำหนด'
export const data_th_cardBack1 = '1. ใบรับรองนี้ออกให้ตามเงื่อนไขและถือเป็นส่วนหนึ่งของกรมธรรม์ประกันชีวิตกลุ่ม  ดังนั้นผู้ถือบัตรจะได้รับสิทธิความคุ้มครองตามข้อกำหนดและเงื่อนไขที่กำหนดไว้ในกรมธรรม์ประกันชีวิตกลุ่ม'
export const data_th_cardBack2 = '2. ใบรับรองนี้เป็นสิทธิเฉพาะบุคคล ไม่สามารถโอนสิทธิให้ผู้อื่นใช้ได้'
export const data_th_cardBack3 = '3. ใบรับรองนี้ใช้คู่กับบัตรประจำตัวประชาชน หรือบัตรอื่นใดที่ทางราชการออกให้ใช้แทนได้'
export const data_th_cardBack4 = '4. เมื่อผู้ถือบัตรสิ้นสุดสมาชิกภาพหรือเมื่อความคุ้มครองตามกรมธรรม์สิ้นสุดลงไม่ว่าด้วยสาเหตุใด ๆ สิทธิในการใช้ใบรับรองนี้เป็นอันระงับ ผู้ถือบัตรต้องส่งคืนใบรับรองนี้แก่ผู้ถือกรมธรรม์หรือบริษัทฯ ทันที'
export const data_th_cardBack5 = '5. บมจ. โตเกียวมารีนประกันชีวิต (ประเทศไทย) ขอสงวนสิทธิในใบรับรองนี้  โดยสามารถยกเลิก เพิกถอนหรือเปลี่ยนแปลงเงื่อนไขในการให้บริการพิเศษอื่นนอกเหนือจากที่ได้กำหนดไว้ในกรมธรรม์ประกันชีวิตกลุ่ม และ'
export const data_th_cardBack5Red = 'การใช้ OPD Credit ณ สถานพยาบาลเครือข่าย หากมีส่วนเกินสิทธิความคุ้มครองผู้ถือบัตรจะต้องรับผิดชอบค่าใช้จ่ายในส่วนนี้ หรือหากตรวจสอบภายหลังพบว่าไม่อยู่ในเงื่อนไขความคุ้มครองหรืออยู่ภายใต้ข้อยกเว้นของกรมธรรม์ หรือใช้เกินวงเงินความคุ้มครองจากสาเหตุใด ๆ ก็ตาม บริษัทจะเรียกเก็บค่าใช้จ่ายส่วนเกินสิทธิความคุ้มครองนี้ จากผู้ถือบัตรภายหลัง'