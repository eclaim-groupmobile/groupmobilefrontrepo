const tintColor = "#526247";

export default {
  tile0: "#d1dad6",
  // tile1: "#FFF176",
  // tile2: "#81C784",
  // tile3: "#f8b9cf",
  // tile4: "#E57373",
  // tile5: "#64B5F6",
  // tile6: "#FFB74D",
  tile1: "rgba(0, 150, 169, 0.8)",
  tile2: "rgba(0, 150, 169, 1.0)",
  tile3: "rgba(0, 150, 169, 0.25)",
  tile4: "rgba(0, 150, 169, 0.1)",
  tile5: "rgba(0, 150, 169, 0.5)",
  tile6: "rgba(0, 150, 169, 0.2)",

  header: "#d1dad6",
  tintColor: tintColor,
  tabIconDefault: "#d8d8d8",
  tabIconSelected: tintColor,
  tabBar: "#fefefe",
  errorBackground: "red",
  errorText: "#fff",
  warningBackground: "#EAEB5E",
  warningText: "#666804",
  noticeBackground: tintColor,
  noticeText: "#fff",

  boxActive: "#0dbbf8",
  boxInactive: "#dbdabc",
  electricLime: "#d2d711",
  electricLimeDark: "#4f5f2e",
  bondiBlueLight: "#7ecdc8",
  bondiBlue: "#0096a9",
  bondiBlueDark: "#005561",
  gunPowder: "#3D3D55",
  gunPowderDark: "#272736",
  lightBlue: "#4FC3F7",
  eagle: "#444444",

  textBoxError: "red",

  textBlue: "#0096a9",

  buttonActive: "#D2D711",
  buttonInactive: "#E0E0E0",
  buttonTextActive: "#0096a9",
  buttonTextInactive: "#41606B",

  //cardBorder: "#EB900F",
  cardBorder: "#D2D711",
  cardLabel: "#0096a9",
  //cardLabel: "#14E42C",
  carouselBlack: "#1a1917",
  //TMFreshGrey: "#d1dad6"
  //TMFreshGrey: "#001826"
  //TMFreshGrey: "#737373"
  TMFreshGrey: "#A7ADB2",
  TMFreshGrey75: "#001826"
};
