import React from "react";
import {
  createStackNavigator,
  createBottomTabNavigator,
  createSwitchNavigator,
} from "react-navigation";
import Colors from '../constants/Colors';

import TabBarIcon from "../components/TabBarIcon";
import HomeScreen from "../screens/HomeScreen";
import MemberScreen from "../screens/MemberScreen";
import PersonalScreen from "../screens/PersonalScreen";
import ClaimScreen from "../screens/ClaimScreen";
import ECardScreen from "../screens/ECardScreen";
import SettingsScreen from "../screens/SettingsScreen";

import HowToClaimScreen from "../screens/HowToClaimScreen";
import HospitalScreen from "../screens/HospitalScreen";
import HospitalByNearScreen from "../screens/HospitalByNearScreen";
import HospitalByProvinceScreen from "../screens/HospitalByProvinceScreen";
import HospitalByHospitalScreen from "../screens/HospitalByHospitalScreen";
import HospitalByClinicScreen from "../screens/HospitalByClinicScreen";
import HealthTipsScreen from "../screens/HealthTipsScreen";
import ContactUsScreen from "../screens/ContactUsScreen";
import NewsScreen from "../screens/NewsScreen";
import LinksScreen from "../screens/LinksScreen";

import MyAccountScreen from "../screens/MyAccountScreen";

import LoginScreen from "../screens/LoginScreen";
import LoginPinScreen from "../screens/LoginPinScreen";
import LoginPinSetupScreen from "../screens/LoginPinSetupScreen";
import LoginTermScreen from "../screens/LoginTermScreen";

import RegisterScreenStep1 from "../screens/RegisterScreenStep1";
import RegisterScreenStep2 from "../screens/RegisterScreenStep2";
import RegisterScreenStep3 from "../screens/RegisterScreenStep3";
import RegisterTermScreen from "../screens/RegisterTermScreen";
import RegisterOTPScreen from "../screens/RegisterOTPScreen";

import ForgetPinScreen from "../screens/ForgetPinScreen";
import ForgetPinSetupScreen from "../screens/ForgetPinSetupScreen";
import ForgetPinTelScreen from "../screens/ForgetPinTelScreen";
import ForgetPinOTPScreen from "../screens/ForgetPinOTPScreen";

import ForgetPasswordScreenStep1 from "../screens/ForgetPasswordScreenStep1"
import ForgetPasswordScreenStep2 from "../screens/ForgetPasswordScreenStep2"

import ReRegisterScreen from "../screens/ReRegisterScreen"
import ReRegisterTermScreen from "../screens/ReRegisterTermScreen"
import ReRegisterOTPScreen from "../screens/ReRegisterOTPScreen"
import ReRegisterPasswordScreen from "../screens/ReRegisterPasswordScreen"
import ReRegisterPinSetupScreen from "../screens/ReRegisterPinSetupScreen"

import {store} from "../store"
import { connect } from "react-redux";
import CustomTabBar from "../components/CustomTabBar"
//console.log(store.getState().auth.language)

const HomeStack = createStackNavigator({
  Home: HomeScreen,
  HowToClaim: HowToClaimScreen,
  Hospital: HospitalScreen,
  HospitalByNear: HospitalByNearScreen,
  HospitalByProvince: HospitalByProvinceScreen,
  HospitalByHospital: HospitalByHospitalScreen,
  HospitalByClinic: HospitalByClinicScreen,
  HealthTips: HealthTipsScreen,
  ContactUs: ContactUsScreen,
  News: NewsScreen,
  Links: LinksScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: "Home",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      iconActive={require('../assets/icon/Home-button-selected.png')}
      iconInactive={require('../assets/icon/Home-button.png')}
      label="Home"
    />
  ),
  tabBarOnPress: ({ navigation }) => { navigation.navigate("Home"); }
};

const MemberStack = createStackNavigator({
  Member: MemberScreen,
});

MemberStack.navigationOptions = {
  tabBarLabel: "Member",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      iconActive={require('../assets/icon/Members-Selected.png')}
      iconInactive={require('../assets/icon/Members.png')}
      label="Member"
    />
  ),
  tabBarOnPress: ({ navigation }) => { navigation.navigate("Member") } 
};

const PersonalStack = createStackNavigator({
  Personal: PersonalScreen
});

PersonalStack.navigationOptions = {
  tabBarLabel: "Personal",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      iconActive={require('../assets/icon/Personal-Selected.png')}
      iconInactive={require('../assets/icon/Personal.png')}
      label="Personal"
    />
  ),
  tabBarOnPress: ({ navigation }) => { navigation.navigate("Personal"); }
};

const ClaimStack = createStackNavigator({
  Claim: ClaimScreen
});

ClaimStack.navigationOptions = {
  tabBarLabel: "Claim",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      badge={true}
      iconActive={require('../assets/icon/Claim-Selected.png')}
      iconInactive={require('../assets/icon/Claim.png')}
      label="Claim"
    />
  ),
  tabBarOnPress: ({ navigation }) => { navigation.navigate("Claim"); }
};

const ECardStack = createStackNavigator({
  ECard: ECardScreen //ECardScreen
});

ECardStack.navigationOptions = {
  tabBarLabel: "ECard",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      iconActive={require('../assets/icon/E-Card-Selected.png')}
      iconInactive={require('../assets/icon/E-Card.png')}
      label="ECard"
    />
  ),
  tabBarOnPress: ({ navigation }) => { store.getState().auth.hasECard =='TRUE' ? navigation.navigate("ECard"): console.log("a") } 
  // tabBarOnPress: ({ navigation }) => { navigation.navigate("ECard") } 
  //tabBarButtonComponent: ({navigation}) => <CustomTabBar />,
  //
};

const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
  MyAccount: MyAccountScreen,
});

SettingsStack.navigationOptions = {
  tabBarLabel: "Settings",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      iconActive={require('../assets/icon/Setting-icon-selected.png')}
      iconInactive={require('../assets/icon/Setting-icon.png')}
      label="Settings"
    />
  ),
  tabBarOnPress: ({ navigation }) => { navigation.navigate("Settings") } 
};

const LoginStack = createStackNavigator({
  Login: { screen: LoginScreen },
  LoginPin: {screen: LoginPinScreen},
  LoginPinSetUp: {screen: LoginPinSetupScreen },
  LoginTerm: {screen: LoginTermScreen},

  RegisterStep1: { screen: RegisterScreenStep1 },
  RegisterTerm: { screen: RegisterTermScreen },
  RegisterStep2: { screen: RegisterScreenStep2 },
  RegisterOTP: {screen: RegisterOTPScreen },
  RegisterStep3: { screen: RegisterScreenStep3 },

  ForgetPin: { screen: ForgetPinScreen },
  ForgetPinSetup: { screen: ForgetPinSetupScreen },
  ForgetPinTel: { screen: ForgetPinTelScreen },
  ForgetPinOTP: { screen: ForgetPinOTPScreen},

  ForgetPasswordStep1: { screen: ForgetPasswordScreenStep1},
  ForgetPasswordStep2: { screen: ForgetPasswordScreenStep2},

  ReRegister: { screen: ReRegisterScreen},
  ReRegisterTerm: { screen: ReRegisterTermScreen},
  ReRegisterOTP: { screen: ReRegisterOTPScreen},
  ReRegisterPassword: { screen: ReRegisterPasswordScreen},
  ReRegisterPinSetup: { screen: ReRegisterPinSetupScreen},
});


const BottomNavigation = createBottomTabNavigator({
  HomeStack,
  MemberStack,
  PersonalStack,
  ClaimStack,
  ECardStack,
  SettingsStack,
},{
  tabBarOptions: {
    // labelStyle  : {fontFamily: 'Prompt',color:'white',fontSize:8,marginTop:-5},
    // allowFontScaling : false,
    showLabel : false,
    //tabStyle : { backgroundColor: Colors.lightBlue },
    style : { backgroundColor: '#fff'},
    activeTintColor : Colors.bondiBlue,
  },
  //tabBarButtonComponent: CustomTabBar,
});

export default createSwitchNavigator(
  {
    BottomNavigation,
    LoginStack,
  },
  {
    headerMode: "none"
  }
);
