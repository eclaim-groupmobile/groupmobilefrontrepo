import Expo from 'expo';
import * as SQLite from 'expo-sqlite';
import * as OtherUtils from './OtherUtils';

const executeSql = async (db, sql, params = []) => {
    return new Promise((resolve, reject) => db.transaction(tx => {
      tx.executeSql(sql, params, (_, { rows }) => {resolve(rows._array), reject})
    }))
}

export const GetMasterDataInfo = async (input) => {
    let getHospital = true
    const db = SQLite.openDatabase('db.MasterData');
    const prefix = "a:"
    let output = {}
    let inputTmp = input['GetMasterDataResponse'][0]['GetMasterDataResult'][0]
    // console.log(input)
    output['appVersionNeedForceUpdate'] = inputTmp["a:appVersionNeedForceUpdate"][0].toString()

    //define province
    let provinceInfo = inputTmp['a:provincesInfo'][0]['a:ProvinceInfo']
    console.log("Retrieve Province : " + provinceInfo.length + " records")
    let province = {}
    let provinceTh = []
    let provinceEn = []
    provinceTh.push("")
    provinceEn.push("")
    for(let i=0;i<provinceInfo.length;i++){
        provinceTh.push(provinceInfo[i]['a:nameTH'][0])
        provinceEn.push(provinceInfo[i]['a:nameEN'][0])
    }
    province['th'] = provinceTh.sort((a,b) => OtherUtils.compareArrayName(a,b))
    province['en'] = provinceEn.sort((a,b) => OtherUtils.compareArrayName(a,b))
    output['province'] = province

    //define city
    let cityInfo = inputTmp['a:citiesInfo'][0]['a:CityInfo']
    console.log("Retrieve City : " + cityInfo.length + " records")
    let city = {}
    let cityTh = []
    let cityEn = []
    cityTh.push("")
    cityEn.push("")
    for(let i=0;i<cityInfo.length;i++){
        cityTh.push(cityInfo[i]['a:nameTH'][0])
        cityEn.push(cityInfo[i]['a:nameEN'][0])
    }
    city['th'] = cityTh.sort((a,b) => OtherUtils.compareArrayName(a,b))
    city['en'] = cityEn.sort((a,b) => OtherUtils.compareArrayName(a,b))
    output['city'] = city

    if(getHospital){
        //define clinic
        let clinicInfo = inputTmp['a:clinicsInfo'][0]['a:ClinicInfo']
        console.log("Retrieve Clinic : " + clinicInfo.length + " records")
        let clinic = {}
        let clinicTh = []
        let clinicEn = []
        clinicTh.push("")
        clinicEn.push("")
        let fieldClinic = ['Dental', 'IPDGroup', 'OPDGroup', 'addressEN', 'addressTH', 'provinceEN', 'provinceTH', 'fax', 'latitude', 'longtitude', 'nameEN', 'nameTH', 'nameforMap', 'telephone', 'cityEN', 'cityTH', 'tumbonEN', 'tumbonTH']
        await executeSql(db,'drop table if exists clinic;')
        await executeSql(db,'create table if not exists clinic (id integer primary key not null, BType text, Dental text, IPDGroup text, OPDGroup text, addressEN text, addressTH text, provinceEN text, provinceTH text, fax text, latitude double, longtitude double, nameEN text, nameTH text, nameforMap text, telephone text, cityEN text, cityTH text, tumbonEN text, tumbonTH text)')
        for(let i=0;i<clinicInfo.length;i++){
            clinicTh.push(clinicInfo[i]['a:nameTH'][0])
            clinicEn.push(clinicInfo[i]['a:nameEN'][0])
            let clinicData = {}
            for(let j=0;j<fieldClinic.length;j++){
                let node = fieldClinic[j]
                let nodeXml = prefix + fieldClinic[j]
                let value = clinicInfo[i][nodeXml][0]
                clinicData[node] = typeof(value) != 'string' ? null  : (value+"").trim()
            }
            await executeSql(db,"insert into clinic (BType, Dental, IPDGroup, OPDGroup, addressEN, addressTH, provinceEN, provinceTH, fax, latitude, longtitude, nameEN, nameTH, nameforMap, telephone, cityEN, cityTH, tumbonEN, tumbonTH) values ('C', ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,? ,?)",
                [clinicData['Dental'],clinicData['IPDGroup'],clinicData['OPDGroup'],clinicData['addressEN'],clinicData['addressTH']
                ,clinicData['provinceEN'],clinicData['provinceTH'],clinicData['fax'],clinicData['latitude'],clinicData['longtitude']
                ,clinicData['nameEN'],clinicData['nameTH'],clinicData['nameforMap'],clinicData['telephone'],clinicData['cityEN']
                    ,clinicData['cityTH'],clinicData['tumbonEN'],clinicData['tumbonTH']])
        }
        clinic['th'] = clinicTh.sort((a,b) => OtherUtils.compareArrayName(a,b))
        clinic['en'] = clinicEn.sort((a,b) => OtherUtils.compareArrayName(a,b))
        

        //define hospital
        let hospitalInfo = inputTmp['a:hospitalInfo'][0]['a:HospitalInfo']
        console.log("Retrieve Hospital : " + hospitalInfo.length + " records")
        let hospital = {}
        let hospitalTh = []
        let hospitalEn = []
        hospitalTh.push("")
        hospitalEn.push("")
        let fieldHospital = ['Dental', 'IPDGroup', 'OPDGroup', 'addressEN', 'addressTH', 'provinceEN', 'provinceTH', 'fax', 'latitude', 'longtitude', 'nameEN', 'nameTH', 'nameforMap', 'telephone', 'cityEN', 'cityTH', 'tumbonEN', 'tumbonTH']
        await executeSql(db,'drop table if exists hospital;')
        await executeSql(db,'create table if not exists hospital (id integer primary key not null, BType text, Dental text, IPDGroup text, OPDGroup text, addressEN text, addressTH text, provinceEN text, provinceTH text, fax text, latitude double, longtitude double, nameEN text, nameTH text, nameforMap text, telephone text, cityEN text, cityTH text, tumbonEN text, tumbonTH text)')

        for(let i=0;i<hospitalInfo.length;i++){
            hospitalTh.push(hospitalInfo[i]['a:nameTH'][0])
            hospitalEn.push(hospitalInfo[i]['a:nameEN'][0])
            let hospitalData = {}
            for(let j=0;j<fieldHospital.length;j++){
                let node = fieldHospital[j]
                let nodeXml = prefix + fieldHospital[j]
                let value = hospitalInfo[i][nodeXml][0]
                hospitalData[node] = typeof(value) != 'string' ? null  : (value+"").trim()
            }
            await executeSql(db,"insert into hospital (BType, Dental, IPDGroup, OPDGroup, addressEN, addressTH, provinceEN, provinceTH, fax, latitude, longtitude, nameEN, nameTH, nameforMap, telephone, cityEN, cityTH, tumbonEN, tumbonTH) values ('Hospital', ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            [hospitalData['Dental'],hospitalData['IPDGroup'],hospitalData['OPDGroup'],hospitalData['addressEN'],hospitalData['addressTH']
            ,hospitalData['provinceEN'],hospitalData['provinceTH'],hospitalData['fax'],hospitalData['latitude'],hospitalData['longtitude']
            ,hospitalData['nameEN'],hospitalData['nameTH'],hospitalData['nameforMap'],hospitalData['telephone'],hospitalData['cityEN']
            ,hospitalData['cityTH'],hospitalData['tumbonEN'],hospitalData['tumbonTH']])
        }
        hospital['th'] = hospitalTh.sort((a,b) => OtherUtils.compareArrayName(a,b))
        hospital['en'] = hospitalEn.sort((a,b) => OtherUtils.compareArrayName(a,b))
        
        output['clinic'] = clinic
        output['hospital'] = hospital
    }
    // db.transaction(tx => {
    //     tx.executeSql(
    //       `select * from clinic;`,
    //       null,
    //       (_, { rows: { _array } }) => console.log(_array)
    //     );
    // });

    let link = []
    let howToClaim = []
    let healthTips = []
    let news = []

    let newsInfo = inputTmp['a:newsInfo'][0]['a:NewInfo']
    let fieldNews = ['attachEN', 'attachTH', 'detail', 'link', 'titleEN', 'titleTH']
    for(let i=0;i<newsInfo.length;i++){
        let newsData = {}
        for(let j=0;j<fieldNews.length;j++){
            let node = fieldNews[j]
            let nodeXml = prefix + fieldNews[j]
            let value = newsInfo[i][nodeXml][0]
            newsData[node] = typeof(value) != 'string' ? null  : (value+"").trim()
        }
        if(newsData.link != null && newsData.link != ""){
            link.push(newsData)
        }else{
            if(newsData.attachTH!='Except.pdf') // Except will use in Personal screen
                howToClaim.push(newsData)
        }
    }

    let healthTipsInfo = inputTmp['a:healthTipsInfo'][0]['a:HealthTip']
    let fieldHealthTips = ['fileDescription', 'fileID', 'fileName']
    for(let i=0;i<healthTipsInfo.length;i++){
        let newsData = {}
        for(let j=0;j<fieldHealthTips.length;j++){
            let node = fieldHealthTips[j]
            let nodeXml = prefix + fieldHealthTips[j]
            let value = healthTipsInfo[i][nodeXml][0]
            newsData[node] = typeof(value) != 'string' ? null  : (value+"").trim()
        }
        healthTips.push(newsData)
    }

    let mediaNewsInfo = inputTmp['a:mediaNews'][0]['a:MediaNews']
    let fieldMediaNews = ['coverPictureLink', 'detail', 'displayOrder', 'pictureLink','title']
    for(let i=0;i<mediaNewsInfo.length;i++){
        let newsData = {}
        for(let j=0;j<fieldMediaNews.length;j++){
            let node = fieldMediaNews[j]
            let nodeXml = prefix + fieldMediaNews[j]
            let value = mediaNewsInfo[i][nodeXml][0]
            newsData[node] = typeof(value) != 'string' ? null  : (value+"").trim()
        }
        news.push(newsData)
    }

    output['linkList'] = link
    output['howToClaimList'] = howToClaim
    output['healthTipsList'] = healthTips
    output['newsList'] = news
    return output
}

export const GetMemberBenefitData = (input) => {
    let output = {}
    let outputMemberInfo = []
    let outputNoti = []
    let field1Emp = ['AgeEN','AgeTH','CitizenID', 'DOB', 'Email', 'FName', 'LName', 'LineID', 'MemberNo', 'Mobile', 'Sex', 'StartDate', 'EndDate', 'CompanyName', 'HasIPD', 'HasOPD', 'HasLAB', 'HasDEN', 'HasAME','CompanyNameEN']
    let field1Other = ['AgeEN','AgeTH','CitizenID', 'DOB', 'Email', 'FName', 'LName', 'LineID', 'MemberNo', 'Mobile', 'Sex', 'StartDate', 'EndDate', 'CompanyName', 'HasIPD', 'HasOPD', 'HasLAB', 'HasDEN', 'HasAME','CompanyNameEN']
    let field2 = ['memberTypeEn', 'memberTypeTh']
    let field3 = ['ClaimIPDNotify', 'ClaimOPDNotify','ClaimLabsNotify','ClaimDentalNotify','ClaimAMENotify']
    let prefix = ''
    let prefix2 = 'a:'

    inputTmp = input['GetMemberBenefitResponse'][0]['GetMemberBenefitResult'][0]
    //console.log(inputTmp)
    output['policyNo'] = getValue(inputTmp['a:policyNo'][0])
    output['startDate'] = getValue(inputTmp['a:startDate'][0])
    output['endDate'] = getValue(inputTmp['a:endDate'][0])
    output['companyNM'] = getValue(inputTmp['a:companyNM'][0])
    output['token'] = getValue(inputTmp['a:token'][0])
    output['language'] = getValue(inputTmp['a:language'][0])
    output['claimTotNotify'] = getValue(inputTmp['a:claimTotNotify'][0])
    if(output['claimTotNotify']==null) output['claimTotNotify'] = "0"
    output['hasECard'] = getValue(inputTmp['a:hasECard'][0])
    
    // if(inputTmp['a:childrensInfo'] == null){
    //     console.log("tag a:childrensInfo is null")
    // }

    //employee
    prefix = 'a:emp'
    let obj = {}
    let objNoti = {}
    try {
        for(let i=0;i<field1Emp.length;i++){
            try{
                let node = field1Emp[i]
                let nodeXml = prefix + field1Emp[i]
                obj[node] = getValue(inputTmp['a:employeeInfo'][0][nodeXml][0])
            }catch(e){
                console.log(e)
            }
        }
        for(let i=0;i<field2.length;i++){
            let node = field2[i]
            let nodeXml = prefix2 + field2[i]
            obj[node] = getValue(inputTmp['a:employeeInfo'][0][nodeXml][0])
        }
        obj['benefitInfo'] = GetMemberBenefitEachInfo(inputTmp['a:employeeInfo'][0]['a:empBenefitInfo'][0],'a:emp','a:Emp')
        for(let i=0;i<field3.length;i++){
            let node = field3[i]
            let nodeXml = prefix + field3[i]
            objNoti[node] =  getValue(inputTmp['a:employeeInfo'][0][nodeXml][0])
            if(objNoti[node] == null) objNoti[node] = "0"
        }
        if(obj['MemberNo']!=null) {
            outputMemberInfo.push(obj)
            outputNoti.push(objNoti)
        }
    }catch(err){
        console.log(err)
    }


    //spouse
    let obj2 = {}
    let obj2Noti = {}
    try {
        if(inputTmp['a:spouseInfo'] != null){
            prefix = 'a:spu'
            for(let i=0;i<field1Other.length;i++){
                try{
                    let node = field1Other[i]
                    let nodeXml = prefix + field1Other[i]
                    obj2[node] = getValue(inputTmp['a:spouseInfo'][0][nodeXml][0])
                }catch(e){
                    console.log(e)
                }
            }
            for(let i=0;i<field2.length;i++){
                let node = field2[i]
                let nodeXml = prefix2 + field2[i]
                obj2[node] = getValue(inputTmp['a:spouseInfo'][0][nodeXml][0])
            }
            obj2['benefitInfo'] = GetMemberBenefitEachInfo(inputTmp['a:spouseInfo'][0]['a:spuBenefitInfo'][0],'a:spu','a:Spu')
            for(let i=0;i<field3.length;i++){
                let node = field3[i]
                let nodeXml = prefix + field3[i]
                obj2Noti[node] = getValue(inputTmp['a:spouseInfo'][0][nodeXml][0])
                if(obj2Noti[node] == null) obj2Noti[node] = "0"
            }
            if(obj2['MemberNo']!=null) {
                outputMemberInfo.push(obj2)
                outputNoti.push(obj2Noti)
            }
        }
    }catch(err){
        console.log(err)
    }

    //childrens
    try{
        let childrenInfo = inputTmp['a:childrensInfo'][0]
        let countChildren = 0, key;
        for(key in childrenInfo['a:ChildInfo']) {
            countChildren++;
        }
        // console.log(countChildren)
        prefix = 'a:child'
        for(let i=0;i<countChildren;i++){
            try{
                let obj3 = {}
                let obj3Noti = {}
                for(let j=0;j<field1Other.length;j++){
                    try{
                        let node = field1Other[j]
                        let nodeXml = prefix + field1Other[j]
                        obj3[node] = getValue(childrenInfo['a:ChildInfo'][i][nodeXml][0])
                    }catch(e){
                        console.log(e)
                    }
                }
                for(let j=0;j<field2.length;j++){
                    let node = field2[j]
                    let nodeXml = prefix2 + field2[j]
                    obj3[node] = getValue(childrenInfo['a:ChildInfo'][i][nodeXml][0])
                }
                obj3['benefitInfo'] = GetMemberBenefitEachInfo(childrenInfo['a:ChildInfo'][i]['a:childBenefitInfo'][0],'a:child','a:Child')
                for(let j=0;j<field3.length;j++){
                    let node = field3[j]
                    let nodeXml = prefix + field3[j]
                    obj3Noti[node] = getValue(childrenInfo['a:ChildInfo'][i][nodeXml][0])
                    if(obj3Noti[node] == null) obj3Noti[node] = "0"
                }
                if(obj3['MemberNo']!=null) {
                    outputMemberInfo.push(obj3)
                    outputNoti.push(obj3Noti)
                }
            }catch(err){
                console.log(err)
            }
        }
    }catch(err){
        console.log(err)
    }

    output['memberInfo'] = outputMemberInfo
    output['notificationList'] = outputNoti
    // console.log(output)
    return output
}

export const GetMemberBenefitEachInfo = (input,prefix,prefix2) => {
    // console.log(input)
    let output = {}
    let field = ['GTLDetlTH', 'GTLDetlEN', 'GTL', 'GADDetlTH', 'GADDetlEN', 'GAD', 'GDADetlTH', 'GDADetlEN', 'GDA', 'GRCDetlTH', 'GRCDetlEN', 'GRC', 'GPTDDetlTH', 'GPTDDetlEN', 'GPTD', 'GCIDetlTH', 'GCIDetlEN', 'GCI']
    // field = ['AME', 'AMEDetlEN', 'AMEDetlTH', 'DentalDetlEN', 'DentalDetlTH', 'Disable', 'DisableDetlEN', 'DisableDetlTH', 'GTL', 
    //     'GTL1', 'GTL2', 'GTLDetl1EN', 'GTLDetl1TH', 'GTLDetl2EN', 'GTLDetl2TH', 'GTLDetlEN', 'GTLDetlTH', 'IPDDetlEN', 'IPDDetlTH', 'OPDDetlEN', 'OPDDetlTH']
    let nodeXml = ""
    let nodeXMLSub = ""
    for(let i=0;i<field.length;i++){
        try{
            nodeXml = prefix + field[i]
            output[field[i]] = getValue(input[nodeXml][0])
        }catch(err){
            console.log(err,nodeXml)
        }
    }

    //List
    let field2 = ['IPD', 'OPD', 'Dental', 'Accident', 'Critical', 'Maternity']
    
    
    for(let i=0;i<field2.length;i++){
        try{
            let nodeList = field2[i] + 'List'
            nodeXml = prefix + field2[i] + 'List'
            nodeXMLSub = prefix2 + field2[i] + 'Info'
            // let value = input[nodeXml][0][nodeXMLSub]
            //console.log(input[nodeXml][0][nodeXMLSub])
            output[nodeList] = []
            for(let j=0;j<input[nodeXml][0][nodeXMLSub].length;j++){
                let obj = {}
                let dataBHCode = prefix + "BHCode"
                let dataNameTH = prefix + field2[i] + "DetlTH"
                let dataNameEN = prefix + field2[i] + "DetlEN"
                obj['id'] = j+""
                obj['BHCode'] = getValue(input[nodeXml][0][nodeXMLSub][j][dataBHCode][0])
                obj['delTH'] = getValue(input[nodeXml][0][nodeXMLSub][j][dataNameTH][0])
                obj['delEN'] = getValue(input[nodeXml][0][nodeXMLSub][j][dataNameEN][0])
                output[nodeList].push(obj)
            }
        }catch(err){
            console.log(err,nodeXml + " " + nodeXMLSub)
        }
    }
    // console.log(output)
    return output
}


export const getECardInfo = (input) => {
    let output = {}
    let output2 = []
    let field1 = ['FName','LName','MemberNo']
    let field2 = ['claimBenefit1', 'claimBenefit2', 'claimBenefit3', 'claimBenefit4', 'claimBenefit5', 'claimBenefit6','claimBenefit7','claimBenefit8', 'claimLifeGTL', 'claimLifeGPTD', 'countryCode', 'genPage01', 'genPage02', 'genPage03', 'genPage04', 'genPage05', 'genPage06', 'genPage07', 'genPage08', 'memberTypeEn', 'memberTypeTh', 'policyEndDT', 'policyStartDT','insuranceNo']
    let prefix = ''
    let prefix2 = 'a:'

    inputTmp = input['GetECardResponse'][0]['GetECardResult'][0]
    // console.log(inputTmp)
    output['policyNo'] = getValue(inputTmp['a:policyNo'][0])
    output['policyHolder'] = getValue(inputTmp['a:policyHolder'][0])
    output['signaturePic'] = getValue(inputTmp['a:signaturePic'][0])

    //employee
    prefix = 'a:emp'
    let obj = {}
    try {
        for(let i=0;i<field1.length;i++){
            let node = field1[i]
            let nodeXml = prefix + field1[i]
            obj[node] = getValue(inputTmp['a:employeeECard'][0][nodeXml][0])
        }
        for(let i=0;i<field2.length;i++){
            let node = field2[i]
            let nodeXml = prefix2 + field2[i]
            obj[node] = getValue(inputTmp['a:employeeECard'][0][nodeXml][0])
        }
        if(obj['genPage01']!=null && obj['genPage01']!="") output2.push(obj)
    }catch(err){
        console.log(err)
    }


    //spouse
    let obj2 = {}
    try {
        if(inputTmp['a:spouseECard'] != null){
            prefix = 'a:spu'
            for(let i=0;i<field1.length;i++){
                let node = field1[i]
                let nodeXml = prefix + field1[i]
                obj2[node] = getValue(inputTmp['a:spouseECard'][0][nodeXml][0])
            }
            for(let i=0;i<field2.length;i++){
                let node = field2[i]
                let nodeXml = prefix2 + field2[i]
                obj2[node] = getValue(inputTmp['a:spouseECard'][0][nodeXml][0])
            }
            if(obj2['genPage01']!=null && obj2['genPage01']!="") output2.push(obj2)
        }
    }catch(err){
        console.log(err)
    }

    //childrens
    try{
        let childrenInfo = inputTmp['a:childrenECard'][0]
        let countChildren = 0, key;
        for(key in childrenInfo['a:ChildECard']) {
            countChildren++;
        }
        // console.log(countChildren)
        prefix = 'a:child'
        for(let i=0;i<countChildren;i++){
            try{
                let obj3 = {}
                for(let j=0;j<field1.length;j++){
                    let node = field1[j]
                    let nodeXml = prefix + field1[j]
                    obj3[node] = getValue(childrenInfo['a:ChildECard'][i][nodeXml][0])
                }
                for(let j=0;j<field2.length;j++){
                    let node = field2[j]
                    let nodeXml = prefix2 + field2[j]
                    obj3[node] = getValue(childrenInfo['a:ChildECard'][i][nodeXml][0])
                }
                if(obj3['genPage01']!=null && obj3['genPage01']!="") output2.push(obj3)
            }catch(err){
                console.log(err)
            }
        }
    }catch(err){
        console.log(err)
    }

    output['eCardInfo'] = output2
    // console.log(output)
    return output
}


export const getIPDInfo = (input) => {
    let output = {}
    let output2 = []
    let field1 = ['startDate', 'endDate', 'claimHospitalTH', 'claimHospitalEN', 'claimDiagnoseTH', 'claimDiagnoseEN', 'claimType', 'claimTypeDesc', 'claimAuthDT', 'claimStatTH', 'claimStatEN', 'claimAmt', 'claimApprvAmt', 'claimOverAmt','productCode']
    let prefix = 'a:'

    inputTmp = input['GetIPDClaimDetailResponse'][0]['GetIPDClaimDetailResult'][0]
    // console.log(inputTmp)
    output['policyNo'] = getValue(inputTmp['a:policyNo'][0])
    output['memberNo'] = getValue(inputTmp['a:memberNo'][0])
    output['policyStartDT'] = getValue(inputTmp['a:policyStartDT'][0])
    output['policyEndDT'] = getValue(inputTmp['a:policyEndDT'][0])
    output['fName'] = getValue(inputTmp['a:fName'][0])
    output['lName'] = getValue(inputTmp['a:lName'][0])
    output['visityType'] = getValue(inputTmp['a:visityType'][0])
    output['claimTotNo'] = getValue(inputTmp['a:claimTotNo'][0])
    output['claimTotAmt'] = getValue(inputTmp['a:claimTotAmt'][0])
    output['claimOverTotNo'] = getValue(inputTmp['a:claimOverTotNo'][0])
    output['claimOverTotAmt'] = getValue(inputTmp['a:claimOverTotAmt'][0])
    output['claimMatTotNo'] = getValue(inputTmp['a:claimMatTotNo'][0])
    output['claimMatTotAmt'] = getValue(inputTmp['a:claimMatTotAmt'][0])

    try{
        let claimInfo = inputTmp['a:claimIPDs'][0]['a:IPDClaimDetail']
        let countChildren = claimInfo.length
        for(let i=0;i<countChildren;i++){
            try {
                let obj = {}
                for(let j=0;j<field1.length;j++){
                    let node = field1[j]
                    let nodeXml = prefix + field1[j]
                    obj[node] = getValue(claimInfo[i][nodeXml][0])
                }
                obj['id'] = i+""
                output2.push(obj)
            }catch(err){
                console.log(err)
            }
        }
    }catch(err){
        console.log(err)
    }

    output['claimInfo'] = output2
    // console.log(output)
    return output
}

export const getOPDInfo = (input) => {
    let output = {}
    let output2 = []
    let field1 = ['startDate', 'claimHospitalTH', 'claimHospitalEN', 'claimDiagnoseTH', 'claimDiagnoseEN', 'claimType', 'claimTypeDesc', 'claimAuthDT', 'claimStatTH', 'claimStatEN', 'claimAmt', 'claimApprvAmt', 'claimOverAmt','productCode']
    let prefix = 'a:'

    inputTmp = input['GetOPDClaimDetailResponse'][0]['GetOPDClaimDetailResult'][0]
    // console.log(inputTmp)
    output['policyNo'] = getValue(inputTmp['a:policyNo'][0])
    output['memberNo'] = getValue(inputTmp['a:memberNo'][0])
    output['policyStartDT'] = getValue(inputTmp['a:policyStartDT'][0])
    output['policyEndDT'] = getValue(inputTmp['a:policyEndDT'][0])
    output['fName'] = getValue(inputTmp['a:fName'][0])
    output['lName'] = getValue(inputTmp['a:lName'][0])
    output['visityType'] = getValue(inputTmp['a:visityType'][0])
    output['claimAS400Amt'] = getValue(inputTmp['a:claimAS400Amt'][0])
    output['claimAS400No'] = getValue(inputTmp['a:claimAS400No'][0])
    output['claimRemainAmt'] = getValue(inputTmp['a:claimRemainAmt'][0])
    output['claimRemainNo'] = getValue(inputTmp['a:claimRemainNo'][0])
    output['claimWebHospAmt'] = getValue(inputTmp['a:claimWebHospAmt'][0])
    output['claimWebHospNo'] = getValue(inputTmp['a:claimWebHospNo'][0])

    try{
        let claimInfo = inputTmp['a:claimOPDs'][0]['a:OPDClaimDetail']
        let countChildren = claimInfo.length
        for(let i=0;i<countChildren;i++){
            try {
                let obj = {}
                for(let j=0;j<field1.length;j++){
                    let node = field1[j]
                    let nodeXml = prefix + field1[j]
                    // let value = getValue(claimInfo[i][nodeXml][0])
                    // obj[node] = typeof(value) != 'string' ? null  : (value+"").trim()
                    obj[node] = getValue(claimInfo[i][nodeXml][0])
                }
                obj['id'] = i+""
                output2.push(obj)
            }catch(err){
                console.log(err)
            }
        }
    }catch(err){
        console.log(err)
    }

    output['claimInfo'] = output2
    // console.log(output)
    return output
}

export const getLABInfo = (input) => {
    let output = {}
    let output2 = []
    let field1 = ['startDate', 'claimHospitalTH', 'claimHospitalEN', 'claimDiagnoseTH', 'claimDiagnoseEN', 'claimType', 'claimTypeDesc', 'claimAuthDT', 'claimStatTH', 'claimStatEN', 'claimAmt', 'claimApprvAmt', 'claimOverAmt','productCode']
    let prefix = 'a:'

    inputTmp = input['GetLABClaimDetailResponse'][0]['GetLABClaimDetailResult'][0]
    // console.log(inputTmp)
    output['policyNo'] = getValue(inputTmp['a:policyNo'][0])
    output['memberNo'] = getValue(inputTmp['a:memberNo'][0])
    output['policyStartDT'] = getValue(inputTmp['a:policyStartDT'][0])
    output['policyEndDT'] = getValue(inputTmp['a:policyEndDT'][0])
    output['fName'] = getValue(inputTmp['a:fName'][0])
    output['lName'] = getValue(inputTmp['a:lName'][0])
    output['visityType'] = getValue(inputTmp['a:visityType'][0])
    output['claimAS400Amt'] = getValue(inputTmp['a:claimAS400Amt'][0])
    output['claimAS400No'] = getValue(inputTmp['a:claimAS400No'][0])
    output['claimRemainAmt'] = getValue(inputTmp['a:claimRemainAmt'][0])
    output['claimRemainNo'] = getValue(inputTmp['a:claimRemainNo'][0])
    output['claimWebHospAmt'] = getValue(inputTmp['a:claimWebHospAmt'][0])
    output['claimWebHospNo'] = getValue(inputTmp['a:claimWebHospNo'][0])

    try{
        let claimInfo = inputTmp['a:claimLABs'][0]['a:LABClaimDetail']
        let countChildren = claimInfo.length
        for(let i=0;i<countChildren;i++){
            try {
                let obj = {}
                for(let j=0;j<field1.length;j++){
                    let node = field1[j]
                    let nodeXml = prefix + field1[j]
                    obj[node] = getValue(claimInfo[i][nodeXml][0])
                }
                obj['id'] = i+""
                output2.push(obj)
            }catch(err){
                console.log(err)
            }
        }
    }catch(err){
        console.log(err)
    }

    output['claimInfo'] = output2
    // console.log(output)
    return output
}

export const getDENInfo = (input) => {
    let output = {}
    let output2 = []
    let field1 = ['startDate', 'claimHospitalTH', 'claimHospitalEN', 'claimDiagnoseTH', 'claimDiagnoseEN', 'claimType', 'claimTypeDesc', 'claimAuthDT', 'claimStatTH', 'claimStatEN', 'claimAmt', 'claimApprvAmt', 'claimOverAmt','productCode']
    let prefix = 'a:'

    inputTmp = input['GetDENClaimDetailResponse'][0]['GetDENClaimDetailResult'][0]
    // console.log(inputTmp)
    output['policyNo'] = getValue(inputTmp['a:policyNo'][0])
    output['memberNo'] = getValue(inputTmp['a:memberNo'][0])
    output['policyStartDT'] = getValue(inputTmp['a:policyStartDT'][0])
    output['policyEndDT'] = getValue(inputTmp['a:policyEndDT'][0])
    output['fName'] = getValue(inputTmp['a:fName'][0])
    output['lName'] = getValue(inputTmp['a:lName'][0])
    output['visityType'] = getValue(inputTmp['a:visityType'][0])
    output['claimAS400Amt'] = getValue(inputTmp['a:claimAS400Amt'][0])
    output['claimAS400No'] = getValue(inputTmp['a:claimAS400No'][0])
    output['claimRemainAmt'] = getValue(inputTmp['a:claimRemainAmt'][0])
    output['claimRemainNo'] = getValue(inputTmp['a:claimRemainNo'][0])
    output['claimWebHospAmt'] = getValue(inputTmp['a:claimWebHospAmt'][0])
    output['claimWebHospNo'] = getValue(inputTmp['a:claimWebHospNo'][0])

    try{
        let claimInfo = inputTmp['a:claimDENs'][0]['a:DENClaimDetail']
        let countChildren = claimInfo.length
        for(let i=0;i<countChildren;i++){
            try {
                let obj = {}
                for(let j=0;j<field1.length;j++){
                    let node = field1[j]
                    let nodeXml = prefix + field1[j]
                    // let value = getValue(claimInfo[i][nodeXml][0])
                    // obj[node] = typeof(value) != 'string' ? null  : (value+"").trim()
                    obj[node] = getValue(claimInfo[i][nodeXml][0])
                }
                obj['id'] = i+""
                output2.push(obj)
            }catch(err){
                console.log(err)
            }
        }
    }catch(err){
        console.log(err)
    }

    output['claimInfo'] = output2
    // console.log(output)
    return output
}

export const getAMEInfo = (input) => {
    let output = {}
    let output2 = []
    let output3 = []
    let field1 = ['visitDate', 'incurredDate', 'claimHospitalTH', 'claimHospitalEN', 'claimDiagnoseTH', 'claimDiagnoseEN', 'claimType', 'claimTypeDesc', 'claimAuthDT', 'claimStatTH', 'claimStatEN', 'claimAmt', 'claimApprvAmt', 'claimOverAmt','productCode']
    let field2 = ['visitDate', 'incurredAmt']

    let prefix = 'a:'

    inputTmp = input['GetAMEClaimDetailResponse'][0]['GetAMEClaimDetailResult'][0]
    // console.log(inputTmp)
    output['policyNo'] = getValue(inputTmp['a:policyNo'][0])
    output['memberNo'] = getValue(inputTmp['a:memberNo'][0])
    output['policyStartDT'] = getValue(inputTmp['a:policyStartDT'][0])
    output['policyEndDT'] = getValue(inputTmp['a:policyEndDT'][0])
    output['fName'] = getValue(inputTmp['a:fName'][0])
    output['lName'] = getValue(inputTmp['a:lName'][0])
    output['visityType'] = getValue(inputTmp['a:visityType'][0])
    output['claimAS400Amt'] = getValue(inputTmp['a:claimAS400Amt'][0])
    output['claimAS400No'] = getValue(inputTmp['a:claimAS400No'][0])
    output['claimRemainAmt'] = getValue(inputTmp['a:claimRemainAmt'][0])
    output['claimRemainNo'] = getValue(inputTmp['a:claimRemainNo'][0])
    output['claimWebHospAmt'] = getValue(inputTmp['a:claimWebHospAmt'][0])
    output['claimWebHospNo'] = getValue(inputTmp['a:claimWebHospNo'][0])
    output['perVisitAmt'] = getValue(inputTmp['a:perVisitAmt'][0])
    

    try{
        let claimSumAMEs = inputTmp['a:claimSumAMEs'][0]['a:ClaimSumAMEInfo']
        let countClaimSumAMEs = claimSumAMEs.length
        for(let i=0;i<countClaimSumAMEs;i++){
            try{
                nodeMain = getValue(claimSumAMEs[i]['a:incurredDate'][0])
                if(nodeMain==null) continue
                let obj = {}
                obj['incurredDate'] = getValue(claimSumAMEs[i]['a:incurredDate'][0])
                obj['totalAmt'] = getValue(claimSumAMEs[i]['a:totalAmt'][0])
                obj['remainAmt'] = getValue(claimSumAMEs[i]['a:remainAmt'][0])
                obj['id'] = i+""
                obj['visitDTAmt'] = []
                let claimSumInfo = claimSumAMEs[i]['a:visitDTAmt'][0]['a:VisitDTAmt']
                let countSumChildren = claimSumInfo.length
                for(let j=0;j<countSumChildren;j++){
                    try {
                        let obj2 = {}
                        for(let k=0;k<field2.length;k++){
                            let node = field2[k]
                            let nodeXml = prefix + field2[k]
                            obj2[node] = getValue(claimSumInfo[j][nodeXml][0])
                        }
                        obj2['id'] = j+""
                        obj['visitDTAmt'].push(obj2)
                    }catch(err){
                        console.log(err)
                    }
                }
                output2.push(obj)
            }catch(err){
                console.log(err)
            }
        }
    }catch(err){
        console.log(err)
    }
    
    try{
        let claimInfo = inputTmp['a:claimAMEs'][0]['a:AMEClaimDetail']
        let countChildren = claimInfo.length
        for(let i=0;i<countChildren;i++){
            try {
                let obj = {}
                let nodeMain = getValue(claimInfo[i]['a:incurredDate'][0])
                if(nodeMain==null) continue
                for(let j=0;j<field1.length;j++){
                    let node = field1[j]
                    let nodeXml = prefix + field1[j]
                    obj[node] = getValue(claimInfo[i][nodeXml][0])
                }
                obj['id'] = i+""
                output3.push(obj)
            }catch(err){
                console.log(err)
            }
        }
    }catch(err){
        console.log(err)
    }

    output['claimHead'] = output2
    output['claimInfo'] = output3
    // console.log(output)
    return output
}

export const getValue = (value) => {
    return typeof(value) != 'string' ? null  : (value+"").trim()
}
