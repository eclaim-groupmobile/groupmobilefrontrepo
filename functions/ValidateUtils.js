export const validatePolicyNumber = (text,text2='') => {
    if(text==null) text = ""
    if(text=="")  return "invalidPolicyNumber"
    if(text.length != 8 ) return "invalidPolicyNumber";
    const patt = new RegExp(/[G][\d]{7}/);
    if(!patt.test(text)) return "invalidPolicyNumber";
    if(text2!=''){
        if(text!=text2) return "invalidPolicyNumber";
    }
    return "";
}

export const validateMemberNo = (text,text2='') => {
    if(text==null) text = ""
    if(text=="")  return "invalidMemberNo"
    if(text.length != 8 && text.length != 7) return "invalidMemberNo";
    const patt = new RegExp(/[\d]{5}[-][\d]{2}/);
    const patt2 = new RegExp(/[\d]{7}/);
    if(!patt.test(text))
        if(!patt2.test(text))
            return "invalidMemberNo";
    if(text2!=''){
        if(text!=text2) return "invalidMemberNo";
    }
    return "";
}

export const formatMemberNo = (text) => {
    if(text==null) return ""
    if(text.length == 7 ) {
        return text.substring(0,5) + "-" + text.substring(5,8)
    }
    return text;
}

export const formatMemberNo2 = (text) => {
    if(text==null) return ""
    if(text.length == 8 && text.substring(5,6) != "-") {
        return text.substring(0,7)
    }
    return text;
}

export const validatePassword = (text) => {
    if(text==null) text = ""
    if(text=="")  return "invalidPassword"
    if(text.length<8 || text.length>128) return "invalidPassword"
    let validateCheck = 0
    const patt1 = new RegExp(/[0-9a-zA-Z]/g);
    const patt2 = new RegExp(/[0-9]/g);
    const patt3 = new RegExp(/[a-z]/g);
    const patt4 = new RegExp(/[A-Z]/g);
    if(patt1.test(text)) validateCheck = validateCheck + 1
    if(patt2.test(text)) validateCheck = validateCheck + 1
    if(patt3.test(text)) validateCheck = validateCheck + 1
    if(patt4.test(text)) validateCheck = validateCheck + 1
    if(validateCheck == 4) return "";
    else return "invalidPassword"
}

export const validatePassword2 = (text) => {
    if(text==null) text = ""
    if(text=="")  return "invalidPassword2"
    if(text.length<8 || text.length>128) return "invalidPassword2"
    return "";
}

export const validatePassport = (text) => {
    if(text==null) text = ""
    if(text=="")  return "invalidPassport"
    // if(/^\d+$/.test(text)){
        // if(text.length!=13) return "invalidPassport"
        // let sum = 0
        // for(let i=0; i<12; i++){
        //     sum += parseFloat(text.charAt(i))*(13-i)
        // }
        // if((11-sum%11)%10!=parseFloat(text.charAt(12))) return "invalidPassport"
    // }
    return "";
}

export const validateDate = (text) => {
    if(text==null) text = ""
    if(text=="")  return "invalidDate"
    return "";
}

export const validateTel = (text) => {
    if(text==null) text = ""
    if(text=="")  return "invalidTel"
    if(text.length != 10) return "invalidTel";
    if(text.substring(0,1) != 0) return "invalidTel";
    const patt = new RegExp(/[\d]{10}/);
    if(!patt.test(text))
        return "invalidTel";
    return "";
}

export const validateEmail = (text) => {
    if(text==null) text = ""
    if(text=="")  return "";
    const patt = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    const patt2 = new RegExp(/^[0-9_a-zA-Z@._-]+$/g);
    if(!patt.test(text))
        return "invalidEmail";
    if(!patt2.test(text))
        return "invalidEmail";
    return "";
}

export const validateLineId = (text) => {
    if(text==null) text = "";
    if(text=="")  return "";
    const patt = new RegExp(/^[0-9_a-zA-Z@._-]+$/g);
    if(!patt.test(text))
        return "invalidLineId";
    return "";
}

export const formatPassport = (text) => {
    if(text==null) text = ""
    // if(text.length == 13 ) {
    //     return text.substring(0,1) + "-" + text.substring(1,5) + "-" + text.substring(5,10) + "-" + text.substring(10,13)
    // }
    const patt = new RegExp(/^[0-9_a-zA-Z@._-]+$/g);
    if(!patt.test(text))
        return "invalidPassport";
    return text;
}