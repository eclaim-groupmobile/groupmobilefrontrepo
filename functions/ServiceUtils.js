import Constants from 'expo-constants';
import { Platform } from 'react-native';
import * as StringUtils from "./StringUtils";
import * as AuthUtils from "./AuthUtils";
import * as ValidateUtils from "./ValidateUtils"
export const headerXml = `<?xml version="1.0" encoding="utf-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:tml="http://schemas.datacontract.org/2004/07/TMLTH.GRPMOB.DTO.Request"><soapenv:Header/><soapenv:Body>`

export const footerXml = `</soapenv:Body></soapenv:Envelope>`

const url = Constants.manifest.extra.urlService
export const urlUserService = url + `UserService.svc`
export const urlMemberService = url + `MemberService.svc`

//UserService
export const LoginSOAPAction = `http://tempuri.org/IUserService/Login`
export const ChkMemberSOAPAction = `http://tempuri.org/IUserService/ChkMember`
export const GetOTPSOAPAction = `http://tempuri.org/IUserService/GetOTP`
export const ChkOTPSOAPAction = `http://tempuri.org/IUserService/ChkOTP`
export const RegisterSOAPAction = `http://tempuri.org/IUserService/Register`
export const UpdPwdSOAPAction = `http://tempuri.org/IUserService/UpdPwd`
export const ChkMemberForgetPINSOAPAction = `http://tempuri.org/IUserService/ChkMemberForgetPIN`
export const LogoutSOAPAction = `http://tempuri.org/IUserService/Logout`
export const ChkForgetPassSOAPAction = `http://tempuri.org/IUserService/ChkForgetPass`

//MemberService
export const GetMemberBenefitSOAPAction = `http://tempuri.org/IMemberService/GetMemberBenefit`
export const GetMasterDataSOAPAction = `http://tempuri.org/IMemberService/GetMasterData`
export const UpdateMemberInfoSOAPAction = `http://tempuri.org/IMemberService/UpdateMemberInfo`
export const GetECardSOAPAction = `http://tempuri.org/IMemberService/GetECard`
export const GetHealthTipDetailSOAPAction = `http://tempuri.org/IMemberService/GetHealthTipDetail`

export const UpdateClaimNotifyKeySOAPAction = `http://tempuri.org/IMemberService/UpdateClaimNotifyKey`
export const GetIPDClaimDetailSOAPAction = `http://tempuri.org/IMemberService/GetIPDClaimDetail`
export const GetOPDClaimDetailSOAPAction = `http://tempuri.org/IMemberService/GetOPDClaimDetail`
export const GetLABClaimDetailSOAPAction = `http://tempuri.org/IMemberService/GetLABClaimDetail`
export const GetDENClaimDetailSOAPAction = `http://tempuri.org/IMemberService/GetDENClaimDetail`
export const GetAMEClaimDetailSOAPAction = `http://tempuri.org/IMemberService/GetAMEClaimDetail`

export const GetMasterAgreementSOAPAction = `http://tempuri.org/IMemberService/GetMasterAgreement`
export const UpdateTransAgreementSOAPAction = `http://tempuri.org/IMemberService/UpdateTransAgreement`

export const genRequest = (action,body) => {
    return {method: "POST",
    headers: {
      "Content-Type": "text/xml;charset=UTF-8",
      "SOAPAction": action
    },
    body: headerXml + body + footerXml}
}

export const getResult = (input) => {
    let data = ""
    const parseString = require("react-native-xml2js").parseString;
    parseString(input, function(err, result) {
        data = result
    });
    return data["s:Envelope"]["s:Body"][0]
}

export const LoginBody = (policyNo,memberNo,password,token) => {
    let data = ""
    let passwordEnc = ""
    // alert(Constants.installationId)
    if(token==null||token==""){
        token="-"
    } 
    else token
    if(password==null||password==""){
        passwordEnc = "-"
    }else{
        passwordEnc = AuthUtils.encryptPassword(password)
    }
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:Login><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>${StringUtils.encodeHTML(token)}</tml:token>`
    data = data + `<tml:installationID>${StringUtils.encodeHTML(Constants.installationId)}</tml:installationID>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:passWord>${StringUtils.encodeHTML(passwordEnc)}</tml:passWord>`
    data = data + `<tml:policyNo>${StringUtils.encodeHTML(policyNo)}</tml:policyNo>`
    data = data + `</tem:request></tem:Login>`
    return data
}

export const ChkMemberBody = (policyNo,memberNo) => {
    let data = ""
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:ChkMember><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>-</tml:token>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:policyNo>${StringUtils.encodeHTML(policyNo)}</tml:policyNo>`
    data = data + `</tem:request></tem:ChkMember>`
    return data
}

//action = REGISTER
//action = RESETPWD
//action = REREGISTER
export const GetOTPBody = (action,policyNo,memberNo,citizenID,tel,token) => {
    let data = ""
    if(token==null||token=="") token = "-"
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:GetOTP><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>${StringUtils.encodeHTML(token)}</tml:token>`
    data = data + `<tml:action>${StringUtils.encodeHTML(action)}</tml:action>`
    data = data + `<tml:citizenID>${StringUtils.encodeHTML(citizenID)}</tml:citizenID>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:mobileNo>${StringUtils.encodeHTML(tel)}</tml:mobileNo>`
    data = data + `<tml:policyNo>${StringUtils.encodeHTML(policyNo)}</tml:policyNo>`
    data = data + `</tem:request></tem:GetOTP>`
    return data
}

//action = REGISTER
//action = RESETPWD
//action = REREGISTER
export const ChkOTPBody = (action,policyNo,memberNo,citizenID,tel,refId,OTP,token) => {
    let data = ""
    if(token==null||token=="") token = "-"
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:ChkOTP><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>${StringUtils.encodeHTML(token)}</tml:token>`
    data = data + `<tml:OTP>${StringUtils.encodeHTML(OTP)}</tml:OTP>`
    data = data + `<tml:action>${StringUtils.encodeHTML(action)}</tml:action>`
    data = data + `<tml:citizenID>${StringUtils.encodeHTML(citizenID)}</tml:citizenID>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:mobileNo>${StringUtils.encodeHTML(tel)}</tml:mobileNo>`
    data = data + `<tml:policyNo>${StringUtils.encodeHTML(policyNo)}</tml:policyNo>`
    data = data + `<tml:refID>${StringUtils.encodeHTML(refId)}</tml:refID>`
    data = data + `</tem:request></tem:ChkOTP>`
    return data
}

export const RegisterBody = (policyNo,memberNo,citizenID,DOB,tel,email,lineID,OTP) => {
    let data = ""
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:Register><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>-</tml:token>`
    data = data + `<tml:DOB>${StringUtils.encodeHTML(DOB)}</tml:DOB>`
    data = data + `<tml:OTP>${StringUtils.encodeHTML(OTP)}</tml:OTP>`
    data = data + `<tml:citizenID>${StringUtils.encodeHTML(citizenID)}</tml:citizenID>`
    data = data + `<tml:email>${StringUtils.encodeHTML(email)}</tml:email>`
    data = data + `<tml:lineID>${StringUtils.encodeHTML(lineID)}</tml:lineID>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:mobileNo>${StringUtils.encodeHTML(tel)}</tml:mobileNo>`
    data = data + `<tml:policyNo>${StringUtils.encodeHTML(policyNo)}</tml:policyNo>`
    data = data + `</tem:request></tem:Register>`
    return data
}

//action = REGISTER
//action = RESETPWD
//action = REREGISTER
//action = FORGETPWD
export const UpdPwdBody = (action,policyNo,memberNo,password,oldPassword,token) => {
    let data = ""
    let passwordEnc = ""
    let oldPasswordEnc = ""
    if(token==null||token=="") token = "-"
    if(password==null||password=="") {
        passwordEnc = "-"
    }else{
        passwordEnc = AuthUtils.encryptPassword(password)
    }
    if(oldPassword==null||oldPassword=="") {
        oldPasswordEnc = "-"
    }else{
        oldPasswordEnc = AuthUtils.encryptPassword(oldPassword)
    }
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:UpdPwd><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>${StringUtils.encodeHTML(token)}</tml:token>`
    data = data + `<tml:installationID>${StringUtils.encodeHTML(Constants.installationId)}</tml:installationID>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:oldPassWord>${StringUtils.encodeHTML(oldPasswordEnc)}</tml:oldPassWord>`
    data = data + `<tml:passWord>${StringUtils.encodeHTML(passwordEnc)}</tml:passWord>`
    data = data + `<tml:policyNo>${StringUtils.encodeHTML(policyNo)}</tml:policyNo>`
    data = data + `<tml:action>${StringUtils.encodeHTML(action)}</tml:action>`
    data = data + `</tem:request></tem:UpdPwd>`
    return data
}

export const ChkMemberForgetPINBody = (policyNo,memberNo,citizenID,DOB) => {
    let data = ""
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:ChkMemberForgetPIN><tem:request>`
    data = data + `<tml:token>-</tml:token>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:DOB>${StringUtils.encodeHTML(DOB)}</tml:DOB>`
    data = data + `<tml:citizenID>${StringUtils.encodeHTML(citizenID)}</tml:citizenID>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:policyNo>${StringUtils.encodeHTML(policyNo)}</tml:policyNo>`
    data = data + `</tem:request></tem:ChkMemberForgetPIN>`
    return data
}

export const GetMemberBenefitBody = (policyNo,memberNo,token) => {
    let data = ""
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:GetMemberBenefit><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>${StringUtils.encodeHTML(token)}</tml:token>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:policyNo>${StringUtils.encodeHTML(policyNo)}</tml:policyNo>`
    data = data + `</tem:request></tem:GetMemberBenefit>`
    return data
}

export const GetMasterDataBody = () => {
    let data = ""
    data = `<tem:GetMasterData><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>-</tml:token>`
    data = data + `<tml:AppVersion>${Constants.manifest.version}</tml:AppVersion>`
    data = data + `</tem:request></tem:GetMasterData>`
    return data
}

export const UpdateMemberInfoBody = (policyNo, memberNo, token, email, lineID, tel) => {
    let data = ""
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:UpdateMemberInfo><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>${StringUtils.encodeHTML(token)}</tml:token>`
    data = data + `<tml:email>${StringUtils.encodeHTML(email)}</tml:email>`
    data = data + `<tml:lineID>${StringUtils.encodeHTML(lineID)}</tml:lineID>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:mobileNo>${StringUtils.encodeHTML(tel)}</tml:mobileNo>`
    data = data + `<tml:policyNo>${StringUtils.encodeHTML(policyNo)}</tml:policyNo>`
    data = data + `</tem:request></tem:UpdateMemberInfo>`
    return data
}

export const GetECardBody = (policyNo,memberNo,token) => {
    let data = ""
    data = `<tem:GetECard><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>${StringUtils.encodeHTML(token)}</tml:token>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:policyNo>${StringUtils.encodeHTML(policyNo)}</tml:policyNo>`
    data = data + `</tem:request></tem:GetECard>`
    return data
}

export const GetHealthTipDetailBody = (fileID) => {
    let data = ""
    data = `<tem:GetHealthTipDetail><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:fileID>${StringUtils.encodeHTML(fileID)}</tml:fileID>`
    data = data + `</tem:request></tem:GetHealthTipDetail>`
    return data
}

export const UpdateClaimNotifyKeyBody = (policyNo,loginedMemberNo,memberNo,visitType,token) => {
    let data = ""
    loginedMemberNo = ValidateUtils.formatMemberNo(loginedMemberNo)
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:UpdateClaimNotifyKey><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>${StringUtils.encodeHTML(token)}</tml:token>`
    data = data + `<tml:loginedMemberNo>${StringUtils.encodeHTML(loginedMemberNo)}</tml:loginedMemberNo>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:policyNo>${StringUtils.encodeHTML(policyNo)}</tml:policyNo>`
    data = data + `<tml:visitType>${StringUtils.encodeHTML(visitType)}</tml:visitType>`
    data = data + `</tem:request></tem:UpdateClaimNotifyKey>`
    return data
}

export const GetIPDClaimDetailBody = (policyNo,loginedMemberNo,memberNo,token) => {
    let data = ""
    loginedMemberNo = ValidateUtils.formatMemberNo(loginedMemberNo)
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:GetIPDClaimDetail><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>${StringUtils.encodeHTML(token)}</tml:token>`
    data = data + `<tml:loginedMemberNo>${StringUtils.encodeHTML(loginedMemberNo)}</tml:loginedMemberNo>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:policy>${StringUtils.encodeHTML(policyNo)}</tml:policy>`
    data = data + `</tem:request></tem:GetIPDClaimDetail>`
    return data
}

export const GetOPDClaimDetailBody = (policyNo,loginedMemberNo,memberNo,token) => {
    let data = ""
    loginedMemberNo = ValidateUtils.formatMemberNo(loginedMemberNo)
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:GetOPDClaimDetail><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>${StringUtils.encodeHTML(token)}</tml:token>`
    data = data + `<tml:loginedMemberNo>${StringUtils.encodeHTML(loginedMemberNo)}</tml:loginedMemberNo>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:policy>${StringUtils.encodeHTML(policyNo)}</tml:policy>`
    data = data + `</tem:request></tem:GetOPDClaimDetail>`
    return data
}

export const GetLABClaimDetailBody = (policyNo,loginedMemberNo,memberNo,token) => {
    let data = ""
    loginedMemberNo = ValidateUtils.formatMemberNo(loginedMemberNo)
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:GetLABClaimDetail><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>${StringUtils.encodeHTML(token)}</tml:token>`
    data = data + `<tml:loginedMemberNo>${StringUtils.encodeHTML(loginedMemberNo)}</tml:loginedMemberNo>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:policy>${StringUtils.encodeHTML(policyNo)}</tml:policy>`
    data = data + `</tem:request></tem:GetLABClaimDetail>`
    return data
}

export const GetDENClaimDetailBody = (policyNo,loginedMemberNo,memberNo,token) => {
    let data = ""
    loginedMemberNo = ValidateUtils.formatMemberNo(loginedMemberNo)
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:GetDENClaimDetail><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>${StringUtils.encodeHTML(token)}</tml:token>`
    data = data + `<tml:loginedMemberNo>${StringUtils.encodeHTML(loginedMemberNo)}</tml:loginedMemberNo>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:policy>${StringUtils.encodeHTML(policyNo)}</tml:policy>`
    data = data + `</tem:request></tem:GetDENClaimDetail>`
    return data
}

export const GetAMEClaimDetailBody = (policyNo,loginedMemberNo,memberNo,token) => {
    let data = ""
    loginedMemberNo = ValidateUtils.formatMemberNo(loginedMemberNo)
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:GetAMEClaimDetail><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>${StringUtils.encodeHTML(token)}</tml:token>`
    data = data + `<tml:loginedMemberNo>${StringUtils.encodeHTML(loginedMemberNo)}</tml:loginedMemberNo>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:policy>${StringUtils.encodeHTML(policyNo)}</tml:policy>`
    data = data + `</tem:request></tem:GetAMEClaimDetail>`
    return data
}

export const GetMasterAgreementBody = (policyNo,memberNo) => {
    let data = ""
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:GetMasterAgreement><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>-</tml:token>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:policyNo>${StringUtils.encodeHTML(policyNo)}</tml:policyNo>`
    data = data + `</tem:request></tem:GetMasterAgreement>`
    return data
}

export const UpdateTransAgreementBody = (policyNo,memberNo,token,version) => {
    let data = ""
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:UpdateTransAgreement><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>${StringUtils.encodeHTML(token)}</tml:token>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:policyNo>${StringUtils.encodeHTML(policyNo)}</tml:policyNo>`
    data = data + `<tml:version>${StringUtils.encodeHTML(version)}</tml:version>`
    data = data + `</tem:request></tem:UpdateTransAgreement>`
    return data
}

export const LogoutBody = (policyNo,memberNo,token) => {
    let data = ""
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:Logout><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>${StringUtils.encodeHTML(token)}</tml:token>`
    data = data + `<tml:installationID>${StringUtils.encodeHTML(Constants.installationId)}</tml:installationID>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:policyNo>${StringUtils.encodeHTML(policyNo)}</tml:policyNo>`
    data = data + `</tem:request></tem:Logout>`
    return data
}

export const ChkForgetPassBody = (policyNo,memberNo,citizenID,DOB) => {
    let data = ""
    memberNo = ValidateUtils.formatMemberNo(memberNo)
    data = `<tem:ChkForgetPass><tem:request>`
    data = data + `<tml:osPlatform>${Platform.OS}</tml:osPlatform>`
    data = data + `<tml:token>-</tml:token>`
    data = data + `<tml:citizenID>${StringUtils.encodeHTML(citizenID)}</tml:citizenID>`
    data = data + `<tml:dob>${StringUtils.encodeHTML(DOB)}</tml:dob>`
    data = data + `<tml:memberNo>${StringUtils.encodeHTML(memberNo)}</tml:memberNo>`
    data = data + `<tml:policyNo>${StringUtils.encodeHTML(policyNo)}</tml:policyNo>`
    data = data + `</tem:request></tem:ChkForgetPass>`
    return data
}