import * as StringUtils from "./StringUtils";
import * as SecureStore from 'expo-secure-store';
import Constants from 'expo-constants';
import CryptoJS from "crypto-js";
import { Alert } from "react-native";

export const encryptPassword = (data) => {
  let deviceID = Constants.installationId + "";
  deviceID = deviceID.replace(/-/g, "").substring(0, 16);
  var key = CryptoJS.enc.Latin1.parse(deviceID);
  var iv = CryptoJS.enc.Latin1.parse(deviceID);
  var encrypted = CryptoJS.AES.encrypt(data, key, { iv: iv });
  return encrypted.toString();
}

storeData = async (key, value) => {
  try {
    await SecureStore.setItemAsync(key, value)
  } catch (error) {
    Alert.alert("error saving data");
  }
};

export const retrieveData = async key => {
  try {
    const value = await SecureStore.getItemAsync(key)
    if (value !== null) {
      return value
    }
  } catch (error) {
    
  }
  return "";
};

export const logIn = (policyNumber, memberNo, token, name, tel, email, lineId) => {
  storeData("policyNumberStore", policyNumber);
  storeData("memberNoStore", memberNo);
  // storeData("passwordStore", password);
  storeData("tokenStore", token);
  storeData("nameStore", name.trim());
  storeData("telStore", tel.trim());
  storeData("emailStore", email.trim());
  storeData("lineIdStore", lineId.trim());
  storeData("pinStore", "");
  storeData("localAuthenticationStore", "0");
}

export const logInWithoutResetData = (policyNumber, memberNo, token, name, tel, email, lineId) => {
  storeData("policyNumberStore", policyNumber);
  storeData("memberNoStore", memberNo);
  //storeData("passwordStore", password);
  storeData("tokenStore", token);
  storeData("nameStore", name.trim());
  storeData("telStore", tel.trim());
  storeData("emailStore", email.trim());
  storeData("lineIdStore", lineId.trim());
}

export const logOut = () => {
  storeData("policyNumberStore", "");
  storeData("memberNoStore", "");
  //storeData("passwordStore", "");
  storeData("tokenStore", "");
  storeData("nameStore", "");
  storeData("telStore", "");
  storeData("emailStore", "");
  storeData("lineIdStore", "");
  storeData("pinStore", "");
  storeData("localAuthenticationStore", "0");
}

export const setPin = (pin) => {
  storeData("pinStore", pin);
}

export const setLocalAuthenticationStore = (boolFlg) => {
  storeData("localAuthenticationStore", boolFlg);
}

export const setLineId = (lineId) => {
  storeData("lineIdStore", lineId.trim());
}

export const setTel = (tel) => {
  storeData("telStore", tel.trim());
}

export const setEmail = (email) => {
  storeData("emailStore", email.trim());
}
