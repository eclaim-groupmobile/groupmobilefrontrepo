import React, { Component } from "react";
import { Alert, PixelRatio, } from 'react-native';
import * as labels from "../constants/label";
import * as StringUtils from "./StringUtils";
import Colors from "../constants/Colors";
import DefaultText from "../components/DefaultText"

export const confirmBack = (lang,navigation,navigatePage,parameter = {}) => {
  Alert.alert(
    "",
    labels.getLabel(lang,'confirmBack'),
    [
      {text: labels.getLabel(lang,'cancel'), onPress: () => console.log(""), style: 'cancel'},
      {text: labels.getLabel(lang,'ok'), onPress: () => navigation.navigate(navigatePage,parameter)},
    ],
    { cancelable: false }
  )
}

export const createDateData = (lang) => {
  let date = {}
  const now = new Date()
  let nowDay = now.getDate()
  let nowMonth = now.getMonth() + 1
  let nowYear = now.getFullYear()
  //console.log(nowDay,nowMonth,nowYear)
  for(let i=1904;i<=nowYear;i++){
    let month = {}
    let nMonth = 12
    if(nowYear==i) nMonth = nowMonth
    for(let j = 1; j<=nMonth;j++){
      let day = []
      let nDay = 30
      if(j in {1:1, 3:1, 5:1, 7:1, 8:1, 10:1, 12:1}){
        nDay = 31
      }
      if(j==2){
        if(i%4==0){
          nDay=29
        }else{
          nDay=28
        }
      }
      if(nowYear==i && nowMonth==j) nDay = nowDay
      for(let k =1;k<=nDay;k++){
        let value = k+""
        if(value.length==1){
          value = "0" + value
        }
        day.push(value)
      }
      let keyMonth = StringUtils.monthNameEn[j-1]
      if(lang=='th') keyMonth = StringUtils.monthNameTh[j-1]
      month[keyMonth] = day
    }
    let i2 = i
    if(lang=='th') i2 = i +543
    date[i2] = month
  }
  return date
}

toRad = (inp) => {
  return inp * Math.PI / 180;
}

toDeg = (inp) => {
  return inp * 180 / Math.PI;
}

export const distance = (lat1, lon1, lat2, lon2, unit) => {
	if ((lat1 == lat2) && (lon1 == lon2)) {
		return 0;
	}
	else {
		let radlat1 = toRad(lat1) //lat1 * Math.PI / 180;
		let radlat2 = toRad(lat2) //lat2 * Math.PI / 180;
		let theta = lon1-lon2;
		let radtheta = toRad(theta) //theta * Math.PI / 180;
		let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist);
		dist = toDeg(dist) //dist * 180 / Math.PI;
		dist = dist * 60 * 1.1515;
		if (unit=="K") { dist = dist * 1.609344 }
		if (unit=="N") { dist = dist * 0.8684 }
		return dist.toFixed(2);
	}
}

//example destinationPoint(13.806992,100.523078,45,2)
//brng need to be 45°
//dist km
export const destinationPoint = (lat, lon, brng, dist) => {
  dist = dist / 6371;  
  brng = toRad(brng);  

  lat1 = toRad(lat)
  lon1 = toRad(lon);

  let lat2 = Math.asin(Math.sin(lat1) * Math.cos(dist) + 
                       Math.cos(lat1) * Math.sin(dist) * Math.cos(brng));

  let lon2 = lon1 + Math.atan2(Math.sin(brng) * Math.sin(dist) *
                               Math.cos(lat1), 
                               Math.cos(dist) - Math.sin(lat1) *
                               Math.sin(lat2));

  if (isNaN(lat2) || isNaN(lon2)) return null;
  lat2 = toDeg(lat2)
  lon2 = toDeg(lon2)
  distLat = Math.abs(lat2-lat);
  distLon = Math.abs(lon2-lon);
  return distLat + "," + distLon;
}

export const getFontScale = (fontSize) => {
  return fontSize/PixelRatio.getFontScale()
}

export const customTag = (input) => {
  let elements = []
  let items = []
  let flagBold = false
  let flagHeader = false
  let align = "left"
  
  elements = input.split("\n");
  for (const [index, value] of elements.entries()) {
    let tmpValue = value
    if(value.indexOf('\\r')>-1){
      align = "right"
      tmpValue = tmpValue.replace("\\\\r","")
    }else if(value.indexOf('\\c')>-1){
      align = "center"
      tmpValue = tmpValue.replace("\\\\c","")
    }else{
      align = "left"
      tmpValue = tmpValue.replace("\\\\l","")
    }
    let items2 = []
    let elements2 = []
    elements2 = tmpValue.split("\\\\t");
    for (const [index2, value2] of elements2.entries()) {
      let tmpValue2 = value2
      flagBold = false
      flagHeader = false
      if(value2.indexOf('\\b')>-1){
        flagBold = true
        tmpValue2 = tmpValue2.replace("\\\\b","")
      }

      if(value2.indexOf('\\h')>-1){
        flagHeader = true
        tmpValue2 = tmpValue2.replace("\\\\h","")
      }
      if(flagBold && flagHeader){
        items2.push(<DefaultText bold style={{fontSize:20}}>{tmpValue2}</DefaultText>)
      }else if(flagBold){
        items2.push(<DefaultText bold>{tmpValue2}</DefaultText>)
      }else if(flagHeader){
        items2.push(<DefaultText style={{fontSize:20}}>{tmpValue2}</DefaultText>)
      }else{
        items2.push(<DefaultText>{tmpValue2}</DefaultText>)
      }
    }
    items.push(<DefaultText style={{textAlign:align,color:Colors.TMFreshGrey}}>{items2}</DefaultText>)
  }
  return items
}

export const compareObjectName = (a,b,name) => {
  let A1 = a[name].toUpperCase().replace(/[^0-9A-Za-zก-ฮ๐-๙]/g, "");
  let B1 = b[name].toUpperCase().replace(/[^0-9A-Za-zก-ฮ๐-๙]/g, "");
  if ( A1 < B1 ){
    return -1;
  }
  if ( A1 > B1 ){
    return 1;
  }
  if (a[name] < b[name]){
    return -1
  }
  if (a[name] > b[name]){
    return 1
  }
  return 0;
}

export const compareArrayName = (a,b) => {
  let A1 = a.toUpperCase().replace(/[^0-9A-Za-zก-ฮ๐-๙]/g, "");
  let B1 = b.toUpperCase().replace(/[^0-9A-Za-zก-ฮ๐-๙]/g, "");
  if ( A1 < B1 ){
    return -1;
  }
  if ( A1 > B1 ){
    return 1;
  }
  if (a < b){
    return -1
  }
  if (a > b){
    return 1
  }
  return 0;
}