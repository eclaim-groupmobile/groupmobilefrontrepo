export const monthNameEn = ["January","February","March","April","May","June","July","August","September","October","November","December"]
export const monthNameTh = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"]
export const monthNameEnShort = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
export const monthNameThShort = ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."]

export const encodeHTML = (input) => {
  if(input==null || input=='') return ''
  return input.replace(/&/g, '&amp;')
             .replace(/</g, '&lt;')
             .replace(/>/g, '&gt;')
             .replace(/"/g, '&quot;')
             .replace(/'/g, '&apos;');
}

export const decodeHTML = (input) => {
  if(input==null || input=='') return ''
  return input.replace(/&apos;/g, "'")
             .replace(/&quot;/g, '"')
             .replace(/&gt;/g, '>')
             .replace(/&lt;/g, '<')
             .replace(/&amp;/g, '&');
};

export const changeDateFormat = (lang,text) => {
  let date = text + "";
  let dateLength = date.length;
  let year = date.substring(0, 4);
  let month = date.substring(5, dateLength - 3);
  if (month != "") {
    let index = ""
    if(lang=='th'){
      index = monthNameTh.indexOf(month);
      // month = monthNameThShort[index]
    }else{
      index = monthNameEn.indexOf(month);
      // month = monthNameEnShort[index]
    }
    month = ("0" + (index+1)).slice(-2)
  }
  let day = date.substring(dateLength - 2);
  let result = day + "/" + month + "/" + year;
  if (result == "//") {
    result = "";
  }
  return result;
}

export const yearDateFormat = (lang,text) => {
  let result = ""
  try{
    splitStr = text.split(' ');
    let year = splitStr[2];
    let month = splitStr[1];
    let day = splitStr[0]
    if(lang=='th' && Number(year)>2300){
      year = (Number(year) - 543) + ""
    }
    let result = day + "/" + month + "/" + year;
    if (result == "//") {
      result = "";
    }
  }catch(err){

  }
  return result;
}

export const getMonthIndex = (lang,month) => {
  let index = 0
  if(lang=='th'){
    index = monthNameTh.indexOf(month);
  }else{
    index = monthNameEn.indexOf(month);
  }
  return index;
}

export const getDateServiceFormat = (lang,date) => {
  date = date+""
  if(date == null || date == ""){
    return ""
  }
  let day = date.substring(0,2)
  let month = date.substring(3,5)
  let year = date.substring(6,10)
  if(lang=="th"){
    year = (Number(year) - 543) + ""
  }
  return year + month + day
}

export const getToday = (lang) => {
  let today = new Date();
  let dd = today.getDate();
  let mm = today.getMonth() + 1; //January is 0!

  let yyyy = today.getFullYear();
  if(lang=="th") yyyy = yyyy+543
  if (dd < 10) {
    dd = '0' + dd;
  } 
  if (mm < 10) {
    mm = '0' + mm;
  } 
  let data = dd + '/' + mm + '/' + yyyy;
  return data
}

export const getDay = (lang,number) => {
  let day = new Date();
  day.setDate(day.getDate() + number);
  let dd = day.getDate();
  let mm = day.getMonth() + 1; //January is 0!

  let yyyy = day.getFullYear();
  if(lang=="th") yyyy = yyyy+543
  if (dd < 10) {
    dd = '0' + dd;
  } 
  if (mm < 10) {
    mm = '0' + mm;
  } 
  let data = dd + '/' + mm + '/' + yyyy;
  return data
}

export const formatMoney = (n, c, d, t) => {
  n = (n + "").trim()
  n = n.replace(new RegExp(',', 'g'), '');
  var c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
    j = (j = i.length) > 3 ? j % 3 : 0;
  let result = s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
  result = result.replace(".00","")
  return result
};

export const secureText = (text) => {
  let result = ""
  let textLength = text.length
  for(let i=1;i<=textLength;i++){
    result = result + "\u2022"
  }
  return result
};

export const mergeWording = (text) => {
  let result = ""
  splitStr = text.split(' ');
  for (let i = 0; i < splitStr.length; i++) {
    if(i==0){
      result = splitStr[i]
    }else{
      result = result + splitStr[i].toLowerCase()
    }  
  }
  // Directly return the joined string
  return result; 
}