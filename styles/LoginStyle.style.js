import { StyleSheet, } from "react-native";
import Colors from "../constants/Colors";
import Constants from 'expo-constants';

export default StyleSheet.create({
    main: {
      flex: 1,
      backgroundColor: 'white',
    },
    container: {
      flex: 1,
      //justifyContent: 'center',
      //paddingTop: Constants.statusBarHeight,
      paddingLeft: 75, 
      paddingRight: 75
    },
    paddingStatus:{
      paddingTop: Constants.statusBarHeight,
    },
    mainTerm: {
      flex: 1,
      paddingLeft: 30, 
      paddingRight: 30,
    },
    imgBackground: {
      width: "100%",
      height: "100%",
      flex: 1
    },
    label:{
      color:'white',
    },
    center: {
      alignItems: "center"
    },
    textBox: {
      height: 40,
      padding: 10,
    },
    textBoxNormal:{
      borderColor: Colors.textBoxNormal,
      color: Colors.textBoxNormal,
    },
    textBoxError:{
      borderColor: Colors.textBoxError,
      color: Colors.textBoxError,
      backgroundColor:Colors.textBoxError05,
    },
    textBoxFocus:{
      borderColor: Colors.textBoxFocus,
      color: Colors.textBoxFocus,
    },
    textBoxLock:{
      borderColor: Colors.textBoxNormal,
      color: Colors.textBoxNormal,
      fontWeight: 'bold',
    },
    borderTop: {
      borderTopLeftRadius: 5,
      borderTopRightRadius: 5,
      borderWidth: 1
    },
    borderBottom: {
      borderBottomLeftRadius: 5,
      borderBottomRightRadius: 5,
      borderWidth: 1,
      borderLeftWidth: 1,
      borderRightWidth: 1
    },
    button: {
      height: 50,
      justifyContent: "center",
      backgroundColor: Colors.buttonActive,
      alignItems: "center",
      borderRadius:5,
      elevation: 5,
      shadowRadius: 1,
      shadowOpacity: 0.2,
      shadowRadius: 5,
      shadowOffset: {
          height: 5,
          width: 0
      },
    },
    buttonInactive: {
      height: 50,
      justifyContent: "center",
      backgroundColor: Colors.buttonInactive,
      alignItems: "center",
      borderRadius:5,
    },
    bottomButton: {
      position:'absolute',
      bottom:20,
      left:75,
      right:75
    }
  });
  