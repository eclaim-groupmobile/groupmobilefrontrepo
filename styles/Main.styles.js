import { StyleSheet, } from "react-native";
import Colors from "../constants/Colors";
import Constants from 'expo-constants';

export default StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: 'white',
      },
      container: {
        flex: 1,
        //justifyContent: 'center',
        marginTop: 10
      },
  });
  