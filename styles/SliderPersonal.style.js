import { StyleSheet, Dimensions, Platform } from 'react-native';
import Colors from '../constants/Colors';

const IS_IOS = Platform.OS === 'ios';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const slideHeight = viewportHeight * 0.1;
const slideWidth = wp(90);
const itemHorizontalMargin = wp(2);

export const sliderWidth = viewportWidth;
export const itemWidth = (slideWidth + itemHorizontalMargin * 2) -10;

const entryBorderRadius = 8;

export default StyleSheet.create({
    slideInnerContainer: {
        width: itemWidth,
        height:50,
        // height: slideHeight,
        // paddingHorizontal: itemHorizontalMargin,
        // marginLeft: 5,
        // marginRight: 5,
        marginBottom: 10,
        borderRadius:5,
        flexDirection:'row',
        backgroundColor:Colors.cardBorder,
    },
    scrollView: {
        width:itemWidth+10,
        margin:5,
    },
    slideInnerCenterContainer: {
        width: itemWidth-100,
        justifyContent:"center",
        alignItems:'center',
    },
    slideInnerSideContainer: {
        width: 50,
        justifyContent:"center",
        alignItems:'center',
    },
    dataContainer:{
        width: itemWidth+10,
        backgroundColor:'white',
        borderRadius:5,
        padding:10,
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row',
        borderColor:Colors.cardBorder,
        borderWidth:1,
    },
    dataContainer2:{
        width: itemWidth+10,
        backgroundColor:'white',
        borderRadius:5,
        padding:10,
        borderColor:Colors.cardBorder,
    },
    dataContainer3:{
        width: itemWidth+10,
        backgroundColor: 'white',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: Colors.cardBorder,
    },
    personalLabel:{
        flex:4,
        textAlign:'right',
        fontSize:12
    },
    personalData:{
        flex:8,
        fontSize:12,
        paddingLeft:10
    },
    personalRemark:{
        flex:1,
        fontSize:10,
        color:'grey'
    },
    personalDataNoFlex:{
        fontSize:12
    },

    personalLabelMyAccount:{
        flex:2,
        textAlign:'right',
        fontSize:12,
        lineHeight: 30
    },
    personalDataMyAccount:{
        flex:3,
        fontSize:12,
        lineHeight: 30,
        paddingLeft:5
    },
    personalDataNoFlexMyAccount:{
        fontSize:12,
        lineHeight: 30
    },

    personalDataBold:{
        flex:1,
        fontSize:12,
        // fontWeight:'bold'
    },
    containerLabel:{
        color:Colors.cardLabel,
        fontSize:20,
        // fontWeight:'bold'
    },
    containerLabel2:{
        color:Colors.bondiBlue,
        fontSize:20,
        // fontWeight:'bold'
    },
    containerInsideBox:{
        borderColor:Colors.cardBorder,
        borderWidth:1,
        borderRadius:5,
    },
});
