import { StyleSheet, } from "react-native";
import Colors from "../constants/Colors";
import { Constants } from 'expo';

export default StyleSheet.create({
  searchBox:{
    borderWidth:1,
    borderRadius:5,
    padding:10,
    borderColor:Colors.bondiBlue,
    flexDirection:'row',
    justifyContent:'space-between',
    marginBottom:20,
  },
  searchBoxContent:{
    
  },
  card:{
    backgroundColor:'white',
    justifyContent:"center",
    marginBottom:20,
  },
  cardHeader:{
    flex:1,
    backgroundColor:Colors.cardBorder,
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    borderWidth:1,
    borderColor:Colors.cardBorder,
    padding:10,
    justifyContent:'space-between',
    flexDirection:'row'
  },
  cardData:{
    backgroundColor:'white',
    borderBottomLeftRadius:5,
    borderBottomRightRadius:5,
    borderWidth:1,
    borderColor:Colors.cardBorder,
  },
  checkBox:{
    backgroundColor:'white',
    padding:0,
    borderWidth:0,
    margin:0,
    marginLeft: 5,
    marginRight: 0,
    borderRadius: 3,
  }
});
  