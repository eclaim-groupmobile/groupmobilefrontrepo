import {Dimensions, PixelRatio, StyleSheet} from 'react-native';
import Colors from '../constants/Colors';
import * as utils from "../functions"
const {height, width} = Dimensions.get('window');

export const codePinStyles = StyleSheet.create({
  container: {
    width           : width - 150,
  },
  containerPin: {
    width           : width - 150,
    height          : 40,
    flexDirection   : 'row',
    justifyContent  : 'space-around',
    alignItems      : 'center',
  },
  pin: {
    textAlign       : 'center',
    flex            : 1,
    marginLeft      : 10,
    marginRight     : 10,
    borderRadius    : 0,
    color           : Colors.eagle,
    fontSize        : utils.getFontScale(20)
  },
  pinValue: {
    borderBottomColor: Colors.eagle,
    borderBottomWidth: 2,
  },
  pinNoValue:{

  },
  error: {
    textAlign   : 'center', 
    color       : 'red', 
    paddingTop  : 10
  }
});